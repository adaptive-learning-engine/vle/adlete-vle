import { Dispatch, SetStateAction, useCallback, useRef, useState } from 'react';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export function useScrollOnState<T>(initialState: T, timeout = 50): [T, Dispatch<SetStateAction<T>>, React.Ref<any>] {
  const ref = useRef(null);
  const [state, setState] = useState(initialState);
  const setAndScroll = useCallback(
    (newState: T) => {
      setState(newState);

      setTimeout(() => {
        ref.current?.scrollIntoView({ block: 'start', inline: 'nearest' });
      }, timeout);
    },
    [ref, timeout]
  );
  return [state, setAndScroll, ref];
}
