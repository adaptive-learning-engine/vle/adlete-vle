import { useCallback, useState } from 'react';

export function useIncrementState(startWith = 0): [number, () => void] {
  const [counter, setCounter] = useState(startWith);
  const increment = useCallback(() => {
    setCounter((count) => count + 1);
  }, [setCounter]);
  return [counter, increment];
}
