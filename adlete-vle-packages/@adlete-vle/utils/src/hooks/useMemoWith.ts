import { useMemo } from 'react';

import type { ArgumentTypes, ReturnType } from '@adlete-utils/typescript';

// eslint-disable-next-line @typescript-eslint/ban-types
export function useMemoWith<T extends Function>(cb: T, deps: ArgumentTypes<T>): ReturnType<T> {
  // eslint-disable-next-line react-hooks/exhaustive-deps
  return useMemo(() => cb(...deps), deps);
}
