import { Dispatch, SetStateAction, useRef, useState } from 'react';

interface IInternals<T, D extends readonly unknown[]> {
  value: T;
  deps: D;
  setState: Dispatch<SetStateAction<T>>;
}

function isArrayEqual<T extends readonly unknown[]>(arr1: T, arr2: T): boolean {
  if (arr1.length !== arr2.length) return false;

  for (let i = 0; i < arr1.length; ++i) if (arr1[i] !== arr2[i]) return false;

  return true;
}

export function useResettableState<T, D extends readonly unknown[]>(
  initialiser: T | ((...args: D) => T),
  deps: D
): [T, Dispatch<SetStateAction<T>>] {
  // eslint-disable-next-line no-multi-assign
  const internals: IInternals<T, D> = (useRef<{ value: T; deps: D; setState: Dispatch<SetStateAction<T>> }>().current ||= {
    value: initialiser instanceof Function ? initialiser(...deps) : initialiser,
    deps,
    setState: (value: T) => {
      internals.value = value;
      setState(value);
    },
  });

  const setState = useState<T>(internals.value)[1];

  // Refresh value if the deps change.
  if (!isArrayEqual<D>(deps, internals.deps)) {
    internals.value = initialiser instanceof Function ? initialiser(...deps) : initialiser;
    internals.setState(internals.value);
    internals.deps = deps;
  }

  return [internals.value, internals.setState];
}
