import { Dispatch, SetStateAction, useCallback, useState } from 'react';

import type { ArgumentTypes, ReturnType } from '@adlete-utils/typescript';

// eslint-disable-next-line @typescript-eslint/ban-types
export function useStateWith<T extends Function>(
  factory: T,
  deps: ArgumentTypes<T>
): [ReturnType<T>, Dispatch<SetStateAction<ReturnType<T>>>] {
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const fact = useCallback(() => factory(...deps), []);
  return useState(fact);
}
