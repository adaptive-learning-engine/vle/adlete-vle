import { useCallback, useState } from 'react';

export function useForceUpdate(): () => void {
  const [, updateState] = useState<object>();
  return useCallback(() => updateState({}), []);
}
