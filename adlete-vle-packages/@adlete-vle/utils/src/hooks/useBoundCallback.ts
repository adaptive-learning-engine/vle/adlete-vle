import { useMemo } from 'react';

import type { FunctionArg1, FunctionWithoutArg1 } from '@adlete-utils/typescript';

// eslint-disable-next-line @typescript-eslint/ban-types
export function useBoundCallback1<T extends Function>(cb: T, deps: [FunctionArg1<T>]): FunctionWithoutArg1<T> {
  // eslint-disable-next-line react-hooks/exhaustive-deps
  return useMemo(() => cb.bind(null, ...deps), deps);
}
