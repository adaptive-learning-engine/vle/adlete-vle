import type { Key } from 'react';

export type WithKey = {
  key?: Key | undefined;
};

// TODO: make more efficient
export function replaceTextWithElements(text: string, elements: Record<string, React.ReactElement>): React.ReactNode {
  const entries = Object.entries(elements);
  let parts = [text];
  for (let i = 0; i < entries.length; ++i) {
    const separator = entries[i][0];
    const prevParts = parts;
    parts = [];
    for (let j = 0; j < prevParts.length; ++j) {
      const split = prevParts[j].split(separator);
      for (let k = 0; k < split.length; ++k) {
        if (k < split.length - 1) parts.push(split[k], separator);
        else parts.push(split[k]);
      }
    }
  }
  const result = [];
  for (let i = 0; i < parts.length; ++i) {
    const part = parts[i];
    if (part in elements) result.push(elements[part]);
    else result.push(part);
  }
  return result;
}
