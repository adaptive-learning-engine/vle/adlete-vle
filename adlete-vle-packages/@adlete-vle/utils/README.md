# @adlete-vle/utils

Various utils.

## React Hooks

### [useBoundCallback](./src/hooks/useBoundCallback.ts)

Binds and memoizes a callback function with the given dependencies.

### [useForceUpdate](./src/hooks/useForceUpdate.ts.ts)

Increments a counter state in order to force a re-render.

### [useIncrementState](./src/hooks/useIncrementState.ts)

Allows to increment a counter state.

### [useMemoWith](./src/hooks/useMemoWith.ts)

Like `useMemo`, but calls the given callback with the dependencies.

### [useResettableState](./src/hooks/useResettableState.ts)

Like `useState`, but state is reset, when the dependencies change.

### [useScrollOnState](./src/hooks/useScrollOnState.ts)

Scrolls to the ref, when the state is changed.

### [useStateWith](./src/hooks/useStateWith.ts)

Like `useState`, but with a memoized factory function.

## Scripts

### shallowClone

Shallow clones an object.

## React Scripts

### replaceTextWithElements

Replaces placeholders in a text with React elements.

## [Typescript](./src/typescript.ts)

Various typescript utils.
