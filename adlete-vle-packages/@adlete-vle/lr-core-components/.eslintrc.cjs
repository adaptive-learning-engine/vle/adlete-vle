module.exports = {
  extends: ['@adlete/eslint-config/eslint', 'plugin:storybook/recommended'],
  parserOptions: {
    project: './tsconfig.json',
    tsconfigRootDir: __dirname,
  },
};
