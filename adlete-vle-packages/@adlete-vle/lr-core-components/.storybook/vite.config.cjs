import path from 'path';

import dotenv from 'dotenv';

import { defineConfig } from 'vite';
import reactPlugin from '@vitejs/plugin-react';

const EMPTY_FILE = 'export default {}';
const EMPTY_FILE_NAME = '\0rollup_plugin_ignore_empty_module_placeholder';

function ignoreModules(list) {
  return {
    enforce: 'pre',
    resolveId(importee) {
      return importee === EMPTY_FILE_NAME || list.includes(importee) ? EMPTY_FILE_NAME : null;
    },
    load(id) {
      return id === EMPTY_FILE_NAME ? EMPTY_FILE : null;
    },
  };
}

// load .env-File into environment variables
dotenv.config();

const { VITE_VLE_PORT } = process.env;

const port = VITE_VLE_PORT || 3000;

const aliasPackages = [
  '@fki/editor-core',
  '@fki/plugin-based-app',
  '@fki/plugin-system',
  '@adlete-utils/events',
  '@adlete-utils/events-react',
  '@adlete-utils/typescript',
  '@adlete-semantics/amb',
  '@adlete-semantics/cass-api',
  '@adlete-semantics/cass-json-ld',
  '@adlete-semantics/json-ld',
  '@adlete-semantics/xapi',
  '@adlete-vle/lr-core',
  '@adlete-vle/lr-core-components',
  '@adlete-vle/utils',
  '@adlete-vle/vle-core',
];

function addAliasPackages(aliases, aliasPackages) {
  aliasPackages.forEach((pkg) => {
    aliases[pkg] = path.join(path.dirname(require.resolve(`${pkg}/package.json`)), 'src');
  });
}

const alias = {};
addAliasPackages(alias, aliasPackages);

// https://vitejs.dev/config/
export default defineConfig({
  base: './',

  build: {
    commonjsOptions: {
      transformMixedEsModules: true,
    },

    // these are the minimum versions supporting top-level-await
    target: ['chrome89', 'edge89', 'firefox89', 'safari15'], // es2021
  },
  plugins: [reactPlugin(), ignoreModules(['plotly.js-dist-min'])],
  resolve: {
    // resolve these modules directly from typescript source code (not builds!)
    alias: alias,
  },
  // server: {
  //   port: port,
  //   proxy: {
  //     '/xAPI': 'http://localhost:2999',
  //   },
  // },
});
