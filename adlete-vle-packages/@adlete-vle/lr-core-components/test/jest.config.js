import BASE from '@adlete/dev-config/jest-base.mjs';

export default {
  ...BASE,
  testEnvironment: 'jsdom',
  silent: true,
  moduleNameMapper: {
    ...BASE.moduleNameMapper,
    '@mui/icons-material/.*': '@adlete/dev-config/jest-mock.mjs',
  },
};
