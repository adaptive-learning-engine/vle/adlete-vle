import { jest } from '@jest/globals';

class SpeechSynthesisUtteranceMock extends EventTarget {
  #text = '';

  #lang = 'en-US';

  #rate = 1;

  #pitch = 1;

  #volume = 1;

  #voice: SpeechSynthesisVoice | null = null;

  constructor(text?: string) {
    super();

    this.#text = text ?? '';
  }

  get text() {
    return this.#text;
  }

  set text(text: string) {
    this.#text = text;
  }

  get lang() {
    return this.#lang;
  }

  set lang(lang: string) {
    this.#lang = lang;
  }

  get pitch() {
    return this.#pitch;
  }

  set pitch(pitch: number) {
    this.#pitch = pitch;
  }

  get rate() {
    return this.#rate;
  }

  set rate(rate: number) {
    this.#rate = rate;
  }

  get volume() {
    return this.#volume;
  }

  set volume(volume: number) {
    this.#volume = volume;
  }

  get voice() {
    return this.#voice;
  }

  set voice(value: SpeechSynthesisVoice | null) {
    this.#voice = value;
  }

  onboundary = jest.fn<SpeechSynthesisUtterance['onboundary']>();

  onend = jest.fn<SpeechSynthesisUtterance['onend']>();

  onerror = jest.fn<SpeechSynthesisUtterance['onerror']>();

  onmark = jest.fn<SpeechSynthesisUtterance['onmark']>();

  onpause = jest.fn<SpeechSynthesisUtterance['onpause']>();

  onresume = jest.fn<SpeechSynthesisUtterance['onresume']>();

  onstart = jest.fn<SpeechSynthesisUtterance['onstart']>();
}

export { SpeechSynthesisUtteranceMock };
