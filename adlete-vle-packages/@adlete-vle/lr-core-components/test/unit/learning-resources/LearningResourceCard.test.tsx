import { describe, expect, test } from '@jest/globals';
import { render, RenderResult, screen } from '@testing-library/react';
import React, { ReactNode } from 'react';

import '@testing-library/jest-dom/jest-globals';

import { LearningResourceCard } from '../../../src/learning-resources/LearningResourceCard';

function renderSimple(label: string = 'SomeLabel'): RenderResult {
  const childrenShared = <div data-testid="children">Children</div>;
  return render(<LearningResourceCard label={label}>{childrenShared}</LearningResourceCard>);
}

describe('LearningResourceCard', () => {
  test('has correct class', () => {
    const { container } = renderSimple();
    expect(container.firstChild).toHaveClass('learning-resource-card');
  });

  test('has correct label', () => {
    renderSimple('My Label');
    expect(screen.getByRole('heading')).toBeInTheDocument();
    expect(screen.getByRole('heading')).toHaveTextContent('My Label');
  });

  test('has custom className', () => {
    const { container } = render(
      <LearningResourceCard label="Label" className="custom-class-name">
        Children
      </LearningResourceCard>
    );
    expect(container.firstChild).toHaveClass('learning-resource-card');
    expect(container.firstChild).toHaveClass('custom-class-name');
  });

  test('renders children', () => {
    renderSimple();
    expect(screen.getByTestId('children')).toBeInTheDocument();
  });

  test("doesn't render children, when 'optional' is set", () => {
    render(
      <LearningResourceCard label="Label" optional>
        <div data-testid="children">Children</div>
      </LearningResourceCard>
    );
    expect(screen.queryByTestId('children')).not.toBeInTheDocument();
  });

  test('renders an error boundary that is an alert', () => {
    function ChildComponent(): ReactNode {
      throw new Error('SpecialError');
    }
    render(
      <LearningResourceCard label="Label">
        <ChildComponent />
      </LearningResourceCard>
    );
    expect(screen.getByRole('alert')).toBeInTheDocument();
    expect(screen.getByRole('alert')).toHaveTextContent('SpecialError');
  });

  test('renders correctly (snapshot)', () => {
    const { container } = renderSimple();
    expect(container).toMatchSnapshot();
  });
});
