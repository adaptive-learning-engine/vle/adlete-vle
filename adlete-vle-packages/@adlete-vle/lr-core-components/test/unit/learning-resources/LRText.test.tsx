import { beforeEach, describe, expect, jest, test } from '@jest/globals';
import { render } from '@testing-library/react';
import React from 'react';

import { NoContentError, WrongEncodingError } from '@adlete-vle/lr-core/learning-resources';

import '@testing-library/jest-dom/jest-globals';

import { LRText } from '../../../src/LRText';
import { PLAIN_EXAMPLE_LR_1, REACT_COMPONENT_LR } from '../../../src/stories/learning-resources/example-learning-resources';
import { createTTSWrapper } from '../../../src/stories/shared/tts-setup';

import { SpeechSynthesisMock } from './mocks/SpeechSynthesis/speechSynthesis.mock';
import { SpeechSynthesisUtteranceMock } from './mocks/SpeechSynthesis/speechSynthesisUtterance.mock';

describe('LRText', () => {
  beforeEach(() => {
    window.HTMLMediaElement.prototype.play = jest.fn(() => Promise.resolve());
    window.HTMLMediaElement.prototype.pause = jest.fn();
    window.HTMLMediaElement.prototype.load = jest.fn();
    window.speechSynthesis = new SpeechSynthesisMock();
    window.SpeechSynthesisUtterance = SpeechSynthesisUtteranceMock;
  });

  test('throws WrongEncodingError, when passed LR with wrong encoding', () => {
    // @ts-expect-error test invalid prop
    expect(async () => render(<LRText text={REACT_COMPONENT_LR} />)).rejects.toBeInstanceOf(WrongEncodingError);
  });

  test('throws NoContentError, when passed LR without content', () => {
    const lrWithoutCountent = { meta: PLAIN_EXAMPLE_LR_1.meta };
    expect(async () => render(<LRText text={lrWithoutCountent} />)).rejects.toBeInstanceOf(NoContentError);
  });

  test('renders correctly (snapshot)', async () => {
    const wrapper = await createTTSWrapper();
    const { container } = render(<LRText text={PLAIN_EXAMPLE_LR_1} />, { wrapper });
    expect(container).toMatchSnapshot();
  });
});
