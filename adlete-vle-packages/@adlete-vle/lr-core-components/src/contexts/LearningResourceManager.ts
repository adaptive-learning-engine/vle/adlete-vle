import React, { useContext } from 'react';

import { LearningResourceManager } from '@adlete-vle/lr-core/learning-resources';

export const LearningResourceManagerContext = React.createContext<LearningResourceManager>(null);

export function useLearningResourceManager(): LearningResourceManager {
  return useContext(LearningResourceManagerContext);
}
