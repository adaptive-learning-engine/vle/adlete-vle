import React, { useContext } from 'react';

import { LRComponentCreator } from '@adlete-vle/lr-core-components/learning-resources';

export const LRComponentCreatorContext = React.createContext<LRComponentCreator>(null);

export function useLRComponentCreator(): LRComponentCreator {
  return useContext(LRComponentCreatorContext);
}
