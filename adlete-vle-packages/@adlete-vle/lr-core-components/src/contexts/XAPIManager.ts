import React, { useContext } from 'react';

import { XAPIManager } from '@adlete-vle/lr-core/xapi';

export const XAPIManagerContext = React.createContext<XAPIManager>(null);

export function useXAPIManager(): XAPIManager {
  return useContext(XAPIManagerContext);
}
