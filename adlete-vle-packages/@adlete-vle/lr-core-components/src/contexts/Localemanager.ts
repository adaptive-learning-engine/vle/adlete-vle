import React, { useContext } from 'react';

import { LocaleManager } from '@adlete-vle/lr-core/locale';

export const LocaleContext = React.createContext<LocaleManager>(null);

export function useLocaleManager(): LocaleManager {
  return useContext(LocaleContext);
}
