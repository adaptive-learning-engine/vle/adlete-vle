/* eslint-disable @typescript-eslint/ban-types */

export type PropMapper<PIn = {}, POut = {}> = (propsIn: PIn) => POut;

export function mapProps<PIn = {}, POut = {}>(mapper: PropMapper<PIn, POut>, props: PIn): POut {
  return mapper(props);
  // return Object.fromEntries(Object.entries(props).map(([key, value]) => [mapping[key], value]));
}

export class ReactPropMapperLibrary {
  mappings: Record<string, Record<string, PropMapper>> = {};

  protected assertMapper(componentId: string, context: string): void {
    if (!this.mappings?.[context]?.[componentId])
      throw new Error(`Mapping for component '${componentId}' in context '${context}' does not exist!`);
  }

  // TODO: use proper index
  public registerMapper(componentId: string, context: string, mapper: PropMapper): void {
    if (this.mappings[componentId]) throw new Error(`Mapping for component '${componentId}' already exist!`);

    if (!this.mappings[context]) this.mappings[context] = {};

    this.mappings[context][componentId] = mapper;
  }

  public hasMapping(componentId: string, context: string): boolean {
    return context in this.mappings && componentId in this.mappings[context];
  }

  public getMapping(componentId: string, context: string): PropMapper {
    this.assertMapper(componentId, context);
    return this.mappings[context][componentId];
  }

  public mapProps<PIn = {}, POut = {}>(componentId: string, context: string, props: PIn): POut {
    const mapping = this.getMapping(componentId, context);
    return mapping(props as PIn) as POut;
  }
}
