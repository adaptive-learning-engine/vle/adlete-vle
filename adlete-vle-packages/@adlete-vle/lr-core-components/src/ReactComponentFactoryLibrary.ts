/* eslint-disable @typescript-eslint/ban-types */
import React from 'react';

export interface IReactFactory<P = {}> {
  createElement?: (props?: P) => React.ReactElement;
  component?: React.ComponentType<P>;
}

export function assertFactoryContent<P = {}>(factory: IReactFactory<P>): void {
  if (!factory.component && !factory.createElement)
    throw new Error(`A factory must contain the 'component' or  the'createElement' property!`);
}

export function createElementUsingFactory<P = {}>(factory: IReactFactory<P>, props?: P): React.ReactElement {
  // create from function
  if (factory.createElement) return factory.createElement(props);

  // create from component
  return React.createElement(factory.component, props);
}

export class ReactComponentFactoryLibrary {
  protected factories: Record<string, IReactFactory<unknown>> = {};

  public registerFactory<P = {}>(id: string, factory: IReactFactory<P>): void {
    assertFactoryContent(factory);
    if (id in this.factories) {
      throw new Error(`Factory with id '${id}' already exists!`);
    }

    this.factories[id] = factory;
  }

  protected assertFactory(id: string): void {
    if (!(id in this.factories)) throw new Error(`No factory found for '${id}'!`);
  }

  public getFactory<P = {}>(id: string): IReactFactory<P> {
    this.assertFactory(id);
    return this.factories[id] as IReactFactory<P>;
  }

  public createElement<P = {}>(idOrFactory: string | IReactFactory<P>, props?: P): React.ReactElement {
    const factory = typeof idOrFactory === 'string' ? this.getFactory<P>(idOrFactory) : idOrFactory;

    // TODO: key
    return createElementUsingFactory(factory, props);
  }
}
