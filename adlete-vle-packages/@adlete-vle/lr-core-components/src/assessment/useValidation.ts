import { useCallback, useState } from 'react';

export interface IUseValidationOptions {
  autoValidation: boolean;
  validateSolutions: boolean;
}

export interface IUseValidation {
  isValidationActive: boolean;
  validSolutionsAreShown: boolean;
  activateValidation: () => void;
  deactivateValidation: () => void;
  showValidSolutions: () => void;
  hideValidSolutions: () => void;
  onSolutionChosen: () => void;
}

export function useValidation(opts: IUseValidationOptions): IUseValidation {
  const autoValidation = opts.autoValidation;
  const validateSolutions = opts.validateSolutions;

  // state
  const [isValidationActive, setIsValidationActive] = useState(autoValidation);
  const [validSolutionsAreShown, setValidSolutionsAreShown] = useState(false);

  // activation functions
  const activateValidation = useCallback(() => {
    if (validateSolutions) setValidSolutionsAreShown(true);
    setIsValidationActive(true);
  }, [validateSolutions]);
  const deactivateValidation = useCallback(() => {
    if (validateSolutions) setValidSolutionsAreShown(false);
    setIsValidationActive(false);
  }, [validateSolutions]);

  const showValidSolutions = useCallback(() => setValidSolutionsAreShown(true), []);
  const hideValidSolutions = useCallback(() => setValidSolutionsAreShown(false), []);

  // callback for autoValidation
  const onSolutionChosen = useCallback(() => {
    if (!autoValidation) setIsValidationActive(false);
  }, [autoValidation]);

  return {
    isValidationActive,
    validSolutionsAreShown,
    activateValidation,
    deactivateValidation,
    showValidSolutions,
    hideValidSolutions,
    onSolutionChosen,
  };
}
