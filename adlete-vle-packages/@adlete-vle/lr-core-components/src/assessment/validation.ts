import { Theme } from '@mui/material';

export type ValidationType = 'no-validation' | 'is-valid' | 'is-invalid';

export function getColorOptionByValidation(
  validation?: ValidationType
): 'primary' | 'secondary' | 'error' | 'info' | 'success' | 'warning' {
  validation = validation || 'no-validation';
  switch (validation) {
    case 'no-validation':
      return null;
    case 'is-valid':
      return 'success';
    case 'is-invalid':
      return 'error';
    default:
      throw new Error(`Unknown ValidationType ${validation}!`);
  }
}

export function getColorByValidation(theme: Theme, validation?: ValidationType): string {
  validation = validation || 'no-validation';
  const palette = theme.palette;
  switch (validation) {
    case 'no-validation':
      return palette.text.secondary;
    case 'is-valid':
      return palette.success.main;
    case 'is-invalid':
      return palette.error.main;
    default:
      throw new Error(`Unknown ValidationType ${validation}!`);
  }
}

export type IsSolutionValid<TItem> = (target: TItem, chosen: TItem) => boolean;

export function getValidationType<TItem>(
  isValid: IsSolutionValid<TItem>,
  target: TItem,
  chosen: TItem,
  isValidationActive: boolean
): ValidationType {
  if (!isValidationActive) return 'no-validation';
  return isValid(target, chosen) ? 'is-valid' : 'is-invalid';
}

export function getValidationTypeBool(isValid: boolean, isValidationActive: boolean): ValidationType {
  if (!isValidationActive) return 'no-validation';
  return isValid ? 'is-valid' : 'is-invalid';
}
