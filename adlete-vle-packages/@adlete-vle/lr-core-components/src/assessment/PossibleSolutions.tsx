import { Card, CardContent, Stack, Typography } from '@mui/material';
import React from 'react';

import { Draggable } from '../dnd';

interface IPossibleSolutionsProps<TItem> {
  itemType: string | symbol;
  items: TItem[];
  label: string;
  children: React.ReactElement[];
}

export function PossibleSolutions<TItem>(props: IPossibleSolutionsProps<TItem>): React.ReactElement {
  const { items, itemType, label, children } = props;

  const draggables = children.map((child, index) => (
    // eslint-disable-next-line react/no-array-index-key
    <Draggable key={index} item={items[index]} type={itemType}>
      {child}
    </Draggable>
  ));
  return (
    <Card variant="outlined">
      <CardContent>
        <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
          {label}
        </Typography>
        <Stack direction="row" spacing={2} sx={{ flexWrap: 'wrap', gap: 1 }}>
          {draggables}
        </Stack>
      </CardContent>
    </Card>
  );
}
