import React, { useCallback } from 'react';

import { DropZone } from '../dnd';

interface ISolutionZoneProps<TItem> {
  itemType: string | symbol;
  children: React.ReactNode;

  // eslint-disable-next-line react/require-default-props
  onSolutionSet?: (item: TItem) => void;
  // eslint-disable-next-line react/require-default-props
  display?: string;
}

export function SolutionZone<TItem>(props: ISolutionZoneProps<TItem>): React.ReactElement {
  const { onSolutionSet, itemType, children, display } = props;
  const onDropZoneDrop = useCallback(
    (droppedItem: TItem) => {
      if (onSolutionSet) onSolutionSet(droppedItem);
    },
    [onSolutionSet]
  );
  return (
    <DropZone accept={itemType} onDrop={onDropZoneDrop} display={display}>
      {children}
    </DropZone>
  );
}
