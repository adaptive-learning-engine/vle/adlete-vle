import React, { useContext } from 'react';

import { StatefulEventEmitter } from '@adlete-utils/events';
import { useStatefulEvent } from '@adlete-utils/events-react';

interface IAssessmentEventData<T> {
  answersUpdated: T[];
  validateUpdated: boolean;
  presentSolutions: boolean;
}

export type IsAnswerValidCB<T> = (a: T, b: T) => boolean;

export class AssessmentController<T> {
  protected _events: StatefulEventEmitter<IAssessmentEventData<T>>;

  public solutions: T[];

  public isAnswerValidCB: IsAnswerValidCB<T>;

  public get events(): StatefulEventEmitter<IAssessmentEventData<T>> {
    return this._events;
  }

  // TODO: auto-bind functions
  // use own version of https://www.npmjs.com/package/ts-class-autobind (ts-class-autobind has weird dependencies)
  public constructor(solutions: T[], isAnswerValidCB: IsAnswerValidCB<T>) {
    this.solutions = solutions;
    this.isAnswerValidCB = isAnswerValidCB;
    const initialAnswers = solutions.map(() => undefined);
    this._events = new StatefulEventEmitter<IAssessmentEventData<T>>({
      answersUpdated: initialAnswers,
      validateUpdated: false,
      presentSolutions: false,
    });
  }

  public enableValidation(): void {
    this._events.emit('validateUpdated', true);
  }

  public disableValidation(): void {
    this._events.emit('validateUpdated', false);
  }

  public enablePresentSolutions(): void {
    this._events.emit('presentSolutions', true);
  }

  public disablePresentSolutions(): void {
    this._events.emit('presentSolutions', false);
  }

  public getAnswer(index: number): T {
    return this._events.getState('answersUpdated')[index];
  }

  public setAnswer(index: number, answer: T): void {
    const answers = this._events.getState('answersUpdated');
    const newAnswers = [...answers];
    newAnswers[index] = answer;
    this._events.emit('answersUpdated', newAnswers);
  }

  public isAnswerValid(index: number): boolean {
    return this.isAnswerValidCB(this.solutions[index], this.getAnswer(index));
  }
}

export const AssessmentContext = React.createContext<AssessmentController<unknown>>(null);

export function useAssessmentContext<T>(): AssessmentController<T> {
  return useContext(AssessmentContext) as AssessmentController<T>;
}

export function useAnswers<T>(): T[] {
  const assessment = useAssessmentContext<T>();
  return useStatefulEvent(assessment.events, 'answersUpdated');
}

export function useAnswer<T>(index: number): T {
  const assessment = useAssessmentContext<T>();
  return useStatefulEvent(assessment.events, 'answersUpdated')[index];
}

export function useValidate(): boolean {
  const assessment = useAssessmentContext();
  return useStatefulEvent(assessment.events, 'validateUpdated');
}

export function usePresentSolutions(): boolean {
  const assessment = useAssessmentContext();
  return useStatefulEvent(assessment.events, 'presentSolutions');
}
