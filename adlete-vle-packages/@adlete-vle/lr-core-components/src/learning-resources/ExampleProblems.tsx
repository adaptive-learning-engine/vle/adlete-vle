import { Pagination, Stack } from '@mui/material';
import { i18n } from 'i18next';
import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';

import {
  getMetaFirstAboutLabel,
  getMetaImage,
  ILearningResource,
  IMultiLingualMarkdownText,
  IMultiLingualRawText,
  metaHasValidFirstAboutLabel,
} from '@adlete-vle/lr-core/learning-resources';

import { LRText } from '../LRText';
import { Image } from '../media/Image';

import { LearningResourceCard } from './LearningResourceCard';

export function validateIsRenderableExampleProblem(resource: ILearningResource, i18next: i18n): void {
  const id = resource.meta.id;
  if (!resource.meta.type.includes('ExampleProblem'))
    throw new Error(`Learning resource '${id}' is not renderable as an example problem. Meta 'type' property must contain 'Example'`);

  if (!metaHasValidFirstAboutLabel(resource.meta, i18next.languages))
    throw new Error(`Learning resource '${id}' is not renderable as an example problem. Meta 'about' property must contain a concept.`);
}

export function validateAreRenderableExampleProblems(resources: ILearningResource[], i18next: i18n): void {
  // TODO: validate examples have same concept
  resources.forEach((resource) => validateIsRenderableExampleProblem(resource, i18next));
}

export interface IExampleProblemsProps {
  problems: ILearningResource<string | IMultiLingualRawText | IMultiLingualMarkdownText>[];
  thumbnailWidth?: number;
  // TODO: config for paginated or not
}

export function getExampleProblemLabel(resource: ILearningResource, i18next: i18n): string {
  const concept = getMetaFirstAboutLabel(resource.meta, i18next.languages);
  return i18next.t('learning-resources:example-problems-for', { concept });
}

export function getExampleProblemLabelIntro(resource: ILearningResource, i18next: i18n): string {
  const concept = getMetaFirstAboutLabel(resource.meta, i18next.languages);
  return i18next.t('learning-resources:example-problems-for-intro', { concept });
}

export function ExampleProblems({ problems, thumbnailWidth = 200 }: IExampleProblemsProps): ReactElement {
  const { i18n: i18next } = useTranslation();

  validateAreRenderableExampleProblems(problems, i18next);

  const [currExampleIndex, setCurrExampleIndex] = React.useState(1);
  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setCurrExampleIndex(value);
  };

  const label = getExampleProblemLabel(problems[0], i18next);
  const intro = getExampleProblemLabelIntro(problems[0], i18next);
  const problem = problems[currExampleIndex - 1];

  const imageURL = getMetaImage(problem.meta);

  return (
    <LearningResourceCard label={label}>
      <div>{intro}</div>
      <Stack spacing={2} direction="row">
        <LRText key={problem.meta.id} text={problem} />
        {imageURL && <Image src={imageURL} width={thumbnailWidth} />}
      </Stack>
      {problems.length > 1 && <Pagination count={problems.length} page={currExampleIndex} onChange={handleChange} />}
    </LearningResourceCard>
  );
}
