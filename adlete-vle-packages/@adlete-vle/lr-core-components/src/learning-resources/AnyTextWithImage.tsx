import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';

import { getMetaImage, getMetaName, HumanReadableTextualData, ILearningResource } from '@adlete-vle/lr-core/learning-resources';

import { LRText } from '../LRText';
import { Image } from '../media/Image';

import { LearningResourceCard } from './LearningResourceCard';

export interface IAnyTextWithImageProps {
  text: ILearningResource<HumanReadableTextualData>;
}

export function AnyTextWithImage({ text }: IAnyTextWithImageProps): ReactElement {
  const { i18n: i18next } = useTranslation();

  const label = getMetaName(text.meta, i18next.languages);
  const imageURL = getMetaImage(text.meta);
  return (
    <LearningResourceCard label={label} stackProps={{ direction: 'row', justifyContent: 'space-between' }}>
      <LRText text={text} />
      {imageURL && (
        <div>
          <Image src={imageURL} width="200" aria-label={label} />
        </div>
      )}
    </LearningResourceCard>
  );
}
