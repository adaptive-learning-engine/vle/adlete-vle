import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';

import { getMetaName, HumanReadableTextualData, ILearningResource } from '@adlete-vle/lr-core/learning-resources';

import { LRText } from '../LRText';

import { LearningResourceCard } from './LearningResourceCard';

export interface IAnyTextProps {
  text: ILearningResource<HumanReadableTextualData>;
}

export function AnyText({ text }: IAnyTextProps): ReactElement {
  const { i18n: i18next } = useTranslation();

  const label = getMetaName(text.meta, i18next.languages);
  return (
    <LearningResourceCard label={label}>
      <LRText text={text} />
    </LearningResourceCard>
  );
}
