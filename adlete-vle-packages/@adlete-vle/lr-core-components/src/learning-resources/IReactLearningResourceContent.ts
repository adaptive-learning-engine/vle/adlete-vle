import { IReactFactory } from '../ReactComponentFactoryLibrary';

export interface IReactLearningResourceContent {
  factory: IReactFactory;
}
