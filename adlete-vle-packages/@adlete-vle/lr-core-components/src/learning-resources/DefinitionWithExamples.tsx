import { Box } from '@mui/material';
import { i18n } from 'i18next';
import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';

import { getMetaFirstAboutLabel, ILearningResource } from '@adlete-vle/lr-core/learning-resources';

import { LRText } from '../LRText';

import { IDefinitionProps, validateIsRenderableDefinition } from './Definition';
import { IExamplesProps, validateAreRenderableExamples } from './Examples';
import { LearningResourceCard } from './LearningResourceCard';

export interface IDefinitionWithExamplesProps extends IDefinitionProps, IExamplesProps {}

export function getDefinitionWithExamplesLabel(resource: ILearningResource, i18next: i18n): string {
  const concept = getMetaFirstAboutLabel(resource.meta, i18next.languages);
  return i18next.t('learning-resources:definition-and-examples-of', { concept });
}

export function DefinitionWithExamples({ definition, examples }: IDefinitionWithExamplesProps): ReactElement {
  const { i18n: i18next } = useTranslation();
  validateIsRenderableDefinition(definition, i18next);
  validateAreRenderableExamples(examples, i18next);
  const label = getDefinitionWithExamplesLabel(definition, i18next);
  const exampleTexts = examples.map((example) => <LRText key={example.meta.id} text={example} />);
  return (
    <LearningResourceCard label={label}>
      <LRText key={definition.meta.id} text={definition} />
      <Box component="span" fontWeight="fontWeightMedium">
        Examples:
      </Box>
      {exampleTexts}
    </LearningResourceCard>
  );
}
