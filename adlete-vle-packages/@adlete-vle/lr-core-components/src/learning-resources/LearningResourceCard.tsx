import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Accordion, AccordionDetails, AccordionSummary, Stack, StackProps, Typography } from '@mui/material';
import React, { ReactElement, useCallback, useContext, useMemo, useState } from 'react';

import { PanelErrorBoundary } from '@fki/editor-core/layout/PanelErrorBoundary';

import './LearningResourceCard.css';

export interface ILearningResourceCardProps {
  label: string;
  children: React.ReactNode;
  optional?: boolean;
  stackProps?: StackProps;
  className?: string;
}

export type ISetClassNameCB = (className: string) => void;

export class LRCardController {
  public setClassName: ISetClassNameCB;

  constructor(setClassName: ISetClassNameCB) {
    this.setClassName = setClassName;
  }
}

export const LRCardContext = React.createContext<LRCardController>(null);

export function useLRCardController(): LRCardController {
  return useContext(LRCardContext);
}

function LearningResourceCardRaw(props: ILearningResourceCardProps, ref: React.Ref<HTMLDivElement>): ReactElement {
  const { children, label, optional, stackProps, className } = props;

  const [showContent, setShowContent] = useState(!optional);

  const [classNameFromContext, setClassNameFromContext] = useState<string>(null);
  const controller = useMemo(() => new LRCardController(setClassNameFromContext), []);

  const onChange = useCallback((event: React.SyntheticEvent, isExpanded: boolean) => {
    setShowContent(isExpanded);
  }, []);

  let fullClassName = 'learning-resource-card';
  if (className) fullClassName += ` ${className}`;
  if (classNameFromContext) fullClassName += ` ${classNameFromContext}`;

  return (
    <LRCardContext.Provider value={controller}>
      <Accordion ref={ref} className={fullClassName} expanded={showContent} onChange={onChange}>
        <AccordionSummary expandIcon={<ExpandMoreIcon />}>
          <Typography role="heading" sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
            {label}
          </Typography>
        </AccordionSummary>
        <AccordionDetails>
          <PanelErrorBoundary>
            {showContent && (
              // eslint-disable-next-line react/jsx-props-no-spreading
              <Stack className="learning-resource-card-content" spacing={2} {...stackProps}>
                {children}
              </Stack>
            )}
          </PanelErrorBoundary>
        </AccordionDetails>
      </Accordion>
    </LRCardContext.Provider>
  );
}

export const LearningResourceCard = React.forwardRef(LearningResourceCardRaw);
