import { i18n } from 'i18next';
import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';

import {
  getMetaFirstAboutLabel,
  ILearningResource,
  IMultiLingualMarkdownText,
  IMultiLingualRawText,
  metaHasValidFirstAboutLabel,
} from '@adlete-vle/lr-core/learning-resources';

import { LRText } from '../LRText';

import { LearningResourceCard } from './LearningResourceCard';

export function validateIsRenderableDefinition(resource: ILearningResource, i18next: i18n): void {
  const id = resource.meta.id;
  if (!resource.meta.type.includes('DefinedTerm'))
    throw new Error(`Learning resource '${id}' is not renderable as a definition. Meta 'type' property must contain 'DefinedTerm'`);

  if (!metaHasValidFirstAboutLabel(resource.meta, i18next.languages))
    throw new Error(`Learning resource '${id}' is not renderable as a definition. Meta 'about' property must contain a concept.`);
}

export interface IDefinitionProps {
  definition: ILearningResource<string | IMultiLingualRawText | IMultiLingualMarkdownText>;
}

export function getDefinitionLabel(resource: ILearningResource, i18next: i18n): string {
  const concept = getMetaFirstAboutLabel(resource.meta, i18next.languages);
  return i18next.t('learning-resources:definition-of', { concept });
}

export function Definition({ definition }: IDefinitionProps): ReactElement {
  const { i18n: i18next } = useTranslation();
  validateIsRenderableDefinition(definition, i18next);
  const label = getDefinitionLabel(definition, i18next);
  return (
    <LearningResourceCard label={label}>
      <LRText text={definition} />
    </LearningResourceCard>
  );
}
