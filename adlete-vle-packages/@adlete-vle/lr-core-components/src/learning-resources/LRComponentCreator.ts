/* eslint-disable @typescript-eslint/ban-types */
import { ILearningResource, ILearningResourceManager } from '@adlete-vle/lr-core/learning-resources';

import { createElementUsingFactory, IReactFactory, ReactComponentFactoryLibrary } from '../ReactComponentFactoryLibrary';
import { PropMapper, ReactPropMapperLibrary } from '../ReactPropMapperLibrary';

export interface ICreateOptions<P = {}> {
  factoryId?: string;
  factory?: IReactFactory<P>;
  props?: P;
}

export interface IResourceProps {
  resource?: ILearningResource;
  resources?: ILearningResource[];
}

export const LR_PROP_MAPPER_CONTEXT = 'learning-resources';

export class LRComponentCreator {
  protected lrMan: ILearningResourceManager;

  protected componentFactoryLib: ReactComponentFactoryLibrary;

  protected propMapperLib: ReactPropMapperLibrary;

  public constructor(
    lrMan: ILearningResourceManager,
    componentFactoryLib: ReactComponentFactoryLibrary,
    propMapperLib: ReactPropMapperLibrary
  ) {
    this.lrMan = lrMan;
    this.componentFactoryLib = componentFactoryLib;
    this.propMapperLib = propMapperLib;
  }

  public fetch(/* ids: string | string[] */): Promise<void> {
    return Promise.resolve();
  }

  public create<P = {}>(ids: string | string[], options?: ICreateOptions<P>): React.ReactElement {
    const [factoryId, factory] = this.getFactoryInfo(ids, options);
    const propMapper = this.getPropMapper(factoryId);
    const resources = this.getLearningResources(ids);

    const unmappedProps: IResourceProps = {
      resource: Array.isArray(resources) ? undefined : resources,
      resources: Array.isArray(resources) ? resources : undefined,
      ...options?.props,
    };

    const mappedProps = propMapper ? propMapper(unmappedProps) : unmappedProps;

    return createElementUsingFactory(factory, mappedProps);
  }

  protected getFactoryInfo<P = {}>(ids: string | string[], options?: ICreateOptions<P>): [string, IReactFactory<P>] {
    if (options?.factory) return [null, options.factory];

    const factoryId = this.getFactoryId(ids, options?.factoryId);
    return [factoryId, this.componentFactoryLib.getFactory(factoryId)];
  }

  protected getFactoryId(ids: string | string[], factoryId?: string): string {
    // TODO: retrieve factory id based on meta-data
    return factoryId;
  }

  protected getPropMapper(factoryId: string): PropMapper {
    return factoryId && this.propMapperLib.hasMapping(factoryId, LR_PROP_MAPPER_CONTEXT)
      ? this.propMapperLib.getMapping(factoryId, LR_PROP_MAPPER_CONTEXT)
      : null;
  }

  protected getLearningResources(ids: string | string[]): ILearningResource | ILearningResource[] {
    if (Array.isArray(ids)) return ids.map((id) => this.lrMan.getResource(id));
    return this.lrMan.getResource(ids);
  }
}
