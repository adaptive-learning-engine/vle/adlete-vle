import { Alert, AlertColor } from '@mui/material';
import { i18n } from 'i18next';
import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';

import type { ILearningResourceMeta } from '@adlete-semantics/amb';
import { getMetaName, HumanReadableTextualData, ILearningResource } from '@adlete-vle/lr-core/learning-resources';

import { LRText } from '../LRText';

import { LearningResourceCard } from './LearningResourceCard';

export interface IHintProps {
  hint: ILearningResource<HumanReadableTextualData>;
  severity?: AlertColor;
}

export function getHintLabel(meta: ILearningResourceMeta, i18next: i18n): string {
  const name = getMetaName(meta, i18next.languages);
  return `${i18next.t('learning-resources:hint')}: ${name}`;
}

export function Hint({ hint, severity }: IHintProps): ReactElement {
  const { i18n: i18next } = useTranslation();
  const label = getHintLabel(hint.meta, i18next);
  return (
    <LearningResourceCard label={label}>
      {severity ? (
        <Alert severity={severity}>
          <LRText text={hint} />
        </Alert>
      ) : (
        <LRText text={hint} />
      )}
    </LearningResourceCard>
  );
}
