import { ILanguageMap } from '@adlete-vle/lr-core/locale/language-map';

export interface ILearningSequence {
  id?: string;
  name?: ILanguageMap<string>;
  resources?: string | string[];
  rendering?: string;
  childEntries?: this[]; // sub-resources
}
