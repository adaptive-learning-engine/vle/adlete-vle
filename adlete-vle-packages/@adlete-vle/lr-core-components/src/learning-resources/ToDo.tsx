import { Alert } from '@mui/material';
import React, { ReactElement } from 'react';

import { ILearningResource } from '@adlete-vle/lr-core/learning-resources';

import { LearningResourceCard } from './LearningResourceCard';

export interface IToDoProps {
  todo: ILearningResource<string>;
}

export function ToDo({ todo }: IToDoProps): ReactElement {
  return (
    <LearningResourceCard label="ToDo">
      <Alert severity="warning">{todo.content}</Alert>
    </LearningResourceCard>
  );
}
