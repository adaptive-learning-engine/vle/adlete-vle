import { Typography } from '@mui/material';
import { i18n } from 'i18next';
import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';

import {
  getMetaFirstAboutLabel,
  getMetaName,
  ILearningResource,
  IMultiLingualMarkdownText,
  IMultiLingualRawText,
  metaHasValidFirstAboutLabel,
} from '@adlete-vle/lr-core/learning-resources';

import { LRText } from '../LRText';

import { LearningResourceCard } from './LearningResourceCard';

export function validateIsRenderableEquation(resource: ILearningResource /* , i18next: i18n */): void {
  const id = resource.meta.id;
  if (!resource.meta.type.includes('Equation'))
    throw new Error(`Learning resource '${id}' is not renderable as an equation. Meta 'type' property must contain 'Equation'`);
}

export interface IEquationProps {
  equation: ILearningResource<string | IMultiLingualRawText | IMultiLingualMarkdownText>;
}

export function getEquationLabel(resource: ILearningResource, i18next: i18n): string {
  if (metaHasValidFirstAboutLabel(resource.meta, i18next.languages)) {
    const concept = getMetaFirstAboutLabel(resource.meta, i18next.languages);
    return i18next.t('learning-resources:equation-of', { concept });
  }

  return getMetaName(resource.meta, i18next.languages);
}

export function Equation({ equation }: IEquationProps): ReactElement {
  const { i18n: i18next } = useTranslation();

  validateIsRenderableEquation(equation);
  const label = getEquationLabel(equation, i18next);
  return (
    <LearningResourceCard label={label}>
      <Typography /* fontSize="1.25em" */ component="div">
        <LRText text={equation} enableTTS={false} />
      </Typography>
    </LearningResourceCard>
  );
}
