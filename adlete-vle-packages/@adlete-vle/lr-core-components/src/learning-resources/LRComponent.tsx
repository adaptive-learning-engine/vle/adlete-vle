import React from 'react';
import { useTranslation } from 'react-i18next';

import { EncodingFormats, getFirstEncodingWithFormat, getMetaName, ILearningResource } from '@adlete-vle/lr-core/learning-resources';

import { createElementUsingFactory } from '../ReactComponentFactoryLibrary';

import { IReactLearningResourceContent } from './IReactLearningResourceContent';
import { LearningResourceCard } from './LearningResourceCard';

const validEncodingFormats: string[] = [EncodingFormats.ReactComponent];

export function validateIsRenderableComponentWrapper(resource: ILearningResource): void {
  const id = resource.meta.id;
  const encoding = getFirstEncodingWithFormat(resource.meta, validEncodingFormats); // TODO: cache

  if (!encoding)
    throw new Error(
      `Learning resource '${id}' is not renderable as a ComponentWrapper. Encodings must contain 'application/x.react-component'`
    );

  if (!resource.content || !resource.content.factory)
    throw new Error(`Learning resource '${id}' is not renderable as a ComponentWrapper. Factory information is missing!`);
}

export interface ILRComponentProps {
  learningResource: ILearningResource<IReactLearningResourceContent>;
  // eslint-disable-next-line @typescript-eslint/ban-types
  forwardedProps: {};
}

export function LRComponent({ learningResource, forwardedProps }: ILRComponentProps): React.ReactElement {
  const { i18n: i18next } = useTranslation();
  validateIsRenderableComponentWrapper(learningResource);

  const label = getMetaName(learningResource.meta, i18next.languages);
  const factory = learningResource.content.factory;
  const content = createElementUsingFactory(factory, forwardedProps);

  return <LearningResourceCard label={label}>{content}</LearningResourceCard>;
}
