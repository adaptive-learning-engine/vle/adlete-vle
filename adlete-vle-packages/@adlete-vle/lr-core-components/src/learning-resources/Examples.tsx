import { Pagination } from '@mui/material';
import { i18n } from 'i18next';
import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';

import {
  getMetaFirstAboutLabel,
  ILearningResource,
  IMultiLingualMarkdownText,
  IMultiLingualRawText,
  metaHasValidFirstAboutLabel,
} from '@adlete-vle/lr-core/learning-resources';

import { LRText } from '../LRText';

import { LearningResourceCard } from './LearningResourceCard';

export function validateIsRenderableExample(resource: ILearningResource, i18next: i18n): void {
  const id = resource.meta.id;
  if (!resource.meta.type.includes('Example'))
    throw new Error(`Learning resource '${id}' is not renderable as an example. Meta 'type' property must contain 'Example'`);

  if (!metaHasValidFirstAboutLabel(resource.meta, i18next.languages))
    throw new Error(`Learning resource '${id}' is not renderable as an example. Meta 'about' property must contain a concept.`);
}

export function validateAreRenderableExamples(resources: ILearningResource[], i18next: i18n): void {
  // TODO: validate examples have same concept
  resources.forEach((resource) => validateIsRenderableExample(resource, i18next));
}

export interface IExamplesProps {
  examples: ILearningResource<string | IMultiLingualRawText | IMultiLingualMarkdownText>[];
  // TODO: config for paginated or not
}

export function getExampleLabel(resource: ILearningResource, i18next: i18n): string {
  const concept = getMetaFirstAboutLabel(resource.meta, i18next.languages);
  return i18next.t('learning-resources:examples-for', { concept });
}

export function Examples({ examples }: IExamplesProps): ReactElement {
  const { i18n: i18next } = useTranslation();

  validateAreRenderableExamples(examples, i18next);

  const [currExampleIndex, setCurrExampleIndex] = React.useState(1);
  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    setCurrExampleIndex(value);
  };

  const label = getExampleLabel(examples[0], i18next);
  const example = examples[currExampleIndex - 1];
  return (
    <LearningResourceCard label={label}>
      <LRText key={example.meta.id} text={example} />
      {examples.length > 1 && <Pagination count={examples.length} page={currExampleIndex} onChange={handleChange} />}
    </LearningResourceCard>
  );
}
