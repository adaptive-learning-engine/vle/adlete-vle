import { i18n } from 'i18next';
import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';

import type { ILearningResourceMeta } from '@adlete-semantics/amb';
import {
  EncodingFormats,
  getFirstEncodingWithFormat,
  getMetaName,
  IEmbeddedVideoByScript,
  ILearningResource,
} from '@adlete-vle/lr-core/learning-resources';

import { EmbeddedVideo as EmbeddedVideoInternal } from '../internal/EmbeddedVideo';

import { LearningResourceCard } from './LearningResourceCard';

const validEncodingFormats: string[] = [EncodingFormats.EmbeddedVideoByScript];

export function validateIsEmbeddableVideo(resource: ILearningResource): void {
  const id = resource.meta.id;

  // TODO: look through all encoding formats
  const encoding = getFirstEncodingWithFormat(resource.meta, validEncodingFormats);

  if (!encoding)
    throw new Error(
      `Learning resource '${id}' is not renderable as a ComponentWrapper. Encodings must contain '${EncodingFormats.EmbeddedVideoByScript}'`
    );

  if (!resource.meta.type.includes('VideoObject'))
    throw new Error(`Learning resource '${id}' is not renderable as an embedded video. Meta 'type' property must contain 'VideoObject'`);
}

export function getVideoLabel(meta: ILearningResourceMeta, i18next: i18n): string {
  const name = getMetaName(meta, i18next.languages);
  return `${i18next.t('learning-resources:video')}: ${name}`;
}

export interface IEmbeddedVideoProps {
  video: ILearningResource<IEmbeddedVideoByScript>;
}

export function EmbeddedVideo({ video }: IEmbeddedVideoProps): ReactElement {
  const { i18n: i18next } = useTranslation();

  // TODO: cache
  validateIsEmbeddableVideo(video);

  const label = getVideoLabel(video.meta, i18next);
  return (
    <LearningResourceCard label={label}>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <EmbeddedVideoInternal mode="script" attributes={video.content} />
    </LearningResourceCard>
  );
}
