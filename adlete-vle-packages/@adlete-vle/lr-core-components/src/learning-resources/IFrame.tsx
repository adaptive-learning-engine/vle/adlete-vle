import { i18n } from 'i18next';
import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';

import { getMetaName, ILearningResource } from '@adlete-vle/lr-core/learning-resources';

import { LearningResourceCard } from './LearningResourceCard';

export interface IIFrameProps {
  iframe: ILearningResource;
}

export function getIFrameLabel(resource: ILearningResource, i18next: i18n): string {
  return getMetaName(resource.meta, i18next.languages);
}

export function IFrame({ iframe }: IIFrameProps): ReactElement {
  const { i18n: i18next } = useTranslation();
  // TODO: iframe validation
  // validateIsRenderableIframe(iframe, i18next);
  const label = getIFrameLabel(iframe, i18next);

  return (
    <LearningResourceCard label={label}>
      <iframe role="document" title={label} src={iframe.meta.id} height="1324 px" />
    </LearningResourceCard>
  );
}
