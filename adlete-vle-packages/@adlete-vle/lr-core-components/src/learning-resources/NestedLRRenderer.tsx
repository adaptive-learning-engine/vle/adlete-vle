/* eslint-disable react/prop-types */
// import type { PropertyKey } from '@adlete-utils/typescript';
import { Box, Button, Pagination, Stack, Typography } from '@mui/material';
import React, { useCallback, useRef } from 'react';
import { useTranslation } from 'react-i18next';

import { getTranslationFromLangMap } from '@adlete-vle/lr-core/locale';

import { PanelErrorBoundary } from '@fki/editor-core/layout/PanelErrorBoundary';

import { ILearningSequence } from './ILearningSequence';
import { LRComponentCreator } from './LRComponentCreator';

export interface INestedLRRendererProps {
  componentCreator: LRComponentCreator;
  learningSequence: ILearningSequence;
  // sharedProps?: Record<PropertyKey, any>;
  section: number;
  onSectionChanged: (section: number) => void;
  devVersion?: string;
}

function getKeyFromSequence(sequence: ILearningSequence, index = 0): React.Key {
  if (sequence.id) return sequence.id;
  const resources = sequence.resources;
  if (resources) {
    if (typeof resources === 'string') return resources;
    if (Array.isArray(resources)) return resources[0];
  }
  return index;
}

interface ILREntryProps {
  item: ILearningSequence;
  componentCreator: LRComponentCreator;
}

const LREntry = React.memo((props: ILREntryProps): React.ReactElement => {
  const { item, componentCreator } = props;
  return componentCreator.create(item.resources, { factoryId: item.rendering });
});

export function NestedLRRenderer(props: INestedLRRendererProps): React.ReactElement {
  const { componentCreator, section, onSectionChanged, devVersion } = props;

  const { i18n, t } = useTranslation();

  const thisRef = useRef(null);

  const handleChange = (event: React.ChangeEvent<unknown>, value: number) => {
    onSectionChanged(value - 1);
    if (thisRef.current) thisRef.current.scrollIntoView();
  };

  const onPrevious = useCallback(() => {
    onSectionChanged(section - 1);
    if (thisRef.current) thisRef.current.scrollIntoView();
  }, [section, onSectionChanged]);

  const onNext = useCallback(() => {
    onSectionChanged(section + 1);
    if (thisRef.current) thisRef.current.scrollIntoView();
  }, [section, onSectionChanged]);

  // 1. filter content to render
  const sections = props.learningSequence.childEntries as ILearningSequence[];
  const sectionChildren = sections[section].childEntries;
  // const lrRenderableItemInfo = convertToRenderable(lrItemInfo, lrm, propMapperMan);

  const elements = sectionChildren.map((item, index) => (
    <PanelErrorBoundary key={getKeyFromSequence(item, index)}>
      {devVersion && (
        <Box sx={{ paddingTop: '10px', fontSize: 'smaller' }}>
          [DevInfo] ({devVersion}) {section + 1}.{index + 1}
        </Box>
      )}
      <div className="nested-lr-entry">
        {/* eslint-disable-next-line react/jsx-props-no-spreading */}
        <LREntry item={item} componentCreator={componentCreator} />
      </div>
    </PanelErrorBoundary>
  ));

  // TODO: section context

  return (
    <Stack className="nested-lr-renderer" ref={thisRef} spacing={2} alignItems="center">
      <Stack alignItems="center">
        <Pagination count={sections.length} page={section + 1} onChange={handleChange} variant="outlined" shape="rounded" />
      </Stack>
      {sections[section].name && (
        <Typography variant="h6" textAlign="center">
          {getTranslationFromLangMap(sections[section].name, i18n.languages)}
        </Typography>
      )}
      {elements}
      <Stack className="prev-next-buttons" justifyContent="center" direction="row">
        <Button onClick={onPrevious} color="secondary" disabled={section === 0}>
          {t('learning-resources:previous')}
        </Button>
        <Button variant="contained" color="secondary" onClick={onNext} disabled={section >= sections.length - 1}>
          {t('learning-resources:next')}
        </Button>
      </Stack>
      <Stack alignItems="center">
        <Pagination
          count={sections.length}
          page={section + 1}
          onChange={handleChange}
          variant="outlined"
          shape="rounded"
          hidePrevButton
          hideNextButton
        />
      </Stack>
    </Stack>
  );
}
