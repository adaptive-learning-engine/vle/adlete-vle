import { Box, Stack } from '@mui/material';
import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';

import {
  EncodingFormats,
  getFirstEncodingWithFormat,
  HumanReadableTextualData,
  ILearningResource,
  IMultiLingualMarkdownText,
  IMultiLingualRawText,
  metaHasEncodingFormat,
  NoContentError,
  WrongEncodingError,
} from '@adlete-vle/lr-core/learning-resources';
import { getTranslationFromLangMap } from '@adlete-vle/lr-core/locale';

import { CustomMarkdown } from './CustomMarkdown';
import { TTSButton } from './internal/TTSButton';

import './LRText.css';

const validEncodingFormats: string[] = [
  EncodingFormats.TextMarkdown,
  EncodingFormats.TextMultiLingualMarkdown,
  EncodingFormats.TextMultiLingualText,
  EncodingFormats.TextPlain,
];

export function validateIsRenderableLRText(resource: ILearningResource<HumanReadableTextualData>): void {
  const id = resource.meta.id;
  if (!metaHasEncodingFormat(resource.meta, validEncodingFormats))
    throw new WrongEncodingError(`Learning resource '${id}' is not renderable as a text'. Meta 'encodingFormat' is not supported!`);

  if (!resource.content)
    throw new NoContentError(
      `Learning resource '${id}' is not renderable as a text'. Content is undefined. Component requires embedded content!`
    );
}

export interface ILRTextProps {
  text: ILearningResource<HumanReadableTextualData>;
  enableTTS?: boolean;
}

export function LRText({ text, enableTTS = true }: ILRTextProps): ReactElement {
  validateIsRenderableLRText(text);

  const { i18n } = useTranslation();
  const languages = i18n.languages;

  const encoding = getFirstEncodingWithFormat(text.meta, validEncodingFormats); // TODO: cache

  const ttsButton = enableTTS && <TTSButton text={text.content} />;

  const className = enableTTS ? 'lr-text tts-enabled' : 'lr-text';

  switch (encoding.encodingFormat) {
    case EncodingFormats.TextPlain: {
      return (
        <Stack className={className} spacing={enableTTS ? 2 : 0} direction="row" alignItems="start">
          {ttsButton}
          <Box>{text.content as string}</Box>
        </Stack>
      );
    }
    case EncodingFormats.TextMarkdown:
      return (
        <Stack className={className} spacing={enableTTS ? 2 : 0} direction="row" alignItems="start">
          {ttsButton}
          <CustomMarkdown text={text.content as string} />
        </Stack>
      );
    case EncodingFormats.TextMultiLingualMarkdown:
      return (
        <Stack className={className} spacing={enableTTS ? 2 : 0} direction="row" alignItems="start">
          {ttsButton}
          <CustomMarkdown text={getTranslationFromLangMap(text.content as IMultiLingualMarkdownText, languages)} />
        </Stack>
      );
    case EncodingFormats.TextMultiLingualText:
      return (
        <Stack className={className} spacing={enableTTS ? 2 : 0} direction="row" alignItems="start">
          {ttsButton}
          <Box>{getTranslationFromLangMap(text.content as IMultiLingualRawText, languages)}</Box>
        </Stack>
      );

    default:
      // shouldn't be necessary, because of validation
      throw new Error(`Unknown dataType ${text.meta.encoding}`);
  }
}
