import React from 'react';
import ReactMarkdown from 'react-markdown';
import rehypeMathjax from 'rehype-mathjax';
import remarkMath from 'remark-math';

import './CustomMarkdown.css';

export interface ICustomMarkdownProps {
  text?: string;
  children?: string;
  className?: string;
}

export function CustomMarkdownRaw({ text, children, className }: ICustomMarkdownProps): React.ReactElement {
  const fullClassName = className ? `${className} custom-markdown` : 'custom-markdown';
  return (
    <ReactMarkdown className={fullClassName} remarkPlugins={[remarkMath]} rehypePlugins={[[rehypeMathjax, { scale: 4 }]]}>
      {text || children}
    </ReactMarkdown>
  );
}

export const CustomMarkdown = React.memo(CustomMarkdownRaw);
