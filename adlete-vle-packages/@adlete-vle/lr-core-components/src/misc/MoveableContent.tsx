import AlignHorizontalCenterIcon from '@mui/icons-material/AlignHorizontalCenter';
import AlignHorizontalLeftIcon from '@mui/icons-material/AlignHorizontalLeft';
import AlignVerticalTopIcon from '@mui/icons-material/AlignVerticalTop';
import { Stack, ToggleButton, ToggleButtonGroup, Tooltip } from '@mui/material';
import React, { useCallback } from 'react';
import { useTranslation } from 'react-i18next';

export type MoveableContentPosition = 'original' | 'left' | 'top';

export interface IMoveableContentButtonsProps {
  value: MoveableContentPosition;
  onChange?: (pos: MoveableContentPosition) => void;
  orientation?: 'horizontal' | 'vertical';
  useIcons?: boolean;
}

export function MoveableContentButtons({ value, onChange, orientation, useIcons }: IMoveableContentButtonsProps): React.ReactElement {
  const { t } = useTranslation();
  const onButtonToggled = useCallback(
    (event: React.MouseEvent<HTMLElement>, newPos: MoveableContentPosition) => {
      if (newPos != null) onChange?.(newPos);
    },
    [onChange]
  );
  return (
    <ToggleButtonGroup value={value} exclusive onChange={onButtonToggled} orientation={orientation}>
      <ToggleButton value="original">
        <Tooltip title={t('Standardposition')} placement="top">
          {useIcons ? <AlignHorizontalCenterIcon /> : <div>{t('Standard')}</div>}
        </Tooltip>
      </ToggleButton>
      <ToggleButton value="left">
        <Tooltip title={t('Links positionieren')} placement="top">
          {useIcons ? <AlignHorizontalLeftIcon /> : <div>{t('Links')}</div>}
        </Tooltip>
      </ToggleButton>
      <ToggleButton value="top">
        <Tooltip title={t('Oben positionieren')} placement="top">
          {useIcons ? <AlignVerticalTopIcon /> : <div>{t('Oben')}</div>}
        </Tooltip>
      </ToggleButton>
    </ToggleButtonGroup>
  );
}

export interface IMoveableContentProps {
  targetIndex: number;
  children: React.ReactElement[];
  position: MoveableContentPosition;
  className?: string;
}

export function MoveableContentRaw(
  { children, position, targetIndex, className }: IMoveableContentProps,
  ref: React.Ref<HTMLDivElement>
): React.ReactElement {
  const targetChild = children[targetIndex];
  const beforeChildren = children.slice(0, targetIndex);
  const afterChildren = children.slice(targetIndex + 1);

  const fullClassName = className ? `${'moveable-content '}${className}` : 'moveable-content';

  const beforeSection = (
    <Stack key="before" className="moveable-content-before" spacing={2}>
      {beforeChildren}
    </Stack>
  );

  const targetSection = (
    <Stack key="target" className="moveable-content-target" spacing={2}>
      {targetChild}
    </Stack>
  );

  const afterSection = (
    <Stack key="after" className="moveable-content-after" spacing={2}>
      {afterChildren}
    </Stack>
  );

  let content: React.ReactNode;
  switch (position) {
    case 'original':
      content = [beforeSection, targetSection, afterSection];
      break;
    case 'top':
      content = [targetSection, beforeSection, afterSection];
      break;
    case 'left':
      content = [
        targetSection,
        <Stack key="right-side" className="moveable-content-right" spacing={2}>
          {beforeSection}
          {afterSection}
        </Stack>,
      ];
      break;
    default:
      throw new Error(`Unknown Position ${position}`);
  }

  return (
    <Stack ref={ref} className={fullClassName} direction={position === 'left' ? 'row' : 'column'} spacing={2}>
      {content}
    </Stack>
  );
}

export const MoveableContent = React.forwardRef(MoveableContentRaw);
