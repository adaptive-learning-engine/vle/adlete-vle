import React from 'react';

export type IImageProps = React.DetailedHTMLProps<React.ImgHTMLAttributes<HTMLImageElement>, HTMLImageElement>;

export function Image(props: IImageProps): React.ReactElement {
  // eslint-disable-next-line react/jsx-props-no-spreading, jsx-a11y/alt-text
  return <img {...props} />;
}
