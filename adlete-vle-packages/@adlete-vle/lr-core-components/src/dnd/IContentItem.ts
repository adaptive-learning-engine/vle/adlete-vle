export interface IContentItem<TContent> {
  content: TContent;
}

export function wrapAsContentItem<TContent>(content: TContent): IContentItem<TContent> {
  return { content };
}

export function wrapAllAsContentItem<TContent>(contents: TContent[]): IContentItem<TContent>[] {
  return contents.map((content) => ({ content }));
}
