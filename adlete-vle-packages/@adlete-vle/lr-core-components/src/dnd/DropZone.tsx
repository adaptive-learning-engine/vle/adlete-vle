import { Box } from '@mui/material';
import React from 'react';
import { useDrop } from 'react-dnd';

export interface IDropZoneProps<TItem> {
  accept: string | symbol;
  children: React.ReactNode;
  onDrop?: (item: TItem) => void;
  display?: string; // TODO: use better type from MUI CSS
}

interface ICollectedFromDnD {
  canDrop: boolean;
}

export function DropZone<TItem>(props: IDropZoneProps<TItem>): React.ReactElement {
  const { accept, children, onDrop, display } = props;

  const [{ canDrop }, drop] = useDrop<TItem, unknown, ICollectedFromDnD>(
    () => ({
      accept,
      // canDrop: () => canMoveKnight(x, y),
      drop: (item) => onDrop && onDrop(item),
      collect: (monitor) => ({
        canDrop: !!monitor.canDrop(),
        // item: monitor.getItem(),
      }),
    }),
    []
  );

  const className = canDrop ? 'can-drop' : 'can-not-drop';

  return (
    <Box className={className} ref={drop} display={display}>
      {children}
    </Box>
  );
}
