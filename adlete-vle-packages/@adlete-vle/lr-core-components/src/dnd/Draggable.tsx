import React from 'react';
import { useDrag } from 'react-dnd';

export interface IDraggableProps<TItem> {
  type: string | symbol;
  children: React.ReactNode;
  item: TItem;
}

export function Draggable<TItem>(props: IDraggableProps<TItem>): React.ReactElement {
  const { type, children, item } = props;

  const [{ isDragging }, drag] = useDrag(
    () => ({
      type,
      item,
      collect: (monitor) => ({
        isDragging: !!monitor.isDragging(),
      }),
    }),
    [item, type]
  );

  // TODO: use CSS
  return (
    <div
      ref={drag}
      style={{
        opacity: isDragging ? 0.5 : 1,
        cursor: 'move',
      }}
    >
      {children}
    </div>
  );
}
