import HearingIcon from '@mui/icons-material/Hearing';
import HearingDisabledIcon from '@mui/icons-material/HearingDisabled';
import { IconButton } from '@mui/material';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { useStatefulEvent } from '@adlete-utils/events-react';
import { HumanReadableTextualData } from '@adlete-vle/lr-core/learning-resources';
import { getTranslationFromLangMap } from '@adlete-vle/lr-core/locale';

import { useTTSManager } from '../speech/synthesis/hooks';

export interface ITTSButtonProps {
  text: HumanReadableTextualData;
}

export function TTSButton({ text }: ITTSButtonProps): React.ReactElement {
  const { i18n } = useTranslation();
  const languages = i18n.languages;
  const ttsManager = useTTSManager();
  const presets = useStatefulEvent(ttsManager.events, 'presets');

  const [isPlaying, setIsPlaying] = useState(false);

  const request = useMemo(() => {
    const textString = typeof text === 'string' ? text : getTranslationFromLangMap(text, languages);
    return ttsManager.createSpeechRequest(textString, languages[0]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [text, languages, ttsManager, presets]);

  useEffect(() => {
    const endListener = () => {
      setIsPlaying(false);
    };
    request.addEventListener('end', endListener);
    request.addEventListener('pause', endListener);

    return () => {
      request.removeEventListener('end', endListener);
      request.removeEventListener('pause', endListener);
    };
  }, [request]);

  const speak = useCallback(() => {
    setIsPlaying(true);
    ttsManager.speakRequest(request);
  }, [request, ttsManager]);

  const stop = useCallback(() => {
    setIsPlaying(false);
    ttsManager.stop();
  }, [ttsManager]);

  return isPlaying ? (
    <IconButton className="tts-button" aria-label="tts-button" size="small" onClick={stop}>
      <HearingDisabledIcon sx={{ transform: 'scaleX(-1)' }} />
    </IconButton>
  ) : (
    <IconButton className="tts-button" aria-label="tts-button" size="small" onClick={speak}>
      <HearingIcon />
    </IconButton>
  );
}
