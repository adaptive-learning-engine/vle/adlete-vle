import React, { useEffect, useRef } from 'react';

import { IEmbeddedVideoByScript } from '@adlete-vle/lr-core/learning-resources/encoding-formats';

export interface IEmbeddedVideoProps {
  // TODO: Support more modes
  // eslint-disable-next-line react/no-unused-prop-types
  mode: 'script';
  attributes?: IEmbeddedVideoByScript;
}

export function EmbeddedVideo(props: IEmbeddedVideoProps): React.ReactElement {
  const { attributes } = props;
  const ref = useRef<HTMLDivElement>(null);
  useEffect(() => {
    const theRef = ref.current;
    if (!theRef) return () => {};

    const script = document.createElement('script');

    Object.entries(attributes).forEach(([attr, value]) => script.setAttribute(attr, value));

    script.async = true;

    theRef.appendChild(script);

    return () => {
      while (theRef.firstChild) {
        theRef.removeChild(theRef.lastChild);
      }
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [attributes, ref.current]);

  return <div ref={ref} />;
}
