import React, { PropsWithChildren } from 'react';
import { I18nextProvider } from 'react-i18next';

import { LocaleManager } from '@adlete-vle/lr-core/locale';

import { InMemoryStorage } from '@fki/editor-core/storage';

import { addLearningResourceComponentLocales } from '../../locales';
import { TTSContext, TTSManager } from '../../speech/synthesis';

export async function createTTSManager() {
  const storage = new InMemoryStorage();
  storage.setItem('locale-manager-data', {
    language: 'en-US',
    fallbackLanguage: 'en-US',
  });

  const localeManager = new LocaleManager(storage, { createInstance: true });
  await localeManager.init();
  addLearningResourceComponentLocales(localeManager.i18n);

  const ttsManager = new TTSManager(localeManager, storage);
  await ttsManager.init();

  return { ttsManager, localeManager, storage };
}

export async function createTTSDecorator(): Promise<(Story: React.FunctionComponent) => React.ReactElement> {
  const { ttsManager, localeManager } = await createTTSManager();
  return function wrapper(Story: React.FunctionComponent) {
    return (
      <I18nextProvider i18n={localeManager.i18n} defaultNS="translation">
        <TTSContext.Provider value={ttsManager}>
          <Story />
        </TTSContext.Provider>
      </I18nextProvider>
    );
  };
}

export async function createTTSWrapper(): Promise<(props: PropsWithChildren) => React.ReactElement> {
  const { ttsManager, localeManager } = await createTTSManager();
  return function wrapper({ children }: PropsWithChildren) {
    return (
      <I18nextProvider i18n={localeManager.i18n} defaultNS="translation">
        <TTSContext.Provider value={ttsManager}>{children}</TTSContext.Provider>
      </I18nextProvider>
    );
  };
}
