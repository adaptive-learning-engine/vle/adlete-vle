export function getFirstElementByClassNameAsHTML(elem: Element, className: string): HTMLElement {
  return elem.getElementsByClassName(className)[0] as HTMLElement;
}
