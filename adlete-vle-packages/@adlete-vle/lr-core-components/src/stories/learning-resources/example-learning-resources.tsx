import React from 'react';

import { ILearningResource, IMultiLingualRawText } from '@adlete-vle/lr-core/learning-resources';

import { IReactLearningResourceContent } from '../../learning-resources/IReactLearningResourceContent';

export const CONTEXT = ['https://w3id.org/kim/amb/draft/context.jsonld', { '@language': 'en' }];
export const ID_1 = 'https://some.id';
export const ID_2 = 'https://some-other.id';
export const NAME_1 = 'English Name';
export const NAME_2 = 'Another English Name';
export const LOCALIZED_NAME_1 = {
  en: 'English Name',
  de: 'Deutscher Name',
};
export const LOCALIZED_NAME_2 = {
  en: 'Another English Name',
  de: 'Ein anderer deutscher Name',
};
export const CONCEPT_1 = {
  id: 'some-concept-id',
  type: 'Concept',
  prefLabel: { en: 'English Concept', de: 'Deutsches Konzept' },
};

export const PLAIN_TEXT_CONTENT_1 = 'English Text';

export const MULTI_LINGUAL_TEXT_CONTENT_1 = {
  en: 'English Text',
  de: 'Deutscher Text',
};
export const MULTI_LINGUAL_TEXT_CONTENT_2 = {
  en: 'Another English Text',
  de: 'Ein anderer deutscher Text',
};

export const MULTI_LINGUAL_HINT_LR: ILearningResource<IMultiLingualRawText> = {
  meta: {
    '@context': CONTEXT,
    id: ID_1,
    type: ['LearningResource', 'Hint'],
    name: NAME_1,
    localizedName: LOCALIZED_NAME_1,
    image: 'https://upload.wikimedia.org/wikipedia/commons/1/19/Rick_Astley_Aint_Too_Proud_To_Beg.jpg',
    inLanguage: ['de', 'en'],
    learningResourceType: [{ id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text' }],
    encoding: [{ encodingFormat: 'text/x.multi-lingual-text+json' }],
  },
  content: MULTI_LINGUAL_TEXT_CONTENT_1,
};

export const MULTI_LINGUAL_DEFINITION_LR: ILearningResource<IMultiLingualRawText> = {
  meta: {
    '@context': CONTEXT,
    id: ID_1,
    type: ['LearningResource', 'DefinedTerm'],
    name: NAME_1,
    localizedName: LOCALIZED_NAME_1,
    inLanguage: ['de', 'en'],
    learningResourceType: [{ id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text' }],
    encoding: [{ encodingFormat: 'text/x.multi-lingual-text+json' }],
    about: [CONCEPT_1],
  },
  content: MULTI_LINGUAL_TEXT_CONTENT_1,
};

export const TODO_LR: ILearningResource<string> = {
  meta: {
    '@context': CONTEXT,
    id: ID_1,
    type: ['LearningResource', 'ToDo'],
    name: NAME_1,
    inLanguage: ['en'],
    learningResourceType: [{ id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text' }],
    encoding: [{ encodingFormat: 'text/plain' }],
  },
  content: 'Something to do',
};

export const PLAIN_EXAMPLE_LR_1: ILearningResource<string> = {
  meta: {
    '@context': CONTEXT,
    id: ID_1,
    name: NAME_1,
    type: ['LearningResource', 'Example'],
    encoding: [{ encodingFormat: 'text/plain' }],
    about: [CONCEPT_1],
  },
  content: PLAIN_TEXT_CONTENT_1,
};

export const MULTI_LINGUAL_EXAMPLE_LR_1: ILearningResource<IMultiLingualRawText> = {
  meta: {
    '@context': CONTEXT,
    id: ID_1,
    name: NAME_1,
    type: ['LearningResource', 'Example'],
    encoding: [{ encodingFormat: 'text/x.multi-lingual-markdown+json' }],
    about: [CONCEPT_1],
  },
  content: MULTI_LINGUAL_TEXT_CONTENT_1,
};

export const MULTI_LINGUAL_EXAMPLE_LR_2: ILearningResource<IMultiLingualRawText> = {
  meta: {
    '@context': CONTEXT,
    id: ID_2,
    name: NAME_2,
    type: ['LearningResource', 'Example'],
    encoding: [{ encodingFormat: 'text/x.multi-lingual-markdown+json' }],
    about: [CONCEPT_1],
  },
  content: MULTI_LINGUAL_TEXT_CONTENT_2,
};

export const REACT_COMPONENT_LR: ILearningResource<IReactLearningResourceContent> = {
  meta: {
    '@context': CONTEXT,
    id: ID_1,
    type: ['LearningResource', 'InteractiveExample'],
    name: NAME_1,
    localizedName: LOCALIZED_NAME_1,
    learningResourceType: [{ id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/simulation' }],
    encoding: [{ encodingFormat: 'application/x.react-component' }],
  },
  content: {
    factory: {
      createElement: () => <div>React Content</div>,
    },
  },
};

export const IFRAME_LR: ILearningResource<unknown> = {
  meta: {
    '@context': CONTEXT,
    id: 'https://en.wikipedia.org/wiki/IFrame',
    type: ['LearningResource', 'DefinedTerm'],
    name: NAME_1,
    localizedName: LOCALIZED_NAME_1,
    learningResourceType: [{ id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text' }],
  },
};
