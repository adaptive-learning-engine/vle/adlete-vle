import { expect } from '@storybook/jest';
import type { Meta, StoryObj } from '@storybook/react';
import { userEvent, within } from '@storybook/testing-library';

import { Examples } from '../../learning-resources/Examples';
import { getFirstElementByClassNameAsHTML } from '../shared/test-utils';
import { createTTSDecorator } from '../shared/tts-setup';

import { MULTI_LINGUAL_EXAMPLE_LR_1, MULTI_LINGUAL_EXAMPLE_LR_2 } from './example-learning-resources';

const TTS_DECORATOR = await createTTSDecorator();

const meta: Meta<typeof Examples> = {
  title: 'learning-resources/Examples',
  component: Examples,
  decorators: [TTS_DECORATOR],
};

// eslint-disable-next-line import/no-default-export
export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    examples: [MULTI_LINGUAL_EXAMPLE_LR_1, MULTI_LINGUAL_EXAMPLE_LR_2],
  },
  play: async ({ canvasElement, step }) => {
    const canvas = within(canvasElement);
    const contentElement = getFirstElementByClassNameAsHTML(canvasElement, 'learning-resource-card-content');

    await step('has navigation between examples', async () => {
      expect(canvas.getByRole('navigation')).toBeInTheDocument();
    });

    await step("shows the phrase 'Examples for' in the label", async () => {
      await expect(canvas.getByRole('heading')).toHaveTextContent('Examples for');
    });

    await step('shows the textual content of the first example upon start', async () => {
      await expect(contentElement).toHaveTextContent(MULTI_LINGUAL_EXAMPLE_LR_1.content.en);
    });

    await step('has a TTS Button', async () => {
      expect(canvas.getByRole('button', { name: 'tts-button' })).toBeInTheDocument();
    });

    await step('shows the textual content of the second example after click on page button', async () => {
      const button = canvas.getByRole('button', { name: 'Go to page 2' });
      await userEvent.click(button);
      await expect(contentElement).toHaveTextContent(MULTI_LINGUAL_EXAMPLE_LR_2.content.en);
    });

    await step('shows the textual content of the first example after click on page button', async () => {
      const button = canvas.getByRole('button', { name: 'Go to page 1' });
      await userEvent.click(button);
      await expect(contentElement).toHaveTextContent(MULTI_LINGUAL_EXAMPLE_LR_1.content.en);
    });

    await step('shows the textual content of the first example after click on next button', async () => {
      const button = canvas.getByRole('button', { name: 'Go to next page' });
      await userEvent.click(button);
      await expect(contentElement).toHaveTextContent(MULTI_LINGUAL_EXAMPLE_LR_2.content.en);
    });

    await step('shows the textual content of the first example after click on previous button', async () => {
      const button = canvas.getByRole('button', { name: 'Go to previous page' });
      await userEvent.click(button);
      await expect(contentElement).toHaveTextContent(MULTI_LINGUAL_EXAMPLE_LR_1.content.en);
    });
  },
};

export const NoPagination: Story = {
  args: {
    examples: [MULTI_LINGUAL_EXAMPLE_LR_1],
  },

  play: async ({ canvasElement, step }) => {
    const canvas = within(canvasElement);

    await step('does not have navigation between examples', async () => {
      expect(canvas.queryByRole('navigation')).not.toBeInTheDocument();
    });
  },
};
