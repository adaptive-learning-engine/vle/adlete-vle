import { expect } from '@storybook/jest';
import type { Meta, StoryObj } from '@storybook/react';
import { within } from '@storybook/testing-library';
import React, { ReactNode } from 'react';

import { LearningResourceCard } from '../../learning-resources/LearningResourceCard';

import './LearningResourceCard.css';

const meta = {
  title: 'learning-resources/LearningResourceCard',
  component: LearningResourceCard,
} satisfies Meta<typeof LearningResourceCard>;

// eslint-disable-next-line import/no-default-export
export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    label: 'My Label',
    children: <div data-testid="children">Children</div>,
  },
  play: async ({ canvasElement, step }) => {
    const canvas = within(canvasElement);
    const elem = canvasElement.firstChild;

    await step('has correct class', async () => {
      expect(elem).toHaveClass('learning-resource-card');
    });

    await step('has correct label', async () => {
      await expect(canvas.getByRole('heading')).toBeInTheDocument();
      await expect(canvas.getByRole('heading')).toHaveTextContent('My Label');
    });

    await step('renders children', async () => {
      await expect(canvas.getByTestId('children')).toBeInTheDocument();
    });
  },
};

export const CustomClass: Story = {
  args: {
    label: 'My Label',
    children: <div data-testid="children">Children</div>,
    className: 'custom-class',
  },
  play: async ({ canvasElement, step }) => {
    const elem = canvasElement.firstChild;

    await step('has custom className', async () => {
      expect(elem).toHaveClass('custom-class');
    });
  },
};

export const Optional: Story = {
  args: {
    label: 'Label',
    children: <div data-testid="children">Children</div>,
    optional: true,
  },
  play: async ({ canvasElement, step }) => {
    const canvas = within(canvasElement);
    await step("doesn't render children, when 'optional' is set", async () => {
      expect(canvas.queryByTestId('children')).not.toBeInTheDocument();
    });
  },
};

function ErrorChild(): ReactNode {
  throw new Error('SpecialError');
}

export const ErrorInChild: Story = {
  args: {
    label: 'Label',
    children: <ErrorChild />,
  },
  play: async ({ canvasElement, step }) => {
    const canvas = within(canvasElement);
    await step('renders an error boundary that is an alert', async () => {
      await expect(canvas.getByRole('alert')).toBeInTheDocument();
      await expect(canvas.getByRole('alert')).toHaveTextContent('SpecialError');
    });
  },
};
