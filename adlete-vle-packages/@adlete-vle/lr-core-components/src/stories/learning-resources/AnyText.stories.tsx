import { expect } from '@storybook/jest';
import type { Meta, StoryObj } from '@storybook/react';
import { within } from '@storybook/testing-library';

import { AnyText } from '../../learning-resources/AnyText';
import { getFirstElementByClassNameAsHTML } from '../shared/test-utils';
import { createTTSDecorator } from '../shared/tts-setup';

import { MULTI_LINGUAL_HINT_LR } from './example-learning-resources';

const TTS_DECORATOR = await createTTSDecorator();

const meta: Meta<typeof AnyText> = {
  title: 'learning-resources/AnyText',
  component: AnyText,
  decorators: [TTS_DECORATOR],
};

// eslint-disable-next-line import/no-default-export
export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    text: MULTI_LINGUAL_HINT_LR,
  },
  play: async ({ canvasElement, step }) => {
    const canvas = within(canvasElement);
    const contentElement = getFirstElementByClassNameAsHTML(canvasElement, 'learning-resource-card-content');

    await step('shows the localizedName of the LR meta-data as the label', async () => {
      await expect(canvas.getByRole('heading')).toHaveTextContent(MULTI_LINGUAL_HINT_LR.meta.localizedName.en);
    });

    await step('shows the textual content', async () => {
      await expect(contentElement).toHaveTextContent(MULTI_LINGUAL_HINT_LR.content.en);
    });

    await step('has a TTS Button', async () => {
      expect(canvas.getByRole('button', { name: 'tts-button' })).toBeInTheDocument();
    });
  },
};
