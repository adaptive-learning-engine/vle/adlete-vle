import { expect } from '@storybook/jest';
import type { Meta, StoryObj } from '@storybook/react';
import { within } from '@storybook/testing-library';

import { IFrame } from '../../learning-resources/IFrame';
import { createTTSDecorator } from '../shared/tts-setup';

import { IFRAME_LR } from './example-learning-resources';

const TTS_DECORATOR = await createTTSDecorator();

const meta: Meta<typeof IFrame> = {
  title: 'learning-resources/IFrame',
  component: IFrame,
  decorators: [TTS_DECORATOR],
};

// eslint-disable-next-line import/no-default-export
export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    iframe: IFRAME_LR,
  },
  play: async ({ canvasElement, step }) => {
    const canvas = within(canvasElement);

    await step('shows the localizedName of the LR meta-data as the label', async () => {
      await expect(canvas.getByRole('heading')).toHaveTextContent(IFRAME_LR.meta.localizedName.en);
    });

    await step('shows the linked document', async () => {
      await expect(canvas.getByRole('document', { name: IFRAME_LR.meta.localizedName.en })).toBeInTheDocument();
    });
  },
};
