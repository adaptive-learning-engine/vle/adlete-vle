import { expect } from '@storybook/jest';
import type { Meta, StoryObj } from '@storybook/react';
import { within } from '@storybook/testing-library';

import { ToDo } from '../../learning-resources/ToDo';
import { getFirstElementByClassNameAsHTML } from '../shared/test-utils';
import { createTTSDecorator } from '../shared/tts-setup';

import { TODO_LR } from './example-learning-resources';

const TTS_DECORATOR = await createTTSDecorator();

const meta: Meta<typeof ToDo> = {
  title: 'learning-resources/ToDo',
  component: ToDo,
  decorators: [TTS_DECORATOR],
};

// eslint-disable-next-line import/no-default-export
export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    todo: TODO_LR,
  },
  play: async ({ canvasElement, step }) => {
    const canvas = within(canvasElement);
    const contentElement = getFirstElementByClassNameAsHTML(canvasElement, 'learning-resource-card-content');
    const content = within(contentElement);

    await step("shows the word 'ToDo' in the label", async () => {
      await expect(canvas.getByRole('heading')).toHaveTextContent('ToDo');
    });

    await step('shows the textual content', async () => {
      await expect(contentElement).toHaveTextContent(TODO_LR.content);
    });

    await step('does use an alert', async () => {
      await expect(content.getByRole('alert')).toBeInTheDocument();
    });
  },
};
