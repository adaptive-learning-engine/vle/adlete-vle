import { expect } from '@storybook/jest';
import type { Meta, StoryObj } from '@storybook/react';
import { within } from '@storybook/testing-library';

import { Hint } from '../../learning-resources/Hint';
import { getFirstElementByClassNameAsHTML } from '../shared/test-utils';
import { createTTSDecorator } from '../shared/tts-setup';

import { MULTI_LINGUAL_HINT_LR } from './example-learning-resources';

const TTS_DECORATOR = await createTTSDecorator();

const meta: Meta<typeof Hint> = {
  title: 'learning-resources/Hint',
  component: Hint,
  decorators: [TTS_DECORATOR],
};

// eslint-disable-next-line import/no-default-export
export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    hint: MULTI_LINGUAL_HINT_LR,
  },
  play: async ({ canvasElement, step }) => {
    const canvas = within(canvasElement);
    const contentElement = getFirstElementByClassNameAsHTML(canvasElement, 'learning-resource-card-content');
    const content = within(contentElement);

    await step("shows the word 'Hint' in the label", async () => {
      await expect(canvas.getByRole('heading')).toHaveTextContent('Hint:');
    });

    await step('shows the textual content', async () => {
      await expect(contentElement).toHaveTextContent(MULTI_LINGUAL_HINT_LR.content.en);
    });

    await step('does not use an alert for the severity', async () => {
      await expect(content.queryByRole('alert')).not.toBeInTheDocument();
    });

    await step('has a TTS Button', async () => {
      expect(canvas.getByRole('button', { name: 'tts-button' })).toBeInTheDocument();
    });
  },
};

export const Severity: Story = {
  args: {
    hint: MULTI_LINGUAL_HINT_LR,
    severity: 'info',
  },
  play: async ({ canvasElement, step }) => {
    const content = within(getFirstElementByClassNameAsHTML(canvasElement, 'learning-resource-card-content'));
    await step('uses an alert for the severity', async () => {
      await expect(content.getByRole('alert')).toBeInTheDocument();
    });
  },
};
