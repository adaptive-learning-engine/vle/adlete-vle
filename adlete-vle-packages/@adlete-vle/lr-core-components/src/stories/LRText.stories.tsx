import { expect } from '@storybook/jest';
import type { Meta, StoryObj } from '@storybook/react';
import { within } from '@storybook/testing-library';

import { LRText } from '../LRText';

import { PLAIN_EXAMPLE_LR_1, PLAIN_TEXT_CONTENT_1 } from './learning-resources/example-learning-resources';
import { createTTSDecorator } from './shared/tts-setup';

const TTS_DECORATOR = await createTTSDecorator();

const meta: Meta<typeof LRText> = {
  title: 'raw-learning-resources/LRText',
  component: LRText,
  decorators: [TTS_DECORATOR],
};

// eslint-disable-next-line import/no-default-export
export default meta;
type Story = StoryObj<typeof meta>;

export const Default: Story = {
  args: {
    text: PLAIN_EXAMPLE_LR_1,
  },
  play: async ({ canvasElement, step }) => {
    const canvas = within(canvasElement);

    await step('renders the text', async () => {
      expect(canvas.getByText(PLAIN_TEXT_CONTENT_1)).toBeInTheDocument();
    });

    await step('has a TTS Button', async () => {
      expect(canvas.getByRole('button', { name: 'tts-button' })).toBeInTheDocument();
    });
  },
};

export const NoTTSButton: Story = {
  args: {
    text: PLAIN_EXAMPLE_LR_1,
    enableTTS: false,
  },
  play: async ({ canvasElement, step }) => {
    const canvas = within(canvasElement);

    await step('does not have a TTS Button', async () => {
      expect(canvas.queryByRole('button', { name: 'tts-button' })).not.toBeInTheDocument();
    });
  },
};
