import React, { useEffect, useRef } from 'react';

import { XAPIManagerEvent } from '@adlete-vle/lr-core/xapi';
import { useForceUpdate } from '@adlete-vle/utils/hooks';

import { useXAPIManager } from '../contexts';

export function XAPIStatementHistory(): React.ReactElement {
  const xapiMan = useXAPIManager();
  const xapiEventsRef = useRef<XAPIManagerEvent[]>([]);
  const forceUpdate = useForceUpdate();

  useEffect(() => {
    const listenerSent = (e: XAPIManagerEvent) => {
      xapiEventsRef.current.push(e);
      forceUpdate();
    };

    xapiMan.events.on('statementsSent', listenerSent);

    const listenerSentError = (e: XAPIManagerEvent) => {
      xapiEventsRef.current.push(e);
      forceUpdate();
    };
    xapiMan.events.on('statementsSentError', listenerSentError);

    return () => {
      xapiMan.events.off('statementsSent', listenerSent);
      xapiMan.events.off('statementsSentError', listenerSentError);
    };
  }, [xapiMan, xapiEventsRef, forceUpdate]);

  return (
    <div>
      {xapiEventsRef.current.map((event, i) => (
        // eslint-disable-next-line react/no-array-index-key
        <div key={i}>{event.type}</div>
      ))}
    </div>
  );
}
