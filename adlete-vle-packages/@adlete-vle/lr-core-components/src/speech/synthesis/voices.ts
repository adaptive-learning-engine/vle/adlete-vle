import type { ISpeechSynthesisVoiceEx } from './functions';

export const VOICE_INFOS: Record<string, Partial<ISpeechSynthesisVoiceEx>> = {
  // update with other data mapping

  // Chrome General:
  'Google Deutsch': { gender: 'female', priority: 5 },
  'Google US English': { gender: 'female', priority: 5 },
  'Google UK English Female': { gender: 'female', priority: 5 },
  'Google UK Male': { gender: 'male', priority: 4 },

  // Windows

  // Apple + Chrome:
  Anna: { gender: 'female', priority: 1 },
  'Sandy (Deutsch (Deutschland))': { gender: 'female', priority: 0 },
  'Shelley (Deutsch (Deutschland))': { gender: 'female', priority: 0 },
  'Grandma (Deutsch (Deutschland))': { gender: 'female', priority: 0 },

  'Eddy (Deutsch (Deutschland))': { gender: 'male', priority: 1 },
  'Flo (Deutsch (Deutschland))': { gender: 'male', priority: 0 },
  'Reed (Deutsch (Deutschland))': { gender: 'male', priority: 0 },
  'Grandpa (Deutsch (Deutschland))': { gender: 'male', priority: 0 },
  'Rocko (Deutsch (Deutschland))': { gender: 'male', priority: 0 },

  // Apple + Safari:
  Sandy: { gender: 'female', priority: 0 },
  Shelley: { gender: 'female', priority: 0 },
  Grandma: { gender: 'female', priority: 0 },

  Eddy: { gender: 'male', priority: 1 },
  Flo: { gender: 'male', priority: 0 },
  Reed: { gender: 'male', priority: 0 },
  Grandpa: { gender: 'male', priority: 0 },
  Rocko: { gender: 'male', priority: 0 },

  // Microsoft Chrome:
  'Microsoft Hedda - German (Germany)': { gender: 'female', priority: 1 },
  'Microsoft Katja - German (Germany)': { gender: 'female', priority: 0 },
  'Microsoft Zira - English (United States)': { gender: 'female', priority: 2 },
  'Microsoft Hazel - English (United Kingdom)': { gender: 'female', priority: 2 },
  'Microsoft Susan - English (United Kingdom)': { gender: 'female', priority: 1 },

  'Microsoft Stefan - German (Germany)': { gender: 'male', priority: 1 },
  'Microsoft David - English (United States)': { gender: 'male', priority: 1 },
  'Microsoft Mark - English (United States)': { gender: 'male', priority: 0 },
  'Microsoft George - English (United Kingdom)': { gender: 'male', priority: 1 },

  // MS Firefox:
  'Microsoft Hedda Desktop - German': { gender: 'female', priority: 0 },
};
