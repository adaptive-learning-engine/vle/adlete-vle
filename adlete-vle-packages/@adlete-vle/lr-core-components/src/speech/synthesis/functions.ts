import { VOICE_INFOS } from './voices';

export type Gender = 'female' | 'male' | 'unknown';
export enum Genders {
  female = 'female',
  male = 'male',
  unknown = 'unknown',
}

export interface ISpeechSynthesisVoiceEx {
  voice: SpeechSynthesisVoice;
  gender: Gender;
  priority: number;
}

export function getVoices(): ISpeechSynthesisVoiceEx[] {
  const voices = window.speechSynthesis.getVoices();
  const voicesOut = voices.map((voice) => {
    let newVoice: ISpeechSynthesisVoiceEx;
    if (VOICE_INFOS[voice.name] === undefined) {
      newVoice = {
        voice,
        gender: 'unknown',
        priority: 0,
      };
    } else {
      newVoice = {
        voice,
        gender: VOICE_INFOS[voice.name].gender,
        priority: VOICE_INFOS[voice.name].priority,
      };
    }
    return newVoice;
  });
  return voicesOut;
}

export interface ISpeechSynthesisOptions {
  voice?: ISpeechSynthesisVoiceEx;
  pitch?: number;
  rate?: number;
  volume?: number;
}

export function createSpeechRequest(text: string, lang: string, options?: ISpeechSynthesisOptions): SpeechSynthesisUtterance {
  const request = new SpeechSynthesisUtterance(text);
  request.lang = lang;
  if (options?.voice) request.voice = options.voice.voice;
  if (options?.pitch) request.pitch = options.pitch;
  if (options?.rate) request.rate = options.rate;
  if (options?.volume) request.volume = options.volume;
  return request;
}

export function speakText(text: string, lang: string, options?: ISpeechSynthesisOptions): void {
  const request = createSpeechRequest(text, lang, options);
  window.speechSynthesis.speak(request);
}

export function speakRequest(request: SpeechSynthesisUtterance): void {
  window.speechSynthesis.speak(request);
}
