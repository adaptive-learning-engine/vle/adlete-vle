import React from 'react';

import { TTSManager } from './TTSManager';

export const TTSContext = React.createContext<TTSManager>(null);
