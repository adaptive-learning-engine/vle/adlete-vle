import MonitorHeartIcon from '@mui/icons-material/MonitorHeart';
import ReplayCircleFilledIcon from '@mui/icons-material/ReplayCircleFilled';
import SpeedIcon from '@mui/icons-material/Speed';
import VolumeUp from '@mui/icons-material/VolumeUp';
import {
  Box,
  Button,
  FormControl,
  IconButton,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
  Slider,
  Stack,
  TextField,
  Tooltip,
  Typography,
} from '@mui/material';
import React, { useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { useStatefulEvent } from '@adlete-utils/events-react';
import { useResettableState } from '@adlete-vle/utils/hooks';

import { useLocaleManager } from '../../contexts';

import { Gender, ISpeechSynthesisOptions, ISpeechSynthesisVoiceEx, speakText } from './functions';
import { useTTSManager } from './hooks';
import { VOICE_INFOS } from './voices';

function valueToText(value: number) {
  return `${value}`;
}

const VOICE_BY_GENDER: Record<Gender, Set<string>> = { female: new Set(), male: new Set(), unknown: new Set() };

Object.entries(VOICE_INFOS).forEach(([name, info]) => {
  VOICE_BY_GENDER[info.gender].add(name);
});

// TODO: as props
const RATE_MIN = 0;
const RATE_MAX = 2;
const RATE_STEP = 0.05;

const PITCH_MIN = 0;
const PITCH_MAX = 2;
const PITCH_STEP = 0.1;

const VOLUME_MIN = 0;
const VOLUME_MAX = 2;
const VOLUME_STEP = 0.1;

export function TTSSettings(): React.ReactElement {
  const { t } = useTranslation('tts-settings');
  const localeManager = useLocaleManager();

  const ttsMan = useTTSManager();
  const voices = useStatefulEvent(ttsMan.events, 'voices');
  const presets = useStatefulEvent(ttsMan.events, 'presets');
  const selectedPresets = useStatefulEvent(ttsMan.events, 'selectedPresets');

  const [language, setLanguage] = useState<string>(localeManager.events.getState('language'));
  const [exampleText, setExampleText] = useResettableState(t('example-text', { lng: language }), [t, language]);
  const selectedPresetId = selectedPresets[language];
  const [voiceName, setVoice] = useResettableState(() => {
    let retVoice: ISpeechSynthesisVoiceEx = voices[0];
    retVoice = voices.find(({ voice }) => voice.name === presets[language][selectedPresetId].voiceName);
    if (retVoice === undefined) {
      retVoice = voices.find((aVoice) => aVoice.voice.lang === language);
    }
    return retVoice;
  }, [voices, language, selectedPresetId]);
  const [gender, setGender] = useResettableState(() => {
    if (voiceName !== undefined) {
      return voiceName.gender;
    }
    return 'unknown';
  }, [voiceName]);

  const [rate, setRate] = useResettableState(() => presets[language][selectedPresetId].rate, [selectedPresetId, language]);
  const [pitch, setPitch] = useResettableState(() => presets[language][selectedPresetId].pitch, [selectedPresetId, language]);
  const [volume, setVolume] = useResettableState(() => presets[language][selectedPresetId].volume, [selectedPresetId, language]);

  const onVoiceChanged = useCallback(
    (event: SelectChangeEvent) => {
      const returnVoice: ISpeechSynthesisVoiceEx = voices.find((aVoice) => aVoice.voice.name === event.target.value);
      setVoice(returnVoice);
    },
    [setVoice, voices]
  );

  const onLanguageChanged = useCallback(
    (event: SelectChangeEvent) => {
      setLanguage(event.target.value);
      const returnVoice: ISpeechSynthesisVoiceEx = voices.find(
        (aVoice) => aVoice.gender === gender && aVoice.voice.lang === event.target.value
      );
      setVoice(returnVoice);
    },
    [gender, setVoice, voices]
  );

  const onGenderChanged = useCallback(
    (event: SelectChangeEvent) => {
      setGender(event.target.value as Gender);
      const returnVoice: ISpeechSynthesisVoiceEx = voices.find(
        (aVoice) => aVoice.voice.lang === language && aVoice.gender === event.target.value
      );
      setVoice(returnVoice);
    },
    [language, setGender, setVoice, voices]
  );

  const onRateChanged = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setRate(Number(event.target.value));
    },
    [setRate]
  );

  const onRateChangedSlider = useCallback(
    (event: Event, newValue: number) => {
      setRate(newValue);
    },
    [setRate]
  );

  const onVolumeChanged = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setVolume(Number(event.target.value));
    },
    [setVolume]
  );

  const onVolumeChangedSlider = useCallback(
    (event: Event, newValue: number) => {
      setVolume(newValue);
    },
    [setVolume]
  );

  const onPitchChanged = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setPitch(Number(event.target.value));
    },
    [setPitch]
  );

  const onPitchChangedSlider = useCallback(
    (event: Event, newValue: number) => {
      setPitch(newValue);
    },
    [setPitch]
  );

  const onExampleTextChanged = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      setExampleText(event.target.value);
    },
    [setExampleText]
  );

  const savePresets = useCallback(() => {
    presets[language][selectedPresetId].voiceName = voiceName.voice.name;
    presets[language][selectedPresetId].pitch = pitch;
    presets[language][selectedPresetId].volume = volume;
    presets[language][selectedPresetId].rate = rate;
    ttsMan.updatePresets({ ...presets });
  }, [voiceName, pitch, volume, rate, presets, language, selectedPresetId, ttsMan]);

  const voiceOptions = voices
    .filter((aVoice) => aVoice.voice.lang === language)
    .filter((aVoice) => VOICE_BY_GENDER[gender].has(aVoice.voice.name))
    .map((aVoice) => (
      <MenuItem key={aVoice.voice.name} value={aVoice.voice.name}>
        {aVoice.voice.name}
      </MenuItem>
    ));

  // Adding the voices into their respecting gender set
  voices.forEach((aVoice) => {
    VOICE_BY_GENDER[aVoice.gender].add(aVoice.voice.name);
  });

  const hasMaleVoices: boolean =
    voices.filter((aVoice) => aVoice.voice.lang === language && VOICE_BY_GENDER.male.has(aVoice.voice.name)).length <= 0;
  const hasFemaleVoices: boolean =
    voices.filter((aVoice) => aVoice.voice.lang === language && VOICE_BY_GENDER.female.has(aVoice.voice.name)).length <= 0;
  const hasUnknownVoices: boolean =
    voices.filter((aVoice) => aVoice.voice.lang === language && VOICE_BY_GENDER.unknown.has(aVoice.voice.name)).length <= 0;

  const isDE: boolean = voices.filter((aVoice) => aVoice.voice.lang === 'de-DE').length <= 0;
  const isUS: boolean = voices.filter((aVoice) => aVoice.voice.lang === 'en-US').length <= 0;
  const isUK: boolean = voices.filter((aVoice) => aVoice.voice.lang === 'en-UK').length <= 0;

  const readText = useCallback(() => {
    const options: ISpeechSynthesisOptions = {
      pitch,
      rate,
      voice: voiceName,
      volume,
    };
    speakText(exampleText, language, options);
  }, [pitch, rate, volume, voiceName, exampleText, language]);

  const resetSlider = useCallback(() => {
    setRate(presets[language][selectedPresetId].rate);
    setPitch(presets[language][selectedPresetId].pitch);
    setVolume(presets[language][selectedPresetId].volume);
  }, [setRate, setPitch, setVolume, presets, language, selectedPresetId]);

  const resetText = useCallback(() => {
    setExampleText(t('example-text'));
  }, [t, setExampleText]);

  return (
    <Stack spacing={5} sx={{ scale: '95%' }}>
      <Box sx={{ border: '1px solid #ccc', borderRadius: '4px', padding: '10px' }}>
        <FormControl sx={{ width: '100%', alignSelf: 'center', padding: '10px' }}>
          <InputLabel id="language">{t('language')}</InputLabel>
          <Select onChange={onLanguageChanged} label="language" value={language} id="language">
            <MenuItem key="en-US" value="en-US" disabled={isUS}>
              en-US
            </MenuItem>
            <MenuItem key="en-GB" value="en-GB" disabled={isUK}>
              en-GB
            </MenuItem>
            <MenuItem key="de-DE" value="de-DE" disabled={isDE}>
              de-DE
            </MenuItem>
          </Select>
        </FormControl>
        <FormControl sx={{ width: '100%', alignSelf: 'center', padding: '10px' }}>
          <InputLabel id="gender">{t('gender')}</InputLabel>
          <Select onChange={onGenderChanged} value={gender} label="gender" id="gender">
            <MenuItem key="male" value="male" disabled={hasMaleVoices}>
              {t('male')}
            </MenuItem>
            <MenuItem key="female" value="female" disabled={hasFemaleVoices}>
              {t('female')}
            </MenuItem>
            <MenuItem key="unknown" value="unknown" disabled={hasUnknownVoices}>
              {t('unknown')}
            </MenuItem>
          </Select>
        </FormControl>
        <FormControl sx={{ width: '100%', alignSelf: 'center', padding: '10px' }}>
          <InputLabel id="voice">{t('voice')}</InputLabel>
          <Select onChange={onVoiceChanged} value={voiceName ? voiceName.voice.name : ''} label="voice" id="voice">
            {voiceOptions}
          </Select>
        </FormControl>
      </Box>
      <Box sx={{ border: '1px solid #ccc', borderRadius: '4px', padding: '10px' }}>
        <Box>
          <Box sx={{ alignSelf: 'flex-start' }}>
            <Tooltip title={t('resetSlider')}>
              <IconButton about={t('resetSlider')} onClick={resetSlider}>
                <ReplayCircleFilledIcon />
              </IconButton>
            </Tooltip>
          </Box>
          <Typography id="rate" gutterBottom align="center">
            {t('rate')}
          </Typography>
          <Stack direction="row" spacing={2} alignItems="center">
            <SpeedIcon />
            <Slider
              aria-labelledby="rate"
              value={rate}
              min={RATE_MIN}
              max={RATE_MAX}
              step={RATE_STEP}
              onChange={onRateChangedSlider}
              getAriaValueText={valueToText}
              valueLabelDisplay="auto"
            />
            <TextField
              value={rate}
              size="small"
              onChange={onRateChanged}
              inputProps={{
                min: RATE_MIN,
                max: RATE_MAX,
                step: RATE_STEP,
                type: 'number',
              }}
            />
          </Stack>
        </Box>
        <Box>
          <Typography id="pitch" gutterBottom align="center">
            {t('pitch')}
          </Typography>
          <Stack direction="row" spacing={2} alignItems="center">
            <MonitorHeartIcon />
            <Slider
              aria-labelledby="pitch"
              value={pitch}
              min={PITCH_MIN}
              max={PITCH_MAX}
              step={PITCH_STEP}
              onChange={onPitchChangedSlider}
              getAriaValueText={valueToText}
              valueLabelDisplay="auto"
            />
            <TextField
              value={pitch}
              size="small"
              onChange={onPitchChanged}
              inputProps={{
                min: PITCH_MIN,
                max: PITCH_MAX,
                step: PITCH_STEP,
                type: 'number',
              }}
            />
          </Stack>
        </Box>
        <Box>
          <Typography id="volume" gutterBottom align="center">
            {t('volume')}
          </Typography>
          <Stack direction="row" spacing={2} alignItems="center">
            <VolumeUp />
            <Slider
              aria-labelledby="volume"
              value={volume}
              min={VOLUME_MIN}
              max={VOLUME_MAX}
              step={VOLUME_STEP}
              onChange={onVolumeChangedSlider}
              getAriaValueText={valueToText}
              valueLabelDisplay="auto"
            />
            <TextField
              value={volume}
              size="small"
              onChange={onVolumeChanged}
              inputProps={{
                min: VOLUME_MIN,
                max: VOLUME_MAX,
                step: VOLUME_STEP,
                type: 'number',
              }}
            />
          </Stack>
        </Box>
      </Box>
      <Stack sx={{ border: '1px solid #ccc', borderRadius: '4px', padding: '10px' }} spacing={2}>
        <Stack direction="row" spacing={2} alignContent="flex-start" justifyContent="flex-start">
          <TextField id="example-text" value={exampleText} multiline onChange={onExampleTextChanged} fullWidth rows={3} />
          <Tooltip title={t('resetText')}>
            <IconButton about={t('resetText')} onClick={resetText}>
              <ReplayCircleFilledIcon />
            </IconButton>
          </Tooltip>
        </Stack>
        <Stack direction="row" justifyContent="space-around" spacing={2}>
          <Button variant="contained" color="primary" onClick={readText}>
            {t('read')}
          </Button>
        </Stack>
      </Stack>
      <Button variant="contained" color="secondary" onClick={savePresets}>
        {t('save')}
      </Button>
    </Stack>
  );
}
