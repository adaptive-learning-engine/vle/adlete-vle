import { IStatefulEventEmitterNoEmit, StatefulEventEmitter } from '@adlete-utils/events';
import { LocaleManager } from '@adlete-vle/lr-core/locale';

import { IStorage } from '@fki/editor-core/storage';

import { createSpeechRequest, getVoices, ISpeechSynthesisOptions, ISpeechSynthesisVoiceEx, speakRequest } from './functions';

export interface ITTSPreset {
  voiceName: string;
  pitch: number;
  rate: number;
  volume: number;
}

export type LanguagePresets = Record<string, Record<string, ITTSPreset>>;

interface IStorageData {
  presets: LanguagePresets; // { "de": { "default": { voice: ..., pitch ... } } }
  selectedPresets: Record<string, string>; // { "de": "default" }
}

interface ITTSEventData extends IStorageData {
  voices: ISpeechSynthesisVoiceEx[]; // coming from the browser (all w/o language filter)
}

const STORAGE_KEY = 'tts-presets';
const DEFAULT_DATA: IStorageData = {
  presets: {
    'de-DE': { default: { voiceName: 'Google Deutsch', pitch: 1, rate: 1, volume: 1 } },
    'en-US': { default: { voiceName: 'Google US English', pitch: 1, rate: 1, volume: 1 } },
    'en-GB': { default: { voiceName: 'Google UK English', pitch: 1, rate: 1, volume: 1 } },
  },
  selectedPresets: { 'de-DE': 'default', 'en-US': 'default', 'en-GB': 'default' },
};

export class TTSManager {
  protected _events: StatefulEventEmitter<ITTSEventData>;

  protected storage: IStorage;

  protected data: IStorageData;

  protected _chromeBugTimer: ReturnType<typeof setTimeout>;

  protected localeManager: LocaleManager;

  public get events(): IStatefulEventEmitterNoEmit<ITTSEventData> {
    return this._events;
  }

  public get voices(): ISpeechSynthesisVoiceEx[] {
    return this._events.getState('voices');
  }

  public get presets(): LanguagePresets {
    return this._events.getState('presets');
  }

  public get selectedPresets(): Record<string, string> {
    return this._events.getState('selectedPresets');
  }

  protected get synthesisOptions(): ISpeechSynthesisOptions {
    let retVoice: ISpeechSynthesisVoiceEx;
    const lang: string = this.localeManager.language;
    const selectedPreset = this.selectedPresets[lang];
    retVoice = this.voices.find((aVoice) => aVoice.voice.name === this.presets[lang][selectedPreset].voiceName);
    // TODO: refactor this
    if (!retVoice) {
      retVoice = this.voices.find((aVoice) => aVoice.voice.lang === lang);
    }
    if (!retVoice) {
      if (!this.localeManager.fallbackLanguage) {
        retVoice = this.voices[0];
      } else {
        this.voices.forEach((aVoice) => {
          if (aVoice.voice.lang === this.localeManager.fallbackLanguage) {
            retVoice = aVoice;
          }
        });
      }
    }
    return {
      voice: retVoice,
      pitch: this.presets[lang][selectedPreset].pitch,
      rate: this.presets[lang][selectedPreset].rate,
      volume: this.presets[lang][selectedPreset].volume,
    };
  }

  public constructor(localeManager: LocaleManager, storage: IStorage) {
    this.localeManager = localeManager;
    this.storage = storage;
  }

  protected onVoicesChanged(): void {}

  public getVoices(): ISpeechSynthesisVoiceEx[] {
    return getVoices().sort((a, b) => b.priority - a.priority);
  }

  public createSpeechRequest(text: string, lang: string, options?: ISpeechSynthesisOptions): SpeechSynthesisUtterance {
    const mergedOptions = { ...this.synthesisOptions, ...options };
    return createSpeechRequest(text, lang, mergedOptions);
  }

  public stop(): void {
    window.speechSynthesis.cancel();
  }

  public speakRequest(request: SpeechSynthesisUtterance): void {
    this.stop();

    // TODO: use browser detection library
    if (!navigator.userAgent.includes('Firefox')) {
      const timer = () => {
        window.speechSynthesis.pause();
        window.speechSynthesis.resume();
        this._chromeBugTimer = setTimeout(timer, 10000);
      };

      this._chromeBugTimer = setTimeout(timer, 10000);

      request.addEventListener(
        'end',
        () => {
          clearTimeout(this._chromeBugTimer);
        },
        { once: true }
      );
    }

    speakRequest(request);
  }

  protected async getStorageData(): Promise<IStorageData> {
    const data = await this.storage.getItem<IStorageData>(STORAGE_KEY);
    if (data) return data;
    return DEFAULT_DATA;
  }

  public async init(): Promise<void> {
    this.data = await this.getStorageData();

    const voices = this.getVoices();

    const { presets, selectedPresets } = this.data;

    this._events = new StatefulEventEmitter<ITTSEventData>({
      presets,
      selectedPresets,
      voices,
    });
    this._events.setMaxListeners(100);

    window.speechSynthesis.addEventListener('voiceschanged', () => {
      this._events.emit('voices', [...this.getVoices()]);
    });
  }

  public updatePresets(presets: LanguagePresets): void {
    if (presets === this.data.presets) return;

    this.data.presets = presets;
    this.storage.setItem(STORAGE_KEY, this.data);
    this._events.emit('presets', this.data.presets);
  }
}
