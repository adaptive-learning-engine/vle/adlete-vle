import { useContext } from 'react';

import { TTSManager } from './TTSManager';
import { TTSContext } from './context';

export function useTTSManager(): TTSManager {
  const ttsManager = useContext(TTSContext);
  if (!ttsManager) throw new Error('TTSManager not found. Did you forget to use the TTSContext?');
  return ttsManager;
}
