import { LocaleManager } from '@adlete-vle/lr-core/locale';

import { IStorage } from '@fki/editor-core/storage';

import { TTSManager } from './TTSManager';

export * from './context';
export * from './functions';
export * from './TTSManager';
export * from './TTSSettings';

export async function createTTSManager(localeManager: LocaleManager, storage: IStorage): Promise<TTSManager> {
  const ttsMan = new TTSManager(localeManager, storage);
  await ttsMan.init();
  return ttsMan;
}
