import type { i18n, KeyPrefix, Namespace, TypeOptions } from 'i18next';
import type { UseTranslationOptions, UseTranslationResponse } from 'react-i18next';
import { useTranslation } from 'react-i18next';

type DefaultNamespace = TypeOptions['defaultNS'];

export function useOptionalTranslation<N extends Namespace = DefaultNamespace, TKPrefix extends KeyPrefix<N> = undefined>(
  i18n?: i18n,
  ns?: N | Readonly<N>,
  options?: UseTranslationOptions<TKPrefix>
): UseTranslationResponse<N, TKPrefix> {
  if (i18n) options.i18n = i18n;

  return useTranslation(ns, options);
}
