const messages = {
  'learning-resources': {
    'definition-of': 'Definition of {concept}',
    'definition-and-examples-of': 'Definition and examples of {concept}',
    'equation-of': 'Equation of {concept}',
    'examples-for': 'Examples for {concept}',
    'example-problems-for': 'Examplary problems for {concept}',
    'example-problems-for-intro': '{concept} can help us solve problems such as:',
    hint: 'Hint',
    'notation-of': 'Notation of {concept}',
    next: 'Next',
    previous: 'Previous',
    'show-content': 'Show Content',
    video: 'Video',
  },
  'tts-settings': {
    'example-text': 'This is an example text to read out loud',
    language: 'Language',
    gender: 'Gender',
    pitch: 'Pitch',
    rate: 'Rate',
    volume: 'Volume',
    voice: 'Voice',
    read: 'Read',
    save: 'Save',
    male: 'male',
    female: 'female',
    unknown: 'unknown',
  },
};

// eslint-disable-next-line import/no-default-export
export default messages;
