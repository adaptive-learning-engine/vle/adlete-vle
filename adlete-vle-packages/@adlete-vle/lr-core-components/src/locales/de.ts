const messages = {
  'learning-resources': {
    'definition-of': 'Definition von {concept}',
    'definition-and-examples-of': 'Definition und Beispiele für {concept}',
    'equation-of': 'Formel für {concept}',
    'examples-for': 'Beispiele für {concept}',
    'example-problems-for': 'Beispielhafte Fragestellungen für {concept}',
    'example-problems-for-intro': '{concept} kann uns dabei helfen Fragestellungen zu beantworten wie:',
    hint: 'Hinweis',
    'notation-of': 'Notation für {concept}',
    next: 'Weiter',
    previous: 'Zurück',
    'show-content': 'Zeige Inhalte',
    video: 'Video',
  },
  'tts-settings': {
    'example-text': 'Dies ist ein Beispieltext zum Vorlesen',
    language: 'Sprache',
    gender: 'Geschlecht',
    pitch: 'Tonhöhe',
    rate: 'Geschwindigkeit',
    volume: 'Lautstärke',
    voice: 'Stimme',
    read: 'Vorlesen',
    save: 'Speichern',
    male: 'männlich',
    female: 'weiblich',
    unknown: 'unbekannt',
  },
};

// eslint-disable-next-line import/no-default-export
export default messages;
