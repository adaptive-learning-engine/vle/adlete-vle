import type { Consola } from 'consola';
// TODO: enable TS moduleResolution: NodeNext for typings to probably work
import { createConsola } from 'consola';

export const Logger: Consola = createConsola({});
