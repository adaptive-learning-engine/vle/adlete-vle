import XAPI, { Statement } from '@xapi/xapi';

import { addMissingIds } from '@adlete-semantics/xapi';
import { ITypedEventEmitter, ITypedEventEmitterNoEmit, TypedEventEmitter } from '@adlete-utils/events';
import type { ConstructorArg1 } from '@adlete-utils/typescript/helpers';

export interface XAPIManagerEvent {
  type: 'statements-sent' | 'statements-sent-error' | 'statements-received';
}

export interface XAPIManagerEventWithStatements {
  statements: Statement[];
}

export interface XAPIStatementsReceivedEvent extends XAPIManagerEventWithStatements {
  type: 'statements-received';
}

export interface XAPIStatementsSentEvent extends XAPIManagerEventWithStatements {
  type: 'statements-sent';
}

export interface XAPIStatementsSentError extends XAPIManagerEventWithStatements {
  type: 'statements-sent-error';
  error: Error;
}

export interface XAPIManagerEvents {
  statementsReceived: XAPIStatementsReceivedEvent;
  statementsSent: XAPIStatementsSentEvent;
  statementsSentError: XAPIStatementsSentError;
}

// XAPIConfig is not properly exported, so we infer it
type XAPIConfig = ConstructorArg1<typeof XAPI>;
export interface IXAPIManagerOptions extends XAPIConfig {}

export class XAPIManager {
  protected xapi: XAPI;

  protected _events: ITypedEventEmitter<XAPIManagerEvents>;

  public get events(): ITypedEventEmitterNoEmit<XAPIManagerEvents> {
    return this._events;
  }

  public get client(): XAPI {
    return this.xapi;
  }

  public constructor(options: IXAPIManagerOptions) {
    this._events = new TypedEventEmitter();
    this.xapi = new XAPI(options);
  }

  public async sendStatements(statements: Statement[]): Promise<string[]> {
    const statementWithIds = addMissingIds(statements);
    this._events.emit('statementsReceived', { type: 'statements-received', statements: statementWithIds });
    try {
      const response = await this.xapi.sendStatements({ statements: statementWithIds });
      this._events.emit('statementsSent', { type: 'statements-sent', statements: statementWithIds });
      return response.data;
    } catch (error) {
      this._events.emit('statementsSentError', { type: 'statements-sent-error', statements: statementWithIds, error });
      throw error;
    }
  }

  public async sendStatementsSafe(statements: Statement[]): Promise<string[]> {
    try {
      return await this.sendStatements(statements);
    } catch (error) {
      return null;
    }
  }

  public async sendStatement(statement: Statement): Promise<string> {
    return (await this.sendStatements([statement]))[0];
  }

  public async sendStatementSafe(statement: Statement): Promise<string> {
    const results = await this.sendStatementsSafe([statement]);
    if (results == null) return null;
    return results[0];
  }
}
