import i18next, { i18n, Module } from 'i18next';

import type { IStorage } from '@fki/editor-core/storage';

import { LocaleManager } from './LocaleManager';
import { ILanguageMap } from './language-map';

export async function createLocaleManager(storage: IStorage, modules?: Module[]): Promise<LocaleManager> {
  const localeMan = new LocaleManager(storage);
  await localeMan.init(modules);
  return localeMan;
}

export interface IWithI18n {
  i18n?: i18n;
}

export type ResourceBundleWithNS = Record<string, Record<string, string>>;
export type MultiLangResourceBundleWithNS = ILanguageMap<Record<string, Record<string, string>>>;

export function addResourceBundleWithNS(lang: string, bundle: ResourceBundleWithNS, i18nextInstance?: i18n): void {
  i18nextInstance = i18nextInstance || i18next;
  Object.entries(bundle).forEach(([ns, resources]) => i18nextInstance.addResourceBundle(lang, ns, resources));
}

export function addMultiLangResourceBundleWithNS(bundle: MultiLangResourceBundleWithNS, i18nextInstance?: i18n): void {
  i18nextInstance = i18nextInstance || i18next;
  Object.entries(bundle).forEach(([lang, langBundle]) => {
    addResourceBundleWithNS(lang, langBundle, i18nextInstance);
  });
}

export function getTranslationFromLangMap(map: ILanguageMap<string>, languages: readonly string[]): string {
  for (let i = 0; i < languages.length; ++i) {
    const lang = languages[i];
    if (lang in map) return map[lang];
  }

  throw new Error(`Could not find language(s) '${languages}' in language map!'`);
}

export function recordHasTranslation(record: Record<string, string>, languages: readonly string[]): boolean {
  for (let i = 0; i < languages.length; ++i) {
    const lang = languages[i];
    if (lang in record) return true;
  }

  return false;
}

export function injectI18n<T extends IWithI18n>(target: T, i18n: i18n): T {
  return target.i18n ? target : { ...target, i18n };
}
