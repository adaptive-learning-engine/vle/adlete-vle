import i18next from 'i18next';
import type { i18n, Module } from 'i18next';
import ICU from 'i18next-icu';

import { IStatefulEventEmitterNoEmit, StatefulEventEmitter } from '@adlete-utils/events';

import type { IStorage } from '@fki/editor-core/storage';

import { Logger } from '../logging';

const LOCALE_LOGGER = Logger.withTag('LocaleManager');

export interface IStorageData {
  language: string;
  fallbackLanguage: string;
}

export type ILocaleEventData = IStorageData;

const STORAGE_KEY = 'locale-manager-data';
const DEFAULT_DATA: IStorageData = {
  language: 'de-DE',
  fallbackLanguage: 'en-US',
};

export class ILocaleManagerOptions {
  createInstance: boolean;
}

export class LocaleManager {
  protected _i18n: i18n;

  protected storage: IStorage;

  protected data: IStorageData;

  protected options: ILocaleManagerOptions;

  protected _events: StatefulEventEmitter<ILocaleEventData>;

  public get events(): IStatefulEventEmitterNoEmit<ILocaleEventData> {
    return this._events;
  }

  public get fallbackLanguage(): string {
    return this._events.getState('fallbackLanguage');
  }

  public get language(): string {
    return this._events.getState('language');
  }

  public get i18n(): i18n {
    return this._i18n;
  }

  // add for language aswell

  public constructor(storage: IStorage, options?: ILocaleManagerOptions) {
    this.options = options ? { ...options } : { createInstance: false };
    this.storage = storage;
  }

  protected async getStorageData(): Promise<IStorageData> {
    const data = await this.storage.getItem<IStorageData>('locale-manager-data');
    if (data) return data;
    return { ...DEFAULT_DATA };
  }

  public async init(modules?: Module[]): Promise<void> {
    this.data = await this.getStorageData();
    this._i18n = await this.initI18n(modules, this.data.language, this.data.fallbackLanguage);
    this._events = new StatefulEventEmitter<ILocaleEventData>({
      language: this.data.language,
      fallbackLanguage: this.data.fallbackLanguage,
    });
    this._events.setMaxListeners(100);
  }

  public updateLanguage(language: string): void {
    if (language === this.data.language) return;

    // TODO: verify language in list of acceptable languages
    this.data.language = language;
    this.storage.setItem(STORAGE_KEY, this.data);
    this._events.emit('language', this.data.language);
    this._i18n.changeLanguage(this.data.language);
  }

  protected async initI18n(modules: Module[], lang: string, fallbackLang: string): Promise<i18n> {
    const i18nInstance = this.options.createInstance ? i18next.createInstance() : i18next;

    if (i18nInstance.isInitialized) {
      // TODO: update other i18nInstance settings appropriately
      await i18nInstance.changeLanguage(lang);
      LOCALE_LOGGER.warn('i18n has already been initialized!');
      return i18nInstance;
    }

    const allModules = modules ? [...modules, ICU] : [ICU];
    allModules.forEach((module) => i18nInstance.use(module));

    await i18nInstance.init({
      lng: lang, // if you're using a language detector, do not define the lng option
      debug: false,
      fallbackLng: fallbackLang,
      fallbackNS: ['translation'],
    });

    return i18nInstance;
  }
}
