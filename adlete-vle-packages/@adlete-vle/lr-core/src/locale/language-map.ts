export interface ILanguageMap<T> {
  aa?: T;
  ab?: T;
  ae?: T;
  af?: T;
  ak?: T;
  am?: T;
  an?: T;
  ar?: T;
  as?: T;
  av?: T;
  ay?: T;
  az?: T;
  ba?: T;
  be?: T;
  bg?: T;
  bi?: T;
  bm?: T;
  bn?: T;
  bo?: T;
  br?: T;
  bs?: T;
  ca?: T;
  ce?: T;
  ch?: T;
  co?: T;
  cr?: T;
  cs?: T;
  cu?: T;
  cv?: T;
  cy?: T;
  da?: T;
  de?: T;
  dv?: T;
  dz?: T;
  ee?: T;
  el?: T;
  en?: T;
  eo?: T;
  es?: T;
  et?: T;
  eu?: T;
  fa?: T;
  ff?: T;
  fi?: T;
  fj?: T;
  fo?: T;
  fr?: T;
  fy?: T;
  ga?: T;
  gd?: T;
  gl?: T;
  gn?: T;
  gu?: T;
  gv?: T;
  ha?: T;
  he?: T;
  hi?: T;
  ho?: T;
  hr?: T;
  ht?: T;
  hu?: T;
  hy?: T;
  hz?: T;
  ia?: T;
  id?: T;
  ie?: T;
  ig?: T;
  ii?: T;
  ik?: T;
  io?: T;
  is?: T;
  it?: T;
  iu?: T;
  ja?: T;
  jv?: T;
  ka?: T;
  kg?: T;
  ki?: T;
  kj?: T;
  kk?: T;
  kl?: T;
  km?: T;
  kn?: T;
  ko?: T;
  kr?: T;
  ks?: T;
  ku?: T;
  kv?: T;
  kw?: T;
  ky?: T;
  la?: T;
  lb?: T;
  lg?: T;
  li?: T;
  ln?: T;
  lo?: T;
  lt?: T;
  lu?: T;
  lv?: T;
  mg?: T;
  mh?: T;
  mi?: T;
  mk?: T;
  ml?: T;
  mn?: T;
  mr?: T;
  ms?: T;
  mt?: T;
  my?: T;
  na?: T;
  nb?: T;
  nd?: T;
  ne?: T;
  ng?: T;
  nl?: T;
  nn?: T;
  no?: T;
  nr?: T;
  nv?: T;
  ny?: T;
  oc?: T;
  oj?: T;
  om?: T;
  or?: T;
  os?: T;
  pa?: T;
  pi?: T;
  pl?: T;
  ps?: T;
  pt?: T;
  qu?: T;
  rm?: T;
  rn?: T;
  ro?: T;
  ru?: T;
  rw?: T;
  sa?: T;
  sc?: T;
  sd?: T;
  se?: T;
  sg?: T;
  si?: T;
  sk?: T;
  sl?: T;
  sm?: T;
  sn?: T;
  so?: T;
  sq?: T;
  sr?: T;
  ss?: T;
  st?: T;
  su?: T;
  sv?: T;
  sw?: T;
  ta?: T;
  te?: T;
  tg?: T;
  th?: T;
  ti?: T;
  tk?: T;
  tl?: T;
  tn?: T;
  to?: T;
  tr?: T;
  ts?: T;
  tt?: T;
  tw?: T;
  ty?: T;
  ug?: T;
  uk?: T;
  ur?: T;
  uz?: T;
  ve?: T;
  vi?: T;
  vo?: T;
  wa?: T;
  wo?: T;
  xh?: T;
  yi?: T;
  yo?: T;
  za?: T;
  zh?: T;
  zu?: T;
  [key: string]: T;
}

export function convertLanguageMapOfListToObjects<T>(map: ILanguageMap<T[]>): ILanguageMap<T>[] {
  const results: ILanguageMap<T>[] = [];
  Object.entries<T[]>(map).forEach(([key, items]) =>
    items.forEach((item, index) => {
      if (index >= results.length) {
        results[index] = {};
      }
      const language = key as keyof ILanguageMap<T>;
      results[index][language] = item;
    })
  );
  return results;
}

export type StringOrLanguageMap = string | ILanguageMap<string>;

export function getLabelForLanguage(stringOrMap: StringOrLanguageMap, lang: string, defaultLang: string): string {
  if (typeof stringOrMap === 'string') return stringOrMap;

  if (lang in stringOrMap) return stringOrMap[lang];
  if (defaultLang in stringOrMap) return stringOrMap[defaultLang];
  throw new Error(`Language map does not contain item for requested language ${lang} or default language ${defaultLang}`);
}
