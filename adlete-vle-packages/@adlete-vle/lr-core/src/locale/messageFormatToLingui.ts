// import { CompiledMessage } from '@lingui/core';
// import { parse } from 'messageformat-parser';

// export const isString = (s: any): s is string => typeof s === 'string';

// // [Tokens] -> (CTX -> String)
// function processTokens(tokens: any[]) {
//   if (!tokens.filter((token) => !isString(token)).length) {
//     return tokens.join('');
//   }

//   return tokens.map((token) => {
//     if (isString(token)) {
//       return token;

//       // # in plural case
//     }
//     if (token.type === 'octothorpe') {
//       return '#';

//       // simple argument
//     }
//     if (token.type === 'argument') {
//       return [token.arg];

//       // argument with custom format (date, number)
//     }
//     if (token.type === 'function') {
//       const _param = token.param && token.param.tokens[0];
//       const param = typeof _param === 'string' ? _param.trim() : _param;
//       return [token.arg, token.key, param].filter(Boolean);
//     }

//     // eslint-disable-next-line radix
//     const offset = token.offset ? parseInt(token.offset) : undefined;

//     // complex argument with cases
//     const formatProps = {};
//     token.cases.forEach((item) => {
//       formatProps[item.key] = processTokens(item.tokens);
//     });

//     return [
//       token.arg,
//       token.type,
//       {
//         offset,
//         ...formatProps,
//       },
//     ];
//   });
// }

// // Message -> (Params -> String)
// export function compile(message: string): CompiledMessage {
//   try {
//     return processTokens(parse(message));
//   } catch (e) {
//     console.error(`Message cannot be parsed due to syntax errors: ${message}`);
//     return message;
//   }
// }

// export function compileAll(messages: Record<string, string>): Record<string, CompiledMessage> {
//   const result: Record<string, CompiledMessage> = {};
//   Object.entries(messages).forEach(([key, value]) => {
//     result[key] = compile(value);
//   });
//   return result;
// }
