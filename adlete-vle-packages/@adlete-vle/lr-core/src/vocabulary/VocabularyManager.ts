import { getShortId } from '../learning-resources/ids';

import { IVocabularyManager } from './IVocabularyManager';
import { IJSkosConcept, IJSkosConceptScheme } from './types';

export class VocabularyManager implements IVocabularyManager {
  protected concepts: Record<string, IJSkosConcept> = {};

  protected idPrefixes: string[] = [];

  protected addConceptById(concept: IJSkosConcept, id: string): void {
    if (id in this.concepts) {
      throw new Error(`Concept '${id}' was already added!`);
    }

    this.concepts[id] = concept;
  }

  public addConcept(concept: IJSkosConcept): void {
    this.addConceptById(concept, concept.uri);

    const shortId = getShortId(concept.uri, this.idPrefixes);
    if (shortId !== '') this.addConceptById(concept, shortId);
  }

  public addConceptsFromScheme(scheme: IJSkosConceptScheme): void {
    scheme.concepts.forEach((concept) => this.addConcept(concept));
  }

  protected validateConcept(id: string): void {
    if (!(id in this.concepts)) throw new Error(`No concept found for '${id}'!`);
  }

  public getConcept(id: string): IJSkosConcept {
    this.validateConcept(id);
    return this.concepts[id];
  }

  public registerIdPrefix(prefix: string): void {
    this.idPrefixes.push(prefix);
  }
}
