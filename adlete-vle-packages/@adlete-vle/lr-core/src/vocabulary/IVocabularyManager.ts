import { IJSkosConcept, IJSkosConceptScheme } from './types';

export interface IVocabularyManager {
  addConcept(concept: IJSkosConcept): void;
  addConceptsFromScheme(scheme: IJSkosConceptScheme): void;
  getConcept(id: string): IJSkosConcept;
  registerIdPrefix(prefix: string): void;
}
