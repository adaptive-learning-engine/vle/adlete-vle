import { flatten } from 'ramda';

import type { ILearningResource } from '../learning-resources';
import { LR_CONTEXT_EN } from '../learning-resources/amb-metadata';
import { EncodingFormats } from '../learning-resources/encoding-formats';
import { convertLanguageMapOfListToObjects } from '../locale/language-map';

import { IJSkosConcept, IJSkosConceptScheme } from './types';

export function learningResourcesFromConcept(concept: IJSkosConcept): ILearningResource[] {
  const lrs: ILearningResource[][] = [];

  const encodingFormat = EncodingFormats.TextMultiLingualMarkdown; // TOOD: support other formats
  const about = { id: concept.uri, type: 'Concept', prefLabel: concept.prefLabel }; // TODO: inScheme

  if (concept.definition) {
    const multiLingualDefs = convertLanguageMapOfListToObjects(concept.definition);
    const defs = multiLingualDefs.map(
      (def, index) =>
        ({
          meta: {
            '@context': LR_CONTEXT_EN,
            id: `${concept.uri}&definition=${index + 1}`,
            name: `Definition of ${concept.prefLabel.en}`,
            type: ['LearningResource', 'DefinedTerm'],
            encoding: [{ encodingFormat }],
            about: [about],
          },
          content: def,
        }) as ILearningResource
    );
    lrs.push(defs);
  }

  if (concept.example) {
    const multiLingualExamples = convertLanguageMapOfListToObjects(concept.example);
    const examples = multiLingualExamples.map(
      (example, index) =>
        ({
          meta: {
            '@context': LR_CONTEXT_EN,
            id: `${concept.uri}&example=${index + 1}`,
            name: `Example of ${concept.prefLabel.en}`,
            type: ['LearningResource', 'Example'],
            encoding: [{ encodingFormat }],
            about: [about],
          },
          content: example,
        }) as ILearningResource
    );
    lrs.push(examples);
  }

  return flatten(lrs);
}

export function learningResourcesFromVocab(vocab: IJSkosConceptScheme): ILearningResource[] {
  return flatten(vocab.concepts.map(learningResourcesFromConcept));
}
