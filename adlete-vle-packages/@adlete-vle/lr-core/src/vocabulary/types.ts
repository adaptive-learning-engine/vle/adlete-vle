import { ILanguageMap } from '../locale/language-map';

export type JSkosList = (null | string)[];
export type JSkosSet<T extends IJSkosResource = IJSkosResource> = (null | T)[];

export type JSkosURI = string;
export type JSkosDate = string;

export interface IJSkosLocation {
  type: string;
}

export interface IJSkosAddress {
  // the street address
  street?: string;

  // the extended address (e.g., apartment or suite number)
  ext?: string;

  // the post office box
  pobox?: string;

  // the locality (e.g., city)
  locality?: string;

  // the region (e.g., state or province)
  region?: string;

  // the postal code
  code?: string;

  // the country name
  country?: string;
}

export interface IJSkosResource {
  // reference to a JSON-LD context] document
  '@context'?: JSkosURI;

  // primary globally unique identifier
  uri?: JSkosURI;

  // additional identifiers
  identifier?: JSkosList;

  // URIs of types
  type?: JSkosList;

  // date of creation
  created?: JSkosDate;

  // date of publication
  issued?: JSkosDate;

  // date of last modification
  modified?: JSkosDate;

  // agent primarily responsible for creation of resource
  creator?: JSkosSet;

  // agent responsible for making contributions to the resource
  contributor?: JSkosSet;

  // sources from which the described resource is derived
  source?: JSkosSet;

  // agent responsible for making the resource available
  publisher?: JSkosSet;

  // [resources] which this resource is part of (if no other field applies)
  partOf?: JSkosSet;

  // UNOFFICIAL: encoding formats necessary for specifying if resource requires special decoding
  encodingFormat?: string[];
}

export interface IJSkosItem extends IJSkosResource {
  // URL of a page with information about the item
  url?: JSkosURI;

  // list of notations
  notation?: JSkosList;

  // preferred labels, index by language
  prefLabel?: ILanguageMap<string>;

  // alternative labels, indexed by language
  altLabel?: ILanguageMap<JSkosList>;

  // hidden labels, indexed by language
  hiddenLabel?: ILanguageMap<JSkosList>;

  // see [SKOS Documentary Notes]
  scopeNote?: ILanguageMap<JSkosList>;

  // see [SKOS Documentary Notes]
  definition?: ILanguageMap<JSkosList>;

  // see [SKOS Documentary Notes]
  example?: ILanguageMap<JSkosList>;

  // see [SKOS Documentary Notes]
  historyNote?: ILanguageMap<JSkosList>;

  // see [SKOS Documentary Notes]
  editorialNote?: ILanguageMap<JSkosList>;

  // see [SKOS Documentary Notes]
  changeNote?: ILanguageMap<JSkosList>;

  // see [SKOS Documentary Notes]
  note?: ILanguageMap<JSkosList>;

  // date of birth, creation, or estabishment of the item
  startDate?: JSkosDate;

  // date death or resolution of the item
  endDate?: JSkosDate;

  // other date somehow related to the item
  relatedDate?: JSkosDate;

  // where an item started (e.g. place of birth)
  startPlace?: JSkosSet;

  // where an item ended (e.g. place of death)
  endPlace?: JSkosSet;

  // geographic location of the item
  location?: IJSkosLocation;

  // postal address of the item
  address?: IJSkosAddress;

  // what this item is about (e.g. topic)
  subject?: JSkosSet;

  // resources about this item (e.g. documentation)
  subjectOf?: JSkosSet;

  // list of image URLs depicting the item
  depiction?: JSkosList;
}

export interface IJSkosConcept extends IJSkosItem {
  // 	narrower concepts
  narrower?: JSkosSet<IJSkosConcept>;

  // 	broader concepts
  broader?: JSkosSet<IJSkosConcept>;

  // 	generally related concepts
  related?: JSkosSet<IJSkosConcept>;

  // 	related concepts ordered somehow before the concept
  previous?: JSkosSet<IJSkosConcept>;

  // 	related concepts ordered somehow after the concept
  next?: JSkosSet<IJSkosConcept>;

  // 	list of ancestors, possibly up to a top concept
  ancestors?: JSkosSet<IJSkosConcept>;

  // 	concept schemes or URI of the concept schemes
  inScheme?: JSkosSet;

  // 	concept schemes or URI of the concept schemes
  topConceptOf?: JSkosSet;

  // 	mappings from and/or to this concept
  mappings?: JSkosSet;

  // 	occurrences with this concept
  occurrences?: JSkosSet;
}

export interface IJSkosConceptScheme extends IJSkosItem {
  // [concept scheme] which this scheme is a version or edition of
  versionOf?: JSkosSet<IJSkosConceptScheme>;

  // top [concepts] of the scheme
  topConcepts?: JSkosSet<IJSkosConcept>;

  // URI namespace that all concepts URIs are expected to start with
  namespace?: JSkosURI;

  // regular expression that all concept URIs are expected to match
  uriPattern?: string;

  // regular expression that all primary notations should follow
  notationPattern?: string;

  // list of some valid notations as examples
  notationExamples?: string[];

  // concepts in the scheme
  concepts?: JSkosSet<IJSkosConcept>;

  // [concept types] of concepts in this scheme
  types?: JSkosSet;

  // [Distributions] to access the content of the concept scheme
  distributions?: JSkosSet;

  // Size of the concept scheme
  extent?: string;

  // Supported languages
  languages?: string[];

  // Licenses which the full scheme can be used under
  license?: JSkosSet;
}
