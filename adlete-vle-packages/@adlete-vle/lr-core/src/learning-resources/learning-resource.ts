import type { ILearningResourceMeta } from '@adlete-semantics/amb';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export interface ILearningResource<TContent = any> {
  meta: ILearningResourceMeta;
  content?: TContent;
}

export class NoContentError extends Error {
  constructor(message: string) {
    super(message);
    this.name = this.constructor.name;
  }
}
