import type { ILearningResourceMeta } from '@adlete-semantics/amb';
import { ITypedEventEmitter, ITypedEventEmitterNoEmit, TypedEventEmitter } from '@adlete-utils/events';

import type { IVocabularyManager } from '../vocabulary';
import { VocabularyManager } from '../vocabulary/VocabularyManager';

import { ILearningResourceManager, ILearningResourceManagerEvents } from './ILearningResourceManager';
import { getShortId } from './ids';
import { ILearningResource } from './learning-resource';

export class LearningResourceManager implements ILearningResourceManager {
  public vocabulary: IVocabularyManager = new VocabularyManager();

  protected _events: ITypedEventEmitter<ILearningResourceManagerEvents>;

  protected resourcesById: Record<string, ILearningResource> = {};

  protected metaById: Record<string, ILearningResourceMeta> = {};

  protected contentById: Record<string, unknown> = {};

  protected idPrefixes: string[] = [];

  public get events(): ITypedEventEmitterNoEmit<ILearningResourceManagerEvents> {
    return this._events;
  }

  public constructor() {
    this._events = new TypedEventEmitter();
  }

  protected addResourceById(resource: ILearningResource, id: string): void {
    if (id in this.resourcesById || id in this.metaById || id in this.contentById) {
      throw new Error(`Learning resource '${id}' was already added!`);
    }

    this.resourcesById[id] = resource;
    this.metaById[id] = resource.meta;
    if (resource.content) this.contentById[id] = resource.content;
  }

  // TODO: use bulk `addResources` instead for better events
  public addResource(resource: ILearningResource): void {
    this.addResources([resource]);
  }

  public addResources(resources: ILearningResource[]): void {
    resources.forEach((resource) => {
      this.addResourceById(resource, resource.meta.id);

      const shortId = getShortId(resource.meta.id, this.idPrefixes);
      if (shortId !== '') this.addResourceById(resource, shortId);
    });
    this._events.emit('resourcesAdded', resources);
  }

  protected validateResource(id: string): void {
    if (!(id in this.resourcesById)) throw new Error(`No learning resource found for '${id}'!`);
  }

  protected validateMeta(id: string): void {
    if (!(id in this.metaById)) throw new Error(`No meta-data found for learning resource '${id}'!`);
  }

  protected validateContent(id: string): void {
    if (!(id in this.contentById)) throw new Error(`No content found for learning resource '${id}'!`);
  }

  public getMeta(id: string): ILearningResourceMeta {
    this.validateMeta(id);
    return this.metaById[id];
  }

  public getResource<TContent>(id: string): ILearningResource<TContent> {
    this.validateResource(id);
    return this.resourcesById[id];
  }

  public getContent<TContent>(id: string): TContent {
    this.validateContent(id);
    return this.contentById[id] as TContent;
  }

  public setContent<TContent>(id: string, content: TContent): void {
    this.validateResource(id);
    this.resourcesById[id].content = content;
    this.contentById[id] = content;

    const shortId = getShortId(id, this.idPrefixes);
    if (shortId !== '') {
      this.resourcesById[shortId].content = content;
      this.contentById[shortId] = content;
    }
  }

  public registerIdPrefix(prefix: string): void {
    this.idPrefixes.push(prefix);
  }
}
