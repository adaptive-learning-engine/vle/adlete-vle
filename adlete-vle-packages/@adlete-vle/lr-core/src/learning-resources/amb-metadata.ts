import type { ILearningResourceEncoding, ILearningResourceMeta } from '@adlete-semantics/amb';

import { getTranslationFromLangMap, recordHasTranslation } from '../locale';

// TODO: move everything to @adlete-semantics/amb

export const LR_CONTEXT_URI = 'https://w3id.org/kim/amb/draft/context.jsonld';
export const LR_CONTEXT_EN = [LR_CONTEXT_URI, { '@language': 'en' }];

export function hasEncodingFormat(encoding: ILearningResourceEncoding, formats: string[]): boolean {
  return encoding.encodingFormat && formats.includes(encoding.encodingFormat);
}

export function getFirstEncodingWithFormat(meta: ILearningResourceMeta, formats: string[]): ILearningResourceEncoding {
  if (!meta.encoding) return null;
  return meta.encoding.find((entry) => hasEncodingFormat(entry, formats));
}

export function metaHasEncodingFormat(meta: ILearningResourceMeta, formats: string[]): boolean {
  if (!meta.encoding) return false;
  return meta.encoding.some((entry) => entry.encodingFormat && formats.includes(entry.encodingFormat));
}

export function metaHasValidFirstAboutLabel(meta: ILearningResourceMeta, languages: readonly string[]): boolean {
  if (!meta.about || meta.about.length === 0 || !meta.about[0].prefLabel) return false;

  return recordHasTranslation(meta.about[0].prefLabel, languages);
}

export function getMetaFirstAboutLabel(meta: ILearningResourceMeta, languages: readonly string[]): string {
  return getTranslationFromLangMap(meta.about[0].prefLabel, languages);
}

export function getMetaImage(meta: ILearningResourceMeta): string {
  return meta.image;
}

export function getMetaName(meta: ILearningResourceMeta, languages: readonly string[]): string {
  if (meta.localizedName && recordHasTranslation(meta.localizedName, languages))
    return getTranslationFromLangMap(meta.localizedName, languages);

  return meta.name; // TODO: multilingual name
}

export class WrongEncodingError extends Error {
  constructor(message: string) {
    super(message);
    this.name = this.constructor.name;
  }
}
