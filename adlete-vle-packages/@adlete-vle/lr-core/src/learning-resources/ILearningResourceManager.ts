import type { ILearningResourceMeta } from '@adlete-semantics/amb';
import type { ITypedEventEmitterNoEmit } from '@adlete-utils/events';

import type { IVocabularyManager } from '../vocabulary';

import { ILearningResource } from './learning-resource';

export interface ILearningResourceManagerEvents {
  resourcesAdded: ILearningResource[];
}

export interface ILearningResourceManager {
  vocabulary: IVocabularyManager;
  events: ITypedEventEmitterNoEmit<ILearningResourceManagerEvents>;
  addResource(resource: ILearningResource): void;
  addResources(resources: ILearningResource[]): void;
  getMeta(id: string): ILearningResourceMeta;
  getResource<TContent>(id: string): ILearningResource<TContent>;
  getContent<TContent>(id: string): TContent;
}
