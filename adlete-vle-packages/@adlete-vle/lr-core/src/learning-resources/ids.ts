export function getShortId(id: string, prefixes: string[]): string {
  for (let i = 0; i < prefixes.length; ++i) if (id.startsWith(prefixes[i])) return id.substring(prefixes[i].length);
  return '';
}
