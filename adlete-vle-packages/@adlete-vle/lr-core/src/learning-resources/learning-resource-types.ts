export enum LearningResourceTypeId {
  Application = 'https://vocabs.openeduhub.de/w3id.org/openeduhub/vocabs/learningResourceType/application.html',
  Image = 'https://vocabs.openeduhub.de/w3id.org/openeduhub/vocabs/learningResourceType/image.html',
  Simulation = 'https://vocabs.openeduhub.de/w3id.org/openeduhub/vocabs/learningResourceType/simulation.html',
  Other = 'https://vocabs.openeduhub.de/w3id.org/openeduhub/vocabs/learningResourceType/other.html',
  Text = 'https://vocabs.openeduhub.de/w3id.org/openeduhub/vocabs/learningResourceType/text.html',
}
