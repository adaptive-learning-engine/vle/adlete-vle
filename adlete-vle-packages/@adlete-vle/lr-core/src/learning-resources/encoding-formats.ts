export enum EncodingFormats {
  EmbeddedVideoByScript = 'application/x.embedded-video-script',
  TextMarkdown = 'text/markdown',
  TextMultiLingualMarkdown = 'text/x.multi-lingual-markdown+json',
  TextMultiLingualText = 'text/x.multi-lingual-text+json',
  TextPlain = 'text/plain',
  ReactComponent = 'application/x.react-component',
  Other = 'application/x.other',
}

export type IMultiLingualMarkdownText = Record<string, string>;
export type IMultiLingualRawText = Record<string, string>;

export type HumanReadableTextualData = string | IMultiLingualRawText | IMultiLingualMarkdownText;

export interface IEmbeddedVideoByScript {
  src: string;
  [key: string]: string;
}
