export interface IWithDifficulty {
  difficulty: number;
}

export function filterByDifficulty<T extends IWithDifficulty>(options: T[], difficulty: number | [number, number]): T[] {
  if (Array.isArray(difficulty))
    return options.filter((option) => option.difficulty >= difficulty[0] && option.difficulty <= difficulty[1]);
  return options.filter((option) => option.difficulty >= difficulty && option.difficulty <= difficulty);
}
