// eslint-disable-next-line import/no-extraneous-dependencies
import { beforeEach, describe, expect, jest, test } from '@jest/globals';
import { type ThirdPartyModule } from 'i18next';

import { InMemoryStorage } from '@fki/editor-core/storage';

import { LocaleManager } from '../../../src/locale';

const UPDATED_LANG = 'it';

describe('LocaleManager', () => {
  let storage: InMemoryStorage;
  let localeManager: LocaleManager;

  beforeEach(() => {
    storage = new InMemoryStorage();

    // for testing, we are creating a new i18n instance per LocaleManager instead of using
    // the default instance, as we can't initialize the same instance multiple timesya
    localeManager = new LocaleManager(storage, { createInstance: true });
  });

  describe('init()', () => {
    test('internal data represents DEFAULT_DATA, when storage is empty', async () => {
      await localeManager.init();
      expect(localeManager.language).toEqual('de-DE');
      expect(localeManager.fallbackLanguage).toEqual('en-US');
    });

    test('internal data represents storage data, when storage is setup accordingly', async () => {
      const data = { language: 'en-US', fallbackLanguage: 'de-DE' };
      storage.setItem('locale-manager-data', data);

      await localeManager.init();
      expect(localeManager.language).toEqual(data.language);
      expect(localeManager.fallbackLanguage).toEqual(data.fallbackLanguage);
    });

    test('stateful event contains data from storage', async () => {
      const data = { language: 'en-US', fallbackLanguage: 'de-DE' };
      storage.setItem('locale-manager-data', data);

      await localeManager.init();
      expect(localeManager.events.getState('language')).toEqual(data.language);
      expect(localeManager.events.getState('fallbackLanguage')).toEqual(data.fallbackLanguage);
    });

    test('i18next uses the data from the LocaleManager', async () => {
      const data = { language: 'en-US', fallbackLanguage: 'de-DE' };
      storage.setItem('locale-manager-data', data);

      await localeManager.init();

      expect(localeManager.i18n.language).toEqual(data.language);
      expect(localeManager.i18n.languages[0]).toEqual(data.language);

      // the fallback language will be somewhere further behind, but not exactly clear where
      expect(localeManager.i18n.languages).toContain(data.fallbackLanguage);
      expect(localeManager.i18n.languages[0]).not.toEqual(data.fallbackLanguage);
    });

    test('arg `modules` is being used by i18next', async () => {
      const module: ThirdPartyModule = { type: '3rdParty', init: jest.fn<() => void>() };
      await localeManager.init([module]);
      expect(module.init).toBeCalled();
    });
  });

  describe('updateLanguage()', () => {
    test('updates the internal data', async () => {
      await localeManager.init();
      localeManager.updateLanguage(UPDATED_LANG);
      expect(localeManager.language).toEqual(UPDATED_LANG);
    });

    test('updates i18next', async () => {
      await localeManager.init();
      localeManager.updateLanguage(UPDATED_LANG);
      expect(localeManager.i18n.language).toEqual(UPDATED_LANG);
    });

    // TODO: for this to work, updateLanguage should be async
    // test('updates the storage', async () => {
    //   await localeManager.init();
    //   localeManager.updateLanguage(UPDATED_LANG);
    //   const data: ILocaleEventData = await storage.getItem('locale-manager-data');
    //   expect(data.language).toEqual(UPDATED_LANG);
    // });

    test('emits a `language` update event', async () => {
      const cbLanguage = jest.fn<(language: string) => void>();
      await localeManager.init();
      localeManager.events.addListener('language', cbLanguage);

      localeManager.updateLanguage(UPDATED_LANG);
      expect(cbLanguage).toBeCalledWith(UPDATED_LANG);
    });
  });
});
