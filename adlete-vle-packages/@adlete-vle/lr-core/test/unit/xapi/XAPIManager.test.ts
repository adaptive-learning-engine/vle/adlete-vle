// eslint-disable-next-line import/no-extraneous-dependencies
import { beforeEach, describe, expect, jest, test } from '@jest/globals';
import type { Statement } from '@xapi/xapi';
import type { Mock } from 'jest-mock';
import { validate as validateUUID } from 'uuid';

import type { XAPIStatementsReceivedEvent, XAPIStatementsSentError, XAPIStatementsSentEvent } from '../../../src/xapi';

import { ForcedError } from './xapi-mock';

// jest ESM mocking requires us to use dynamic imports, but
// we gotta do ugly things to keep XAPIManager as a class when using such imports
const XAPIManagerClass = (await import('../../../src/xapi')).XAPIManager;
class XAPIManager extends XAPIManagerClass {}

const STATEMENT_WITHOUT_ID: Statement = {
  actor: {
    name: 'Sally Glider',
    mbox: 'mailto:sally@example.com',
    objectType: 'Agent',
  },
  verb: {
    id: 'http://adlnet.gov/expapi/verbs/experienced',
    display: { 'en-US': 'experienced' },
  },
  object: {
    id: 'http://example.com/activities/solo-hang-gliding',
    objectType: 'Activity',
    definition: {
      name: { 'en-US': 'Solo Hang Gliding' },
      type: 'http://adlnet.gov/expapi/activities/objective',
    },
  },
};

const ID_1 = 'd6e4b410-c376-470d-a347-c4ebad0cf232';
const ID_2 = '43b7b739-e719-4453-8637-ab1543ef1610';
const ID_3 = 'b3430895-079a-4994-8eb0-e151551453e7';
const STATEMENT_WITH_ID_1: Statement = { ...STATEMENT_WITHOUT_ID, id: ID_1 };
const STATEMENT_WITH_ID_2: Statement = { ...STATEMENT_WITHOUT_ID, id: ID_2 };
const STATEMENT_FORCING_ERROR: Statement = { ...STATEMENT_WITHOUT_ID, id: ID_3, context: { platform: 'ERROR' } };

describe('XAPIManager', () => {
  let xAPIManager: XAPIManager;
  let cbReceived: Mock<(event: XAPIStatementsReceivedEvent) => void>;
  let cbSent: Mock<(event: XAPIStatementsSentEvent) => void>;
  let cbError: Mock<(event: XAPIStatementsSentError) => void>;

  beforeEach(() => {
    xAPIManager = new XAPIManager({ endpoint: 'http://mock-endpoint.com/xapi' });

    // events
    cbReceived = jest.fn<(event: XAPIStatementsReceivedEvent) => void>();
    cbSent = jest.fn<(event: XAPIStatementsSentEvent) => void>();
    cbError = jest.fn<(event: XAPIStatementsSentError) => void>();

    xAPIManager.events.addListener('statementsReceived', cbReceived);
    xAPIManager.events.addListener('statementsSent', cbSent);
    xAPIManager.events.addListener('statementsSentError', cbError);
  });

  describe('sendStatement()', () => {
    test('returns id of statement that was send', async () => {
      const id = await xAPIManager.sendStatement(STATEMENT_WITH_ID_1);
      expect(id).toBe(STATEMENT_WITH_ID_1.id);
    });

    test('returns a valid UUID-id, if the statement had no id', async () => {
      const id = await xAPIManager.sendStatement(STATEMENT_WITHOUT_ID);
      expect(validateUUID(id)).toBeTruthy();
    });

    test('throws an error, if something is wrong', async () => {
      await expect(xAPIManager.sendStatement(STATEMENT_FORCING_ERROR)).rejects.toBeInstanceOf(Error);
    });
  });

  describe('sendStatementSafe()', () => {
    test('returns id of statement that was send', async () => {
      const id = await xAPIManager.sendStatementSafe(STATEMENT_WITH_ID_1);
      expect(id).toBe(STATEMENT_WITH_ID_1.id);
    });

    test('returns a valid UUID-id, if the statement had no id', async () => {
      const id = await xAPIManager.sendStatementSafe(STATEMENT_WITHOUT_ID);
      expect(validateUUID(id)).toBeTruthy();
    });

    test('returns null on error', async () => {
      const id = await xAPIManager.sendStatementSafe(STATEMENT_FORCING_ERROR);
      expect(id).toBeNull();
    });
  });

  describe('sendStatements()', () => {
    test('returns ids of statements that were send', async () => {
      const ids = await xAPIManager.sendStatements([STATEMENT_WITH_ID_1, STATEMENT_WITH_ID_2]);
      expect(ids).toEqual([STATEMENT_WITH_ID_1.id, STATEMENT_WITH_ID_2.id]);
    });

    test('returns valid UUID-ids, if the statements had no id', async () => {
      const ids = await xAPIManager.sendStatements([STATEMENT_WITHOUT_ID, STATEMENT_WITHOUT_ID]);
      const validated = ids.map(validateUUID);
      expect(validated).toEqual([true, true]);
    });

    test('throws an error, if something is wrong', async () => {
      await expect(xAPIManager.sendStatements([STATEMENT_FORCING_ERROR])).rejects.toBeInstanceOf(Error);
    });
  });

  describe('sendStatementsSafe()', () => {
    test('returns ids of statements that were send', async () => {
      const ids = await xAPIManager.sendStatementsSafe([STATEMENT_WITH_ID_1, STATEMENT_WITH_ID_2]);
      expect(ids).toEqual([STATEMENT_WITH_ID_1.id, STATEMENT_WITH_ID_2.id]);
    });

    test('returns valid UUID-ids, if the statements had no id', async () => {
      const ids = await xAPIManager.sendStatementsSafe([STATEMENT_WITHOUT_ID, STATEMENT_WITHOUT_ID]);
      const validated = ids.map(validateUUID);
      expect(validated).toEqual([true, true]);
    });

    test('returns null on error', async () => {
      const id = await xAPIManager.sendStatementsSafe([STATEMENT_FORCING_ERROR]);
      expect(id).toBeNull();
    });
  });

  describe('events', () => {
    test('statementsReceived/statementsSent emitting, when using sendStatement', async () => {
      await xAPIManager.sendStatement(STATEMENT_WITH_ID_1);
      expect(cbReceived).toBeCalledWith({ type: 'statements-received', statements: [STATEMENT_WITH_ID_1] });
      expect(cbSent).toBeCalledWith({ type: 'statements-sent', statements: [STATEMENT_WITH_ID_1] });
    });

    test('statementsReceived/statementsSent emitting, when using sendStatementSafe', async () => {
      await xAPIManager.sendStatementSafe(STATEMENT_WITH_ID_1);
      expect(cbReceived).toBeCalledWith({ type: 'statements-received', statements: [STATEMENT_WITH_ID_1] });
      expect(cbSent).toBeCalledWith({ type: 'statements-sent', statements: [STATEMENT_WITH_ID_1] });
    });

    test('statementsReceived/statementsSent emitting, when using sendStatements', async () => {
      const statements = [STATEMENT_WITH_ID_1, STATEMENT_WITH_ID_2];
      await xAPIManager.sendStatements(statements);
      expect(cbReceived).toBeCalledWith({ type: 'statements-received', statements });
      expect(cbSent).toBeCalledWith({ type: 'statements-sent', statements });
    });

    test('statementsReceived/statementsSent emitting, when using sendStatementsSafe', async () => {
      const statements = [STATEMENT_WITH_ID_1, STATEMENT_WITH_ID_2];
      await xAPIManager.sendStatementsSafe(statements);
      expect(cbReceived).toBeCalledWith({ type: 'statements-received', statements });
      expect(cbSent).toBeCalledWith({ type: 'statements-sent', statements });
    });

    test('statementsReceived/statementsSentError emitting, when error occurs in sendStatement', async () => {
      const statements = [STATEMENT_FORCING_ERROR];
      try {
        await xAPIManager.sendStatement(statements[0]);
      } catch (e) {
        /* empty */
      }
      expect(cbReceived).toBeCalledWith({ type: 'statements-received', statements });
      expect(cbError).toBeCalledWith({ type: 'statements-sent-error', statements, error: new ForcedError() });
    });

    test('statementsReceived/statementsSentError emitting, when error occurs in sendStatementSafe', async () => {
      const statements = [STATEMENT_FORCING_ERROR];
      await xAPIManager.sendStatementSafe(statements[0]);
      expect(cbReceived).toBeCalledWith({ type: 'statements-received', statements });
      expect(cbError).toBeCalledWith({ type: 'statements-sent-error', statements, error: new ForcedError() });
    });

    test('statementsReceived/statementsSentError emitting, when error occurs in sendStatements', async () => {
      const statements = [STATEMENT_FORCING_ERROR];
      try {
        await xAPIManager.sendStatements(statements);
      } catch (e) {
        /* empty */
      }
      expect(cbReceived).toBeCalledWith({ type: 'statements-received', statements });
      expect(cbError).toBeCalledWith({ type: 'statements-sent-error', statements, error: new ForcedError() });
    });

    test('statementsReceived/statementsSentError emitting, when error occurs in sendStatementsSafe', async () => {
      const statements = [STATEMENT_FORCING_ERROR];
      await xAPIManager.sendStatementsSafe(statements);
      expect(cbReceived).toBeCalledWith({ type: 'statements-received', statements });
      expect(cbError).toBeCalledWith({ type: 'statements-sent-error', statements, error: new ForcedError() });
    });
  });
});
