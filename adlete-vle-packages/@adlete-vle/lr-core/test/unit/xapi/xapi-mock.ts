// eslint-disable-next-line import/no-extraneous-dependencies
import { jest } from '@jest/globals';
import type { SendStatementsParams } from '@xapi/xapi';
import type { AxiosPromise } from 'axios';
import { v4 as uuidV4 } from 'uuid';

export class ForcedError extends Error {
  constructor() {
    super('Forced Error in XAPIMock');
  }
}

export class XAPIMock {
  sendStatements(params: SendStatementsParams): AxiosPromise<string[]> {
    const ids = params.statements.map((statement) => statement.id || uuidV4());

    params.statements.forEach((statement) => {
      if (statement?.context?.platform === 'ERROR') throw new ForcedError();
    });

    return Promise.resolve({ data: ids, status: 0, statusText: 'mock', headers: null, config: null });
  }
}

await jest.unstable_mockModule('@xapi/xapi', () => ({ default: XAPIMock }));
