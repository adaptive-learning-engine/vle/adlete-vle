# @adlete-vle/vle-core

Various components (scripts and react components) for creating VLEs.

## Plugins

Plugins based on components from [@adlete-vle/lr-core](../lr-core/) and others, f. ex.

- [ContextManager](./src/plugins/ContextManager.ts)
- [LearningResourceManager](./src/plugins/LearningResourceManager.ts)
- [LocaleManager](./src/plugins/LocaleManager.ts)
- [LRComponentCreator](./src/plugins/LRComponentCreator.ts)
- [RandomExperimentManager](./src/plugins/RandomExperimentManager.ts)
- [Storage](./src/plugins/StoragePlugin.ts)
- [TTSManager](./src/plugins/TTSManager.ts)

## React Components

- [TTSSettingsDialog](./src/components/TTSSettingsDialog.tsx)
