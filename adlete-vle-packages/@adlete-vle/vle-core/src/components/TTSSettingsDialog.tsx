import CloseIcon from '@mui/icons-material/Close';
import { DialogContent, DialogTitle, IconButton } from '@mui/material';
import Dialog from '@mui/material/Dialog';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { TTSSettings } from '@adlete-vle/lr-core-components/speech/synthesis';

export interface ITTSSettingsDialogProps {
  open: boolean;
  onClose: () => void;
}

// Translate save changes
export function TTSSettingsDialog({ open, onClose }: ITTSSettingsDialogProps): React.ReactElement {
  const { t } = useTranslation('tts-dialog');

  return (
    <Dialog onClose={onClose} aria-labelledby="customized-dialog-title" open={open} maxWidth="xs" fullWidth>
      <DialogTitle>
        {t('tts-settings')}
        <IconButton
          aria-label="close"
          onClick={onClose}
          sx={{
            position: 'absolute',
            right: 8,
            top: 8,
          }}
        >
          <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent dividers>
        <TTSSettings />
      </DialogContent>
    </Dialog>
  );
}
