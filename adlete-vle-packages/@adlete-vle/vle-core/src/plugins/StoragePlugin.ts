import { IStorage } from '@fki/editor-core/storage';
import type { IPlugin, IPluginManager, IPluginMeta } from '@fki/plugin-system/specs';

export interface IWithStorage {
  storage: StoragePlugin;
}

export const StorageMeta: IPluginMeta<keyof IWithStorage, 'storage'> = {
  id: 'storage',
};

export class StoragePlugin implements IPlugin<IWithStorage> {
  public meta: IPluginMeta<keyof IWithStorage, 'storage'> = StorageMeta;

  public storage: IStorage;

  public constructor(storage: IStorage) {
    this.storage = storage;
  }

  // eslint-disable-next-line class-methods-use-this, @typescript-eslint/no-unused-vars
  public initialize(pluginMan: IPluginManager<IWithStorage>): Promise<void> {
    return Promise.resolve();
  }

  // eslint-disable-next-line class-methods-use-this
  public terminate(): Promise<void> {
    return Promise.resolve();
  }
}
