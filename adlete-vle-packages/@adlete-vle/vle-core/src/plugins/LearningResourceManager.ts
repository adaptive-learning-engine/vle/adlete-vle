import { LearningResourceManager } from '@adlete-vle/lr-core/learning-resources';
import { LearningResourceManagerContext } from '@adlete-vle/lr-core-components/contexts';

import type { IPlugin, IPluginManager, IPluginMeta } from '@fki/plugin-system/specs';

import { IWithContextManager } from './ContextManager';

export interface IWithLearningResourceManager extends IWithContextManager {
  learningResourceMan: LearningResourceManagerPlugin;
}

export const LearningResourceManagerMeta: IPluginMeta<keyof IWithLearningResourceManager, 'learningResourceMan'> = {
  id: 'learningResourceMan',
  dependencies: ['contextManager'],
};

export class LearningResourceManagerPlugin implements IPlugin<IWithLearningResourceManager> {
  public meta: IPluginMeta<keyof IWithLearningResourceManager, 'learningResourceMan'> = LearningResourceManagerMeta;

  public manager: LearningResourceManager;

  // eslint-disable-next-line class-methods-use-this
  public initialize(pluginMan: IPluginManager<IWithLearningResourceManager>): Promise<void> {
    this.manager = new LearningResourceManager();

    const contextManager = pluginMan.get('contextManager');
    contextManager.addContext(LearningResourceManagerContext, this.manager);

    return Promise.resolve();
  }

  // eslint-disable-next-line class-methods-use-this
  public terminate(): Promise<void> {
    return Promise.resolve();
  }
}
