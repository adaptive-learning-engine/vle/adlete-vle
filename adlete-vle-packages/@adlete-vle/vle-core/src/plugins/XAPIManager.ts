import { type IXAPIManagerOptions, XAPIManager } from '@adlete-vle/lr-core/xapi';
import { XAPIManagerContext } from '@adlete-vle/lr-core-components/contexts';

import type { IPlugin, IPluginManager, IPluginMeta } from '@fki/plugin-system/specs';

import { IWithContextManager } from './ContextManager';
import { IWithLocaleManager } from './LocaleManager';
import { IWithStorage } from './StoragePlugin';

export interface IWithXAPIManager extends IWithStorage, IWithLocaleManager, IWithContextManager {
  xAPIManager: XAPIManagerPlugin;
}

export const XAPIManagerMeta: IPluginMeta<keyof IWithXAPIManager, 'xAPIManager'> = {
  id: 'xAPIManager',
  dependencies: ['contextManager'],
};

export class XAPIManagerPlugin implements IPlugin<IWithXAPIManager> {
  public meta: IPluginMeta<keyof IWithXAPIManager, 'xAPIManager'> = XAPIManagerMeta;

  public manager: XAPIManager;

  public constructor(protected options: IXAPIManagerOptions) {}

  public async initialize(pluginMan: IPluginManager<IWithXAPIManager>): Promise<void> {
    const contextManager = pluginMan.get('contextManager');

    this.manager = new XAPIManager(this.options);

    contextManager.addContext(XAPIManagerContext, this.manager);
  }

  // eslint-disable-next-line class-methods-use-this
  public terminate(): Promise<void> {
    return Promise.resolve();
  }
}
