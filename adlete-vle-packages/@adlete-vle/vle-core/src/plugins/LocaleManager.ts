import { initReactI18next } from 'react-i18next';

import { createLocaleManager, LocaleManager } from '@adlete-vle/lr-core/locale';
import { LocaleContext } from '@adlete-vle/lr-core-components/contexts';

import type { IPlugin, IPluginManager, IPluginMeta } from '@fki/plugin-system/specs';

import { IWithContextManager } from './ContextManager';
import { IWithStorage } from './StoragePlugin';

export interface IWithLocaleManager extends IWithStorage, IWithContextManager {
  localeManager: LocaleManagerPlugin;
}

export const LocaleManagerMeta: IPluginMeta<keyof IWithLocaleManager, 'localeManager'> = {
  id: 'localeManager',
  dependencies: ['storage', 'contextManager'],
};

export class LocaleManagerPlugin implements IPlugin<IWithLocaleManager> {
  public meta: IPluginMeta<keyof IWithLocaleManager, 'localeManager'> = LocaleManagerMeta;

  public manager: LocaleManager;

  public async initialize(pluginMan: IPluginManager<IWithLocaleManager>): Promise<void> {
    const storage = pluginMan.get('storage').storage;
    const contextManager = pluginMan.get('contextManager');

    // TODO: support more modules
    this.manager = await createLocaleManager(storage, [initReactI18next]);

    contextManager.addContext(LocaleContext, this.manager);
  }

  // eslint-disable-next-line class-methods-use-this
  public terminate(): Promise<void> {
    return Promise.resolve();
  }
}
