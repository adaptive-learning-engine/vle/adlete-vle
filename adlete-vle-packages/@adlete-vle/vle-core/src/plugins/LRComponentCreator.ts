import { ReactComponentFactoryLibrary } from '@adlete-vle/lr-core-components/ReactComponentFactoryLibrary';
import { ReactPropMapperLibrary } from '@adlete-vle/lr-core-components/ReactPropMapperLibrary';
import { LRComponentCreatorContext } from '@adlete-vle/lr-core-components/contexts';
import { LRComponentCreator } from '@adlete-vle/lr-core-components/learning-resources';

import type { IPlugin, IPluginManager, IPluginMeta } from '@fki/plugin-system/specs';

import { IWithContextManager } from './ContextManager';
import { IWithLearningResourceManager } from './LearningResourceManager';

export interface IWithLRComponentCreator extends IWithContextManager, IWithLearningResourceManager {
  lrComponentCreator: LRComponentCreatorPlugin;
}

export const LRComponentCreatorMeta: IPluginMeta<keyof IWithLRComponentCreator, 'lrComponentCreator'> = {
  id: 'lrComponentCreator',
  dependencies: ['contextManager', 'learningResourceMan'],
};

export class LRComponentCreatorPlugin implements IPlugin<IWithLRComponentCreator> {
  public meta: IPluginMeta<keyof IWithLRComponentCreator, 'lrComponentCreator'> = LRComponentCreatorMeta;

  public creator: LRComponentCreator;

  public factoryLib: ReactComponentFactoryLibrary;

  public propMapper: ReactPropMapperLibrary;

  // eslint-disable-next-line class-methods-use-this
  public initialize(pluginMan: IPluginManager<IWithLRComponentCreator>): Promise<void> {
    // TODO: move to own plugins
    this.factoryLib = new ReactComponentFactoryLibrary();
    this.propMapper = new ReactPropMapperLibrary();

    const lrm = pluginMan.get('learningResourceMan').manager;

    this.creator = new LRComponentCreator(lrm, this.factoryLib, this.propMapper);

    const contextManager = pluginMan.get('contextManager');
    contextManager.addContext(LRComponentCreatorContext, this.creator);

    return Promise.resolve();
  }

  // eslint-disable-next-line class-methods-use-this
  public terminate(): Promise<void> {
    return Promise.resolve();
  }
}
