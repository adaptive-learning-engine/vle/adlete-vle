import { createTTSManager, TTSContext, TTSManager } from '@adlete-vle/lr-core-components/speech/synthesis';

import type { IPlugin, IPluginManager, IPluginMeta } from '@fki/plugin-system/specs';

import { IWithContextManager } from './ContextManager';
import { IWithLocaleManager } from './LocaleManager';
import { IWithStorage } from './StoragePlugin';

export interface IWithTTSManager extends IWithStorage, IWithLocaleManager, IWithContextManager {
  ttsManager: TTSManagerPlugin;
}

export const TTSManagerMeta: IPluginMeta<keyof IWithTTSManager, 'ttsManager'> = {
  id: 'ttsManager',
  dependencies: ['storage', 'localeManager', 'contextManager'],
};

export class TTSManagerPlugin implements IPlugin<IWithTTSManager> {
  public meta: IPluginMeta<keyof IWithTTSManager, 'ttsManager'> = TTSManagerMeta;

  public manager: TTSManager;

  public async initialize(pluginMan: IPluginManager<IWithTTSManager>): Promise<void> {
    const localeMan = pluginMan.get('localeManager').manager;
    const storage = pluginMan.get('storage').storage;
    const contextManager = pluginMan.get('contextManager');

    this.manager = await createTTSManager(localeMan, storage);

    contextManager.addContext(TTSContext, this.manager);
  }

  // eslint-disable-next-line class-methods-use-this
  public terminate(): Promise<void> {
    return Promise.resolve();
  }
}
