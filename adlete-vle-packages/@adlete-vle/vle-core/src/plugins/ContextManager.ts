import React, { Context } from 'react';

import type { IPlugin, IPluginManager, IPluginMeta } from '@fki/plugin-system/specs';

export interface IWithContextManager {
  contextManager: ContextManagerPlugin;
}

export const ContextManagerMeta: IPluginMeta<keyof IWithContextManager, 'contextManager'> = {
  id: 'contextManager',
};

export class ContextManagerPlugin implements IPlugin<IWithContextManager> {
  public meta: IPluginMeta<keyof IWithContextManager, 'contextManager'> = ContextManagerMeta;

  protected contexts: Map<Context<unknown>, unknown> = new Map();

  // eslint-disable-next-line class-methods-use-this, @typescript-eslint/no-unused-vars
  public initialize(pluginMan: IPluginManager<IWithContextManager>): Promise<void> {
    return Promise.resolve();
  }

  // eslint-disable-next-line class-methods-use-this
  public terminate(): Promise<void> {
    return Promise.resolve();
  }

  public addContext<T>(context: Context<T>, value?: T): void {
    this.contexts.set(context, value);
  }

  public render(children: React.ReactNode): React.ReactElement {
    if (this.contexts.size === 0) return React.createElement(React.Fragment, null, children);

    const wrapped = [...this.contexts.entries()].reduce(
      (prevChildren, [context, value]) => React.createElement(context.Provider, { value }, prevChildren),
      children
    ) as React.ReactElement;
    return wrapped;
  }
}
