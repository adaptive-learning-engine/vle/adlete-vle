const messages = {
  dialogs: {
    close: 'Close',
  },
  'tts-dialog': {
    'tts-settings': 'Text to speech settings',
  },
  'main-menu': {
    'tts-settings': 'Text to speech',
    settings: 'Settings',
  },
};

// eslint-disable-next-line import/no-default-export
export default messages;
