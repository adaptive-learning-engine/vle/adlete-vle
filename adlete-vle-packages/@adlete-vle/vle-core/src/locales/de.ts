const messages = {
  dialogs: {
    close: 'Schließen',
  },
  'tts-dialog': {
    'tts-settings': 'Einstellungen der Sprachausgabe',
  },
  'main-menu': {
    'tts-settings': 'Sprachausgabe',
    settings: 'Einstellungen',
  },
};

// eslint-disable-next-line import/no-default-export
export default messages;
