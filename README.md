# adlete-vle / KIWI

This repository contains the code of a personalized learning environment created as part of the [KIWI](https://kiwerkstatt.f2.htw-berlin.de/lehre) project.

- The first prototype consists of digital learning environment that offers instruction on conditional probability in both English and German. It is available as an open educational resource [at this location](https://iug-research.gitlab.io/oer-conditional-probability/). The course provides a comprehensive learning experience, featuring textual content (with text-to-speech functionality), interactive visualizations, and auto-generated exercises to guide learners through the concepts of conditional probability and conditional independence. The learning environment incorporates _context personalization_ within its interactive learning objects.
- The _ongoing development_ is dedicated to the second prototype, which involves a personalized learning engine as a service, including learner modeling and a recommender system.

**Little disclaimer**

> Please note that the code was developed as part of a scientific project, where the focus was on research and experimentation rather than the usual quality standards of a fully-fledged product code base.

## Open-Source Mirror

ATTENTION: This GitLab-Repository is a mirror of the development repository at the HTW Berlin GitLab instance. As such, it does not contain all development branches and the `master` branch may not be up-to-date.

## Dependencies

- [node.js](https://nodejs.org/en) version 18
- [yarn](https://yarnpkg.com/getting-started/install) package manager

## Installation

1. Checkout this repository
2. Checkout the [adlete-packages](https://gitlab.com/adaptive-learning-engine/adlete-packages) repository into `{this-repo}/adlete-packages`. This repo contains several packages of the adlete adaptive learning engine.
3. Execute `yarn install` in the repository directory

## Running / Building

All necessary npm/yarn commands can be found in the main [package.json](./package.json).

Most important commands are:

- `yarn start:vle-naive-bayes` - starts the dev server of the VLE (currently only including the topic of conditional probability and not yet Naive Bayes, despite its name)

The main entry point of the prototype for conditional probability resides in [kiwi-packages/@kiwi-al/vle-naive-bayes](https://gitlab.com/adaptive-learning-engine/vle/adlete-vle/-/tree/master/kiwi-packages/@kiwi-al/vle-naive-bayes).

## IDE

### Editorconfig

This package uses [Editorconfig](https://editorconfig.org/). It is advised to install the respective extensions in your IDE of choice.

### Linting

This package uses a prettier configuration to set a style format for all source code files and an eslint configuration for linting the typescript code. These configurations are part of the `@adlete/dev-config` package. It is advised to install the respective extensions in your IDE of choice.

### Version Control

Use the [bluejava git commit message guide](https://github.com/bluejava/git-commit-guide).

Since this is a monorepo, make sure to use the package name in your commit messages, e.g. `FIX(vle-core): fixed foo`.

## Common problems

### Adding new packages

Don't use `yarn install *package-name*`, as this repository is a yarn workspace. Installing a package this way, will only add it to the root `package.json`, which is most likely not what you want. Instead, simply add the respective package definitions to the `package.json` of the package that it is needed in and call `yarn install` afterwards in the root folder.

### Problems with Typescript types

Especially after installing new packages, you should restart your IDE so it picks up the new types.

### Duplicate Typescript types

Sometimes multiple version of the same type package, e.g. `@types/react`, are installed, resulting in typescript errors. You can install and execute the [yarn-deduplicate](https://www.npmjs.com/package/yarn-deduplicate) package, followed by a call of `yarn install`, to remove such duplicate packages.

If this does not help, add a [version resolution](https://classic.yarnpkg.com/lang/en/docs/selective-version-resolutions/) to the root `package.json` and call `yarn install` again.

### Vite: Packages from this Monorepo

If Vite should treat packages from this monorepo as source packages (e.g. for rebuilding / hot module reloading), then add the according packages to the list of aliases in the according `vite.conf.js`.

### Weird problems

- Try deleting the `node_modules/.vite` directory of the frontend package that uses Vite.
