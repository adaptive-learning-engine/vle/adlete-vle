// eslint-disable-next-line import/no-extraneous-dependencies
import { describe, expect, test } from '@jest/globals';
import type { Activity, ActivityDefinition, Agent, Context, Statement, Verb } from '@xapi/xapi';

import { createActivity, createAgent, createAgentActivityStatement, createRegistrationContext, createVerb } from '../../src/create';

const AGENT_ID = 'agent@foo.bar';
const AGENT_NAME = 'Austin Powers';
const AGENT_MBOX = `mailto:${AGENT_ID}`;
const AGENT_MBOX_SHA1SUM = '323b553ee0f68cf67a5019a6a7001478fa3b5d3b';
const AGENT_ACCOUNT = { homepage: 'http://my.server.name', name: AGENT_NAME };
const AGENT_OPENID = 'http://my.server.name/openidserver/users/austin_powers';

// const AGENT_IFI

describe('createAgent()', () => {
  test('creates Agent from string', () => {
    const expected: Agent = { objectType: 'Agent', mbox: `mailto:${AGENT_ID}` };
    const result = createAgent(AGENT_ID);
    expect(result).toEqual(expected);
  });

  test('creates Agent from string with name', () => {
    const expected: Agent = { objectType: 'Agent', mbox: `mailto:${AGENT_ID}`, name: AGENT_NAME };
    const result = createAgent(AGENT_ID, AGENT_NAME);
    expect(result).toEqual(expected);
  });

  test('creates Agent from IFI with mbox', () => {
    const expected: Agent = { objectType: 'Agent', mbox: AGENT_MBOX };
    const result = createAgent({ mbox: AGENT_MBOX });
    expect(result).toEqual(expected);
  });

  test('creates Agent from IFI with mbox and name', () => {
    const expected: Agent = { objectType: 'Agent', mbox: AGENT_MBOX, name: AGENT_NAME };
    const result = createAgent({ mbox: AGENT_MBOX }, AGENT_NAME);
    expect(result).toEqual(expected);
  });

  test('creates Agent from IFI with mbox_sha1sum', () => {
    const expected: Agent = { objectType: 'Agent', mbox_sha1sum: AGENT_MBOX_SHA1SUM };
    const result = createAgent({ mbox_sha1sum: AGENT_MBOX_SHA1SUM });
    expect(result).toEqual(expected);
  });

  test('creates Agent from IFI with mbox_sha1sum and name', () => {
    const expected: Agent = { objectType: 'Agent', mbox_sha1sum: AGENT_MBOX_SHA1SUM, name: AGENT_NAME };
    const result = createAgent({ mbox_sha1sum: AGENT_MBOX_SHA1SUM }, AGENT_NAME);
    expect(result).toEqual(expected);
  });

  test('creates Agent from IFI with account', () => {
    const expected: Agent = { objectType: 'Agent', account: AGENT_ACCOUNT };
    const result = createAgent({ account: AGENT_ACCOUNT });
    expect(result).toEqual(expected);
  });

  test('creates Agent from IFI with account and name', () => {
    const expected: Agent = { objectType: 'Agent', account: AGENT_ACCOUNT, name: AGENT_NAME };
    const result = createAgent({ account: AGENT_ACCOUNT }, AGENT_NAME);
    expect(result).toEqual(expected);
  });

  test('creates Agent from IFI with openid', () => {
    const expected: Agent = { objectType: 'Agent', openid: AGENT_OPENID };
    const result = createAgent({ openid: AGENT_OPENID });
    expect(result).toEqual(expected);
  });

  test('creates Agent from IFI with openid and name', () => {
    const expected: Agent = { objectType: 'Agent', openid: AGENT_OPENID, name: AGENT_NAME };
    const result = createAgent({ openid: AGENT_OPENID }, AGENT_NAME);
    expect(result).toEqual(expected);
  });

  test('throws an error if not passed a string or object', () => {
    expect(() => {
      // @ts-expect-error test invalid argument
      createAgent(123);
    }).toThrow();
  });

  test.todo('validate IFI argument');
  test.todo('validate IFI mbox property format');
  test.todo('validate IFI mbox_sha1sum property format');
  test.todo('validate IFI account property');
  test.todo('validate IFI openid property format');
});

const VERB_ID = 'http://adlnet.gov/expapi/verbs/experienced';
const VERB_DISPLAY_1 = { en: 'experienced' };
const VERB_DISPLAY_2 = { en: 'experienced', de: 'hat erfahren' };
const VERB_1: Verb = { id: VERB_ID, display: VERB_DISPLAY_1 };
const VERB_2: Verb = { id: VERB_ID, display: VERB_DISPLAY_2 };

describe('createVerb()', () => {
  test('creates a Verb from an id and uses last path element as english display ', () => {
    const expected: Verb = VERB_1;
    const result = createVerb(VERB_ID);
    expect(result).toEqual(expected);
  });

  test('creates a Verb from an id and display information', () => {
    const expected: Verb = VERB_2;
    const result = createVerb(VERB_ID, VERB_DISPLAY_2);
    expect(result).toEqual(expected);
  });

  test.todo('validate arguments');
});

const ACTIVITY_ID = 'http://some-activity.com';
const ACTIVITY_DEFINTION: ActivityDefinition = { name: { en: 'Some Activity' } };
const ACTIVITY_1: Activity = { objectType: 'Activity', id: ACTIVITY_ID };
// const ACTIVITY_WITH_DEFINITION: Activity = { objectType: 'Activity', id: ACTIVITY_ID, definition: ACTIVITY_DEFINTION };

describe('createActivity()', () => {
  test('creates an Activity from an id', () => {
    const expected: Activity = { objectType: 'Activity', id: ACTIVITY_ID };
    const result = createActivity(ACTIVITY_ID);
    expect(result).toEqual(expected);
  });

  test('creates an Activity from an id and a definition', () => {
    const expected: Activity = { objectType: 'Activity', id: ACTIVITY_ID, definition: ACTIVITY_DEFINTION };
    const result = createActivity(ACTIVITY_ID, ACTIVITY_DEFINTION);
    expect(result).toEqual(expected);
  });

  test.todo('validate arguments');
  test.todo(
    'creates an Activity from an id, a definition and extensions. validate merging of definition and extensions, when both are provided'
  );
});

const ACTOR_WITH_MBOX: Agent = { objectType: 'Agent', mbox: `mailto:${AGENT_ID}` };
const TIMESTAMP = new Date().toISOString();

describe('createAgentActivityStatement()', () => {
  test('creates a statement when passing string actor', () => {
    const expected: Statement = { actor: ACTOR_WITH_MBOX, verb: VERB_1, object: ACTIVITY_1, timestamp: TIMESTAMP };
    const result = createAgentActivityStatement(AGENT_ID, VERB_1, ACTIVITY_1, TIMESTAMP, false);
    expect(result).toEqual(expected);
  });

  test('creates a statement when passing IFI mbox actor', () => {
    const ifi = { mbox: AGENT_MBOX };
    const expected: Statement = { actor: ACTOR_WITH_MBOX, verb: VERB_1, object: ACTIVITY_1, timestamp: TIMESTAMP };
    const result = createAgentActivityStatement(ifi, VERB_1, ACTIVITY_1, TIMESTAMP, false);
    expect(result).toEqual(expected);
  });

  test('creates a statement when passing Agent actor ', () => {
    const expected: Statement = { actor: ACTOR_WITH_MBOX, verb: VERB_1, object: ACTIVITY_1, timestamp: TIMESTAMP };
    const result = createAgentActivityStatement(ACTOR_WITH_MBOX, VERB_1, ACTIVITY_1, TIMESTAMP, false);
    expect(result).toEqual(expected);
  });

  test('creates a statement when passing string object ', () => {
    const expected: Statement = { actor: ACTOR_WITH_MBOX, verb: VERB_1, object: ACTIVITY_1, timestamp: TIMESTAMP };
    const result = createAgentActivityStatement(ACTOR_WITH_MBOX, VERB_1, ACTIVITY_ID, TIMESTAMP, false);
    expect(result).toEqual(expected);
  });

  test('creates a statement when passing Activity object ', () => {
    const expected: Statement = { actor: ACTOR_WITH_MBOX, verb: VERB_1, object: ACTIVITY_1, timestamp: TIMESTAMP };
    const result = createAgentActivityStatement(ACTOR_WITH_MBOX, VERB_1, ACTIVITY_1, TIMESTAMP, false);
    expect(result).toEqual(expected);
  });

  test('creates a statement with a new timestamp', () => {
    const expected = { actor: ACTOR_WITH_MBOX, verb: VERB_1, object: ACTIVITY_1 };
    const result = createAgentActivityStatement(ACTOR_WITH_MBOX, VERB_1, ACTIVITY_1);
    expect(result).toEqual(expect.objectContaining(expected));
    expect(result).toHaveProperty('timestamp'); // TODO: validate timestamp format
    expect(typeof result.timestamp).toBe('string');
  });

  test('creates a statement when passing a timestamp', () => {
    const expected = { actor: ACTOR_WITH_MBOX, verb: VERB_1, object: ACTIVITY_1, timestamp: TIMESTAMP };
    const result = createAgentActivityStatement(ACTOR_WITH_MBOX, VERB_1, ACTIVITY_1, TIMESTAMP);
    expect(result).toEqual(expect.objectContaining(expected));
  });

  test('creates a statement with a new id', () => {
    const expected = { actor: ACTOR_WITH_MBOX, verb: VERB_1, object: ACTIVITY_1 };
    const result = createAgentActivityStatement(ACTOR_WITH_MBOX, VERB_1, ACTIVITY_1, null, true);
    expect(result).toEqual(expect.objectContaining(expected));
    expect(result).toHaveProperty('id'); // TODO: validate id format
    expect(typeof result.id).toBeTruthy();
  });

  test('creates a statement without an id', () => {
    const expected = { actor: ACTOR_WITH_MBOX, verb: VERB_1, object: ACTIVITY_1 };
    const result = createAgentActivityStatement(ACTOR_WITH_MBOX, VERB_1, ACTIVITY_1, null, false);
    expect(result).toEqual(expect.objectContaining(expected));
    expect(result).not.toHaveProperty('id');
    expect(typeof result.id).toBeTruthy();
  });

  test('creates a statement with a new id (using default value)', () => {
    const expected = { actor: ACTOR_WITH_MBOX, verb: VERB_1, object: ACTIVITY_1 };
    const result = createAgentActivityStatement(ACTOR_WITH_MBOX, VERB_1, ACTIVITY_1);
    expect(result).toEqual(expect.objectContaining(expected));
    expect(result).toHaveProperty('id'); // TODO: validate id format
    expect(typeof result.id).toBeTruthy();
  });

  test.todo('validate arguments');
  test.todo('use group actor');
  test.todo('validate format of id');
  test.todo('validate format of timestamp');
});

const CONTEXT_UUID = 'd6e4b410-c376-470d-a347-c4ebad0cf232';

describe('createRegistrationContext()', () => {
  test('creates a context, when passed a string', () => {
    const expected: Context = { registration: CONTEXT_UUID };
    const result = createRegistrationContext(CONTEXT_UUID);
    expect(result).toEqual(expected);
  });

  test('creates a context with a new UUID, when passed nothing', () => {
    const result = createRegistrationContext(CONTEXT_UUID);
    expect(result).toHaveProperty('registration');
    expect(typeof result.registration).toBe('string'); // TODO: validate UUID format
  });
});
