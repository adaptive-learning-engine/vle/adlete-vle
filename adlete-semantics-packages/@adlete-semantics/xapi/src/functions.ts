import type { Actor, LanguageMap, Statement } from '@xapi/xapi';
import { v4 as uuidV4 } from 'uuid';

import { ACTOR_IFI_PROPERTIES, ActorIFI } from './types';

export function serializeActorIFI(actor: Actor): string {
  // TODO: support custom order
  const serialized: ActorIFI = {};

  // we're only serializing the IFI, which are part of the actor
  ACTOR_IFI_PROPERTIES.forEach((prop) => {
    // Weird typescript bug, when not converting to any
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    if (prop in actor) serialized[prop] = actor[prop] as any;
  });

  return JSON.stringify(serialized);
}

export function serializeActor(actor: Actor): string {
  return JSON.stringify(actor);
}

export function deserializeActor(serialized: string): Actor {
  return JSON.parse(serialized);
}

export function isSerializedActor(serialized: string): boolean {
  // TODO: better check
  return serialized[0] === '{';
}

export function isSerializedActorIFI(serialized: string): boolean {
  // TODO: better check
  return serialized[0] === '{';
}

export function deserializeActorIFI(serialized: string): ActorIFI {
  return JSON.parse(serialized);
}

export function getActorIFIasString(actor: Actor): string {
  // TODO: support custom order
  if (!actor.account) return actor.openid || actor.mbox || actor.mbox_sha1sum;
  return JSON.stringify(actor.account);
}

export function addMissingId(statement: Statement): Statement {
  return 'id' in statement ? statement : { ...statement, id: uuidV4() };
}

export function addMissingIds(statements: Statement[]): Statement[] {
  return statements.map((statement) => ('id' in statement ? statement : { ...statement, id: uuidV4() }));
}

export function getDisplayFromIRI(id: string, language = 'en'): LanguageMap {
  const lastIndex = id.lastIndexOf('/');
  if (lastIndex === -1 || lastIndex === id.length - 1) return null;

  const displayName = id.substring(lastIndex + 1);
  return { [language]: displayName };
}
