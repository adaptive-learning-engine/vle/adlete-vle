import type { Activity, ActivityDefinition, Agent, Context, Extensions, LanguageMap, Statement, Verb } from '@xapi/xapi';
import { v4 as uuidV4 } from 'uuid';

import { getDisplayFromIRI } from './functions';
import type { ActorIFI } from './types';

export function createAgent(idOrIFI: string | ActorIFI, name?: string): Agent {
  let agent: Partial<Agent>;

  if (typeof idOrIFI === 'string') {
    agent = { objectType: 'Agent', mbox: `mailto:${idOrIFI}` };
  } else if (typeof idOrIFI === 'object') {
    // TODO don't copy extra properties
    agent = { ...idOrIFI, objectType: 'Agent' };
  } else {
    throw new Error(`'idOrIFI must be of type string or object. Received ${typeof idOrIFI}!`);
  }

  agent.objectType = 'Agent';
  if (name) agent.name = name;

  return agent as Agent;
}

export function createVerb(id: string, display?: LanguageMap): Verb {
  display = display || getDisplayFromIRI(id);

  const result: Partial<Verb> = { id };
  if (display) result.display = display;

  return result as Verb;
}

export function createActivity(id: string, definition?: ActivityDefinition, extensions?: Extensions): Activity {
  const result: Activity = { id, objectType: 'Activity' };

  if (definition || extensions) {
    result.definition = definition || { extensions };
  }

  return result;
}

// TODO: name
export function createAgentActivityStatement(
  actor: string | ActorIFI | Agent,
  verb: Verb,
  object: string | Activity,
  timestamp?: string,
  createId: boolean = true
): Statement {
  if (typeof actor === 'string' || !('name' in actor)) actor = createAgent(actor);
  if (typeof object === 'string') object = createActivity(object);

  const statement: Statement = { actor: actor as Agent, verb, object };
  if (createId) statement.id = uuidV4();

  statement.timestamp = timestamp || new Date().toISOString();

  return statement;
}

export function createRegistrationContext(registration?: string): Context {
  registration = registration || uuidV4();
  return { registration };
}
