import type { Agent } from '@xapi/xapi';

export const ACTOR_IFI_PROPERTIES = ['mbox', 'mbox_sha1sum', 'account', 'openid'] as const;

export type ActorIFIProperties = (typeof ACTOR_IFI_PROPERTIES)[number];

export type ActorIFI = Pick<Agent, ActorIFIProperties>;
