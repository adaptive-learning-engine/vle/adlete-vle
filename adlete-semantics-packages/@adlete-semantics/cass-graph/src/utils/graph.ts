import type { Attributes } from 'graphology-types';

// TODO: move to another module?
// TODO: use Typescript helper types
export interface IGraphConstructor<
  NodeAttributes extends Attributes = Attributes,
  EdgeAttributes extends Attributes = Attributes,
  GraphAttributes extends Attributes = Attributes,
> {
  // TODO: use original functions, but with void return type
  // addNode: AbstractGraph<NodeAttributes, EdgeAttributes, GraphAttributes>['addNode'];
  // addEdgeWithKey: AbstractGraph<NodeAttributes, EdgeAttributes, GraphAttributes>['addEdgeWithKey'];
  // setAttribute: AbstractGraph<NodeAttributes, EdgeAttributes, GraphAttributes>['setAttribute'];

  addNode(node: unknown, attributes?: NodeAttributes): void;
  addDirectedEdgeWithKey(edge: unknown, source: unknown, target: unknown, attributes?: EdgeAttributes): void;
  setAttribute<AttributeName extends keyof GraphAttributes>(name: AttributeName, value: GraphAttributes[AttributeName]): void;
}
