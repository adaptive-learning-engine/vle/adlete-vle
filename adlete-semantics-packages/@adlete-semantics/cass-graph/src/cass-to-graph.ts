import Graph from 'graphology';
import type AbstractGraph from 'graphology-types';

import { getTrimmedId, ICassNodeObject, ICompetency, IFramework, IRelation, trimVersionFromUrl } from '@adlete-semantics/cass-json-ld';
import { asArraySafe, nodeListToRecord } from '@adlete-semantics/json-ld';

import { IGraphConstructor } from './utils/graph';

export interface ICassObjectsToGraphOptions {
  frameworkIds?: Set<string>;
}

export interface ICassGraphAttributes {
  frameworks: IFramework[];
}

export type ICASSGraph = AbstractGraph<ICompetency, IRelation, ICassGraphAttributes>;

export function cassObjectsToGenericGraph(
  cassObjects: Record<string, ICassNodeObject>,
  constructor: IGraphConstructor<ICompetency, IRelation, ICassGraphAttributes>,
  options?: ICassObjectsToGraphOptions
) {
  const cassObjectArr = Object.values(cassObjects);

  // TODO support expanded versions
  const frameworks: IFramework[] = cassObjectArr
    .filter((obj) => obj['@type'] === 'Framework')
    .filter((obj) => {
      if (!options?.frameworkIds) return true;
      return options.frameworkIds.has(getTrimmedId(obj));
    });

  // adding competencies
  frameworks.forEach((framework) => {
    asArraySafe(framework.competency).forEach((competencyId) => {
      const id = trimVersionFromUrl(competencyId);
      constructor.addNode(id, cassObjects[id]);
    });
  });

  frameworks.forEach((framework) => {
    asArraySafe(framework.relation).forEach((relationId) => {
      const id = trimVersionFromUrl(relationId);
      const relation: IRelation = cassObjects[id] as IRelation;
      const source = trimVersionFromUrl(relation.source);
      const target = trimVersionFromUrl(relation.target);
      constructor.addDirectedEdgeWithKey(id, source, target, relation);
    });
  });

  constructor.setAttribute('frameworks', frameworks);
}

export function cassObjectsToGraph(
  cassObjects: Record<string, ICassNodeObject> | ICassNodeObject[],
  options?: ICassObjectsToGraphOptions
): ICASSGraph {
  if (Array.isArray(cassObjects)) {
    cassObjects = nodeListToRecord(cassObjects, trimVersionFromUrl);
  }

  const graph = new Graph<ICompetency, IRelation, ICassGraphAttributes>();
  cassObjectsToGenericGraph(cassObjects, graph, options);
  return graph;
}
