import { getAllIdVariants, getIdComparer, IFramework } from '@adlete-semantics/cass-json-ld';
import { asArraySafe } from '@adlete-semantics/json-ld';

export function removeCompetenciesFromFrameworkObject(framework: IFramework, competencyIds: string[]): boolean {
  const ids = getAllIdVariants(competencyIds);
  const comparer = getIdComparer(ids);

  const competencyArr = asArraySafe(framework.competency);
  framework.competency = competencyArr.filter((id) => !comparer(id));

  return competencyArr.length !== framework.competency.length;
}

export function removeRelationsFromFrameworkObject(framework: IFramework, relationIds: string[]): boolean {
  const ids = getAllIdVariants(relationIds);
  const comparer = getIdComparer(ids);

  const relationArr = asArraySafe(framework.relation);
  framework.relation = relationArr.filter((id) => !comparer(id));

  return relationArr.length !== framework.relation.length;
}
