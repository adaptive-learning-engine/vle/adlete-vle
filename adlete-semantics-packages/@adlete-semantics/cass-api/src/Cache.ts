import { getIdWithoutVersion, ICassNodeObject, trimVersionFromUrl } from '@adlete-semantics/cass-json-ld';

export class Cache<T extends ICassNodeObject> {
  protected cache: Record<string, T>;

  public constructor() {
    this.cache = {};
  }

  public update(obj: T) {
    const shortId = getIdWithoutVersion(obj);
    this.cache[shortId] = obj;
  }

  public get<TObj extends T>(id: string): TObj {
    const shortId = trimVersionFromUrl(id);
    return this.cache[shortId] as TObj;
  }

  public remove(id: string) {
    const shortId = trimVersionFromUrl(id);
    if (shortId in this.cache) delete this.cache[shortId];
  }

  public getIds(): string[] {
    return Object.keys(this.cache);
  }
}
