import { IFramework, trimVersionFromUrl } from '@adlete-semantics/cass-json-ld';
import { asArraySafe } from '@adlete-semantics/json-ld';

export function collectRelationIds(frameworks: IFramework[]): Set<string> {
  const ids = new Set<string>();
  frameworks.forEach((framework) => asArraySafe(framework.relation).forEach((id) => ids.add(trimVersionFromUrl(id))));
  return ids;
}

export function collectCompetencyIds(frameworks: IFramework[]): Set<string> {
  const ids = new Set<string>();
  frameworks.forEach((framework) => asArraySafe(framework.competency).forEach((id) => ids.add(trimVersionFromUrl(id))));
  return ids;
}

export function intersectionArr<T>(a: Set<T>, b: Set<T>): T[] {
  return [...a].filter((x) => !b.has(x));
}
