import { v4 as uuidV4 } from 'uuid';

import {
  getIdWithoutVersion,
  ICompetency,
  IFramework,
  IRelation,
  trimVersionFromUrl,
  updateNodeVersion,
} from '@adlete-semantics/cass-json-ld';
import { asArraySafe } from '@adlete-semantics/json-ld';

export function createEmptyCompetencyObject(endPoint: string, data?: Partial<ICompetency>): ICompetency {
  const id = uuidV4();
  const version = Date.now();
  const dateString = new Date().toISOString();

  const pre = { name: { '@language': 'en-us', '@value': id } };
  const post = {
    '@context': 'https://schema.cassproject.org/0.4',
    '@id': `${endPoint}/data/schema.cassproject.org.0.4.Competency/${id}/${version}`,
    '@type': 'Competency',
    'schema:dateCreated': dateString,
    'schema:dateModified': dateString,
  };

  return { ...pre, ...data, ...post };
}

// TODO: make source, target and relationType required
export function createRelationObject(endPoint: string, data: Partial<IRelation>): IRelation {
  const id = uuidV4();
  const version = Date.now();
  const dateString = new Date().toISOString();

  const pre = { name: { '@language': 'en-us', '@value': id } };
  const post = {
    '@context': 'https://schema.cassproject.org/0.4',
    '@id': `${endPoint}/data/schema.cassproject.org.0.4.Relation/${id}/${version}`,
    '@type': 'Relation',
    'schema:dateCreated': dateString,
    'schema:dateModified': dateString,
  };

  return { ...pre, ...data, ...post } as IRelation;
}

export function addCompetenciesToFramework(framework: IFramework, competencies: ICompetency[] | string[]): IFramework {
  const dateString = new Date().toISOString();

  const frameworkCopy = { ...framework, 'schema:dateModified': dateString };
  const competencyList = [...asArraySafe(frameworkCopy.competency)];
  const competencySet = new Set(competencyList);

  competencies.forEach((competency) => {
    const competencyId = typeof competency === 'string' ? trimVersionFromUrl(competency) : getIdWithoutVersion(competency);
    if (!competencySet.has(competencyId)) {
      competencyList.push(competencyId);
    }
  });

  frameworkCopy.competency = competencyList;
  updateNodeVersion(frameworkCopy, true);
  return frameworkCopy;
}

export function addRelationsToFramework(framework: IFramework, relations: ICompetency[] | string[]): IFramework {
  const dateString = new Date().toISOString();

  const frameworkCopy = { ...framework, 'schema:dateModified': dateString };
  const relationList = [...asArraySafe(frameworkCopy.relation)];
  const relationSet = new Set(relationList);

  relations.forEach((relation) => {
    const relationId = typeof relation === 'string' ? trimVersionFromUrl(relation) : getIdWithoutVersion(relation);
    if (!relationSet.has(relationId)) {
      relationList.push(relationId);
    }
  });

  frameworkCopy.relation = relationList;
  updateNodeVersion(frameworkCopy, true);
  return frameworkCopy;
}
