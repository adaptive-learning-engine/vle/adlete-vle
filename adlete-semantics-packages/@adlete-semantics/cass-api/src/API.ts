import { NodeObject } from 'jsonld';

import {
  createUpdatedCopy,
  filterRelationsBySourceAndTarget,
  getAllIdVariants,
  getIdComparer,
  getIds,
  getIdWithoutVersion,
  ICassNodeObject,
  ICassObjectCollection,
  ICompetency,
  IFramework,
  IRelation,
  isFramework,
} from '@adlete-semantics/cass-json-ld';
import { asString } from '@adlete-semantics/json-ld';

import { Cache } from './Cache';
import { addCompetenciesToFramework, addRelationsToFramework, createEmptyCompetencyObject, createRelationObject } from './create';
import { removeCompetenciesFromFrameworkObject, removeRelationsFromFrameworkObject } from './remove';
import { collectCompetencyIds, collectRelationIds } from './utils';

const URL_REGEX = /(https?:\/\/)([da-z.-]+).([a-z]{2,6})([/w.-]*)*\/?/;

function isURL(url: string): boolean {
  return URL_REGEX.test(url);
}

export function isCassType(node: NodeObject, type: string): boolean {
  return node['@type'] !== type;
}

export function assertCassType(node: NodeObject, type: string, uid: string): void {
  if (node['@type'] !== type) throw new Error(`Given uid '${uid}' is not a ${type}, but a '${node['@type']}'!`);
}

export async function handleResponseAs<T = unknown>(response: Response): Promise<T> {
  if (!response.ok) throw Error(response.statusText);

  return (await response.json()) as T;
}

export async function fetchAs<T = unknown>(input: RequestInfo | URL, init?: RequestInit): Promise<T> {
  const response = await fetch(input, init);
  const obj = await handleResponseAs<T>(response);
  return obj;
}

export type RelationRemovalMode = 'keep' | 'remove' | 'destroy';

export interface IAddRelationsOptions {
  competencyAddition?: boolean;
  throwOnMissing?: boolean;
}

export interface IRemoveCompetenciesOptions {
  relationRemoval?: RelationRemovalMode;
  throwOnMissing?: boolean;
}

export interface IDeleteRelationsOptions {
  removeFromFrameworks: boolean;
}

export interface IDeleteCompetenciesOptions {
  removeFromFrameworks: boolean;
  deleteRelations: boolean;
}

// TODO: user credentials
export class API {
  public defaultEndpoint: string;

  protected cache: Cache<ICassNodeObject>;

  public constructor(defaultEndPoint: string) {
    // TODO: handle trailing slashs
    this.defaultEndpoint = defaultEndPoint;
    this.cache = new Cache();
  }

  // #region Cache

  protected getFrameworksFromCache(): IFramework[] {
    return this.cache
      .getIds()
      .map((id) => this.cache.get(id))
      .filter(isFramework);
  }

  protected async updateCacheFromServerFrameworks(frameworkIds?: string[]): Promise<ICassObjectCollection> {
    if (!frameworkIds) {
      const currentFrameworks = this.getFrameworksFromCache();
      frameworkIds = getIds(currentFrameworks);
    }

    return this.fetchFullFrameworks(frameworkIds);
  }

  // #endregion

  // #region Fetch

  protected async fetchDataInternal<T extends ICassNodeObject = ICassNodeObject>(uri: string): Promise<T> {
    const obj = await fetchAs<T>(uri);
    this.cache.update(obj);
    return obj;
  }

  protected async fetchDataBulkInternal<T extends ICassNodeObject = ICassNodeObject>(uris: string[]): Promise<T[]> {
    // TODO: use CASS bulk functions
    const objects = await Promise.all(uris.map((uri) => this.fetchDataInternal<T>(uri)));
    return objects;
  }

  public async fetchData<T extends ICassNodeObject = ICassNodeObject>(id: string): Promise<T> {
    const url = isURL(id) ? id : `${this.defaultEndpoint}/data/${id}`;
    return this.fetchDataInternal(url);
  }

  public async fetchDataBulk<T extends ICassNodeObject = ICassNodeObject>(ids: string[]): Promise<T[]> {
    const urls = ids.map((id) => (isURL(id) ? id : `${this.defaultEndpoint}/data/${id}`));
    return this.fetchDataBulkInternal(urls);
  }

  public async fetchDataOfType<T extends ICassNodeObject>(id: string, type: string): Promise<T> {
    const url = isURL(id) ? id : `${this.defaultEndpoint}/data/${type}/${id}`;
    return this.fetchDataInternal(url);
  }

  public async fetchDataOfTypeBulk<T extends ICassNodeObject>(ids: string[], type: string): Promise<T[]> {
    const urls = ids.map((id) => (isURL(id) ? id : `${this.defaultEndpoint}/data/${type}/${id}`));
    return this.fetchDataBulkInternal(urls);
  }

  public async fetchFramework(id: string): Promise<IFramework> {
    const data = await this.fetchDataOfType<IFramework>(id, 'schema.cassproject.org.0.4.Framework');
    assertCassType(data, 'Framework', id);
    return data;
  }

  public async fetchFrameworks(ids: string[]): Promise<IFramework[]> {
    const frameworks = await this.fetchDataOfTypeBulk<IFramework>(ids, 'schema.cassproject.org.0.4.Framework');
    frameworks.forEach((framework, i) => assertCassType(framework, 'Framework', ids[i]));
    return frameworks;
  }

  public async fetchCompetency(id: string): Promise<ICompetency> {
    const data = await this.fetchDataOfType<ICompetency>(id, 'schema.cassproject.org.0.4.Competency');
    assertCassType(data, 'Competency', id);
    return data;
  }

  public async fetchCompetencies(ids: string[]): Promise<ICompetency[]> {
    const competencies = await this.fetchDataOfTypeBulk<ICompetency>(ids, 'schema.cassproject.org.0.4.Competency');
    competencies.forEach((competency, i) => assertCassType(competency, 'Competency', ids[i]));
    return competencies;
  }

  public async fetchRelation(id: string): Promise<IRelation> {
    const data = await this.fetchDataOfType<IRelation>(id, 'schema.cassproject.org.0.4.Relation');
    assertCassType(data, 'Relation', id);
    return data;
  }

  public async fetchRelations(ids: string[]): Promise<IRelation[]> {
    const relations = await this.fetchDataOfTypeBulk<IRelation>(ids, 'schema.cassproject.org.0.4.Relation');
    relations.forEach((relation, i) => assertCassType(relation, 'Relation', ids[i]));
    return relations;
  }

  public async fetchFullFrameworks(uids: string[]): Promise<ICassObjectCollection> {
    const frameworks = await this.fetchFrameworks(uids);

    const competencyIds = [...collectCompetencyIds(frameworks)];
    const relationIds = [...collectRelationIds(frameworks)];

    // TODO: better bulk fetching using multiGet?
    const competencies = await this.fetchCompetencies(competencyIds);
    const relations = await this.fetchRelations(relationIds);

    return {
      frameworks,
      competencies,
      relations,
    };
  }

  // #endregion

  // #region Post

  public async postNode<T extends ICassNodeObject>(node: T): Promise<void> {
    const url = asString(node['@id']); // TODO: handle external ids

    const formData = new FormData();
    formData.append('data', JSON.stringify(node));

    await fetch(url, {
      method: 'POST',
      // headers: { 'Content-Type': null },
      body: formData,
    });

    this.cache.update(node);
  }

  public async postNodes<T extends ICassNodeObject>(nodes: T[]): Promise<void> {
    // TODO: use CASS bulk functions
    await Promise.all(nodes.map((node) => this.postNode(node)));
  }

  // #endregion

  // #region Delete

  public async deleteNode<T extends ICassNodeObject>(node: T): Promise<void> {
    const url = getIdWithoutVersion(node); // TODO: handle external ids

    await this.deleteNodeById(url);

    // const formData = new FormData();
    // formData.append('data', JSON.stringify(node));
  }

  public async deleteNodes<T extends ICassNodeObject>(nodes: T[]): Promise<void> {
    // TODO: use cass bulk functions if available
    await Promise.all(nodes.map((node) => this.deleteNode(node)));
  }

  public async deleteNodeById(uri: string): Promise<void> {
    await fetch(uri, {
      method: 'DELETE',
      // headers: { 'Content-Type': null },
      // body: formData,
    });

    this.cache.remove(uri);
  }

  public async deleteNodesById(ids: string[]): Promise<void> {
    // TODO: use cass bulk functions if available
    await Promise.all(ids.map((id) => this.deleteNodeById(id)));
  }

  public async deleteRelationsById(ids: string[], options: IDeleteRelationsOptions): Promise<void> {
    if (options?.removeFromFrameworks) {
      const collection = await this.updateCacheFromServerFrameworks();

      const updatedFrameworks = collection.frameworks
        .map((framework) => {
          const copy = createUpdatedCopy(framework);
          const relationsRemoved = removeRelationsFromFrameworkObject(copy, ids);
          return relationsRemoved ? copy : null; // no update necessary if nothing was removed
        })
        .filter((framework) => framework !== null);

      await this.postNodes(updatedFrameworks);
    }

    await this.deleteNodesById(ids);
  }

  public async deleteCompetenciesById(
    ids: string[],
    options: IDeleteCompetenciesOptions = { removeFromFrameworks: true, deleteRelations: true }
  ): Promise<void> {
    if (options?.removeFromFrameworks || options?.deleteRelations) {
      const collection = await this.updateCacheFromServerFrameworks();

      // --- removing from frameworks
      if (options?.removeFromFrameworks) {
        const updatedFrameworks = collection.frameworks
          .map((framework) => {
            const copy = createUpdatedCopy(framework);
            const competenciesRemoved = removeCompetenciesFromFrameworkObject(copy, ids);
            const relationsRemoved = this.removeCompetencyRelationsFromFrameworkObject(copy, ids);
            return competenciesRemoved || relationsRemoved ? copy : null; // no update necessary if nothing was removed
          })
          .filter((framework) => framework !== null);

        await this.postNodes(updatedFrameworks);
      }

      // --- deleting relations
      if (options?.deleteRelations) {
        const relations = this.getRelationsContainingCompetencies(collection.frameworks, ids);
        const relationIds = getIds(relations);

        // we don't alter the frameworks anymore, because either it was already done in `removeFromFrameworks` above
        // or is unwanted
        await this.deleteRelationsById(relationIds, { removeFromFrameworks: false });
      }
    }

    await this.deleteNodesById(ids);
  }

  // #endregion

  // #region Framework manipulation

  public async addRelationsToFramework(
    frameworkId: string,
    relationIds: string[],
    options: IAddRelationsOptions = { competencyAddition: true, throwOnMissing: true }
  ): Promise<IFramework> {
    // ensure we have the most current version of the framework
    let framework = await this.fetchFramework(frameworkId);

    const competencyAddition = options.competencyAddition ?? true;

    // add competencies to the framework
    if (competencyAddition) {
      // TODO: try cache first as source and target are unlikely to change
      const relations = await this.fetchRelations(relationIds);

      const relationCompetencies: string[] = [];
      relations.forEach((relation) => {
        relationCompetencies.push(relation.source);
        relationCompetencies.push(relation.target);
      });

      // TODO: prevent double copy
      // function takes care of duplicate or existing competencies
      framework = addCompetenciesToFramework(framework, relationCompetencies);
    }

    const newFramework = addRelationsToFramework(framework, relationIds);
    await this.postNode(newFramework);
    return newFramework;
  }

  public async removeCompetenciesFromFramework(
    frameworkId: string,
    competencyIds: string[],
    options: IRemoveCompetenciesOptions = { relationRemoval: 'remove', throwOnMissing: true }
  ): Promise<IFramework> {
    // ensure we have the most current version of the framework
    let framework = await this.fetchFramework(frameworkId);

    const relationRemoval = options?.relationRemoval || 'remove';

    // if edges are destroyed, deleteRelationsById will update and post frameworks
    if (relationRemoval === 'destroy') {
      const relations = this.getRelationsContainingCompetencies([framework], competencyIds);
      const relationIds = getIds(relations);
      await this.deleteRelationsById(relationIds, { removeFromFrameworks: true });

      // getting updated version from cache
      framework = this.cache.get(getIdWithoutVersion(framework));
    }

    const frameworkCopy = createUpdatedCopy(framework);

    // remove competencies
    const competenciesRemoved = removeCompetenciesFromFrameworkObject(frameworkCopy, competencyIds);

    if (options?.throwOnMissing && !competenciesRemoved) {
      // TODO: provid id(s) of missing competencies
      throw new Error(`One of the given competencies was not part of framework '${getIdWithoutVersion(framework)}'`);
    }

    // removing relations if necessary
    if (relationRemoval === 'remove') this.removeCompetencyRelationsFromFrameworkObject(frameworkCopy, competencyIds);

    // updating
    await this.postNode(frameworkCopy);

    return frameworkCopy;
  }

  // TODO: pass cache instance
  // protected filterRelationIdsBySourceAndTarget(relations: IFramework['relation'], comparer: IdComparer, include: boolean): string[] {
  //   const relationsArr = asArraySafe(relations);
  //   return relationsArr.filter((relationId) => {
  //     // TODO: provide option to get newest relation data from server
  //     const relation = this.cache.get<IRelation>(relationId);
  //     const containsSourceOrTarget = comparer(relation.source) || comparer(relation.target);
  //     return include ? containsSourceOrTarget : !containsSourceOrTarget;
  //   });
  // }

  // TODO: pass cache instance
  protected getRelationsContainingCompetencies(frameworks: IFramework[], competencyIds: string[]): IRelation[] {
    const allRelationIds = [...collectRelationIds(frameworks)];
    const relations = allRelationIds.map((relationId) => this.cache.get(relationId) as IRelation);
    const comparer = getIdComparer(getAllIdVariants(competencyIds));
    const filteredRelations = filterRelationsBySourceAndTarget(relations, comparer, true);
    return filteredRelations;
  }

  protected removeCompetencyRelationsFromFrameworkObject(framework: IFramework, competencyIds: string[]): boolean {
    const relations = this.getRelationsContainingCompetencies([framework], competencyIds);
    const relationIds = getIds(relations);
    return removeRelationsFromFrameworkObject(framework, relationIds);
  }

  public async removeCompetencyFromFramework(frameworkId: string, competencyId: string): Promise<IFramework> {
    return this.removeCompetenciesFromFramework(frameworkId, [competencyId]);
  }

  public async removeRelationsFromFramework(frameworkId: string, relationIds: string[]): Promise<IFramework> {
    // ensure we have the most current version of the framework
    const framework = await this.fetchFramework(frameworkId);

    // update
    const frameworkCopy = createUpdatedCopy(framework);
    await removeRelationsFromFrameworkObject(frameworkCopy, relationIds);

    // updating
    await this.postNode(frameworkCopy);

    return frameworkCopy;
  }

  // #endregion

  // #region CASS Object Creation

  public createCompetencyObject(data?: Partial<ICompetency>, endPoint: string = this.defaultEndpoint): ICompetency {
    return createEmptyCompetencyObject(endPoint, data);
  }

  public createRelationObject(data: Partial<IRelation>, endPoint: string = this.defaultEndpoint): IRelation {
    return createRelationObject(endPoint, data);
  }

  // #endregion
}
