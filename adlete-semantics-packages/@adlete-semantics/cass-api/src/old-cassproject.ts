// import 'cassproject';

// async function setupRepo(): Promise<typeof EcRepository> {
//   const repo = new EcRepository();
//   repo.selectedServer = 'https://cass.fki.htw-berlin.de/api';

//   console.log(repo);
//   return repo;
// }

// async function login(user: string, pw: string, repo: typeof EcRepository): Promise<typeof EcIdentityManager> {
//   const identMan = new EcRemoteIdentityManager();
//   identMan.server = repo.selectedServer;
//   console.log('configure');
//   await identMan.configureFromServer();
//   console.log('startLogin');
//   await identMan.startLogin(user, pw);
//   console.log('fetch');
//   return identMan.fetch();
// }

// function getOwnerId(identMan: typeof EcIdentityManager): string {
//   return identMan.ids[0].ppk.toPk();
// }

// export async function cassTest() {
//   const repo = await setupRepo();
//   const identMan = await login('newuser', 'test', repo);
//   console.log('ids', identMan.ids);
//   console.log('ids', EcIdentityManager.ids);
//   const ownerId = getOwnerId(identMan);
//   console.log(ownerId);
//   return;

//   const framework = await global.EcFramework.get(
//     'https://cass.fki.htw-berlin.de/api/data/schema.cassproject.org.0.4.Framework/db98cd4e-c455-4ae9-9463-219fd6fde8a1'
//   );
//   console.log(framework);

//   // const competency = await global.EcCompetency.get(framework.competency[1]);
//   // console.log(competency);
//   // console.log(competency.customprop);
//   // // competency.customprop.push('foo');
//   // // competency.Foobar = 'blubb';
//   // // console.log(competency);
//   // // await competency.save();

//   // const newCompetency = new EcCompetency();
//   // newCompetency.generateId(repo.selectedServer);
//   // newCompetency.name = 'My new competence. BAM';
//   // newCompetency.customprop = ['new prop'];
//   // newCompetency.owner = ownerId;
//   // newCompetency.description = 'Subject can create competencies using CASS in the Javascript Console.';
//   // await newCompetency.save();

//   // framework.addCompetency(newCompetency.shortId());
//   // framework.save(console.log, console.log);
// }
