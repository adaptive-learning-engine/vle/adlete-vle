export class OersiBackendError extends Error {
  public status: number;

  public statusText: string;

  public bodyText: string;

  public constructor(status: number, statusText: string, bodyText: string) {
    const message = `(${status}) ${statusText}\n\n${bodyText}`;
    super(message);

    this.name = 'OersiBackendError';

    this.status = status;
    this.statusText = statusText;
    this.bodyText = bodyText;
  }
}
