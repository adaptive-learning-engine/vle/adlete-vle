import type { ILearningResourceMeta } from '@adlete-semantics/amb';

import { OersiBackendError } from './errors';

export interface IOersiAPIOptions {
  backendEndPoint?: string;
  frontendEndPoint?: string;
  preferFrontend?: boolean;
  userName?: string;
  password?: string;
}

function convertIdForURL(id: string): string {
  return encodeURIComponent(btoa(id));
}

function addAuthentication(headers: Record<string, string>, options: IOersiAPIOptions): void {
  if (options.password && options.userName) {
    headers.Authorization = `Basic ${btoa(`${options.userName}:${options.password}`)}`;
  }
}

async function handleResonseError(response: Response): Promise<void> {
  if (response.status !== 200) {
    const bodyText = await response.text();
    throw new OersiBackendError(response.status, response.statusText, bodyText);
  }
}

export class OersiAPI {
  protected options: IOersiAPIOptions;

  protected metaBackendEndpoint: string;

  public constructor(options: IOersiAPIOptions) {
    this.options = options;
    this.metaBackendEndpoint = `${options.backendEndPoint}/api/metadata/`;
  }

  public async getMeta(id: string, useFrontend = true): Promise<ILearningResourceMeta> {
    if (this.options.frontendEndPoint && (useFrontend || (useFrontend === undefined && this.options.preferFrontend))) {
      return this.getMetaFromFrontend(id);
    }
    return this.getMetaFromBackend(id);
  }

  protected async getMetaFromBackend(id: string): Promise<ILearningResourceMeta> {
    const idBase64 = convertIdForURL(id);
    const url = this.metaBackendEndpoint + idBase64;

    const request: RequestInit = { headers: {} };
    addAuthentication(request.headers as Record<string, string>, this.options);
    const response = await fetch(url, request);
    handleResonseError(response);
    const json = await response.json();
    return json;
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  protected async getMetaFromFrontend(id: string): Promise<ILearningResourceMeta> {
    throw new Error('Not implemented');
  }

  public async setMeta(meta: ILearningResourceMeta): Promise<void> {
    const url = this.metaBackendEndpoint;
    const request: RequestInit = { method: 'POST', headers: { 'Content-Type': 'application/json' }, body: JSON.stringify(meta) };
    addAuthentication(request.headers as Record<string, string>, this.options);

    const response = await fetch(url, request);

    handleResonseError(response);
    // const json = await response.json();
  }
}
