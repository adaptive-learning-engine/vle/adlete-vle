export const SCHEMA_VERSIONS = ['0.1', '0.2', '0.3', '0.4'];

export const SCHEMA_URLS: Record<string, string> = {
  '0.1': 'https://schema.cassproject.org/0.1',
  '0.2': 'https://schema.cassproject.org/0.2',
  '0.3': 'https://schema.cassproject.org/0.3',
  '0.4': 'https://schema.cassproject.org/0.4',
};

export const DEFAULT_SCHEMA_VERSION = '0.4';

export function getSchemaURL(version: string = DEFAULT_SCHEMA_VERSION): string {
  return SCHEMA_URLS[version];
}
