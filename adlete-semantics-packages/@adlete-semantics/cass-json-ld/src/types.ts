import { NodeObject } from 'jsonld';

import type { OrArray, StringAttribute } from '@adlete-semantics/json-ld';
import type { WithRequired } from '@adlete-utils/typescript';

type RequiredProperties = '@context' | '@id' | '@type';

export interface IWithName {
  name?: StringAttribute;
}

export type ICassNodeObject = WithRequired<NodeObject, RequiredProperties>;

export interface IFramework extends ICassNodeObject, IWithName {
  description?: StringAttribute;
  competency?: OrArray<string>;
  relation?: OrArray<string>;
}

export interface ICompetency extends ICassNodeObject, IWithName {
  description?: StringAttribute;
}

export interface IRelation extends ICassNodeObject {
  relationType: string;
  source: string;
  target: string;
}

export interface ICassObjectCollection {
  frameworks?: IFramework[];
  competencies?: ICompetency[];
  relations?: IRelation[];
}

// TODO: find out proper type of http://schema.eduworks.com/ebac/0.3/EncryptedValue
export type EncryptedValue = string;

/**
 * A claim of competence in CASS is called an Assertion. It states with some confidence
 * that an individual has mastered a competency at a given level, provides evidence of
 * such mastery, and records data such as the time of assertion and the party making the
 * assertion.
 */
export interface IAssertion extends ICassNodeObject {
  /**
   * An encrypted identifier of the individual who is making the assertion.
   */
  agent?: EncryptedValue;

  /**
   * Milliseconds since the epoch before the assertion was made.
   */
  assertionDate: number; // integer

  /**
   * Specifies the URL of the competency the assertion is made about.
   */
  competency: string;

  /**
   * The agent's confidence in the assertion.
   */
  confidence?: number; // float

  /**
   * Describes the slope of the line from the initial confidence at the assertion date
   * and the expiration date. t is a number between [0,1] representing the percentage of
   * time that has elapsed. Examples include t^2 and ln(t).
   */
  decayFunction?: EncryptedValue;

  /**
   * An encrypted piece of evidence or reference to evidence.
   */
  evidence?: EncryptedValue;

  /**
   * Milliseconds since the epoch before the assertion expires.
   */
  expirationDate?: number; // integer

  /**
   * Specifies the URL of the framework the assertion was made in.
   */
  framework?: string;

  /**
   * Specifies the URL of the level of performance the assertion is made at.
   */
  level?: string;

  /**
   * True if the assertion is a claim that the subject cannot demonstrate the competency.
   */
  negative?: boolean;

  /**
   * An encrypted identifier of the individual whom the assertion is being made about.
   */
  subject: EncryptedValue;
}
