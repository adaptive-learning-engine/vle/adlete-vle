import { v4 as uuidV4 } from 'uuid';

import type { WithRequired } from '@adlete-utils/typescript';

import { getSchemaURL } from './schema';
import { IAssertion } from './types';

type PartialAssertionRequiredProperties = 'competency' | 'subject';

export interface ISharedCreationOptions {
  endPoint?: string;
  schemaVersion?: string;
}

export type ICreateAssertionTemplate = WithRequired<Partial<IAssertion>, PartialAssertionRequiredProperties>;

export interface IAssertionCreationOptions extends WithRequired<Partial<IAssertion>, PartialAssertionRequiredProperties> {}

export function createAssertion(template: ICreateAssertionTemplate, options?: ISharedCreationOptions): IAssertion {
  const context = template['@context'] || getSchemaURL(options?.schemaVersion);
  const type = template['@type'] || 'Assertion';
  const assertionDate = template.assertionDate || Date.now();

  let id = template['@id'];
  if (!id) {
    const uuid = uuidV4();
    id = options?.endPoint ? `${options.endPoint}/${uuid}` : uuid; // TODO: really add slash here?
  }

  // TODO: deep copy
  return { ...template, '@context': context, '@type': type, '@id': id, assertionDate };
}
