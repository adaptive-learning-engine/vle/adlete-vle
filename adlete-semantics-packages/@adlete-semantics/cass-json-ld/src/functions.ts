import { asArraySafe, asFirstValue, asString, isType } from '@adlete-semantics/json-ld';

import { ICassNodeObject, ICassObjectCollection, IFramework, IRelation, IWithName } from './types';

// taken from here: https://github.com/cassproject/cass-npm/blob/578af08425aa8d9210cac2de93c37fed3c71abae/src/org/cassproject/schema/general/EcRemoteLinkedData.js
export function trimVersionFromUrl(id: string): string {
  if (id == null) return null;
  if (id.indexOf('/api/data/') === -1 && id.indexOf('/api/custom/data/') === -1) return id;
  if (!id.substring(id.lastIndexOf('/')).match('^\\/[0-9]+$')) return id;
  let rawId = id.substring(0, id.lastIndexOf('/'));
  if (rawId.endsWith('/')) rawId = rawId.substring(0, rawId.length - 1);
  return rawId;
}

export function getUUIDFromURI(uri: string): string {
  const shortId = trimVersionFromUrl(uri);
  const parts = shortId.split('/');
  return parts[parts.length - 1];
}

export function getTrimmedId(obj: ICassNodeObject): string {
  return trimVersionFromUrl(asFirstValue(obj['@id']));
}

export function getNodeName(node: IWithName, lang: string = null): string {
  return asString(node.name, lang);
}

export function getFrameworkCompetencyIds(framework: IFramework): string[] {
  return asArraySafe(framework.competency);
}

export function updateVersion(id: string): string {
  // TODO: check if proper cass URL
  const idWithoutVersion = trimVersionFromUrl(id);
  return `${idWithoutVersion}/${Date.now()}`;
}

export function updateNodeVersion<T extends ICassNodeObject>(node: T, inPlace = false): T {
  const newId = updateVersion(asString(node['@id']));
  if (inPlace) {
    node['@id'] = newId;
    return node;
  }

  return { ...node, '@id': newId };
}

export function createUpdatedCopy<T extends ICassNodeObject>(node: T): T {
  const dateString = new Date().toISOString();
  const copy = { ...node, 'schema:dateModified': dateString };
  updateNodeVersion(copy, true);
  return copy;
}

export function getIdVariants(uri: string): [string, string] {
  return [uri, trimVersionFromUrl(uri)];
}

export function getAllIdVariants(uris: string[]): string[] {
  const result: string[] = [];
  uris.forEach((uri) => {
    result.push(uri);
    result.push(trimVersionFromUrl(uri));
  });
  return result;
}

export function getNodeIdVariants<T extends ICassNodeObject>(node: T): [string, string] {
  const longId = asString(node['@id']);
  return [longId, trimVersionFromUrl(longId)];
}

export function getIdWithoutVersion<T extends ICassNodeObject>(node: T): string {
  return trimVersionFromUrl(asString(node['@id']));
}

export type IdComparer = (id: string) => boolean;

export function getObjectIdComparer(obj: ICassNodeObject): IdComparer {
  return getIdComparer(getNodeIdVariants(obj));
}

export function getIdComparer(matches: string[]): IdComparer {
  return (id: string) => matches.includes(id);
}

export function flattenCassObjectCollection(collection: ICassObjectCollection): ICassNodeObject[] {
  const result: ICassNodeObject[] = [];
  collection?.frameworks.forEach((obj) => result.push(obj));
  collection?.competencies.forEach((obj) => result.push(obj));
  collection?.relations.forEach((obj) => result.push(obj));
  return result;
}

export function cassObjectCollectionToMapObj(collection: ICassObjectCollection): Record<string, ICassNodeObject> {
  const result: Record<string, ICassNodeObject> = {};
  collection?.frameworks.forEach((obj) => {
    result[getTrimmedId(obj)] = obj;
  });
  collection?.competencies.forEach((obj) => {
    result[getTrimmedId(obj)] = obj;
  });
  collection?.relations.forEach((obj) => {
    result[getTrimmedId(obj)] = obj;
  });
  return result;
}

export function isFramework(obj: ICassNodeObject): boolean {
  return isType(obj, 'Framework');
}

export function isCompetency(obj: ICassNodeObject): boolean {
  return isType(obj, 'Competency');
}

export function isRelation(obj: ICassNodeObject): boolean {
  return isType(obj, 'Relation');
}

export function getIds(objects: ICassNodeObject[]): string[] {
  return objects.map((obj) => getIdWithoutVersion(obj));
}

export function filterRelationsBySourceAndTarget(relations: IRelation[], comparer: IdComparer, include: boolean): IRelation[] {
  return relations.filter((relation) => {
    const sourceId = trimVersionFromUrl(relation.source);
    const targetId = trimVersionFromUrl(relation.target);
    const containsSourceOrTarget = comparer(sourceId) || comparer(targetId);
    return include ? containsSourceOrTarget : !containsSourceOrTarget;
  });
}
