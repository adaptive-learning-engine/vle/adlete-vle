// eslint-disable-next-line import/no-extraneous-dependencies
import { describe, expect, test } from '@jest/globals';

import {
  asArray,
  asArraySafe,
  asFirstValue,
  asString,
  ERROR_MSG_STRINGVALUEOBJECT_VALUE_MISSING,
  ERROR_MSG_STRINGVALUEOBJECT_VALUE_NOT_STRING,
  isType,
  nodeListToRecord,
} from '../../src/utils';

describe('asArray()', () => {
  test('returns an array with the same elements, if passed an array', () => {
    const input = ['val1', 'val2'];
    const result = asArray(input);
    expect(result).toEqual(expect.arrayContaining(input));
  });

  test('returns an empty array, if passed an empty array', () => {
    const input: unknown[] = [];
    const result = asArray(input);
    expect(result).toHaveLength(0);
  });

  test('returns an array with one value, if passed a single value', () => {
    const value = 'value';
    const result = asArray(value);
    expect(result).toEqual(expect.arrayContaining([value]));
  });

  test('returns an array with single undefined, if passed nothing', () => {
    // @ts-expect-error test missing arguments
    const result = asArray();
    expect(result).toEqual(expect.arrayContaining([undefined]));
  });
});

describe('asArraySafe()', () => {
  test('returns an array with the same elements, if passed an array', () => {
    const input = ['val1', 'val2'];
    const result = asArray(input);
    expect(result).toEqual(expect.arrayContaining(input));
  });

  test('returns an array with one value, if passed a single value', () => {
    const value = 'value';
    const result = asArray(value);
    expect(result).toEqual(expect.arrayContaining([value]));
  });

  test('returns an empty array, if passed a nullish value', () => {
    const resultNull = asArraySafe(null);
    expect(Array.isArray(resultNull)).toBeTruthy();
    expect(resultNull).toHaveLength(0);

    const resultUndefined = asArraySafe(null);
    expect(Array.isArray(resultUndefined)).toBeTruthy();
    expect(resultUndefined).toHaveLength(0);
  });

  test('returns an empty array, if passed nothing (incorrect TS usage test)', () => {
    // @ts-expect-error test missing arguments
    const result = asArraySafe();
    expect(Array.isArray(result)).toBeTruthy();
    expect(result).toHaveLength(0);
  });
});

describe('asFirstValue()', () => {
  test('returns same value as passed', () => {
    const input = { someVal: 'foo' };
    const result = asFirstValue(input);
    expect(result).toEqual(input);
  });

  test('returns first value, if passed array', () => {
    const input = { someVal: 'foo' };
    const result = asFirstValue([input, { someVal: 'bar' }]);
    expect(result).toEqual(input);
  });

  test('returns undefined value, if passed empty array', () => {
    const result = asFirstValue([]);
    expect(result).toEqual(undefined);
  });

  test('returns undefined value, if passed nothing (incorrect TS usage test)', () => {
    // @ts-expect-error test missing arguments
    const result = asFirstValue();
    expect(result).toEqual(undefined);
  });
});

describe('asString()', () => {
  test('returns input string, if passed a string', () => {
    const input = 'string';
    const result = asString(input);
    expect(result).toEqual(input);
  });

  test('returns first value as string, if passed an array of strings', () => {
    const input = ['value1', 'value2'];
    const result = asString(input);
    expect(result).toEqual(input[0]);
  });

  test('throws an error, if an empty array was passed', () => {
    expect(() => asString([])).toThrow('Value array is empty!');
  });

  test('returns @value, if passed a StringValueObject', () => {
    const stringValue = { '@value': 'foo' };
    const result = asString(stringValue);
    expect(result).toEqual(stringValue['@value']);
  });

  test('returns @value of first StringValueObject, if passed an array of StringValueObjects', () => {
    const stringValues = [{ '@value': 'foo' }, { '@value': 'bar' }];
    const result = asString(stringValues);
    expect(result).toEqual(stringValues[0]['@value']);
  });

  test('throws an error, if an object was passed that is not a valid StringValueObject (incorrect TS usage test)', () => {
    // @ts-expect-error test invalid argument
    expect(() => asString({})).toThrow(ERROR_MSG_STRINGVALUEOBJECT_VALUE_MISSING);
    // @ts-expect-error test invalid argument
    expect(() => asString({ '@value': 343 })).toThrow(ERROR_MSG_STRINGVALUEOBJECT_VALUE_NOT_STRING);
  });

  test('throws an error, if passed value is neither string nor StringValueObject', () => {
    // @ts-expect-error test invalid argument
    expect(() => asString(123)).toThrow();
  });

  test('throws an error, if first value of passed array is neither string nor StringValueObject', () => {
    // @ts-expect-error test invalid argument
    expect(() => asString([123])).toThrow();
  });

  // TODO: tests for languages
});

describe('nodeListToRecord()', () => {
  const elems = [{ '@id': 'foo' }, { '@id': 'bar' }];

  test("returns a matching record based on the node @id's", () => {
    const result = nodeListToRecord(elems);
    expect(typeof result).toBe('object');
    expect(Object.keys(result)).toHaveLength(2);
    expect(result.foo).toBe(elems[0]);
    expect(result.bar).toBe(elems[1]);
  });

  test('returns a matching record with transformed ids', () => {
    const result = nodeListToRecord(elems, (id) => `pre-${id}`);
    expect(result['pre-foo']).toBe(elems[0]);
    expect(result['pre-bar']).toBe(elems[1]);
  });
});

describe('isType()', () => {
  test('returns false if @type does not exist', () => {
    const node = {};
    expect(isType(node, 'someType')).toBe(false);
  });

  test('returns true if @type is a string value matching the passed type', () => {
    const node = { '@type': 'someType' };
    expect(isType(node, 'someType')).toBe(true);
  });

  test('returns false if @type is a string value note matching the passed type', () => {
    const node = { '@type': 'someType' };
    expect(isType(node, 'otherType')).toBe(false);
  });

  test('returns true if @type is an array containing the passed type', () => {
    const node = { '@type': ['someType', 'extraType'] };
    expect(isType(node, 'someType')).toBe(true);
  });

  test('returns false if @type is an array not containing the passed type', () => {
    const node = { '@type': ['typeA', 'typeB'] };
    expect(isType(node, 'someType')).toBe(false);
  });

  test('returns false if @type is neither a string nor an array (incorrect TS usage test)', () => {
    const node = { '@type': 123 };
    // @ts-expect-error test invalid argument
    expect(isType(node, 'someType')).toBe(false);
  });
});
