import { ContextDefinition, IncludedBlock } from 'jsonld';

// TODO: move

/**
 * A list of keywords and their types.
 * Only used for internal reference; not an actual interface.
 * Not for export.
 * @see https://www.w3.org/TR/json-ld/#keywords
 */
// tslint:disable-next-line:interface-over-type-literal
type Keyword = {
  '@base': string | null;
  '@container': OrArray<'@list' | '@set' | ContainerType> | ContainerTypeArray | null;
  '@context': OrArray<null | string | ContextDefinition>;
  '@direction': 'ltr' | 'rtl' | null;
  // '@graph': OrArray<ValueObject | NodeObject>;
  '@id': OrArray<string>;
  '@import': string;
  '@included': IncludedBlock;
  '@index': string;
  '@json': '@json';
  '@language': string;
  // '@list': OrArray<null | boolean | number | string | NodeObject | ValueObject>;
  // eslint-disable-next-line @typescript-eslint/ban-types
  '@nest': object;
  '@none': '@none';
  '@prefix': boolean;
  '@propagate': boolean;
  '@protected': boolean;
  '@reverse': string;
  // '@set': OrArray<null | boolean | number | string | NodeObject | ValueObject>;
  '@type': string;
  '@value': null | boolean | number | string;
  '@version': '1.1';
  '@vocab': string | null;
};

export type ContextProperty = Keyword['@context'];

/*
 * Helper Types
 * (not for export)
 */
export type OrArray<T> = T | T[];
type ContainerType = '@language' | '@index' | '@id' | '@graph' | '@type';
type ContainerTypeArray =
  | ['@graph', '@id']
  | ['@id', '@graph']
  | ['@set', '@graph', '@id']
  | ['@set', '@id', '@graph']
  | ['@graph', '@set', '@id']
  | ['@id', '@set', '@graph']
  | ['@graph', '@id', '@set']
  | ['@id', '@graph', '@set']
  | ['@set', ContainerType]
  | [ContainerType, '@set'];

/*
 * JSON Types
 * (not for export)
 */
type JsonPrimitive = string | number | boolean | null;
type JsonArray = Array<JsonValue>;
interface JsonObject {
  [key: string]: JsonValue | undefined;
}
type JsonValue = JsonPrimitive | JsonArray | JsonObject;

export type TypedValueObject<T extends Keyword['@value'] | JsonObject | JsonArray> = {
  '@index'?: Keyword['@index'] | undefined;
  '@context'?: Keyword['@context'] | undefined;
} & (
  | {
      '@value': T;
      '@language'?: Keyword['@language'] | undefined;
      '@direction'?: Keyword['@direction'] | undefined;
    }
  | {
      '@value': T;
      '@type': Keyword['@type'];
    }
  | {
      '@value': T;
      '@type': '@json';
    }
);

export type StringValueObject = TypedValueObject<string>;
export type StringAttribute = OrArray<string | StringValueObject>;
