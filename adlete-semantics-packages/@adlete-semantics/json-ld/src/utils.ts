import { NodeObject } from 'jsonld';

import type { WithRequired } from '@adlete-utils/typescript';

import { OrArray, StringValueObject } from './types';

export const ERROR_MSG_STRINGVALUEOBJECT_VALUE_MISSING = 'Object is not a StringValueObject. Missing @value property!';
export const ERROR_MSG_STRINGVALUEOBJECT_VALUE_NOT_STRING = '@value property of StringValueObject must be of type string!';

export function validateIsStringValueObject(obj: StringValueObject): void {
  if (!('@value' in obj)) throw new Error(ERROR_MSG_STRINGVALUEOBJECT_VALUE_MISSING);
  if (typeof obj['@value'] !== 'string') throw new Error(ERROR_MSG_STRINGVALUEOBJECT_VALUE_NOT_STRING);
}

export function asArray<T>(orArray: OrArray<T>): T[] {
  if (Array.isArray(orArray)) return orArray;
  return [orArray];
}

export function asFirstValue<T>(orArray: OrArray<T>): T {
  if (Array.isArray(orArray)) return orArray[0];
  return orArray;
}

export function asArraySafe<T>(orArray: OrArray<T>): T[] {
  if (orArray == null) return [];
  if (Array.isArray(orArray)) return orArray;
  return [orArray];
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
export function asString(value: OrArray<string | StringValueObject>, lang: string = null): string {
  if (typeof value === 'string') return value;

  if (Array.isArray(value)) {
    if (value.length === 0) throw new Error('Value array is empty!');

    // TODO: languages
    const item = value[0];
    return asString(item);
  }

  if (typeof value === 'object') {
    validateIsStringValueObject(value);
    return value['@value'];
  }

  throw new Error(`Value has unrecognized type: ${typeof value}`);
}

export function nodeListToRecord<TNodeObject extends WithRequired<NodeObject, '@id'>>(
  elems: TNodeObject[],
  idTransformer?: (id: string) => string
): Record<string, TNodeObject> {
  const record: Record<string, TNodeObject> = {};
  elems.forEach((elem) => {
    const rawId = asString(elem['@id']);
    const id = idTransformer ? idTransformer(rawId) : rawId;
    record[id] = elem;
  });
  return record;
}

export function isType(obj: NodeObject, type: string): boolean {
  const objType = obj['@type'];

  if (!objType) return false;

  if (typeof objType === 'string') return objType === type;

  if (Array.isArray(objType)) return objType.includes(type);

  return false;
}
