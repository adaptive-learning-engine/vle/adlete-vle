import { NodeObject } from 'jsonld';

import { ContextProperty } from '@adlete-semantics/json-ld';

import { ILanguageMap } from './language-map';

// export const LR_CONTEXT_URI = 'https://w3id.org/kim/amb/draft/context.jsonld';
// export const LR_CONTEXT_EN = [LR_CONTEXT_URI, { '@language': 'en' }];

export interface IInScheme<TId extends string> {
  id?: TId;
  [k: string]: unknown;
}

/**
 * The learning resource type of the resource, taken from the controlled HCRT list or the OpenEduHub vocabulary for learningResourceType
 */
export interface ILearningResourceType extends NodeObject {
  type?: 'Concept';
  id: string;
  inScheme?: IInScheme<'https://w3id.org/kim/hcrt/scheme' | 'http://w3id.org/openeduhub/vocabs/learningResourceType/'>;
  prefLabel?: ILanguageMap<string>;
}

export interface ILearningResourceEncoding extends NodeObject {
  type?: string;
  contentURL?: string;
  encodingFormat?: string;
}

export interface ILearningResourceAbout extends NodeObject {
  id: string;
  type?: string;
  inScheme?: IInScheme<string>;
  prefLabel?: ILanguageMap<string>;
}

export interface IMainEntityOfPage extends NodeObject {
  id: string;
}

export interface ICompetencyReference extends NodeObject {
  id: string;
  prefLabel?: ILanguageMap<string>;
}

export interface ILearningResourceMeta extends NodeObject {
  '@context': ContextProperty;
  id: string;
  name: string;
  localizedName?: ILanguageMap<string>;
  type: string[];
  learningResourceType?: ILearningResourceType[];
  encoding?: ILearningResourceEncoding[];
  about?: ILearningResourceAbout[];
  mainEntityOfPage?: IMainEntityOfPage[];
  image?: string;
  inLanguage?: string[];
  teaches?: ICompetencyReference[];
  assesses?: ICompetencyReference[];
  competencyRequired?: ICompetencyReference[];
}
