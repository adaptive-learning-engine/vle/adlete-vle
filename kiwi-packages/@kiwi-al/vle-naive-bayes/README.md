# @kiwi-al/vle-naive-bayes

Frontend package of a VLE for learning Naive Bayes and conditional probability.

See the main repository's [README.md](../../../README.md) for more information.

## Running

- `yarn start:vle-naive-bayes` - start the dev server (from the root folder of the monorepo)

## Development

This package makes use of [@fki/plugin-based-app](https://gitlab.com/adaptive-learning-engine/adlete-packages/-/tree/kiwi/master/%40fki/plugin-system).
