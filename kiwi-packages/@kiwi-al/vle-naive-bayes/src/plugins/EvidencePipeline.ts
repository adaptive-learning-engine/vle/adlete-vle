import type { Statement } from '@xapi/xapi';

import type { ILearningResource, ILearningResourceManager } from '@adlete-vle/lr-core/learning-resources';
import type { XAPIManager, XAPIStatementsReceivedEvent } from '@adlete-vle/lr-core/xapi';
import type { IWithLearningResourceManager, IWithXAPIManager } from '@adlete-vle/vle-core/plugins';

import type { IPlugin, IPluginManager, IPluginMeta } from '@fki/plugin-system/specs';

import { EvidencePipeline, IXAPIInputs, IXAPIOutputs, LearningResourcesXAPIEvidencePipe } from '@kiwi-al/pl-engine/analytics';

export interface IWithEvidencePipeline extends IWithXAPIManager, IWithLearningResourceManager {
  evidencePipeline: EvidencePipelinePlugin;
}

export const EvidencePipelineMeta: IPluginMeta<keyof IWithEvidencePipeline, 'evidencePipeline'> = {
  id: 'evidencePipeline',
  dependencies: ['learningResourceMan', 'xAPIManager'],
};

export class EvidencePipelinePlugin implements IPlugin<IWithEvidencePipeline> {
  public meta: IPluginMeta<keyof IWithEvidencePipeline, 'evidencePipeline'> = EvidencePipelineMeta;

  public pipeline: EvidencePipeline<IXAPIInputs, IXAPIOutputs>;

  protected lrPipe: LearningResourcesXAPIEvidencePipe;

  protected xAPIManager: XAPIManager;

  protected learningResourceMan: ILearningResourceManager;

  public async initialize(pluginMan: IPluginManager<IWithEvidencePipeline>): Promise<void> {
    this.learningResourceMan = pluginMan.get('learningResourceMan').manager;
    this.xAPIManager = pluginMan.get('xAPIManager').manager;

    this.onResourcesAdded = this.onResourcesAdded.bind(this);
    this.onInputStatements = this.onInputStatements.bind(this);
    this.onOutputStatements = this.onOutputStatements.bind(this);

    this.pipeline = new EvidencePipeline<IXAPIInputs, IXAPIOutputs>();
    this.lrPipe = new LearningResourcesXAPIEvidencePipe();

    // TODO: no casting once pipeline has better types
    this.pipeline.addStartPipe(this.lrPipe as never);
    this.pipeline.addEndPipe(this.lrPipe as never);

    this.learningResourceMan.events.on('resourcesAdded', this.onResourcesAdded);
    this.pipeline.events.on('xAPIStatements', this.onOutputStatements);
    this.xAPIManager.events.on('statementsReceived', this.onInputStatements);
  }

  protected onResourcesAdded(resources: ILearningResource[]) {
    resources.forEach((resource) => {
      if (!resource.meta || this.lrPipe.isWatchingLearningResource(resource.meta)) return;

      this.lrPipe.watchLearningResource(resource.meta);
    });
  }

  protected onInputStatements(statementsReceived: XAPIStatementsReceivedEvent) {
    this.pipeline.receive('xAPIStatements', statementsReceived.statements);
  }

  protected onOutputStatements(statements: Statement[]) {
    // sending the assertion statements
    this.xAPIManager.sendStatementsSafe(statements);
  }

  public async terminate(): Promise<void> {
    this.learningResourceMan.events.off('resourcesAdded', this.onResourcesAdded);
    this.pipeline.events.off('xAPIStatements', this.onOutputStatements);
    this.xAPIManager.events.off('statementsReceived', this.onInputStatements);
  }
}
