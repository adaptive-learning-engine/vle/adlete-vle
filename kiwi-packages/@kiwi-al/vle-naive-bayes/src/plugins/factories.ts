import { createInstance } from 'localforage';

import {
  ContextManagerMeta,
  ContextManagerPlugin,
  LearningResourceManagerMeta,
  LearningResourceManagerPlugin,
  LocaleManagerMeta,
  LocaleManagerPlugin,
  LRComponentCreatorMeta,
  LRComponentCreatorPlugin,
  StorageMeta,
  StoragePlugin,
  TTSManagerMeta,
  TTSManagerPlugin,
  XAPIManagerMeta,
  XAPIManagerPlugin,
} from '@adlete-vle/vle-core/plugins';

import { createInstantFactory } from '@fki/plugin-system';
import type { IPluginFactory } from '@fki/plugin-system/specs';

import type { IVLEPlugins } from '../scripts/VLECore';

import { EvidencePipelineMeta, EvidencePipelinePlugin } from './EvidencePipeline';
import { RandomExperimentManagerMeta, RandomExperimentManagerPlugin } from './RandomExperimentManager';
import { SetupLearningResourcesMeta, SetupLearningResourcesPlugin } from './SetupLearningResources';

// TODO: retrieve from some configuration plugin
const XAPI_END_POINT = 'http://localhost:3000/xAPI';

// eslint-disable-next-line @typescript-eslint/ban-types
export const FACTORIES: IPluginFactory<IVLEPlugins<{}>, keyof IVLEPlugins>[] = [
  createInstantFactory(ContextManagerMeta, () => new ContextManagerPlugin()),
  createInstantFactory(LocaleManagerMeta, () => new LocaleManagerPlugin()),
  createInstantFactory(StorageMeta, () => {
    // TODO: move outside
    const storage = createInstance({});
    return new StoragePlugin(storage);
  }),
  createInstantFactory(TTSManagerMeta, () => new TTSManagerPlugin()),
  createInstantFactory(SetupLearningResourcesMeta, () => new SetupLearningResourcesPlugin()),
  createInstantFactory(LearningResourceManagerMeta, () => new LearningResourceManagerPlugin()),
  createInstantFactory(RandomExperimentManagerMeta, () => new RandomExperimentManagerPlugin()),
  createInstantFactory(LRComponentCreatorMeta, () => new LRComponentCreatorPlugin()),
  createInstantFactory(XAPIManagerMeta, () => new XAPIManagerPlugin({ endpoint: XAPI_END_POINT })),
  createInstantFactory(EvidencePipelineMeta, () => new EvidencePipelinePlugin()),
];
