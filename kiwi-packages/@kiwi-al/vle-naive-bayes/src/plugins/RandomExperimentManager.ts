import type { IWithContextManager } from '@adlete-vle/vle-core/plugins';

import type { IPlugin, IPluginManager, IPluginMeta } from '@fki/plugin-system/specs';

import { RandomExperimentManagerContext } from '@kiwi-al/lr-naive-bayes/contexts';
import { RandomExperimentManager } from '@kiwi-al/math-core/random-experiment';

export interface IWithRandomExperimentManager extends IWithContextManager {
  randomExperimentMan: RandomExperimentManagerPlugin;
}

export const RandomExperimentManagerMeta: IPluginMeta<keyof IWithRandomExperimentManager, 'randomExperimentMan'> = {
  id: 'randomExperimentMan',
  dependencies: ['contextManager'],
};

export class RandomExperimentManagerPlugin implements IPlugin<IWithRandomExperimentManager> {
  public meta: IPluginMeta<keyof IWithRandomExperimentManager, 'randomExperimentMan'> = RandomExperimentManagerMeta;

  public manager: RandomExperimentManager;

  // eslint-disable-next-line class-methods-use-this
  public initialize(pluginMan: IPluginManager<IWithRandomExperimentManager>): Promise<void> {
    this.manager = new RandomExperimentManager();

    const contextManager = pluginMan.get('contextManager');
    contextManager.addContext(RandomExperimentManagerContext, this.manager);

    return Promise.resolve();
  }

  // eslint-disable-next-line class-methods-use-this
  public terminate(): Promise<void> {
    return Promise.resolve();
  }
}
