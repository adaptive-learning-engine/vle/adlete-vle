import { addLearningResourceComponentLocales } from '@adlete-vle/lr-core-components/locales';
import { addVLELocales } from '@adlete-vle/vle-core/locales';
import { IWithLearningResourceManager, IWithLocaleManager, IWithLRComponentCreator } from '@adlete-vle/vle-core/plugins';

import type { IPlugin, IPluginManager, IPluginMeta } from '@fki/plugin-system/specs';

import { initReactComponentFactoryLibrary } from '@kiwi-al/lr-naive-bayes/initialization/component-factory-library';
import { initLearningResourceManager, initRandomExperimentManager } from '@kiwi-al/lr-naive-bayes/initialization/learning-resources';
import { initPropMappings } from '@kiwi-al/lr-naive-bayes/initialization/prop-mapping';
import { addLocales as addNaiveBayesLocales } from '@kiwi-al/lr-naive-bayes/locales';
// import { ProbabilityTreeDiagram } from '@kiwi-al/lr-naive-bayes/probability-tree-diagram/ProbabilityTreeDiagram';
// import { IViewport } from '@kiwi-al/lr-naive-bayes/utils/visualization';
import { addMathLocales } from '@kiwi-al/math-core/locales';
import { addMathComponentLocales } from '@kiwi-al/math-core-components/locales';

import { addVLENaiveBayesLocales } from '../locales';

import { IWithRandomExperimentManager } from './RandomExperimentManager';

export interface IWithSetupLearningResources
  extends IWithLocaleManager,
    IWithLearningResourceManager,
    IWithRandomExperimentManager,
    IWithLRComponentCreator {
  setupLearningResources: SetupLearningResourcesPlugin;
}

export const SetupLearningResourcesMeta: IPluginMeta<keyof IWithSetupLearningResources, 'setupLearningResources'> = {
  id: 'setupLearningResources',
  dependencies: ['localeManager', 'learningResourceMan', 'randomExperimentMan', 'lrComponentCreator'],
};

export class SetupLearningResourcesPlugin implements IPlugin<IWithSetupLearningResources> {
  public meta: IPluginMeta<keyof IWithSetupLearningResources, 'setupLearningResources'> = SetupLearningResourcesMeta;

  public async initialize(pluginMan: IPluginManager<IWithSetupLearningResources>): Promise<void> {
    const localeManager = pluginMan.get('localeManager').manager;
    const lrm = pluginMan.get('learningResourceMan').manager;
    const rem = pluginMan.get('randomExperimentMan').manager;
    const componentCreator = pluginMan.get('lrComponentCreator');

    initPropMappings(componentCreator.propMapper);
    initReactComponentFactoryLibrary(componentCreator.factoryLib);

    initLearningResourceManager(lrm);
    initRandomExperimentManager(rem, localeManager.i18n);

    addMathLocales(localeManager.i18n);
    addMathComponentLocales(localeManager.i18n);
    addNaiveBayesLocales(localeManager.i18n);
    addLearningResourceComponentLocales(localeManager.i18n);
    addVLELocales(localeManager.i18n);
    addVLENaiveBayesLocales(localeManager.i18n);
  }

  // eslint-disable-next-line class-methods-use-this
  public terminate(): Promise<void> {
    return Promise.resolve();
  }
}
