const messages = {
  'info-dialog': {
    title: 'Informationen',
    license:
      '[Eine digitale Lernumgebung zum Thema "bedingte Wahrscheinlichkeit"](https://iug-research.gitlab.io/oer-conditional-probability) von der [Forschungsgruppe Informatik & Gesellschaft (HTW Berlin)](https://iug.htw-berlin.de/) ist lizenziert unter [CC BY 4.0](http://creativecommons.org/licenses/by/4.0/?ref=chooser-v1).',
    creation:
      'Diese Open Educational Resource wurde im Rahmen des Projekts [KI-Lehrwerkstatt Interdisziplinär (KIWI)](https://kiwerkstatt.f2.htw-berlin.de/lehre) an der [Hochschule für Technik und Wirtschaft Berlin](https://www.htw-berlin.de/) erstellt.\n\n**Entwickler**: André Selmanagić, Simon Lugenbiehl\n\n**Unterstützt durch**: Prof. Dr. Katharina Simbeck, Prof. Dr. Andre Beinrucker, Prof. Dr. Thoralf Chrobok, weiteres Lehrpersonal für Statistik des Studiengangs BWL',
  },
};

// eslint-disable-next-line import/no-default-export
export default messages;
