const messages = {
  'info-dialog': {
    title: 'Information',
    license:
      '[A Digital Learning Environment for "Conditional Probability"](https://iug-research.gitlab.io/oer-conditional-probability) by [Research Group Informatik & Gesellschaft (HTW Berlin)](https://iug.htw-berlin.de/) is licensed under [CC BY 4.0](http://creativecommons.org/licenses/by/4.0/?ref=chooser-v1).',
    creation:
      'This open educational resource was developed within the [KI-Lehrwerkstatt Interdisziplinär (KIWI)](https://kiwerkstatt.f2.htw-berlin.de/lehre) project at the [Hochschule für Technik und Wirtschaft Berlin](https://www.htw-berlin.de/).\n\n**Developers**: André Selmanagić, Simon Lugenbiehl\n\n**Supported by**: Prof. Dr. Katharina Simbeck, Prof. Dr. Andre Beinrucker, Prof. Dr. Thoralf Chrobok, weiteres Lehrpersonal für Statistik des Studiengangs BWL',
  },
};

// eslint-disable-next-line import/no-default-export
export default messages;
