import { Box, CssBaseline, ThemeProvider } from '@mui/material';
import React from 'react';
import { createRoot } from 'react-dom/client';

import { createTheme } from '@fki/editor-core/theme/createTheme';

import 'typeface-roboto';
import { VLE } from './components/VLE';
import { VLELogger } from './scripts/logger';

const FONT_FAMILY = 'Calibri, Roboto, Helvetica, Arial, sans-serif';

const theme = createTheme({ typography: { fontFamily: FONT_FAMILY } }, 15);

const root = createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Box>
        <VLE />
      </Box>
    </ThemeProvider>
  </React.StrictMode>
);

if (import.meta.hot) {
  // eslint-disable-next-line no-console
  import.meta.hot.on('vite:beforeUpdate', () => console.clear());
  import.meta.hot.accept((newModule) => {
    // eslint-disable-next-line no-console
    VLELogger.info('updated: count is now ', newModule.count);
  });
}

// adding quick debugger functionality
declare global {
  interface Window {
    debuggerIn: (seconds: number) => void;
  }
}

window.debuggerIn = (seconds: number) => {
  setTimeout(() => {
    // eslint-disable-next-line no-debugger
    debugger;
  }, seconds * 1000);
};
