import InfoIcon from '@mui/icons-material/Info';
import TuneIcon from '@mui/icons-material/Tune';
import { Box, IconButton, Stack, Toolbar, Typography } from '@mui/material';
import AppBar from '@mui/material/AppBar';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { TTSSettingsDialog } from '@adlete-vle/vle-core/components';

import { InfoDialog } from './InfoDialog';
import { LanguageChooser } from './LanguageChooser';

export function MainMenu(): React.ReactElement {
  const [ttsDialogOpen, setTTSDialogOpen] = React.useState(false);
  const [infoDialogOpen, setInfoDialogOpen] = React.useState(false);
  const [t] = useTranslation();

  const condProb = t('conditional-probability:conditional-probability');

  const openTTSSettings = () => {
    setTTSDialogOpen(true);
  };

  const closeTTSSettings = () => {
    setTTSDialogOpen(false);
  };

  const openInfoDialog = () => {
    setInfoDialogOpen(true);
  };

  const closeInfoDialog = () => {
    setInfoDialogOpen(false);
  };

  return (
    <Box sx={{ display: 'flex' }}>
      <AppBar position="sticky" color="secondary">
        <Toolbar>
          <Typography variant="h6" noWrap component="div" sx={{ flexGrow: 1 }}>
            {condProb}
          </Typography>
          <Stack direction="row" spacing={4}>
            <IconButton color="inherit" onClick={openTTSSettings} edge="end">
              <TuneIcon />
            </IconButton>
            <LanguageChooser className="language-chooser" />
            <IconButton color="inherit" onClick={openInfoDialog} edge="end">
              <InfoIcon />
            </IconButton>
          </Stack>
        </Toolbar>
      </AppBar>
      <TTSSettingsDialog open={ttsDialogOpen} onClose={closeTTSSettings} />
      <InfoDialog open={infoDialogOpen} onClose={closeInfoDialog} />
    </Box>
  );
}
