import { CircularProgress, Stack } from '@mui/material';
import React, { useEffect, useMemo } from 'react';
import { HashRouter } from 'react-router-dom';

import { IPluginFactory, Plugins } from '@fki/plugin-system/specs';

import { ConditionalProbabilityLO } from '@kiwi-al/lr-naive-bayes/conditional-probability/ConditionalProbabilityLO';

import { IWithVLEPlugins, VLECore } from '../scripts/VLECore';
import { VLELogger } from '../scripts/logger';

import { MainMenu } from './MainMenu';

import './VLE.css';

interface IVLEViewProps<TPlugins extends Plugins<TPlugins>> {
  core: VLECore<IWithVLEPlugins<TPlugins>>;
}

function VLEView<TPlugins extends Plugins<TPlugins>>({ core }: IVLEViewProps<TPlugins>): React.ReactElement {
  const contextMan = core.plugins.get('contextManager');
  const content: React.ReactNode = (
    <Stack spacing={2}>
      <MainMenu />
      <p />
      <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <ConditionalProbabilityLO />
      </div>
    </Stack>
  );

  const wrapped = contextMan.render(content);

  // TODO: create router plugin
  return <HashRouter>{wrapped}</HashRouter>;
}

export interface IVLEProps<TPlugins extends Plugins<TPlugins>> {
  pluginFactories?: IPluginFactory<TPlugins, keyof TPlugins>[];
}

export function VLE<TPlugins extends Plugins<TPlugins>>(props: IVLEProps<TPlugins>): React.ReactElement {
  const [isInitialized, setIsInitialized] = React.useState(false);
  // eslint-disable-next-line @typescript-eslint/ban-types, react-hooks/exhaustive-deps
  const core = useMemo(() => new VLECore<TPlugins>(props.pluginFactories), []);

  useEffect(() => {
    async function inner() {
      if (core.phase !== 'before-start') {
        VLELogger.info('Ignoring duplicate useEffect call, as VLECore is already starting!');
        return;
      }

      await core.start();
      setIsInitialized(true);
    }
    inner();
  }, [core]);

  // only render application when core was properly initialized
  if (isInitialized) {
    return <VLEView core={core} />;
  }
  return <CircularProgress />;
}
