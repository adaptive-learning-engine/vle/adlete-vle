import { ToggleButton, ToggleButtonGroup } from '@mui/material';
import React, { ReactElement } from 'react';

import { useStatefulEvent } from '@adlete-utils/events-react';
import { useLocaleManager } from '@adlete-vle/lr-core-components/contexts';

export interface ILanguageChooserProps {
  className?: string;
}

export function LanguageChooser({ className }: ILanguageChooserProps): ReactElement {
  const localeManager = useLocaleManager();

  const language = useStatefulEvent(localeManager.events, 'language');

  const handleLanguage = (event: React.MouseEvent<HTMLElement>, newLanguage: string | null) => {
    if (newLanguage) localeManager.updateLanguage(newLanguage);
  };

  return (
    <ToggleButtonGroup className={className} value={language} onChange={handleLanguage} exclusive size="small">
      <ToggleButton value="de-DE" aria-label="left aligned">
        DE
      </ToggleButton>
      <ToggleButton value="en-US" aria-label="centered">
        EN
      </ToggleButton>
    </ToggleButtonGroup>
  );
}
