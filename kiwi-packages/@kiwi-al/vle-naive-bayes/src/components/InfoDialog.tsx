import CloseIcon from '@mui/icons-material/Close';
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, Divider, IconButton, Stack } from '@mui/material';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { CustomMarkdown } from '@adlete-vle/lr-core-components/CustomMarkdown';

import HTW_Logo from '../assets/htw_berlin_logo.svg';
import KI_Werkstatt_Logo from '../assets/ki_werkstatt_logo.svg';

export interface IInfoDialogProps {
  open: boolean;
  onClose: () => void;
}

export function InfoDialog({ open, onClose }: IInfoDialogProps) {
  const { t } = useTranslation();

  return (
    <Dialog onClose={onClose} aria-labelledby="customized-dialog-title" open={open}>
      <DialogTitle sx={{ m: 0, p: 2 }} id="customized-dialog-title">
        {t('info-dialog:title')}
      </DialogTitle>
      <IconButton
        aria-label="close"
        onClick={onClose}
        sx={{
          position: 'absolute',
          right: 8,
          top: 8,
          color: (theme) => theme.palette.grey[500],
        }}
      >
        <CloseIcon />
      </IconButton>
      <DialogContent dividers>
        <Stack spacing={2} divider={<Divider orientation="horizontal" flexItem />}>
          <Stack direction="row" spacing={2}>
            <div>
              <img
                width={150}
                src="http://mirrors.creativecommons.org/presskit/buttons/88x31/svg/by.svg"
                alt="License: Creative Commons BY 4.0 Logo"
              />
            </div>
            <CustomMarkdown>{t('info-dialog:license')}</CustomMarkdown>
          </Stack>
          <Stack spacing={2}>
            <CustomMarkdown>{t('info-dialog:creation')}</CustomMarkdown>
            <Stack direction="row" spacing={8} justifyContent="center" alignItems="center">
              <div>
                <a href="https://www.htw-berlin.de/" target="_blank" rel="noreferrer">
                  <img width={100} src={HTW_Logo} alt="HTW Berlin" />
                </a>
              </div>
              <div>
                <a href="https://kiwerkstatt.f2.htw-berlin.de/lehre" target="_blank" rel="noreferrer">
                  <img width={200} src={KI_Werkstatt_Logo} alt="KI-Werkstatt" />
                </a>
              </div>
            </Stack>
          </Stack>
        </Stack>
      </DialogContent>
      <DialogActions>
        <Button autoFocus onClick={onClose}>
          {t('dialogs:close')}
        </Button>
      </DialogActions>
    </Dialog>
  );
}
