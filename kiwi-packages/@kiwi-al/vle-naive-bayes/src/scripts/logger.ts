import { Logger } from '@adlete-vle/lr-core/logging';

export const VLELogger = Logger.withTag('VLE');
