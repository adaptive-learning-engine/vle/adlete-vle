import {
  IWithContextManager,
  IWithLocaleManager,
  IWithLRComponentCreator,
  IWithStorage,
  IWithTTSManager,
  IWithXAPIManager,
} from '@adlete-vle/vle-core/plugins';

import PluginBasedApp from '@fki/plugin-based-app';
import { PluginFactoryRegistry } from '@fki/plugin-system/PluginFactoryRegistry';
import type { IPluginFactory, IPluginMeta, Plugins } from '@fki/plugin-system/specs';

import { IWithEvidencePipeline } from '../plugins/EvidencePipeline';
import { IWithRandomExperimentManager } from '../plugins/RandomExperimentManager';
import { IWithSetupLearningResources } from '../plugins/SetupLearningResources';
import { FACTORIES } from '../plugins/factories';

export interface IWithVLECore<TPlugins extends Plugins<TPlugins>> {
  vle: VLECore<TPlugins & IVLEPlugins<TPlugins>>;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export interface IVLEPlugins<TPlugins extends Plugins<TPlugins> = any>
  extends IWithVLECore<TPlugins>,
    IWithContextManager,
    IWithLocaleManager,
    IWithStorage,
    IWithTTSManager,
    IWithLRComponentCreator,
    IWithSetupLearningResources,
    IWithRandomExperimentManager,
    IWithXAPIManager,
    IWithEvidencePipeline {}

export type IWithVLEPlugins<TPlugins extends Plugins<TPlugins>> = TPlugins & IVLEPlugins<TPlugins>;

// eslint-disable-next-line @typescript-eslint/ban-types
export const VLECoreMeta: IPluginMeta<keyof IWithVLECore<{}>, 'vle'> = {
  id: 'vle',
};

export class VLECore<TPlugins extends Plugins<TPlugins>> extends PluginBasedApp<TPlugins & IVLEPlugins<TPlugins>> {
  public constructor(factories?: IPluginFactory<TPlugins, keyof TPlugins>[]) {
    const registry = new PluginFactoryRegistry<TPlugins & IVLEPlugins<TPlugins>>();

    factories?.forEach((factory) => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      registry.add(factory.meta.id, factory as any);
    });

    FACTORIES.forEach((factory) => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      registry.add(factory.meta.id, factory as any);
    });

    super(VLECoreMeta, registry);
  }
}
