export interface IGraphLearnerModel<TGraph> {
  graph: TGraph;
}
