import type { ICASSGraph } from '@adlete-semantics/cass-graph';
import { ITypedEventEmitter, ITypedEventEmitterNoEmit, TypedEventEmitter } from '@adlete-utils/events';
import type { JSONObject } from '@adlete-utils/typescript/json';

import { IGraphLearnerModel } from './GraphLearnerModel';
import { ILearnerModel, ILearnerModelEvents } from './ILearnerModel';

// export interface ICompetencyLearnerModelOptions {

// }

function assertSupportsKey(key: string, graph: ICASSGraph): void {
  if (!graph.hasNode(key)) throw new Error(`Learner model does not support key '${key}'!`);
}

function assertSupportsKeys(keys: string[], graph: ICASSGraph): void {
  keys.forEach((key) => assertSupportsKey(key, graph));
}

export class CompetencyLearnerModel<TValue, TContext = unknown> implements ILearnerModel<TValue>, IGraphLearnerModel<ICASSGraph> {
  protected _graph: ICASSGraph;

  protected _values: Record<string, TValue>;

  protected _events: ITypedEventEmitter<ILearnerModelEvents<TValue>>;

  public get events(): ITypedEventEmitterNoEmit<ILearnerModelEvents<TValue>> {
    return this._events;
  }

  public context: TContext;

  public constructor(graph: ICASSGraph, context?: TContext) {
    this.context = context;
    this._graph = graph;
    this._values = {};
    this._events = new TypedEventEmitter();
  }

  public get keys(): string[] {
    return this._graph.nodes();
  }

  public get graph(): ICASSGraph {
    return this._graph;
  }

  public supportsKey(key: string): boolean {
    return this._graph.hasNode(key);
  }

  public getValue(key: string): TValue {
    assertSupportsKey(key, this._graph);
    return this._values[key];
  }

  public setValue(key: string, value: TValue): void {
    this.setValues([key], [value]);
  }

  public getValues(keys: string[]): TValue[] {
    assertSupportsKeys(keys, this._graph);
    return keys.map((key) => this._values[key]);
  }

  public setValues(keys: string[], values: TValue[]): void {
    assertSupportsKeys(keys, this._graph);
    keys.forEach((key, i) => {
      this._values[key] = values[i];
    });
    this._events.emit('change', keys, values);
  }

  public serialize(): JSONObject {
    // TODO: deep copy
    return { values: { ...this._values } } as JSONObject;
  }
}
