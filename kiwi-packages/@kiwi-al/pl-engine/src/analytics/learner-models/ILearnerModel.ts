export interface ILearnerModel<TValue, TContext = unknown> {
  // TODO: generic context for user token etc.

  context: TContext;

  keys: string[];

  supportsKey(key: string): boolean;

  getValue(key: string): TValue;
  setValue(key: string, value: TValue): void;

  getValues(keys: string[]): TValue[];
  setValues(keys: string[], value: TValue[]): void;
}

export interface IAsyncLearnerModel<TValue> {
  keys: string[];

  supportsKey(key: string): boolean;

  getValue(key: string): Promise<TValue>;
  setValue(key: string, value: TValue): Promise<void>;

  getValues(keys: string[]): Promise<TValue[]>;
  setValues(keys: string[], values: TValue[]): Promise<void>;
}

// TODO: async Pushable vs. pullable learner models

export interface ILearnerModelEvents<TValue> {
  change: [string[], TValue[]];
}
