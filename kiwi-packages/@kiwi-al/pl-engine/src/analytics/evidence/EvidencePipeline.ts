import { type ITypedEventEmitter, type ITypedEventEmitterNoEmit, TypedEventEmitter } from '@adlete-utils/events';
import { RestTuple } from '@adlete-utils/typescript';

export interface IEvidencePipe<
  TInputs extends Record<keyof Partial<TInputs>, unknown>,
  TOutputs extends Record<keyof Partial<TOutputs>, unknown>,
> {
  events: ITypedEventEmitterNoEmit<TOutputs>;
  receive<K extends keyof TInputs>(context: K, input: TInputs[K]): void;
  inputContexts: (keyof TInputs)[];
  outputContexts: (keyof TOutputs)[];
}

// TODO: use Rx instead of events?
export class EvidencePipeline<
  TInputs extends Record<keyof Partial<TInputs>, unknown>,
  TOutputs extends Record<keyof Partial<TOutputs>, unknown>,
> {
  protected _events: ITypedEventEmitter<TOutputs>;

  protected foo: Partial<TInputs>;

  protected _startPipes: IEvidencePipe<TInputs, unknown>[];

  protected _endPipes: IEvidencePipe<unknown, TOutputs>[];

  public get events(): ITypedEventEmitterNoEmit<TOutputs> {
    return this._events;
  }

  public constructor() {
    this._startPipes = [];
    this._endPipes = [];
    this._events = new TypedEventEmitter();
  }

  // TODO: better generic types so that pipes don't have to be added via any/unknown
  public addStartPipe(pipe: IEvidencePipe<TInputs, unknown>) {
    this._startPipes.push(pipe);
  }

  // TODO: better generic types so that pipes don't have to be added via any/unknown
  public addEndPipe(pipe: IEvidencePipe<unknown, TOutputs>, contexts?: (keyof TOutputs)[]) {
    this._endPipes.push(pipe);
    // TODO: clean up event listeners
    const usedContexts = contexts || pipe.outputContexts;
    usedContexts.forEach((context) => pipe.events.on(context, this.forwardOutputs.bind(this, context)));
  }

  protected forwardOutputs<K extends keyof TOutputs>(context: K, ...output: RestTuple<TOutputs[K]>) {
    this._events.emit(context, ...output);
  }

  public receive<K extends keyof TInputs>(context: K, input: TInputs[K]): void {
    this._startPipes.forEach((pipe) => {
      if (pipe.inputContexts.includes(context)) pipe.receive(context, input);
    });
  }
}
