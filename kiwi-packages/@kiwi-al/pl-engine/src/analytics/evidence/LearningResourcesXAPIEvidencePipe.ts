import type { Activity, Statement } from '@xapi/xapi';
import { flatten } from 'ramda';

import type { ILearningResourceMeta } from '@adlete-semantics/amb';
import { type ITypedEventEmitter, type ITypedEventEmitterNoEmit, TypedEventEmitter } from '@adlete-utils/events';

import {
  createAssertionStatementFromStatement,
  DEFAULT_ASSERTION_OBJECT_ID,
  IAssertionCompetencyResult,
} from '../../competencies/assertions/xapi-conversion';

import type { IEvidencePipe } from './EvidencePipeline';

export function canStatementBeUsedForAssertions(statement: Statement): boolean {
  if ((statement.object as Activity)?.id === DEFAULT_ASSERTION_OBJECT_ID) return false;

  return !!statement.result;
}

export interface IXAPIInputs {
  xAPIStatements: Statement[];
}
export interface IXAPIOutputs extends IXAPIInputs {}

export class LearningResourcesXAPIEvidencePipe implements IEvidencePipe<IXAPIInputs, IXAPIOutputs> {
  protected _watchedResources: Record<string, ILearningResourceMeta>;

  protected _events: ITypedEventEmitter<IXAPIOutputs>;

  public get events(): ITypedEventEmitterNoEmit<IXAPIOutputs> {
    return this._events;
  }

  public inputContexts: (keyof IXAPIInputs)[] = ['xAPIStatements'];

  public outputContexts: (keyof IXAPIOutputs)[] = ['xAPIStatements'];

  public constructor() {
    this._watchedResources = {};
    this.belongsToWatchedLR = this.belongsToWatchedLR.bind(this);
    this.hasCompetencies = this.hasCompetencies.bind(this);
    this._events = new TypedEventEmitter();
  }

  public isWatchingLearningResource(lrMeta: ILearningResourceMeta): boolean {
    return lrMeta.id in this._watchedResources;
  }

  public watchLearningResource(lrMeta: ILearningResourceMeta) {
    if (lrMeta.id in this._watchedResources)
      // TODO: throw error or log
      return;

    this._watchedResources[lrMeta.id] = lrMeta;
  }

  public unwatchLearningResource(lrMeta: ILearningResourceMeta) {
    if (!(lrMeta.id in this._watchedResources))
      // TODO: throw error or log
      return;

    delete this._watchedResources[lrMeta.id];
  }

  protected getLRMetaFromStatement(statement: Statement): ILearningResourceMeta {
    const id = (statement.object as Activity).id;
    return this._watchedResources[id];
  }

  protected belongsToWatchedLR(statement: Statement): boolean {
    const activity = statement.object as Activity;
    // TODO: support other objectTypes
    if (activity.objectType !== 'Activity') return false;
    return activity.id in this._watchedResources;
  }

  protected hasCompetencies(statement: Statement): boolean {
    const lrMeta = this.getLRMetaFromStatement(statement);
    return lrMeta?.assesses?.length > 0 || lrMeta?.teaches?.length > 0;
  }

  public receive(context: keyof IXAPIInputs, statements: Statement[]): void {
    const assertableStatements = statements
      .filter(this.belongsToWatchedLR)
      .filter(this.hasCompetencies)
      .filter(canStatementBeUsedForAssertions);

    const assertionsStatements = assertableStatements.map((statement) => {
      const lrMeta = this.getLRMetaFromStatement(statement);
      const competencyObjects =
        lrMeta.teaches && lrMeta.assesses ? flatten([lrMeta.teaches, lrMeta.assesses]) : lrMeta.teaches || lrMeta.assesses;
      const competencyIds = competencyObjects.map((co) => co.id);

      // TODO: filter by verb and result?
      // TODO: add more information from result
      const competencyExtensions: IAssertionCompetencyResult[] = competencyIds.map((id) => ({ id }));

      return createAssertionStatementFromStatement(statement, competencyExtensions);
    });

    if (assertableStatements.length > 0) this._events.emit('xAPIStatements', assertionsStatements);
  }
}
