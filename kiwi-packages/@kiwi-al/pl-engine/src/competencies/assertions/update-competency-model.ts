import { IAssertion } from '@adlete-semantics/cass-json-ld';

import { CompetencyLearnerModel } from '../../analytics';

export function updateCompetencyLearnerModel(learnerModel: CompetencyLearnerModel<number>, assertions: IAssertion[]): void {
  // TODO: more sophisticated updating mechanism

  const filteredAssertions = assertions.filter((assertion) => learnerModel.supportsKey(assertion.competency));

  const competencyIds = filteredAssertions.map((assertion) => assertion.competency);
  const scores = filteredAssertions.map(() => 1); // TODO: getScore function

  learnerModel.setValues(competencyIds, scores);
}
