import type { Statement } from '@xapi/xapi';

import { IAssertion } from '@adlete-semantics/cass-json-ld';

export interface ICompetencyAssertionGenerator {
  generate(statements: Statement[]): IAssertion[];
}
