import type { Activity, Result, Statement, Verb } from '@xapi/xapi';

import { createAssertion, IAssertion, ICreateAssertionTemplate } from '@adlete-semantics/cass-json-ld';
import { deserializeActor, isSerializedActor, serializeActor } from '@adlete-semantics/xapi';

// Example AssertionStatement
//
// {
//   "actor": {
//       "mbox": "mailto:some-agent@foo.bar",
//       "objectType": "Agent"
//   },
//   "verb": {
//       "id": "http://adlnet.gov/expapi/verbs/passed"
//   },
//   "object": {
//       "id": "https://schema.cassproject.org/0.4/Assertion",
//       "objectType": "Activity"
//   },
//   "result": {
//     "success": true,
//     "extensions": {
//       "competencies": [ {
//         "id": "https://cass.fki.htw-berlin.de/api/data/schema.cassproject.org.0.4.Competency/8794f37f-c455-4af6-9db2-c7edd7b93a15",
//         "score": 0.5
//       } ]
//     }
//   }
// }

export const DEFAULT_ASSERTION_OBJECT_ID = 'https://schema.cassproject.org/0.4/Assertion';
export const DEFAULT_ASSERTION_VERB: Verb = { id: 'http://activitystrea.ms/schema/1.0/receive', display: { en: 'receive' } };

export interface IAssertionCompetencyResult {
  id: string;
  score?: number;
  confidence?: number;
  evidence?: string;
}

export interface IAssertionResultExtensions {
  competencies: IAssertionCompetencyResult[];
}

// TODO: store assertion data in object or result?
export interface IAssertionStatement extends Statement {
  object: Activity;
  result: Result & { extensions: IAssertionResultExtensions };
}

export function isAssertionStatement(statement: Statement): boolean {
  const object = statement.object as Activity;

  // TODO: custom version
  return object.id === DEFAULT_ASSERTION_OBJECT_ID;
}

export function validateAssertionStatement(statement: IAssertionStatement): void {
  if (!isAssertionStatement(statement))
    throw new Error(`Given xAPI statement (${statement.id || 'unknown id'}) is not an assertion statement!`);

  if (!statement.result) throw new Error(`xAPI AssertionStatement (${statement.id || 'unknown id'}) must have a 'result'!`);

  if (!statement.result.extensions)
    throw new Error(`xAPI AssertionStatement (${statement.id || 'unknown id'}) must have 'result.extensions'!`);

  if (!statement.result.extensions.competencies)
    throw new Error(`xAPI AssertionStatement (${statement.id || 'unknown id'}) must have 'result.extensions.competencies'!`);
}

export function createAssertionsFromStatement(statement: Statement, assertionEndpoint?: string): IAssertion[] {
  const assertionStatement = statement as IAssertionStatement;
  validateAssertionStatement(assertionStatement);

  const subject = serializeActor(statement.actor);
  const agent = statement.authority && serializeActor(statement.authority);
  const assertionDate = statement.timestamp && new Date(statement.timestamp).getTime();

  return assertionStatement.result.extensions.competencies.map((competencyResult) => {
    const partialAssertion: ICreateAssertionTemplate = {
      competency: competencyResult.id,
      subject,
      assertionDate,
    };

    if (agent) partialAssertion.agent = agent;

    if (competencyResult.confidence) partialAssertion.confidence = competencyResult.confidence;
    if (statement.id) partialAssertion.evidence = statement.id;

    return createAssertion(partialAssertion, { endPoint: assertionEndpoint });
  });
}

export function createStatementsFromAssertions(assertions: IAssertion[]): IAssertionStatement[] {
  // TODO: check if multiple assertions could be coerced to a single statement

  return assertions.map((assertion) => {
    // TODO: better object if not serialized actor ifi
    const actor = isSerializedActor(assertion.subject)
      ? deserializeActor(assertion.subject)
      : ({ objectType: 'Agent', mbox: assertion.subject } as const);
    const verb = DEFAULT_ASSERTION_VERB;
    const object = { objectType: 'Activity', id: DEFAULT_ASSERTION_OBJECT_ID } as const;
    const result = { extensions: { competencies: [{ id: assertion.competency }] } };
    const statement: IAssertionStatement = { actor, verb, object, result };
    if ('negative' in assertion) statement.result.success = !assertion.negative;
    if (assertion.agent)
      statement.authority = isSerializedActor(assertion.agent)
        ? deserializeActor(assertion.agent)
        : ({ objectType: 'Agent', mbox: assertion.agent } as const);
    return statement;
  });
}

export function createAssertionStatementFromStatement(
  statement: Statement,
  competencies: IAssertionCompetencyResult[]
): IAssertionStatement {
  const actor = statement.actor;
  const verb = DEFAULT_ASSERTION_VERB;
  const object = { objectType: 'Activity', id: DEFAULT_ASSERTION_OBJECT_ID } as const;
  const result = { extensions: { competencies } };

  // TODO: original statement id as evidence!

  const assertionStatement: IAssertionStatement = { actor, verb, object, result };

  if (statement.authority) assertionStatement.authority = statement.authority;
  if (statement.context) assertionStatement.context = statement.context;

  return assertionStatement;
}
