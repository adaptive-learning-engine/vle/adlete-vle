import type { ICASSGraph } from '@adlete-semantics/cass-graph';

// Events?
export class CompetencyRecommender {
  protected graph: ICASSGraph;

  public constructor(graph: ICASSGraph) {
    this.graph = graph;
  }

  public recommend(): string[] {
    return this.graph.nodes();
  }
}
