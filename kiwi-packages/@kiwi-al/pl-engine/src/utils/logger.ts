import { Logger } from '@adlete-vle/lr-core/logging';

export const PLEngineLogger = Logger.withTag('pl-engine');
