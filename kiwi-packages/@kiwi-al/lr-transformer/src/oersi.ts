import 'cross-fetch/polyfill';

import { OersiAPI } from '@adlete-semantics/oersi-api';

const endPoint = 'http://localhost:8080/oersi';

async function convert() {
  const id = 'https://axel-klinger.gitlab.io/gitlab-for-documents/index.html';

  const api = new OersiAPI({ backendEndPoint: endPoint, userName: 'test', password: 'test' });

  const meta = await api.getMeta(id);
  // console.log(meta);
  // meta.id = 'test';
  // const newMeta = { ...meta, learningResourceType: ['asdasd'] };
  await api.setMeta(meta);
}

convert();
