import { createConsola } from 'consola';

export const Logger = createConsola({ defaults: { tag: 'Transformer' } });
