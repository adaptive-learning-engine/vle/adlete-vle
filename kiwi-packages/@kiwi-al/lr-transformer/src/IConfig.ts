import { IDownloadCompetencyFrameworksOptions } from './competencies/download-competencies';

export interface IConfig {
  sourceMode: 'filesystem';
  sourceDir: string;
  destinationMode: 'filesystem';
  destinationFile: string;

  downloadCompetencyFrameworks: IDownloadCompetencyFrameworksOptions;
}
