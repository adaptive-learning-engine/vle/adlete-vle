import fs, { promises as fsPromise } from 'fs';
import path from 'path';

import glob from 'glob-promise';

import type { ILearningResourceMeta } from '@adlete-semantics/amb';
import type { ILearningResource } from '@adlete-vle/lr-core/learning-resources';

import { Logger } from '../utils/logger';

type Content = string | Record<string, unknown>;

async function loadMetaJSON(filepath: string): Promise<ILearningResourceMeta> {
  const content = await fsPromise.readFile(filepath, { encoding: 'utf-8' });
  return JSON.parse(content);
}

async function getMetaFilepaths(sourceDir: string): Promise<string[]> {
  return glob(path.join(sourceDir, '/**/*.meta.json'));
}

async function getContentFilepaths(metaFilePaths: string[]): Promise<Record<string, string>> {
  const contentPathsGlobs = metaFilePaths.map((metaPath) => metaPath.replace('.meta.json', '.*'));

  const contentFilePaths = await Promise.all(
    contentPathsGlobs.map(async (contentPathGlob, i) => {
      const filePaths = await glob(contentPathGlob, { ignore: [metaFilePaths[i]] });

      if (filePaths.length === 0) {
        Logger.info(`No content file found for meta entry ${contentPathGlob}`);

        return Promise.resolve(null);
      }
      if (filePaths.length === 1) return filePaths[0];

      Logger.warn(`Multiple content files found for meta entry ${contentPathGlob}`);
      return filePaths[0];
    })
  );

  return Object.fromEntries(contentFilePaths.map((contentFilePath, i) => [metaFilePaths[i], contentFilePath]));
}

async function loadContent(filepath: string): Promise<Content> {
  const ext = path.extname(filepath);

  let content: Content;

  switch (ext) {
    case '.md':
      content = await fsPromise.readFile(filepath, { encoding: 'utf-8' });
      break;
    case '.json':
      content = JSON.parse(await fsPromise.readFile(filepath, { encoding: 'utf-8' }));
      break;
    default:
      throw new Error(`Unsupported file type ${ext}!`);
  }

  return content;
}

async function loadAllContent(filePaths: Record<string, string>): Promise<Record<string, Content>> {
  const entries = Object.entries(filePaths).filter(([, value]) => value != null);
  const contents = await Promise.all(entries.map(([, filePath]) => loadContent(filePath)));

  return Object.fromEntries(contents.map((content, i) => [entries[i][0], content]));
}

function createLearningResources(
  metaFilePaths: string[],
  metaData: ILearningResourceMeta[],
  content: Record<string, Content>
): ILearningResource[] {
  return metaFilePaths.map((metaFilePath, i) => ({ meta: metaData[i], content: content[metaFilePath] }));
}

export async function loadLearningResources(sourceDir: string): Promise<ILearningResource[]> {
  if (!fs.existsSync(sourceDir)) throw new Error(`Directory '${sourceDir}' does not exist!`);

  // loading meta data
  const metaFilepaths = await getMetaFilepaths(sourceDir);
  const metaData = await Promise.all(metaFilepaths.map(loadMetaJSON));

  // loading content
  const contentFilepaths = await getContentFilepaths(metaFilepaths);
  const content = await loadAllContent(contentFilepaths);

  return createLearningResources(metaFilepaths, metaData, content);
}
