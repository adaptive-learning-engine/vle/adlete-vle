import { promises as fsPromise } from 'fs';
import path from 'path';

import { ILearningResource } from '@adlete-vle/lr-core/learning-resources';

export async function saveLearningResources(learningResources: ILearningResource[], outFile: string): Promise<void> {
  const ext = path.extname(outFile);

  switch (ext) {
    case '.js': {
      let content = `const content = ${JSON.stringify(learningResources, null, 2)};\n\n`;
      content += 'export default content;';
      await fsPromise.writeFile(outFile, content);
      break;
    }
    case '.json': {
      await fsPromise.writeFile(outFile, JSON.stringify(learningResources, null, 2));
      break;
    }
    case '.ts': {
      let content = "import type { ILearningResource } from '@adlete-vle/lr-core/learning-resources/learning-resource';\n\n";
      content += `const content: ILearningResource[] = ${JSON.stringify(learningResources, null, 2)};\n\n`;
      content += 'export default content;';
      await fsPromise.writeFile(outFile, content);
      break;
    }
    default:
      throw new Error(`Cannot save file. Unsupported file type ${ext}!`);
  }
}
