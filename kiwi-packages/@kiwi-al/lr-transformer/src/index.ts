import { promises as fsPromise } from 'fs';

import { program } from 'commander';
import { parse } from 'yaml';

import { IConfig } from './IConfig';
import { downloadCompetencyFrameworks } from './competencies/download-competencies';
import { loadLearningResources } from './learning-resources/load-filesystem';
import { saveLearningResources } from './learning-resources/save-filesystem';
import { Logger } from './utils/logger';

async function executeLR(configFilePath: string) {
  Logger.info("Executing command 'convert-learning-resources'");

  const configFile = await fsPromise.readFile(configFilePath, 'utf8');
  const config: IConfig = parse(configFile);

  if (config.sourceMode !== 'filesystem') throw new Error(`Unknown sourceMode: ${config.sourceMode}`);
  if (config.destinationMode !== 'filesystem') throw new Error(`Unknown destinationMode: ${config.destinationMode}`);

  const learningResources = await loadLearningResources(config.sourceDir);
  await saveLearningResources(learningResources, config.destinationFile);
}

async function executeDownloadCompetencies(configFilePath: string) {
  Logger.info("Executing command 'download-competencies'");
  const configFile = await fsPromise.readFile(configFilePath, 'utf8');
  const config: IConfig = parse(configFile);

  // TODO: validate options
  await downloadCompetencyFrameworks(config.downloadCompetencyFrameworks);

  // const learningResources = await loadLearningResources(config.sourceDir);
  // await saveLearningResources(learningResources, config.destinationFile);
}

program.command('convert-learning-resources').argument('[config]', 'Path to config file', './config.yaml').action(executeLR);
program.command('download-competencies').argument('[config]', 'Path to config file', './config.yaml').action(executeDownloadCompetencies);

await program.parseAsync();
