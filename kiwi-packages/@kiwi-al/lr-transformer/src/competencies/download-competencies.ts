import fsAsync from 'fs/promises';
import path from 'path';

import { API } from '@adlete-semantics/cass-api';
import { flattenCassObjectCollection, getUUIDFromURI, ICassNodeObject } from '@adlete-semantics/cass-json-ld';
import { asString } from '@adlete-semantics/json-ld';

export interface IFetchCompetencyFrameworksOptions {
  endPoint: string;
  frameworkIds: string[];
}

export interface IDownloadCompetencyFrameworksOptions extends IFetchCompetencyFrameworksOptions {
  merge: boolean;
  destination: string;
}

export async function fetchCompetencyFrameworks(options: IFetchCompetencyFrameworksOptions): Promise<ICassNodeObject[]> {
  const api = new API(options.endPoint);

  const collection = await api.fetchFullFrameworks(options.frameworkIds);

  return flattenCassObjectCollection(collection);
}

async function writeFileEnsureDir(filePath: string, content: string): Promise<void> {
  await fsAsync.mkdir(path.dirname(filePath), { recursive: true });
  await fsAsync.writeFile(filePath, content, { encoding: 'utf-8' });
}

export async function saveCASSObjects(objects: ICassNodeObject[], destinationDir: string): Promise<void> {
  await Promise.all(
    objects.map((obj) =>
      writeFileEnsureDir(
        `${path.join(destinationDir, asString(obj['@type']), getUUIDFromURI(asString(obj['@id'])))}.json`,
        JSON.stringify(obj, null, 2)
      )
    )
  );
}

export async function saveCASSObjectsSingle(objects: ICassNodeObject[], destinationFile: string) {
  const ext = path.extname(destinationFile);

  switch (ext) {
    case '.js': {
      let content = `const content = ${JSON.stringify(objects, null, 2)};\n\n`;
      content += 'export default content;';
      await writeFileEnsureDir(destinationFile, content);
      break;
    }
    case '.json': {
      await writeFileEnsureDir(destinationFile, JSON.stringify(objects, null, 2));
      break;
    }
    case '.ts': {
      let content = "import type { ICassNodeObject } from '@adlete-semantics/cass-json-ld';\n\n";
      content += `const content: ICassNodeObject[] = ${JSON.stringify(objects, null, 2)};\n\n`;
      content += 'export default content;';
      await writeFileEnsureDir(destinationFile, content);
      break;
    }
    default:
      throw new Error(`Cannot save file. Unsupported file type ${ext}!`);
  }
}

export async function downloadCompetencyFrameworks(options: IDownloadCompetencyFrameworksOptions): Promise<void> {
  const objects = await fetchCompetencyFrameworks(options);
  if (options.merge) {
    await saveCASSObjectsSingle(objects, options.destination);
  } else {
    await saveCASSObjects(objects, options.destination);
  }
}
