import React, { useContext } from 'react';

import { RandomExperimentManager } from '@kiwi-al/math-core/random-experiment';

export const RandomExperimentManagerContext = React.createContext<RandomExperimentManager>(null);

export function useRandomExperimentManager(): RandomExperimentManager {
  return useContext(RandomExperimentManagerContext);
}
