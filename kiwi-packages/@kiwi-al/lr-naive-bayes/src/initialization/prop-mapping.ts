import { ReactPropMapperLibrary } from '@adlete-vle/lr-core-components/ReactPropMapperLibrary';
import { IResourceProps } from '@adlete-vle/lr-core-components/learning-resources';

export function initPropMappings(propMapperLibrary: ReactPropMapperLibrary): void {
  propMapperLibrary.registerMapper('AnyText', 'learning-resources', (props: IResourceProps) => ({ text: props.resource }));
  propMapperLibrary.registerMapper('AnyTextWithImage', 'learning-resources', (props: IResourceProps) => ({
    text: props.resource,
  }));
  propMapperLibrary.registerMapper('Definition', 'learning-resources', (props: IResourceProps) => ({
    definition: props.resource,
  }));
  propMapperLibrary.registerMapper('EmbeddedVideo', 'learning-resources', (props: IResourceProps) => ({
    video: props.resource,
  }));
  propMapperLibrary.registerMapper('Examples', 'learning-resources', (props: IResourceProps) => ({
    examples: props.resources,
  }));
  propMapperLibrary.registerMapper('ExampleProblems', 'learning-resources', (props: IResourceProps) => ({
    problems: props.resources,
  }));
  propMapperLibrary.registerMapper('IFrame', 'learning-resources', (props: IResourceProps) => ({ iframe: props.resource }));
  propMapperLibrary.registerMapper('Equation', 'learning-resources', (props: IResourceProps) => ({ equation: props.resource }));
  propMapperLibrary.registerMapper('Hint', 'learning-resources', (props: IResourceProps) => ({ hint: props.resource }));
  propMapperLibrary.registerMapper('LRComponent', 'learning-resources', (props: IResourceProps) => ({
    learningResource: props.resource,
    forwardedProps: props,
  }));
  propMapperLibrary.registerMapper('ToDo', 'learning-resources', (props: IResourceProps) => ({ todo: props.resource }));
}
