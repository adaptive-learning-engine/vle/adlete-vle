import { ReactComponentFactoryLibrary } from '@adlete-vle/lr-core-components/ReactComponentFactoryLibrary';
import {
  AnyText,
  AnyTextWithImage,
  Definition,
  EmbeddedVideo,
  Equation,
  ExampleProblems,
  Examples,
  Hint,
  IFrame,
  LRComponent,
  ToDo,
} from '@adlete-vle/lr-core-components/learning-resources';

export function initReactComponentFactoryLibrary(factoryLib: ReactComponentFactoryLibrary): void {
  factoryLib.registerFactory('AnyText', { component: AnyText });
  factoryLib.registerFactory('AnyTextWithImage', { component: AnyTextWithImage });
  factoryLib.registerFactory('Definition', { component: Definition });
  factoryLib.registerFactory('EmbeddedVideo', { component: EmbeddedVideo });
  factoryLib.registerFactory('Equation', { component: Equation });
  factoryLib.registerFactory('Examples', { component: Examples });
  factoryLib.registerFactory('ExampleProblems', { component: ExampleProblems });
  factoryLib.registerFactory('IFrame', { component: IFrame });
  factoryLib.registerFactory('Hint', { component: Hint });
  factoryLib.registerFactory('LRComponent', { component: LRComponent });
  factoryLib.registerFactory('ToDo', { component: ToDo });
}
