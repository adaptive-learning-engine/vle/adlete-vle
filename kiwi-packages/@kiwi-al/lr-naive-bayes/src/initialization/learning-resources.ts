import i18next, { type i18n } from 'i18next';

import { ILearningResource, LearningResourceManager } from '@adlete-vle/lr-core/learning-resources';
import { addMultiLangResourceBundleWithNS } from '@adlete-vle/lr-core/locale';
import { IJSkosConceptScheme, learningResourcesFromVocab } from '@adlete-vle/lr-core/vocabulary';

import LEARNING_RESOURCES from '@kiwi-al/lr-naive-bayes-static/learning-resources';
import { IRandomExperimentTemplate, learningResourcesFromRET, RandomExperimentManager } from '@kiwi-al/math-core/random-experiment';

import { DevComponent } from '../conditional-probability/DevComponent';
import {
  CPTExercise1,
  RepeatableCalculateCPsFromFreqsExercise1,
  RepeatableCalculateCPsFromProbsExercise1,
  RepeatableCalculateFromCPsExercise1,
  RepeatableCPCardsExercise1,
  RepeatableIndependenceExercise1,
  RepeatableTotalProbabilitiesExercise1,
} from '../conditional-probability/exercises';
import {
  CPDiceExplainer1,
  CPFromFreqsExplainer,
  CPFromProbsExplaienr,
  CPIndependenceExplainer,
  CPStackedBarChartExplainer,
  CPTExplainer,
  CPTreeExplainer,
  CPVennFreqsExplainer,
  CPVennSampleSpaceExplainer,
  TotalProbabilityExplainer,
} from '../conditional-probability/explainers';
import { CourseContentsLR, FinalPageLR, LearningEnvironmentLR, WelcomeLR } from '../extra-learning-resources';
import {
  DrawingACardRETLearningResource,
  LaptopsWithVirusRETLearningResource,
  LecturesAndExamsRETLearningResource,
  OnlineDatingRETLearningResource,
  PersonalizedAdvertisementRETLearningResource,
  Rolling2DiceRETLearningResource,
  StudentsInClassesRETLearningResource,
} from '../random-experiments';

const ADDITIONAL_LRS: ILearningResource[] = [WelcomeLR, CourseContentsLR, LearningEnvironmentLR, FinalPageLR];

export function addLRComponentFactories(lrm: LearningResourceManager): void {
  // DEV
  lrm.setContent('http://localhost/data/content/kiwi-vle/probability/conditional-probability/dev/dev-1.json', {
    factory: {
      component: DevComponent,
    },
  });

  // EXERCISES
  lrm.setContent('http://localhost/data/content/kiwi-vle/probability/conditional-probability/exercises/cpt-1.json', {
    factory: {
      component: CPTExercise1,
    },
  });
  lrm.setContent('http://localhost/data/content/kiwi-vle/probability/conditional-probability/exercises/calculate-cps-from-freqs-1.json', {
    factory: {
      component: RepeatableCalculateCPsFromFreqsExercise1,
    },
  });
  lrm.setContent('http://localhost/data/content/kiwi-vle/probability/conditional-probability/exercises/calculate-cps-from-probs-1.json', {
    factory: {
      component: RepeatableCalculateCPsFromProbsExercise1,
    },
  });
  lrm.setContent('http://localhost/data/content/kiwi-vle/probability/conditional-probability/exercises/calculate-freqs-from-cps-1.json', {
    factory: {
      component: RepeatableCalculateFromCPsExercise1,
    },
  });
  lrm.setContent('http://localhost/data/content/kiwi-vle/probability/conditional-probability/exercises/cards-1.json', {
    factory: {
      component: RepeatableCPCardsExercise1,
    },
  });
  lrm.setContent(
    'http://localhost/data/content/kiwi-vle/probability/conditional-probability/exercises/calculate-total-probabilities-1.json',
    {
      factory: {
        component: RepeatableTotalProbabilitiesExercise1,
      },
    }
  );
  lrm.setContent('http://localhost/data/content/kiwi-vle/probability/conditional-probability/exercises/calculate-independence-1.json', {
    factory: {
      component: RepeatableIndependenceExercise1,
    },
  });

  // EXPLAINERS
  lrm.setContent('http://localhost/data/content/kiwi-vle/probability/conditional-probability/explainers/cpt.json', {
    factory: {
      component: CPTExplainer,
    },
  });
  lrm.setContent('http://localhost/data/content/kiwi-vle/probability/conditional-probability/explainers/cp-stacked-bar-chart.json', {
    factory: {
      component: CPStackedBarChartExplainer,
    },
  });
  lrm.setContent('http://localhost/data/content/kiwi-vle/probability/conditional-probability/explainers/cptree.json', {
    factory: {
      component: CPTreeExplainer,
    },
  });
  lrm.setContent('http://localhost/data/content/kiwi-vle/probability/conditional-probability/explainers/dice-1.json', {
    factory: {
      component: CPDiceExplainer1,
    },
  });
  lrm.setContent('http://localhost/data/content/kiwi-vle/probability/conditional-probability/explainers/from-freqs.json', {
    factory: {
      component: CPFromFreqsExplainer,
    },
  });
  lrm.setContent('http://localhost/data/content/kiwi-vle/probability/conditional-probability/explainers/from-probs.json', {
    factory: {
      component: CPFromProbsExplaienr,
    },
  });
  lrm.setContent('http://localhost/data/content/kiwi-vle/probability/conditional-probability/explainers/venn-freqs.json', {
    factory: {
      component: CPVennFreqsExplainer,
    },
  });
  lrm.setContent('http://localhost/data/content/kiwi-vle/probability/conditional-probability/explainers/independence.json', {
    factory: {
      component: CPIndependenceExplainer,
    },
  });
  lrm.setContent('http://localhost/data/content/kiwi-vle/probability/conditional-probability/explainers/total-probability.json', {
    factory: {
      component: TotalProbabilityExplainer,
    },
  });
  lrm.setContent('http://localhost/data/content/kiwi-vle/probability/conditional-probability/explainers/venn-sample-space.json', {
    factory: {
      component: CPVennSampleSpaceExplainer,
    },
  });
}

// TODO: get random experiment template from learning resources, not file
const RANDOM_EXPERIMENT_LRS: ILearningResource<IRandomExperimentTemplate>[] = [
  OnlineDatingRETLearningResource,
  LaptopsWithVirusRETLearningResource,
  PersonalizedAdvertisementRETLearningResource,
  StudentsInClassesRETLearningResource,
  LecturesAndExamsRETLearningResource,
  Rolling2DiceRETLearningResource,
  DrawingACardRETLearningResource,
];

export function initLearningResourceManager(lrm: LearningResourceManager): void {
  lrm.registerIdPrefix('http://localhost/data/content/math/probability/');
  lrm.registerIdPrefix('http://localhost/data/content/kiwi-vle/probability/');
  lrm.addResources(LEARNING_RESOURCES);
  addLRComponentFactories(lrm);

  // vocabulary
  const vocab = lrm.getContent<IJSkosConceptScheme>('conditional-probability/vocabulary.json');
  lrm.vocabulary.registerIdPrefix(vocab.uri);
  lrm.vocabulary.addConceptsFromScheme(vocab);

  // resources from vocab
  const lrsFromVocab = learningResourcesFromVocab(vocab);
  lrm.registerIdPrefix(vocab.uri);
  lrm.addResources(lrsFromVocab);

  // additional learning resources
  ADDITIONAL_LRS.forEach((lr) => {
    lrm.addResource(lr);
  });

  // resources from random experiments
  RANDOM_EXPERIMENT_LRS.forEach((lr) => {
    lrm.addResources(learningResourcesFromRET(lr));
  });
}

export function initRandomExperimentManager(rem: RandomExperimentManager, i18nextInstance?: i18n): void {
  RANDOM_EXPERIMENT_LRS.forEach((lr) => {
    rem.addTemplate(lr.content);
  });

  function addLocales(i18nextInstance?: i18n) {
    i18nextInstance = i18nextInstance || i18next;
    RANDOM_EXPERIMENT_LRS.forEach((lr) => {
      addMultiLangResourceBundleWithNS(lr.content.locale, i18nextInstance);
    });
  }

  if (i18next.isInitialized) {
    addLocales(i18nextInstance);
  } else {
    i18next.on('initialized', addLocales);
  }
}
