import { IContentItem } from '@adlete-vle/lr-core-components/dnd';

import { fisherYatesShuffle, getRandomInt } from '@kiwi-al/math-core/randomization';

export function createProbabilitySolutionOptions(solutions: number[], numSolutions: number): number[] {
  const options = [...solutions];
  while (options.length < numSolutions) {
    options.push(Math.random());
  }
  fisherYatesShuffle(options);
  return options;
}

export function createIntegerSolutionOptions(solution: number, min: number, max: number, numSolutions: number): number[] {
  const options = [solution];
  for (let iSol = 0; iSol < numSolutions - 1; ++iSol) {
    for (let iTry = 0; iTry < 1000; ++iTry) {
      const randomSolution = getRandomInt(min, max);
      if (randomSolution !== solution) {
        options.push(randomSolution);
        break;
      }
    }
  }
  fisherYatesShuffle(options);
  return options;
}

export function isPrimitiveSolutionValid(
  target: IContentItem<number | string | boolean>,
  chosen: IContentItem<number | string | boolean>
): boolean {
  return target.content === chosen.content;
}
