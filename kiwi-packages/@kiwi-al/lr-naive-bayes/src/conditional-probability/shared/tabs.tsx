import { Box, Tab, Tabs } from '@mui/material';
import React from 'react';

export interface ITabHeaderProps {
  tabIndex: number;
  onChange: (event: React.SyntheticEvent, newValue: number) => void;
  labels: string[];
}

export function TabHeader(props: ITabHeaderProps): React.ReactElement {
  const { tabIndex, onChange, labels } = props;
  return (
    <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
      <Tabs value={tabIndex} onChange={onChange} scrollButtons="auto">
        {labels.map((label) => (
          <Tab key={label} label={label} wrapped />
        ))}
      </Tabs>
    </Box>
  );
}

export interface ITabPanelProps {
  children: React.ReactNode;
  index: number;
  value: number;
}

export function TabPanel(props: ITabPanelProps): React.ReactNode {
  const { children, value, index } = props;
  return value === index && children;
}

export function useTab(): [number, (event: React.SyntheticEvent, newValue: number) => void] {
  const [value, setValue] = React.useState(0);

  const handleChange = (event: React.SyntheticEvent, newValue: number) => {
    setValue(newValue);
  };
  return [value, handleChange];
}

export type TabPanelInfo = [string, React.ReactNode];

export type TabPanelInfos = (TabPanelInfo | boolean)[] | Record<string, TabPanelInfo>;

export interface ITabbedContentProps {
  panels: TabPanelInfos;
}

export function TabbedContent({ panels }: ITabbedContentProps): React.ReactElement {
  const [tabIndex, setTabIndex] = useTab();

  const panelsArray = (Array.isArray(panels) ? panels : Object.values(panels)).filter((elem) => elem) as TabPanelInfo[];

  const labels = panelsArray.map((panel) => panel[0]);

  return (
    <>
      <TabHeader tabIndex={tabIndex} onChange={setTabIndex} labels={labels} />
      {panelsArray.map((panel, i) => (
        <TabPanel key={panel[0]} value={tabIndex} index={i}>
          {panel[1]}
        </TabPanel>
      ))}
    </>
  );
}
