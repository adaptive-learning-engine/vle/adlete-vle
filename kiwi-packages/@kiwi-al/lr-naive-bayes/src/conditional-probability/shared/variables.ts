import { i18n } from 'i18next';

import { convertToEventVariables } from '@kiwi-al/math-core/events';
import { IBivarFreqRET } from '@kiwi-al/math-core/frequencies';
import { getAlternateVariableNamesFromRET, getShortCategoryNamesFromRET } from '@kiwi-al/math-core/random-experiment';
import { BivarCategoricalVarDefs, copyVariablesWithNewNames } from '@kiwi-al/math-core/variables';

export interface IVariableSet {
  short: BivarCategoricalVarDefs;
  middle: BivarCategoricalVarDefs;
  long: BivarCategoricalVarDefs;
}

export function getVariableSet(ret: IBivarFreqRET, preferEventVariables?: boolean, i18n?: i18n): IVariableSet {
  const variables = ret.variables as BivarCategoricalVarDefs;
  const renamedNames = getAlternateVariableNamesFromRET(ret, i18n);
  const renamedCats = getShortCategoryNamesFromRET(ret);

  const useEventVariables = preferEventVariables && variables[0].categories.length === 2 && variables[1].categories.length === 2;

  const shortVariables = useEventVariables
    ? convertToEventVariables(variables, renamedNames)
    : (copyVariablesWithNewNames(variables, renamedNames, renamedCats) as BivarCategoricalVarDefs);

  return {
    short: shortVariables,
    middle: shortVariables.map((shortVar, index) => ({ ...shortVar, name: variables[index].name })) as BivarCategoricalVarDefs,
    long: variables,
  };
}
