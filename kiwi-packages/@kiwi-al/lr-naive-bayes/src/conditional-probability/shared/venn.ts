import { extractFromExpression, ISetCombination, ISets } from '@upsetjs/react';

import { BivariateJointFrequencyDataFrame } from '@kiwi-al/math-core/frequencies';
import { BivariateJointProbabilityDataFrame } from '@kiwi-al/math-core/probability';
import { RandomVarState } from '@kiwi-al/math-core/variables';
import { convertFrequenciesToVennData, IVennDiagramDatum, IVennLabelConfig } from '@kiwi-al/math-core-components/venn';

export const SET_LABEL_CONFIG_FREQUENCY: IVennLabelConfig = {
  showName: true,
  showCardinality: true,
  numDecimals: 0,
  rename: ['A', 'B', 'C', 'D'],
};

export const SET_LABEL_CONFIG_PROBABILITY: IVennLabelConfig = {
  showName: true,
  showCardinality: true,
  numDecimals: 2,
  rename: ['A', 'B', 'C', 'D'],
};

export interface IVennGraphData {
  sets: ISets<unknown>;
  combinations: readonly (IVennDiagramDatum & ISetCombination<unknown>)[];
}

export function createVennGraphData(freqsOrProbs: BivariateJointFrequencyDataFrame | BivariateJointProbabilityDataFrame): IVennGraphData {
  const categories: [RandomVarState, RandomVarState] = [freqsOrProbs.variables[0].categories[0], freqsOrProbs.variables[1].categories[0]];
  const vennData = convertFrequenciesToVennData(freqsOrProbs, categories);
  return extractFromExpression(vennData, { type: 'distinctIntersection' });
}
