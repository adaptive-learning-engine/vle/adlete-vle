import { IBivarFreqRET } from '@kiwi-al/math-core/frequencies';
import { RandomExperimentManager, RandomExperimentType } from '@kiwi-al/math-core/random-experiment';
import { getRandomInt } from '@kiwi-al/math-core/randomization';

interface RandomExperimentTypes {
  BivariateBinary: IBivarFreqRET;
  BivariatePolynary: IBivarFreqRET;
}

export function getRETsOfType<K extends keyof RandomExperimentTypes>(rem: RandomExperimentManager, type: K): RandomExperimentTypes[K][] {
  return rem.getTemplateByType<IBivarFreqRET>(type);
}

export function getRETsOfTypes<K extends keyof RandomExperimentTypes>(
  rem: RandomExperimentManager,
  types: K[]
): RandomExperimentTypes[K][] {
  const results: RandomExperimentTypes[K][] = [];
  types.forEach((type) => results.push(...rem.getTemplateByType<IBivarFreqRET>(type)));
  return results;
}

export function getRandomBivarBinaryFreqRET(rem: RandomExperimentManager): IBivarFreqRET {
  const templates = rem.getTemplateByType<IBivarFreqRET>(RandomExperimentType.BivariateBinary);
  const randomIndex = getRandomInt(0, templates.length - 1);
  return templates[randomIndex];
}

export function getNextRandomBivarBinaryFreqRET(rem: RandomExperimentManager, previous: IBivarFreqRET[]): IBivarFreqRET {
  const templates = rem.getTemplateByType<IBivarFreqRET>(RandomExperimentType.BivariateBinary);
  const filteredTemplates = templates.filter((template) => !previous.includes(template));

  if (filteredTemplates.length === 0) {
    const randomIndex = getRandomInt(0, templates.length - 1);
    const template = templates[randomIndex];
    previous.length = 0;
    previous.push(template);
    return template;
  }

  const randomIndex = getRandomInt(0, filteredTemplates.length - 1);
  const template = filteredTemplates[randomIndex];
  previous.push(template);
  return template;
}
