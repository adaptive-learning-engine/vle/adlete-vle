import { BivariateJointFrequencyDataFrame, generateJointFrequencyData, IBivarFreqREGenInput } from '@kiwi-al/math-core/frequencies';
import {
  BivariateCPDataFrame,
  BivariateJointProbabilityDataFrame,
  getConditionalProbabilitiesFromJointFrequency,
  getCPsFromJointProbs,
  jointProbabilityDFFromFrequencyDF,
} from '@kiwi-al/math-core/probability';
import {
  BivarCategoricalVarDefs,
  getVarWithState,
  ICategoricalVarDef,
  jointVarsToString,
  varWithCatToString,
} from '@kiwi-al/math-core/variables';
import { SingleColorMap } from '@kiwi-al/math-core-components/shared/colors';

export const EVENT_COLORS = ['#ff9800', '#3f51b5', '#4caf50'];
export const EVENT_NAMES = ['A', 'B', 'C'];

export interface ICPData {
  frequencies: BivariateJointFrequencyDataFrame;
  probabilities: BivariateJointProbabilityDataFrame;
  condProbs: [BivariateCPDataFrame, BivariateCPDataFrame];
}

export interface ICreateCPDataOptions {
  condProbsSource?: 'joint-freqs' | 'joint-probs';
  roundProbCalculationsTo?: number;
  roundCPCalculationsTo?: number;
}

export function createCPData(input: IBivarFreqREGenInput, variables: BivarCategoricalVarDefs, options?: ICreateCPDataOptions): ICPData {
  const cpSource = options?.condProbsSource || 'joint-freqs';
  const roundProbCalculationsTo = options?.roundProbCalculationsTo;
  const roundCPCalculationsTo = options?.roundCPCalculationsTo;

  const calcCPOptions = { fractionDigits: roundCPCalculationsTo };

  const frequencies = generateJointFrequencyData(input, variables);
  const probabilities = jointProbabilityDFFromFrequencyDF(frequencies, { fractionDigits: roundProbCalculationsTo });
  const condProbs0 =
    cpSource === 'joint-freqs'
      ? getConditionalProbabilitiesFromJointFrequency(frequencies, true, calcCPOptions)
      : getCPsFromJointProbs(probabilities, true, calcCPOptions);
  const condProbs1 =
    cpSource === 'joint-freqs'
      ? getConditionalProbabilitiesFromJointFrequency(frequencies, false, calcCPOptions)
      : getCPsFromJointProbs(probabilities, false, calcCPOptions);

  return {
    frequencies,
    probabilities,
    condProbs: [condProbs0, condProbs1],
  };
}

export function createColorMap(keys: string[]): SingleColorMap {
  const colorMap: SingleColorMap = {};
  for (let i = 0; i < keys.length; i++) {
    colorMap[keys[i]] = EVENT_COLORS[i];
  }
  return colorMap;
}

export function createBivarMarginalJointColorMap(variables: BivarCategoricalVarDefs): SingleColorMap {
  // TODO: support versions where 0 is not positive
  const varWithCat0 = varWithCatToString(getVarWithState(variables, 0, 0));
  const varWithCat1 = varWithCatToString(getVarWithState(variables, 1, 0));
  const keys = [varWithCat0, varWithCat1, jointVarsToString(varWithCat0, varWithCat1)];
  const colorMap = createColorMap(keys);
  colorMap[variables[0].name] = colorMap[keys[0]];
  colorMap[variables[1].name] = colorMap[keys[1]];
  colorMap[jointVarsToString(varWithCat1, varWithCat0)] = colorMap[keys[2]];

  // TODO: get rid of joint
  colorMap.joint = colorMap[keys[2]];
  return colorMap;
}

export function createVariableColorMap(variables: ICategoricalVarDef[], extraColors?: string[]): SingleColorMap {
  const colorMap: SingleColorMap = {};
  let i = 0;
  for (; i < variables.length; i++) {
    colorMap[variables[i].name] = EVENT_COLORS[i];
  }

  if (extraColors) {
    for (let j = 0; j < extraColors.length; j++) {
      colorMap[extraColors[j]] = EVENT_COLORS[j + i];
    }
  }

  return colorMap;
}
