import { Dispatch, SetStateAction, useCallback, useRef, useState } from 'react';

import { useLRCardController } from '@adlete-vle/lr-core-components/learning-resources';
import { MoveableContentPosition } from '@adlete-vle/lr-core-components/misc/MoveableContent';

export function useMakeLRCardWide(
  timeout = 50
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
): [MoveableContentPosition, Dispatch<SetStateAction<MoveableContentPosition>>, React.Ref<any>] {
  const controller = useLRCardController();
  const ref = useRef(null);
  const [position, setPosition] = useState<MoveableContentPosition>('original');

  const setPositionAndClass = useCallback(
    (pos: MoveableContentPosition) => {
      setPosition(pos);
      controller.setClassName(pos === 'left' ? 'wide' : null);

      setTimeout(() => {
        ref.current?.scrollIntoView({ block: 'start', inline: 'nearest' });
      }, timeout);

      // console.log(ref.current);
      // ref.current?.scrollIntoView({ block: 'start', inline: 'nearest' });
    },
    [controller, timeout]
  );
  return [position, setPositionAndClass, ref];
}
