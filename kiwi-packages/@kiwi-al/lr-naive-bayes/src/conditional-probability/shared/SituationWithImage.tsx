import React from 'react';
import { useTranslation } from 'react-i18next';

import { LearningResourceCard } from '@adlete-vle/lr-core-components/learning-resources';
import { Image } from '@adlete-vle/lr-core-components/media';

export interface ISituationWithImageProps {
  text: string;
  imageURL: string;
}

export function SituationWithImage({ text, imageURL }: ISituationWithImageProps): React.ReactElement {
  const { t } = useTranslation('conditional-probability');
  return (
    <LearningResourceCard label={t('situation')} stackProps={{ direction: 'row', spacing: 2, justifyContent: 'space-between' }}>
      <div>{text}</div>
      {imageURL && (
        <div>
          <Image src={imageURL} width="200" />
        </div>
      )}
    </LearningResourceCard>
  );
}
