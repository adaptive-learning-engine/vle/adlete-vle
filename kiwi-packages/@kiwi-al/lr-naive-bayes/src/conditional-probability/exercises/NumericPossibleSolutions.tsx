import React from 'react';

import { getValidationTypeBool, PossibleSolutions, useAssessmentContext, useValidate } from '@adlete-vle/lr-core-components/assessment';
import { IContentItem, wrapAllAsContentItem } from '@adlete-vle/lr-core-components/dnd';
import { useMemoWith } from '@adlete-vle/utils/hooks';

import { PrimitiveItemBox } from '../components/PrimitiveItemBox';

export interface INumericPossibleSolutionsProps {
  options: number[];
  itemType?: string;
  label?: string;
  format?: (value: number) => string;
}

export function NumericPossibleSolutions(props: INumericPossibleSolutionsProps): React.ReactElement {
  const { label, options, format } = props;
  const itemType = props.itemType || 'exercise';

  const assessment = useAssessmentContext();
  const validate = useValidate();

  const optionItems = useMemoWith(wrapAllAsContentItem, [options]) as IContentItem<number>[];

  const optionElements = optionItems.map((item) => {
    const isValid = validate && assessment.solutions.some((solution) => assessment.isAnswerValidCB(item.content, solution));
    const validation = getValidationTypeBool(isValid, validate);
    return <PrimitiveItemBox key={item.content} item={item} validation={validation} format={format} />;
  });

  return (
    <PossibleSolutions items={optionItems} itemType={itemType} label={label}>
      {optionElements}
    </PossibleSolutions>
  );
}
