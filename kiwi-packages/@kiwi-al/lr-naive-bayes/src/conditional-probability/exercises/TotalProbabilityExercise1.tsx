import { Alert, Button, Divider, Stack } from '@mui/material';
import { i18n } from 'i18next';
import React, { useCallback, useMemo } from 'react';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { useTranslation } from 'react-i18next';

import { AssessmentContext, AssessmentController } from '@adlete-vle/lr-core-components/assessment';
import { LearningResourceCard } from '@adlete-vle/lr-core-components/learning-resources';
import { useResettableState } from '@adlete-vle/utils/hooks';

import { convertToEventVariables } from '@kiwi-al/math-core/events';
import { createRandomGenInput, IBivarFreqRandomPopMarginalJointGenInput, IBivarFreqRET } from '@kiwi-al/math-core/frequencies';
import { isNumberEqual2 } from '@kiwi-al/math-core/numbers';
import { BivariateCPDataFrame, formatProbability2, formatProbabilityExpression } from '@kiwi-al/math-core/probability';
import { getAlternateVariableNamesFromRET, RandomExperimentManager } from '@kiwi-al/math-core/random-experiment';
import { BivarCategoricalVarDefs, VariableWithState, VariableWithStateFormat, varToVarsWithState } from '@kiwi-al/math-core/variables';
import { CPStackedBarChart, ProbabilityBarChart } from '@kiwi-al/math-core-components/probability';
import { VarCatNameMapper } from '@kiwi-al/math-core-components/shared/VarCatNameMapper';

import { useRandomExperimentManager } from '../../contexts';
import { NumberTaskTable } from '../components/NumberTaskTable';
import { createCPData } from '../shared/data';
import { getRandomBivarBinaryFreqRET } from '../shared/rets';
import { createProbabilitySolutionOptions } from '../shared/solution-options';

import { NumericPossibleSolutions } from './NumericPossibleSolutions';
import { RepeatableExercise } from './RepeatableExercise';
import { isDnD } from './shared';

export interface ITotalProbabilityExercise1Config {
  inputMode: 'dnd' | 'text-field';
}

export interface ITotalProbabilityExercise1Data {
  ret: IBivarFreqRET;
  probTargets: VariableWithState[];
  probSolutions: number[];
  probSolutionOptions?: number[];
  givenMarginalProbs: number[];
  condProbs: BivariateCPDataFrame;
  condVarIndex: number;
  shortVariables: BivarCategoricalVarDefs;
}

export interface ITotalProbabilityExercise1Props {
  config: ITotalProbabilityExercise1Config;
  data: ITotalProbabilityExercise1Data;
  varFormat?: VariableWithStateFormat;
}

function createRandomMathData(rem: RandomExperimentManager, i18n?: i18n): ITotalProbabilityExercise1Data {
  const ret = getRandomBivarBinaryFreqRET(rem);
  const variables = ret.variables as BivarCategoricalVarDefs;
  const eventNames = getAlternateVariableNamesFromRET(ret, i18n);
  const shortVariables = convertToEventVariables(variables, eventNames);

  // const renamedNames = getAlternateVariableNamesFromRET(ret);
  // const renamedCats = getShortCategoryNamesFromRET(ret);
  // const shortVariables = copyVariablesWithNewNames(variables, renamedNames, renamedCats) as BivarCategoricalVarDefs;

  const genData = ret.generation.random as IBivarFreqRandomPopMarginalJointGenInput;
  const input = createRandomGenInput(genData);
  const cpData = createCPData(input, variables);

  const condVarIndex = 1;
  const givenVarIndex = 0;

  const probTargets = varToVarsWithState(shortVariables[condVarIndex]);
  const probSolutions = cpData.probabilities.getMarginalFrequencies(variables[condVarIndex]);
  const probSolutionOptions = createProbabilitySolutionOptions(probSolutions, variables[condVarIndex].categories.length * 3);
  return {
    ret,
    probTargets,
    probSolutions,
    probSolutionOptions,
    condProbs: cpData.condProbs[condVarIndex],
    givenMarginalProbs: cpData.probabilities.getMarginalFrequencies(variables[givenVarIndex]),
    condVarIndex,
    shortVariables,
  };
}

function createRandomConfig(): ITotalProbabilityExercise1Config {
  return {
    inputMode: Math.random() < 0.5 ? 'dnd' : 'text-field',
  };
}

export function TotalProbabilityExercise1(props: ITotalProbabilityExercise1Props): React.ReactElement {
  const { data, config, varFormat = 'StateOnly' } = props;
  const { condProbs, condVarIndex, shortVariables, givenMarginalProbs, probSolutions, probSolutionOptions, probTargets, ret } = data;

  const givenVarIndex = condVarIndex === 0 ? 1 : 0;

  const { t } = useTranslation('conditional-probability');

  const [assessment] = useResettableState(() => new AssessmentController(probSolutions, isNumberEqual2), [probSolutions]);
  const activateValidation = useCallback(() => assessment.enableValidation(), [assessment]);
  const presentSolutions = useCallback(() => assessment.enablePresentSolutions(), [assessment]);

  const renderData = useMemo(() => {
    const probLabels = probTargets.map((target) =>
      // target = eventVariables ? convertTargetByEventVariables(target, frequencies.variables, eventVariables) : target;
      formatProbabilityExpression(target, { probFormat: 'Notation', varFormat })
    );

    const description = t(ret.localeKeys['problem-description']);

    return { probLabels, description };
  }, [probTargets, varFormat, ret, t]);

  return (
    <AssessmentContext.Provider value={assessment}>
      <DndProvider backend={HTML5Backend}>
        <LearningResourceCard label={t('situation')}>
          <div>{renderData.description}</div>
          <Alert severity="warning">TODO: remove question at the end</Alert>
        </LearningResourceCard>
        <LearningResourceCard label={t('Merkmale')}>
          {t('Folgende Merkmale werden betrachtet:')}
          <Stack spacing={2} direction="row" justifyContent="space-around">
            <VarCatNameMapper variable={condProbs.variables[0]} renamedVariable={shortVariables[0]} showLabel />
            <VarCatNameMapper variable={condProbs.variables[1]} renamedVariable={shortVariables[1]} showLabel />
          </Stack>
        </LearningResourceCard>
        <LearningResourceCard label={t('Wahrscheinlichkeiten')}>
          <div>{t('Folgende bedingten Wahrscheinlichkeiten und Randwahrscheinlichkeiten sind bekannt: ')}</div>
          <Stack spacing={2} direction="row" justifyContent="space-around" divider={<Divider orientation="vertical" flexItem />}>
            <CPStackedBarChart data={condProbs} />
            <ProbabilityBarChart probabilities={givenMarginalProbs} variable={shortVariables[givenVarIndex]} />
          </Stack>
        </LearningResourceCard>
        <LearningResourceCard label={t('task')}>
          <div>{t('Berechne die Wahrscheinlichkeiten!')}</div>
        </LearningResourceCard>
        <LearningResourceCard label={t('Lösungsversuch')}>
          {/* <div>{solutions.join('  ==== ')}</div> */}
          {!isDnD(config) && t('Runde auf die zweite Nachkommastelle!')}
          <div>
            <NumberTaskTable type={config.inputMode} labels={renderData.probLabels} format={formatProbability2} />
          </div>
          {config.inputMode === 'dnd' && (
            <NumericPossibleSolutions
              options={probSolutionOptions}
              label={t('Wähle eine Lösung! (drag & drop)')}
              format={formatProbability2}
            />
          )}
          <Stack direction="row" justifyContent="center">
            <Button onClick={activateValidation}>{t('Überprüfen')}</Button>
            <Button onClick={presentSolutions}>{t('Zeige Lösungen')}</Button>
          </Stack>
        </LearningResourceCard>
      </DndProvider>
    </AssessmentContext.Provider>
  );
}

export function RandomTotalProbabilitiesExercise1(): React.ReactElement {
  const randomExperimentManager = useRandomExperimentManager();
  // const { t } = useTranslation();

  const config: ITotalProbabilityExercise1Config = createRandomConfig();
  const data = createRandomMathData(randomExperimentManager);

  return <TotalProbabilityExercise1 config={config} data={data} />;
}

export function RepeatableTotalProbabilitiesExercise1(): React.ReactElement {
  return (
    <RepeatableExercise numRepititions={3}>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <RandomTotalProbabilitiesExercise1 />
    </RepeatableExercise>
  );
}
