import CancelIcon from '@mui/icons-material/Cancel';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import { Button, Divider, Stack } from '@mui/material';
import React, { useCallback, useMemo } from 'react';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { useTranslation } from 'react-i18next';

import { useStatefulEvent } from '@adlete-utils/events-react';
import { AssessmentContext, AssessmentController } from '@adlete-vle/lr-core-components/assessment';
import { LearningResourceCard } from '@adlete-vle/lr-core-components/learning-resources';
import { useResettableState } from '@adlete-vle/utils/hooks';

import {
  createDescriptionPercent,
  createRandomGenInput,
  IBivarFreqRandomPopMarginalJointGenInput,
  IBivarFreqRET,
} from '@kiwi-al/math-core/frequencies';
import { isNumberEqual2 } from '@kiwi-al/math-core/numbers';
import { BivariateCPDataFrame, getJointProbsFromProbsAndCPs, isDistributionIndependent } from '@kiwi-al/math-core/probability';
import { RandomExperimentManager } from '@kiwi-al/math-core/random-experiment';
import { CPStackedBarChart, JointProbsFromCPsGeneratorInput, ProbabilityBarChart } from '@kiwi-al/math-core-components/probability';
import { VarCatNameMapper } from '@kiwi-al/math-core-components/shared/VarCatNameMapper';

import { useRandomExperimentManager } from '../../contexts';
import { createCPData } from '../shared/data';
import { getRandomBivarBinaryFreqRET } from '../shared/rets';
import { getVariableSet, IVariableSet } from '../shared/variables';

import { RepeatableExercise } from './RepeatableExercise';

export interface IIndependenceExercise1Config {
  inputMode: 'dnd' | 'text-field';
}

export interface IIndependenceExercise1Data {
  ret: IBivarFreqRET;
  probSolutions: number[];
  givenMarginalProbs: number[];
  condProbs: BivariateCPDataFrame;
  condVarIndex: number;
  variables: IVariableSet;
}

export interface IIndependenceExercise1Props {
  // eslint-disable-next-line react/no-unused-prop-types
  config: IIndependenceExercise1Config;
  data: IIndependenceExercise1Data;
}

function createRandomMathData(rem: RandomExperimentManager /* , i18n?: i18n */): IIndependenceExercise1Data {
  const ret = getRandomBivarBinaryFreqRET(rem);

  const variables = getVariableSet(ret, false);

  const genData = ret.generation.random as IBivarFreqRandomPopMarginalJointGenInput;
  const input = createRandomGenInput(genData);
  const cpData = createCPData(input, variables.long);

  const condVarIndex = 1;
  const givenVarIndex = 0;

  const probSolutions = cpData.probabilities.getMarginalFrequencies(variables.long[condVarIndex]);
  return {
    ret,
    variables,
    probSolutions,
    condProbs: cpData.condProbs[condVarIndex],
    givenMarginalProbs: cpData.probabilities.getMarginalFrequencies(variables.long[givenVarIndex]),
    condVarIndex,
  };
}

function createRandomConfig(): IIndependenceExercise1Config {
  return {
    inputMode: Math.random() < 0.5 ? 'dnd' : 'text-field',
  };
}

export function IndependenceExercise1(props: IIndependenceExercise1Props): React.ReactElement {
  const { data } = props;
  const { condProbs, condVarIndex, variables, givenMarginalProbs, probSolutions, ret } = data;

  const { t, i18n } = useTranslation('conditional-probability');

  const [input, setInput] = useResettableState(
    () => ({
      condProbs,
      givenMarginalProbs,
    }),
    [ret]
  );

  const [assessment] = useResettableState(() => new AssessmentController(probSolutions, isNumberEqual2), [probSolutions]);
  const activateValidation = useCallback(() => assessment.enableValidation(), [assessment]);
  // const presentSolutions = useCallback(() => assessment.enablePresentSolutions(), [assessment]);

  const showValidation = useStatefulEvent(assessment.events, 'validateUpdated');

  const derivedData = useMemo(() => {
    const jointProbs = getJointProbsFromProbsAndCPs(input);
    const isIndependent = isDistributionIndependent(jointProbs, 0.005);
    const description = createDescriptionPercent(ret, jointProbs, 'description-marginal-joint-percent', i18n);
    const marginalProbs = [
      jointProbs.getMarginalFrequencies(jointProbs.variables[0]),
      jointProbs.getMarginalFrequencies(jointProbs.variables[1]),
    ];
    return { jointProbs, isIndependent, description, marginalProbs };
  }, [input, i18n, ret]);

  const validationElem = showValidation && (
    <>
      <Stack spacing={2} direction="row" justifyContent="center" alignItems="center">
        {derivedData.isIndependent ? (
          <>
            <div>{t('independence-exercise:correct')}</div>
            <CheckCircleIcon color="success" />
          </>
        ) : (
          <>
            <div>{t('independence-exercise:wrong')}</div>
            <CancelIcon color="error" />
          </>
        )}
      </Stack>
      <Divider />
    </>
  );

  return (
    <AssessmentContext.Provider value={assessment}>
      <DndProvider backend={HTML5Backend}>
        <LearningResourceCard label={t('situation')}>
          <div>{derivedData.description}</div>
        </LearningResourceCard>
        <LearningResourceCard label={t('events')}>
          {t('considered-events')}
          <Stack spacing={2} direction="row" justifyContent="space-around">
            <VarCatNameMapper variable={variables.long[0]} renamedVariable={variables.short[0]} showLabel />
            <VarCatNameMapper variable={variables.long[1]} renamedVariable={variables.short[1]} showLabel />
          </Stack>
        </LearningResourceCard>
        <LearningResourceCard label={t('independence-exercise:prob')}>
          <div>{t('independence-exercise:known-prob')}</div>
          <Stack spacing={2} direction="row" justifyContent="space-around" divider={<Divider orientation="vertical" flexItem />}>
            <CPStackedBarChart data={input.condProbs} displayVariables={variables.middle} />
            <ProbabilityBarChart probabilities={derivedData.marginalProbs[condVarIndex]} variable={variables.short[condVarIndex]} />
          </Stack>
        </LearningResourceCard>
        <LearningResourceCard label={t('task')}>
          <div>{t('independence-exercise:generate-distribution')}</div>
        </LearningResourceCard>
        <LearningResourceCard label={t('exercise:solution-attempt')}>
          <JointProbsFromCPsGeneratorInput
            input={input}
            onChange={setInput}
            hiddenSliders={['marginal']}
            displayVariables={variables.middle}
          />
          {validationElem}
          <Stack direction="row" justifyContent="center">
            <Button onClick={activateValidation}>{t('exercise:validate')}</Button>
            {/* <Button onClick={presentSolutions}>{t('Zeige Lösungen')}</Button> */}
          </Stack>
          {/* <LearningResourceCard label={t('Hilfsmittel')} optional>
            <TabbedContent panels={[[t('Berechnung'), <IndependenceCalculation probabilities={derivedData.jointProbs} />]]} />
          </LearningResourceCard> */}
        </LearningResourceCard>
      </DndProvider>
    </AssessmentContext.Provider>
  );
}

export function RandomIndependenceExercise1(): React.ReactElement {
  const randomExperimentManager = useRandomExperimentManager();
  // const { t } = useTranslation();

  const config: IIndependenceExercise1Config = createRandomConfig();
  const data = createRandomMathData(randomExperimentManager);

  return <IndependenceExercise1 config={config} data={data} />;
}

export function RepeatableIndependenceExercise1(): React.ReactElement {
  return (
    <RepeatableExercise numRepititions={3}>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <RandomIndependenceExercise1 />
    </RepeatableExercise>
  );
}
