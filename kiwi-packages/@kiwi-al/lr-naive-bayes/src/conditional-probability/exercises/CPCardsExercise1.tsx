import { Alert, Button, Stack } from '@mui/material';
import React, { useCallback, useMemo, useState } from 'react';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { useTranslation } from 'react-i18next';

import { getMetaImage, HumanReadableTextualData } from '@adlete-vle/lr-core/learning-resources';
import { LRText } from '@adlete-vle/lr-core-components/LRText';
import { AssessmentContext, AssessmentController } from '@adlete-vle/lr-core-components/assessment';
import { useLearningResourceManager } from '@adlete-vle/lr-core-components/contexts';
import { LearningResourceCard } from '@adlete-vle/lr-core-components/learning-resources';
import { useMemoWith, useResettableState } from '@adlete-vle/utils/hooks';

import { IBivarFreqRET } from '@kiwi-al/math-core/frequencies';
import { isNumberEqual2 } from '@kiwi-al/math-core/numbers';
import {
  BivariateCPDataFrame,
  formatConditionalProbability,
  formatProbability2,
  getConditionalProbabilitiesFromJointFrequency,
} from '@kiwi-al/math-core/probability';
import { RandomExperimentManager, RandomExperimentType, variablesToSampleSpace2 } from '@kiwi-al/math-core/random-experiment';
import { ICategoricalVarDef } from '@kiwi-al/math-core/variables';
import { ContingencyBarTable, IContingencyBarTableFormatting } from '@kiwi-al/math-core-components/frequency';
import { coloredEventsToColorMap } from '@kiwi-al/math-core-components/random-experiment';

import { useRandomExperimentManager } from '../../contexts';
import { CardsSampleSpace } from '../../random-experiments/drawing-a-card/components';
import {
  createCardsJointFrequencyDF,
  createJointEventCards,
  createOverlappingRandomCardEvents,
} from '../../random-experiments/drawing-a-card/functions';
import { CPCalculation, CPCalculationMode } from '../components/CPCalculation';
import { NumberTaskTable } from '../components/NumberTaskTable';
import { SituationWithImage } from '../shared/SituationWithImage';
import { createProbabilitySolutionOptions } from '../shared/solution-options';
import { TabbedContent } from '../shared/tabs';

import { NumericAnswerInputType } from './NumericAnswerInput';
import { NumericPossibleSolutions } from './NumericPossibleSolutions';
import { RepeatableExercise } from './RepeatableExercise';
import { createCPTargetSolutions, createRandomCPTaskTargets, isDnD } from './shared';

const CONTINGENCY_FORMAT: IContingencyBarTableFormatting = {
  labelFormat: { format: 'StateOnly' },
};

function getCardsRET(rem: RandomExperimentManager): IBivarFreqRET {
  const polynaries = rem.getTemplateByType<IBivarFreqRET>(RandomExperimentType.BivariatePolynary);
  return polynaries.find((ret) => ret.id === 'drawing-a-card-freq-random-experiment');
}

export function CPCardsExercise1(): React.ReactElement {
  const learningResourceManager = useLearningResourceManager();
  const randomExperimentManager = useRandomExperimentManager();

  const { t } = useTranslation('conditional-probability');

  // === picking the random experiment
  const [reTemplate] = useState<IBivarFreqRET>(getCardsRET(randomExperimentManager));

  // === calculating the data
  const sampleSpace = useMemo(
    () => variablesToSampleSpace2<string>(reTemplate.variables as ICategoricalVarDef<string>[]),
    [reTemplate.variables]
  );

  const events = useMemo(
    () => createOverlappingRandomCardEvents(sampleSpace),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [sampleSpace]
  );

  const frequencies = useMemoWith(createCardsJointFrequencyDF, [events]);

  const data = useMemo(() => {
    // const probabilities = jointProbabilityDFFromFrequencyDF(frequencies);
    const condProbs: [BivariateCPDataFrame, BivariateCPDataFrame] = [
      getConditionalProbabilitiesFromJointFrequency(frequencies, true),
      getConditionalProbabilitiesFromJointFrequency(frequencies, false),
    ];

    const cpTargets = createRandomCPTaskTargets(frequencies.variables, 8).filter(
      (target) => target[0][1] === target[0][0].categories[0] && target[1][1] === target[1][0].categories[0]
    );
    const cpSolutions = createCPTargetSolutions(cpTargets, condProbs);
    const cpLabels = cpTargets.map(
      (target) => `${formatConditionalProbability(target[0], target[1], { condProbFormat: 'Notation', varFormat: 'VarOnly' })} = `
    );
    const cpSolutionOptions = createProbabilitySolutionOptions(cpSolutions, cpSolutions.length * 3);

    return {
      cpTargets,
      cpSolutions,
      cpLabels,
      cpSolutionOptions,
    };
  }, [frequencies]);

  // adding the extra joint event
  const visibleEvents = useMemo(() => [...events, createJointEventCards(events, frequencies.variables)], [events, frequencies.variables]);

  const colors = coloredEventsToColorMap(visibleEvents);
  colors.joint = colors[visibleEvents[2].name];

  const [assessment] = useResettableState(() => new AssessmentController(data.cpSolutions, isNumberEqual2), [data.cpSolutions]);
  const activateValidation = useCallback(() => assessment.enableValidation(), [assessment]);
  const presentSolutions = useCallback(() => assessment.enablePresentSolutions(), [assessment]);

  const config = {
    inputMode: 'dnd' as NumericAnswerInputType,
  };

  const reducedSampleSpaceHint = learningResourceManager.getResource<HumanReadableTextualData>(
    'conditional-probability/reduced-sample-space.json'
  );

  const focusHint = learningResourceManager.getResource<HumanReadableTextualData>('conditional-probability/focus-hint.json');

  const imageURL = useMemo(() => getMetaImage(learningResourceManager.getMeta(reTemplate.id)), [reTemplate, learningResourceManager]);

  // === rendering
  return (
    <AssessmentContext.Provider value={assessment}>
      <DndProvider backend={HTML5Backend}>
        <Stack spacing={2}>
          <SituationWithImage text={t('cp-cards-exercise:explanation')} imageURL={imageURL} />
          <LearningResourceCard label={t('results-events')}>
            <div>{t('cp-cards-exercise:all-combinations')}</div>
            <CardsSampleSpace variables={reTemplate.variables} events={visibleEvents} />
          </LearningResourceCard>

          <LearningResourceCard label={t('task')}>
            <div>{t('exercise:calculate-prob')}</div>
            <Alert severity="info">{t('cp-cards-exercise:help')}</Alert>
            {/* <CPCalculation
          data={frequencies}
          mode={CPCalculationMode.Frequency}
          condProbFormat={ConditionalProbabilityFormat.Phrase}
          colors={colors}
          showData
        /> */}
          </LearningResourceCard>
          <LearningResourceCard label={t('exercise:solution-attempt')}>
            {/* <div>{solutions.join('  ==== ')}</div> */}
            {!isDnD(config) && t('exercise:round-2')}
            <div>
              <NumberTaskTable type={config.inputMode} labels={data.cpLabels} format={formatProbability2} />
            </div>
            {isDnD(config) && (
              <NumericPossibleSolutions
                options={data.cpSolutionOptions}
                label={t('exercise:solution-drag-drop')}
                format={formatProbability2}
              />
            )}
            <Stack direction="row" justifyContent="center">
              <Button onClick={activateValidation}>{t('exercise:validate')}</Button>
              <Button onClick={presentSolutions}>{t('exercise:show-solution')}</Button>
            </Stack>
          </LearningResourceCard>
          <LearningResourceCard label={t('exercise:help-tools')} optional>
            <TabbedContent
              panels={[
                [
                  t('math:contingency-table'),
                  <>
                    <div>{t('contingency-table-freqs-intro')}</div>
                    <ContingencyBarTable initialData={frequencies} showMarginals formatting={CONTINGENCY_FORMAT} />
                  </>,
                ],
                [`${t('learning-resources:hint')} #1`, <LRText text={reducedSampleSpaceHint} />],
                [`${t('learning-resources:hint')} #2`, <LRText text={focusHint} />],
              ]}
            />
          </LearningResourceCard>
          <LearningResourceCard label={t('exercise:solution-path')} optional>
            <CPCalculation
              data={frequencies}
              mode={CPCalculationMode.Frequency}
              showEquation
              showData
              condProbsToCalculate={data.cpTargets}
              colors={colors}
            />
          </LearningResourceCard>
        </Stack>
      </DndProvider>
    </AssessmentContext.Provider>
  );
}

export function RepeatableCPCardsExercise1(): React.ReactElement {
  return (
    <RepeatableExercise numRepititions={3}>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <CPCardsExercise1 />
    </RepeatableExercise>
  );
}
