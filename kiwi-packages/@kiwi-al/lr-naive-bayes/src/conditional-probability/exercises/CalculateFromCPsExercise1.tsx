import { Button, Stack } from '@mui/material';
import React, { useCallback, useMemo } from 'react';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { useTranslation } from 'react-i18next';

import { filterByDifficulty } from '@adlete-vle/lr-core/difficulty';
import { AssessmentContext, AssessmentController } from '@adlete-vle/lr-core-components/assessment';
import { useLearningResourceManager } from '@adlete-vle/lr-core-components/contexts';
import { Equation, LearningResourceCard } from '@adlete-vle/lr-core-components/learning-resources';
import { useResettableState } from '@adlete-vle/utils/hooks';
import { replaceTextWithElements } from '@adlete-vle/utils/react';

import { createRandomGenInput, IBivarFreqRandomPopMarginalJointGenInput, IBivarFreqRET } from '@kiwi-al/math-core/frequencies';
import { isNumberEqual2 } from '@kiwi-al/math-core/numbers';
import { formatPercent0 } from '@kiwi-al/math-core/probability';
import { RandomExperimentManager } from '@kiwi-al/math-core/random-experiment';
import { getRandomInt } from '@kiwi-al/math-core/randomization';
import { ConditionalProbabilityTable, CPStackedBarChart } from '@kiwi-al/math-core-components/probability';
import { VariablesAndCatsNameMapper } from '@kiwi-al/math-core-components/shared/VariablesAndCatsNameMapper';

import { useRandomExperimentManager } from '../../contexts';
import { createCPData, ICPData } from '../shared/data';
import { getNextRandomBivarBinaryFreqRET } from '../shared/rets';
import { createIntegerSolutionOptions, createProbabilitySolutionOptions } from '../shared/solution-options';
import { TabbedContent } from '../shared/tabs';
import { getVariableSet, IVariableSet } from '../shared/variables';

import { NumericAnswerInput } from './NumericAnswerInput';
import { NumericPossibleSolutions } from './NumericPossibleSolutions';
import { RepeatableExercise } from './RepeatableExercise';
import {
  createConditionalProbDescription,
  createFrequencyTaskDescriptionWithPlaceholders,
  FrequencySolutionType,
  pickFrequencySolution,
} from './shared';

import './shared.css';

export type ExerciseInputOutputType = 'frequencies' | 'probabilties';

export interface ICalculateFromCPsExercise1Config {
  inputType: ExerciseInputOutputType;
  targetType: FrequencySolutionType;
  difficulty: number;
}

function createExerciseConfig(
  difficulty: number,
  inputType: ExerciseInputOutputType,
  targetType: FrequencySolutionType
): ICalculateFromCPsExercise1Config {
  return {
    difficulty,
    inputType,
    targetType,
  };
}

const CONFIGS: ICalculateFromCPsExercise1Config[] = [
  createExerciseConfig(1, 'probabilties', 'joint'),
  createExerciseConfig(2, 'probabilties', 'marginal0'),
  createExerciseConfig(2, 'probabilties', 'marginal1'),
  // createExerciseConfig(3, 'frequencies', 'joint'),
  // createExerciseConfig(4, 'frequencies', 'marginal0'),
  // createExerciseConfig(4, 'frequencies', 'marginal1'),
];

const REPITITION_TO_DIFFICULTY: Record<number, [number, number]> = {
  1: [1, 1],
  2: [1, 1],
  3: [2, 2],
};

function isFreqs(config: ICalculateFromCPsExercise1Config): boolean {
  return config.inputType === 'frequencies';
}

interface IExerciseMathData extends ICPData {
  ret: IBivarFreqRET;
  variables: IVariableSet;
  solution: number;
  solutionOptions: number[];
  targetType: FrequencySolutionType;
}

const PREVIOUS_RETS: IBivarFreqRET[] = [];

function createRandomMathData(rem: RandomExperimentManager, config: ICalculateFromCPsExercise1Config): IExerciseMathData {
  const ret = getNextRandomBivarBinaryFreqRET(rem, PREVIOUS_RETS);
  const variables = getVariableSet(ret, false);

  const { targetType } = config;

  const genData = ret.generation.random as IBivarFreqRandomPopMarginalJointGenInput;
  const input = createRandomGenInput(genData);
  const cpData = createCPData(input, variables.long, {
    condProbsSource: 'joint-probs',
    roundProbCalculationsTo: 2,
    roundCPCalculationsTo: 4,
  });

  // solution
  const data = isFreqs(config) ? cpData.frequencies : cpData.probabilities;
  const solution = pickFrequencySolution(data, targetType);
  const solutionOptions = isFreqs(config)
    ? createIntegerSolutionOptions(solution, 0, cpData.frequencies.getPopulation(), 5)
    : createProbabilitySolutionOptions([solution], 5);

  return {
    ret,
    ...cpData,
    variables,
    targetType,
    solution,
    solutionOptions,
  };
}

function createRandomExerciseConfig(repitition: number): ICalculateFromCPsExercise1Config {
  const difficulty = REPITITION_TO_DIFFICULTY[repitition] || [0, 100];
  const filteredConfigs = filterByDifficulty(CONFIGS, difficulty);
  return filteredConfigs[getRandomInt(0, filteredConfigs.length - 1)];
}

export interface IRandomCalculateFromCPsExercise1Props {
  // Seems to be a bug in react eslint
  // TODO: remove disable instructions
  // eslint-disable-next-line react/no-unused-prop-types
  repitition: number;
}

export interface ICalculateFromCPsExercise1Props extends IRandomCalculateFromCPsExercise1Props {
  mathData: IExerciseMathData;
  config: ICalculateFromCPsExercise1Config;
}

export function CalculateFromCPsExercise1(props: ICalculateFromCPsExercise1Props): React.ReactElement {
  const { mathData, config } = props;
  const { targetType, solution, solutionOptions, ret, frequencies, condProbs, variables, probabilities } = mathData;

  const learningResourceManager = useLearningResourceManager();

  const { i18n, t } = useTranslation('conditional-probability');

  // === Assessment context and helpers
  const [assessment] = useResettableState(() => new AssessmentController([solution], isNumberEqual2), [solution]);
  const activateValidation = useCallback(() => assessment.enableValidation(), [assessment]);

  const formatter = isFreqs(config) ? null : formatPercent0;

  // ==== Calculating the necessary data
  const data = useMemo(() => {
    const descData = isFreqs(config) ? frequencies : probabilities;
    const localeKey = isFreqs(config) ? 'description-population-marginal-joint' : 'description-marginal-joint-percent';
    return {
      descriptionWithPlaceholders: createFrequencyTaskDescriptionWithPlaceholders(ret, descData, targetType, localeKey, formatter, i18n),
    };
  }, [ret, frequencies, probabilities, targetType, config, formatter, i18n]);

  // ==== Task description with placeholder
  const descriptionWithSolutionZone = replaceTextWithElements(data.descriptionWithPlaceholders, {
    [`{${targetType}}`]: (
      <NumericAnswerInput key="target" type="dnd" index={0} itemType="exercise" display="inline-block" format={formatter} />
    ),
  });
  const condProb0Description = createConditionalProbDescription(condProbs[0], variables.short, i18n);
  const condProb1Description = createConditionalProbDescription(condProbs[1], variables.short, i18n);

  const equation = learningResourceManager.getResource<string>(
    targetType === 'joint' ? 'events/multiplication-rule-equation.md' : 'conditional-probability/probability-given-cp-equation.md'
  );

  // ==== Rendering

  return (
    <AssessmentContext.Provider value={assessment}>
      <DndProvider backend={HTML5Backend}>
        <Stack spacing={2}>
          <LearningResourceCard label={t('task')}>
            <div className="description-with-solution-zone">
              {descriptionWithSolutionZone} {condProb0Description} {condProb1Description}
            </div>
            <NumericPossibleSolutions
              itemType="exercise"
              options={solutionOptions}
              label={t('exercise:solution-drag-drop-round')}
              format={formatter}
            />
            <Button onClick={activateValidation}>{t('exercise:validate')}</Button>
          </LearningResourceCard>
          <LearningResourceCard label={t('events')}>
            {t('considered-events')}
            <VariablesAndCatsNameMapper variables={variables.long} renamedVariables={variables.short} />
          </LearningResourceCard>
          <LearningResourceCard label={t('exercise:help-tools')} optional>
            <TabbedContent
              panels={[
                [t('math:equation'), <Equation equation={equation} />],
                [
                  t('math:stacked-bar-chart'),
                  <Stack spacing={2}>
                    <CPStackedBarChart data={condProbs[0]} displayVariables={variables.middle} />
                    <CPStackedBarChart data={condProbs[1]} displayVariables={variables.middle} />
                  </Stack>,
                ],
                [
                  t('math:conditional-probability-table'),
                  <Stack spacing={2}>
                    <ConditionalProbabilityTable initialData={condProbs[0]} displayVariables={variables.middle} />
                    <ConditionalProbabilityTable initialData={condProbs[1]} displayVariables={variables.middle} />
                  </Stack>,
                ],
                // isFreqs(config) && [
                //   t('math:joint-probability-table'),
                //   <>
                //     <div>{t('joint-probability-table-intro')}</div>
                //     <ProbabilityTable initialData={probabilities} showMarginals />
                //   </>,
                // ],
                // [
                //   t('math:venn-diagram'),
                //   <>
                //     <div>{t('venn-probs-intro')}</div>
                //     <VennDiagramWithCircles
                //       sets={data.vennGraphData.sets}
                //       combinations={data.vennGraphData.combinations}
                //       width={800}
                //       height={400}
                //       setLabelConfig={SET_LABEL_CONFIG_PROBABILITY}
                //     />
                //   </>,
                // ],
                // [
                //   t('math:contingency-table'),
                //   <>
                //     <div>{t('contingency-table-freqs-intro')}</div>
                //     <ContingencyBarTable initialData={data.frequencies} showMarginals />
                //   </>,
                // ],
              ]}
            />
          </LearningResourceCard>
          {/* <LearningResourceCard label={t('Lösungsweg')} optional>
          TODO
        </LearningResourceCard> */}
        </Stack>
      </DndProvider>
    </AssessmentContext.Provider>
  );
}

export function RandomCalculateFromCPsExercise1(props: IRandomCalculateFromCPsExercise1Props): React.ReactElement {
  const { repitition } = props;

  const randomExperimentManager = useRandomExperimentManager();

  const config = createRandomExerciseConfig(repitition);
  const mathData = createRandomMathData(randomExperimentManager, config);

  /* eslint-disable-next-line react/jsx-props-no-spreading */
  return <CalculateFromCPsExercise1 {...props} mathData={mathData} config={config} />;
}

// TODO RandomCalculateCPsFromFreqsExercise1

export function RepeatableCalculateFromCPsExercise1(props: ICalculateFromCPsExercise1Props): React.ReactElement {
  return (
    <RepeatableExercise numRepititions={3}>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <RandomCalculateFromCPsExercise1 {...props} />
    </RepeatableExercise>
  );
}
