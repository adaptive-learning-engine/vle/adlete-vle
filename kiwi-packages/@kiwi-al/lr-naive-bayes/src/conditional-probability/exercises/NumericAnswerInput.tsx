import { TextField } from '@mui/material';
import React, { useCallback } from 'react';
import { NumberFormatValues, NumericFormat } from 'react-number-format';

import {
  getColorOptionByValidation,
  getValidationTypeBool,
  useAnswer,
  useAssessmentContext,
  useValidate,
} from '@adlete-vle/lr-core-components/assessment';
import { DropZone, IContentItem, wrapAsContentItem } from '@adlete-vle/lr-core-components/dnd';
import { useMemoWith } from '@adlete-vle/utils/hooks';

import { PrimitiveItemBox } from '../components/PrimitiveItemBox';

import { getAdornmentByValidationType } from './shared';

export type NumericAnswerInputType = 'dnd' | 'text-field' | 'disabled-text-field';

export interface INumericAnswerInputProps {
  // eslint-disable-next-line react/no-unused-prop-types
  type: NumericAnswerInputType;
  index: number;
  // eslint-disable-next-line react/no-unused-prop-types
  itemType: string;
  // eslint-disable-next-line react/no-unused-prop-types
  display?: string;
  // eslint-disable-next-line react/no-unused-prop-types
  format?: (value: number) => string;
}

export function NumericAnswerInputDnD(props: INumericAnswerInputProps): React.ReactElement {
  const { index, itemType, display, format } = props;
  const assessment = useAssessmentContext<number>();
  const answer = useAnswer<number>(index);
  const item = useMemoWith(wrapAsContentItem, [answer]) as IContentItem<number>;
  const validate = useValidate();

  const onDropZoneDrop = useCallback(
    (droppedItem: IContentItem<number>) => {
      assessment.setAnswer(index, droppedItem.content);
    },
    [assessment, index]
  );

  const validationType = getValidationTypeBool(assessment.isAnswerValid(index), validate);
  const adornment = getAdornmentByValidationType(validationType);

  return (
    <DropZone accept={itemType} onDrop={onDropZoneDrop} display={display}>
      <PrimitiveItemBox item={item} validation={validationType} format={format} endAdornment={adornment} />
    </DropZone>
  );
}

export function NumericAnswerInputTextField(props: INumericAnswerInputProps): React.ReactElement {
  const { index, type } = props;
  const assessment = useAssessmentContext<number>();

  const isDisabled = type === 'disabled-text-field';

  // calling useAnswer for forcing re-render; possible TODO: pass as value to NumericFormat
  useAnswer<number>(index);

  const validate = useValidate();

  const onValueChange = useCallback(
    (value: NumberFormatValues) => {
      assessment.setAnswer(index, value.floatValue);
    },
    [assessment, index]
  );

  const validationType = getValidationTypeBool(assessment.isAnswerValid(index), validate);
  const adornment = getAdornmentByValidationType(validationType);

  return (
    <NumericFormat
      name={index.toString()}
      onValueChange={onValueChange}
      customInput={TextField}
      color={getColorOptionByValidation(validationType)}
      focused={validate}
      decimalSeparator=","
      disabled={isDisabled}
      InputProps={{ endAdornment: adornment }}
    />
  );
}

export function NumericAnswerInput(props: INumericAnswerInputProps): React.ReactElement {
  const { type } = props;

  switch (type) {
    case 'dnd': {
      // eslint-disable-next-line react/jsx-props-no-spreading
      return <NumericAnswerInputDnD {...props} />;
    }
    case 'text-field':
      // eslint-disable-next-line react/jsx-props-no-spreading
      return <NumericAnswerInputTextField {...props} />;
    case 'disabled-text-field':
      // eslint-disable-next-line react/jsx-props-no-spreading
      return <NumericAnswerInputTextField {...props} />;
    default:
      throw new Error(`Unknown type: ${type}`);
  }
}
