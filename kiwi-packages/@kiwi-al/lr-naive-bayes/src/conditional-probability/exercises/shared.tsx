import CancelIcon from '@mui/icons-material/Cancel';
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import { InputAdornment } from '@mui/material';
import { i18n } from 'i18next';
import { mapObjIndexed } from 'ramda';
import React from 'react';

import { ValidationType } from '@adlete-vle/lr-core-components/assessment';

import { BivariateJointFrequencyDataFrame } from '@kiwi-al/math-core/frequencies';
import {
  createBivarFreqPopMarginalJointDescriptionData,
  getTemplateLocaleKey,
  IBivarFreqRET,
} from '@kiwi-al/math-core/frequencies/random-experiment';
import { BivariateCPDataFrame, formatConditionalProbability, formatProbability } from '@kiwi-al/math-core/probability';
import { fisherYatesShuffle, getRandomElement } from '@kiwi-al/math-core/randomization';
import { BivarCategoricalVarDefs, getVarWithState, ICategoricalVarDef, VariableWithState } from '@kiwi-al/math-core/variables';

export function createConditionalProbDescription(
  condProbs: BivariateCPDataFrame,
  displayVariables: BivarCategoricalVarDefs,
  i18n: i18n
): string {
  const conditionalVarIndex = condProbs.getConditionalVariableIndex();
  const givenVarIndex = condProbs.getGivenVariableIndex();

  // TODO allow other cats than 0
  const conditionalVarState = condProbs.variables[conditionalVarIndex].categories[0];
  const givenVarState = condProbs.variables[givenVarIndex].categories[0];
  const conditionalVarWithStateDisplay = getVarWithState(displayVariables, conditionalVarIndex, 0);
  const givenVarWithStateDisplay = getVarWithState(displayVariables, givenVarIndex, 0);

  const condProbDescriptionValues = {
    condProbNotation: formatConditionalProbability(conditionalVarWithStateDisplay, givenVarWithStateDisplay, {
      condProbFormat: 'Notation',
      varFormat: 'VarOnly',
      i18n,
    }),
    condProbValue: formatProbability(condProbs.getConditionalProbability(conditionalVarState, givenVarState), {
      format: 'Percent',
      fractionDigits: 2,
    }),
  };

  return i18n.t('conditional-probability:cp-exercise-1-cond-prob', condProbDescriptionValues);
}

export type FrequencySolutionType = 'marginal0' | 'marginal1' | 'joint';
const FREQUENCY_SOLUTION_TYPES: FrequencySolutionType[] = ['marginal0', 'marginal1', 'joint'];

export function pickRandomSolutionType(): FrequencySolutionType {
  return getRandomElement(FREQUENCY_SOLUTION_TYPES);
}

export function createFrequencyTaskDescriptionWithPlaceholders(
  reTemplate: IBivarFreqRET,
  frequencies: BivariateJointFrequencyDataFrame,
  solutionType: FrequencySolutionType,
  localeKey: string,
  format?: (value: number) => string,
  i18n?: i18n
): string {
  const descriptionData = createBivarFreqPopMarginalJointDescriptionData(reTemplate, frequencies);
  const formattedData = format ? mapObjIndexed(format, descriptionData) : descriptionData;
  const descriptionDataForReplace = { ...formattedData, [solutionType]: `{${solutionType}}` };
  const descriptionLocaleKey = getTemplateLocaleKey(reTemplate, localeKey);

  // i18n.t has weird typings
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  return i18n.t(descriptionLocaleKey, descriptionDataForReplace);
}
export function pickFrequencySolution(frequencies: BivariateJointFrequencyDataFrame, solutionType: FrequencySolutionType): number {
  const variables = frequencies.variables;
  switch (solutionType) {
    case 'marginal0':
      return frequencies.getMarginalFrequencies(0)[0];
      break;
    case 'marginal1':
      return frequencies.getMarginalFrequencies(1)[0];
      break;
    case 'joint':
      return frequencies.getFrequency(variables[0], variables[0].categories[0], variables[1], variables[1].categories[0]);
      break;
    default:
      throw new Error(`Unknown FrequencySolutionType ${solutionType}!`);
  }
}

// export function createFrequencyItems(frequencies: BivariateJointFrequencyDataFrame, solutionType: FrequencySolutionType): ITaskItems {
//   const max = frequencies.getPopulation();
//   let solution;
//   const variables = frequencies.variables;
//   switch (solutionType) {
//     case 'marginal0':
//       solution = frequencies.getMarginalFrequencies(0)[0];
//       break;
//     case 'marginal1':
//       solution = frequencies.getMarginalFrequencies(1)[0];
//       break;
//     case 'joint':
//       solution = frequencies.getFrequency(variables[0], variables[0].categories[0], variables[1], variables[1].categories[0]);
//       break;
//     default:
//       throw new Error(`Unknown FrequencySolutionType ${solutionType}!`);
//   }

//   const solutionOptions = createIntegerSolutionOptions(solution, 0, max, 5);
//   solutionOptions.sort((a, b) => a - b);

//   return {
//     solution: { content: solution },
//     default: { content: 'X' },
//     options: wrapAllAsContentItem(solutionOptions),
//   };
// }

export function createCPTaskTargets(variables: [ICategoricalVarDef, ICategoricalVarDef]): [VariableWithState, VariableWithState][] {
  const targets: [VariableWithState, VariableWithState][] = [];
  for (let i = 0; i < variables.length; ++i) {
    const conditionedVar = variables[i];
    const conditioningVar = variables[i === 0 ? 1 : 0];
    for (let condCatI = 0; condCatI < conditioningVar.categories.length; ++condCatI) {
      for (let givenI = 0; givenI < conditioningVar.categories.length; ++givenI)
        targets.push([
          [conditionedVar, conditionedVar.categories[condCatI]],
          [conditioningVar, conditioningVar.categories[givenI]],
        ]);
    }
  }
  return targets;
}

export function cpTargetContainsOnlyFirstCats(target: [VariableWithState, VariableWithState]): boolean {
  return target[0][0].categories[0] === target[0][1] && target[1][0].categories[0] === target[1][1];
}

export function createRandomCPTaskTargets(
  variables: [ICategoricalVarDef, ICategoricalVarDef],
  maxElems: number,
  filter?: (target: [VariableWithState, VariableWithState]) => boolean
): [VariableWithState, VariableWithState][] {
  let targets = createCPTaskTargets(variables);
  if (filter) targets = targets.filter(filter);
  return shuffleAndReduce(targets, maxElems);
}

export function convertTargetByEventVariables(
  target: [VariableWithState, VariableWithState],
  variables: ICategoricalVarDef[],
  eventVariables: ICategoricalVarDef[]
): [VariableWithState, VariableWithState] {
  const [varWithState0, varWithState1] = target;
  const eventVar0 = eventVariables[variables.indexOf(varWithState0[0])];
  const eventCat0 = eventVar0.categories[varWithState0[0].categories.indexOf(varWithState0[1])];
  const eventVar1 = eventVariables[variables.indexOf(varWithState1[0])];
  const eventCat1 = eventVar1.categories[varWithState1[0].categories.indexOf(varWithState1[1])];

  return [
    [eventVar0, eventCat0],
    [eventVar1, eventCat1],
  ];
}

export function shuffleAndReduce<T>(arr: T[], maxElems: number): T[] {
  fisherYatesShuffle(arr);
  return arr.slice(0, maxElems);
}

export function createCPTargetSolutions(
  cpTargets: [VariableWithState, VariableWithState][],
  condProbs: [BivariateCPDataFrame, BivariateCPDataFrame]
): number[] {
  return cpTargets.map((target) => {
    const conditionalVar = condProbs[0].getConditionalVariable();
    const condProbIndex = target[0][0] === conditionalVar ? 0 : 1;
    return condProbs[condProbIndex].getConditionalProbability(target[0][1], target[1][1]);
  });
}

export interface IConfigWithInputMode {
  inputMode: string;
}

export function isDnD(config: IConfigWithInputMode): boolean {
  return config.inputMode === 'dnd';
}

export function getAdornmentByValidationType(validationType: ValidationType): React.ReactElement {
  let icon = null;

  if (validationType === 'no-validation') return null;

  if (validationType === 'is-valid') icon = <CheckCircleIcon color="success" />;
  else if (validationType === 'is-invalid') icon = <CancelIcon color="error" />;

  return <InputAdornment position="end">{icon}</InputAdornment>;
}
