import { Button, Stack, Typography } from '@mui/material';
import React, { useCallback, useRef } from 'react';
import { useTranslation } from 'react-i18next';

import { useIncrementState } from '@adlete-vle/utils/hooks';

export interface IRepeatableExerciseProps {
  numRepititions: number;
  children: React.ReactElement;
  onFinished?: () => void;
}

export function RepeatableExercise(props: IRepeatableExerciseProps): React.ReactElement {
  const { children, numRepititions, onFinished } = props;
  const thisRef = useRef(null);
  const { t } = useTranslation('conditional-probability');
  const [currRepitition, incrementRepitition] = useIncrementState(1);

  const goToNext = useCallback(() => {
    incrementRepitition();
    if (thisRef.current) thisRef.current.scrollIntoView();
  }, [incrementRepitition]);

  const nextIsExtra = currRepitition >= numRepititions;
  const thisIsExtra = currRepitition > numRepititions;

  const cloned = React.cloneElement(children, { key: currRepitition, repitition: currRepitition, isVoluntary: thisIsExtra });

  return (
    <Stack ref={thisRef} spacing={2}>
      <Typography textAlign="center" variant="subtitle2">
        Aufgabe {thisIsExtra ? `${currRepitition} (optional)` : `${currRepitition}/${numRepititions}`}
      </Typography>
      {cloned}
      {!nextIsExtra ? (
        <Stack spacing={2} direction="row" justifyContent="center">
          <Button variant="contained" onClick={goToNext}>
            {t('repeatable-exercise:next-exercise')}
          </Button>
          {onFinished && <Button onClick={onFinished}>{t('repeatable-exercise:skip')}</Button>}
        </Stack>
      ) : (
        <Stack spacing={2} direction="row" justifyContent="center">
          <Button onClick={goToNext}>{t('repeatable-exercise:additional-exercise')}</Button>
          {onFinished && (
            <Button variant="contained" onClick={onFinished}>
              {t('repeatable-exercise:next')}
            </Button>
          )}
        </Stack>
      )}
    </Stack>
  );
}
