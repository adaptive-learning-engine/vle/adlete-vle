import { Button, Stack } from '@mui/material';
import React, { useCallback, useMemo, useState } from 'react';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { useTranslation } from 'react-i18next';

import { filterByDifficulty } from '@adlete-vle/lr-core/difficulty';
import { getMetaImage } from '@adlete-vle/lr-core/learning-resources';
import { AssessmentContext, AssessmentController } from '@adlete-vle/lr-core-components/assessment';
import { useLearningResourceManager } from '@adlete-vle/lr-core-components/contexts';
import { Equation, LearningResourceCard } from '@adlete-vle/lr-core-components/learning-resources';
import { useResettableState } from '@adlete-vle/utils/hooks';

import {
  createDescriptionFreqs,
  createDescriptionPercent,
  createRandomGenInput,
  IBivarFreqPopMarginalJointGenInput,
  IBivarFreqRandomPopMarginalJointGenInput,
  IBivarFreqRET,
} from '@kiwi-al/math-core/frequencies';
import { isNumberEqual2 } from '@kiwi-al/math-core/numbers';
import { formatConditionalProbability, formatProbability2 } from '@kiwi-al/math-core/probability';
import { RandomExperimentManager } from '@kiwi-al/math-core/random-experiment';
import { getRandomInt } from '@kiwi-al/math-core/randomization';
import { VariableWithState } from '@kiwi-al/math-core/variables';
import { ContingencyBarTable } from '@kiwi-al/math-core-components/frequency';
import { ProbabilityTable } from '@kiwi-al/math-core-components/probability';
import { VariablesAndCatsNameMapper } from '@kiwi-al/math-core-components/shared/VariablesAndCatsNameMapper';
import { VennDiagramWithCircles } from '@kiwi-al/math-core-components/venn';

import { useRandomExperimentManager } from '../../contexts';
import { CPCalculation, CPCalculationMode } from '../components/CPCalculation';
import { NumberTaskTable } from '../components/NumberTaskTable';
import { SituationWithImage } from '../shared/SituationWithImage';
import { createCPData, ICPData, ICreateCPDataOptions } from '../shared/data';
import { getNextRandomBivarBinaryFreqRET } from '../shared/rets';
import { createProbabilitySolutionOptions } from '../shared/solution-options';
import { TabbedContent, TabPanelInfos } from '../shared/tabs';
import { getVariableSet, IVariableSet } from '../shared/variables';
import { createVennGraphData, SET_LABEL_CONFIG_PROBABILITY } from '../shared/venn';

import { NumericPossibleSolutions } from './NumericPossibleSolutions';
import { RepeatableExercise } from './RepeatableExercise';
import {
  convertTargetByEventVariables,
  cpTargetContainsOnlyFirstCats,
  createCPTargetSolutions,
  createRandomCPTaskTargets,
  isDnD,
} from './shared';

type ExerciseInputOutputType = 'frequencies' | 'probabilties' | 'conditional-probabilities';
type ExerciseTaskPresentationType = 'situation' | 'contingency-table' | 'cpt';
type ExerciseInputMode = 'dnd' | 'text-field';
type ExerciseAnswerInput = 'situation' | 'text-fields';

export interface ICalculateCPsExercise1Config {
  inputType: ExerciseInputOutputType;
  // type: 'freqs-to-cps' | 'cps-to-freqs' | 'probs-to-cps' | 'cps-to-probs';
  taskPresentation: ExerciseTaskPresentationType;
  answerInput: ExerciseAnswerInput;
  inputMode: ExerciseInputMode;
  targetsWithComplements: boolean;
  difficulty: number;
  // subType: 'situation-and-boxes' | 'situation-with-solution-zones' | 'table-and-boxes';
}

function createExerciseConfig(
  difficulty: number,
  inputType: ExerciseInputOutputType,
  taskPresentation: ExerciseTaskPresentationType,
  answerInput: ExerciseAnswerInput,
  inputMode: ExerciseInputMode,
  targetsWithComplements: boolean
): ICalculateCPsExercise1Config {
  return {
    inputType,
    difficulty,
    taskPresentation,
    answerInput,
    inputMode,
    targetsWithComplements,
  };
}

const FREQUENCY_CONFIGS: ICalculateCPsExercise1Config[] = [
  createExerciseConfig(1, 'frequencies', 'situation', 'text-fields', 'dnd', false),
  createExerciseConfig(1, 'frequencies', 'contingency-table', 'text-fields', 'dnd', false),
  createExerciseConfig(2, 'frequencies', 'contingency-table', 'text-fields', 'dnd', true),
  createExerciseConfig(3, 'frequencies', 'contingency-table', 'text-fields', 'text-field', false),
  createExerciseConfig(3, 'frequencies', 'situation', 'text-fields', 'text-field', false),
  createExerciseConfig(4, 'frequencies', 'contingency-table', 'text-fields', 'text-field', true),
  createExerciseConfig(5, 'frequencies', 'situation', 'text-fields', 'dnd', true),
  createExerciseConfig(6, 'frequencies', 'situation', 'text-fields', 'text-field', true),
];

const PROBABILITY_CONFIGS: ICalculateCPsExercise1Config[] = [
  createExerciseConfig(1, 'probabilties', 'situation', 'text-fields', 'dnd', false),
  createExerciseConfig(1, 'probabilties', 'contingency-table', 'text-fields', 'dnd', false),
  createExerciseConfig(2, 'probabilties', 'contingency-table', 'text-fields', 'dnd', true),
  createExerciseConfig(3, 'probabilties', 'contingency-table', 'text-fields', 'text-field', false),
  createExerciseConfig(3, 'probabilties', 'situation', 'text-fields', 'text-field', false),
  createExerciseConfig(4, 'probabilties', 'contingency-table', 'text-fields', 'text-field', true),
  createExerciseConfig(5, 'probabilties', 'situation', 'text-fields', 'dnd', true),
  createExerciseConfig(6, 'probabilties', 'situation', 'text-fields', 'text-field', true),
];

const REPITITION_TO_DIFFICULTY: Record<number, [number, number]> = {
  1: [1, 1],
  2: [2, 3],
  3: [4, 4],
};

interface IExerciseMathData extends ICPData {
  ret: IBivarFreqRET;
  // eslint-disable-next-line react/no-unused-prop-types
  input: IBivarFreqPopMarginalJointGenInput;
  variables: IVariableSet;
  cpTargets: [VariableWithState, VariableWithState][];
  cpSolutions: number[];
  cpSolutionOptions: number[];
}

const PREVIOUS_RETS: IBivarFreqRET[] = [];

function createRandomMathData(rem: RandomExperimentManager, config: ICalculateCPsExercise1Config): IExerciseMathData {
  const ret = getNextRandomBivarBinaryFreqRET(rem, PREVIOUS_RETS);
  const variables = getVariableSet(ret, false);

  const genData = ret.generation.random as IBivarFreqRandomPopMarginalJointGenInput;
  const input = createRandomGenInput(genData);

  const cpDataOptions: ICreateCPDataOptions =
    config.inputType === 'frequencies' ? null : { condProbsSource: 'joint-probs', roundProbCalculationsTo: 2 };
  const cpData = createCPData(input, variables.long, cpDataOptions);
  const cpTargets = createRandomCPTaskTargets(variables.long, 4, config.targetsWithComplements ? null : cpTargetContainsOnlyFirstCats);

  const cpSolutions = createCPTargetSolutions(cpTargets, cpData.condProbs);
  const cpSolutionOptions = createProbabilitySolutionOptions(cpSolutions, cpSolutions.length * 3);
  cpSolutionOptions.sort((a, b) => a - b);

  return {
    ret,
    input,
    variables,
    ...cpData,
    cpTargets,
    cpSolutions,
    cpSolutionOptions,
  };
}

function createRandomExerciseConfig(inputType: ExerciseInputOutputType, repitition: number): ICalculateCPsExercise1Config {
  const difficulty = REPITITION_TO_DIFFICULTY[repitition] || [0, 100];
  const configSet = inputType === 'frequencies' ? FREQUENCY_CONFIGS : PROBABILITY_CONFIGS;

  const filteredConfigs = filterByDifficulty(configSet, difficulty);
  return filteredConfigs[getRandomInt(0, filteredConfigs.length - 1)];
}

export interface IRandomCalculateCPsExercise1Props {
  // Seems to be a bug in react eslint
  // TODO: remove disable instructions
  // eslint-disable-next-line react/no-unused-prop-types
  repitition: number;
  // eslint-disable-next-line react/no-unused-prop-types
  isVoluntary: boolean;
  // eslint-disable-next-line react/no-unused-prop-types
  inputType?: ExerciseInputOutputType;
  // eslint-disable-next-line react/no-unused-prop-types
  onFinished?: () => void;
}

export interface ICalculateCPsExercise1Props extends IRandomCalculateCPsExercise1Props, IExerciseMathData {
  config: ICalculateCPsExercise1Config;
  helpers: TabPanelInfos;
}

// split / combined

function isFreqs(config: ICalculateCPsExercise1Config): boolean {
  return config.inputType === 'frequencies';
}

// function isWithPlaceholders(config: ICalculateCPsExercise1Config): boolean {
//   return config.inputMode === 'dnd';
// }

function isSplitView(config: ICalculateCPsExercise1Config): boolean {
  return config.taskPresentation !== config.answerInput;
}

function showSituation(config: ICalculateCPsExercise1Config): boolean {
  return config.taskPresentation === 'situation';
}

function showTable(config: ICalculateCPsExercise1Config): boolean {
  return config.taskPresentation === 'contingency-table';
}

// function createSituationText(
//   ret: IBivarFreqRET,
//   frequencies: BivariateJointFrequencyDataFrame,
//   probabilities: BivariateJointProbabilityDataFrame,
//   condProbs: [BivariateCPDataFrame, BivariateCPDataFrame],
//   config: ICalculateCPsExercise1Config,
//   i18n: i18n
// ): string {
//   if (isFreqs(config)) {
//     if (isToCPs(config)) {
//       if (isWithPlaceholders(config)) {
//       } else {
//         return createDescription(ret, frequencies, 'description-population-marginal-joint', i18n);
//       }
//     } else if (isWithPlaceholders(config)) {
//       let text = createFrequencyTaskDescriptionWithPlaceholders(ret, frequencies, i18n);
//       text += ` ${createConditionalProbDescription(condProbs[0], i18n)}`;
//       return text;
//     }
//   }
//   throw new Error(`Unsupported config for situation text`);
// }

export function CalculateCPsExercise1(props: ICalculateCPsExercise1Props): React.ReactElement {
  const { helpers, ret, frequencies, probabilities, cpTargets, config, cpSolutions, cpSolutionOptions, variables } = props;

  const learningResourceManager = useLearningResourceManager();
  const { i18n, t } = useTranslation('conditional-probability');

  const data = useMemo(() => {
    const problem = i18n.t(ret.localeKeys.introduction);

    const situation =
      showSituation(config) && isFreqs(config)
        ? createDescriptionFreqs(ret, frequencies, 'description-population-marginal-joint', i18n)
        : createDescriptionPercent(ret, probabilities, 'description-marginal-joint-percent', i18n);

    const cpLabels = cpTargets.map((target) => {
      target = convertTargetByEventVariables(target, variables.long, variables.short);
      return formatConditionalProbability(target[0], target[1], { condProbFormat: 'Notation', varFormat: 'StateOnly' });
    });
    return { problem, situation, cpLabels };
  }, [ret, cpTargets, frequencies, probabilities, config, i18n, variables]);

  const [assessment] = useResettableState(() => new AssessmentController(cpSolutions, isNumberEqual2), [cpSolutions]);
  const activateValidation = useCallback(() => assessment.enableValidation(), [assessment]);
  const presentSolutions = useCallback(() => assessment.enablePresentSolutions(), [assessment]);

  const imageURL = useMemo(() => getMetaImage(learningResourceManager.getMeta(ret.id)), [ret, learningResourceManager]);

  // ==== Rendering

  // == Task card
  // presents the task to the user; always shown
  const taskCard = (
    <LearningResourceCard label={t('task')}>
      <div>{t('exercise:calculate-prob')}</div>
    </LearningResourceCard>
  );

  // presents the situation as text to the user depends on type
  const situationCard = (
    <SituationWithImage text={isSplitView(config) && showSituation(config) ? data.situation : data.problem} imageURL={imageURL} />
  );

  // == Situation with Boxes
  // const descriptionWithSolutionZone =
  //   !isSplitView(config) &&
  //   replaceTextWithElements(data.situation, {
  //     '{marginal1}': <NumericAnswerInput index={0} type="dnd" itemType="exercise" />,
  //   });

  // presents the situation as a contingency-table to the user; depends on type
  const contingencyTableCard = isSplitView(config) && showTable(config) && isFreqs(config) && (
    <LearningResourceCard label={t('math:contingency-table')}>
      <div>{t('contingency-table-freqs-intro')}</div>
      <ContingencyBarTable initialData={isFreqs(config) ? frequencies : probabilities} showMarginals />
    </LearningResourceCard>
  );
  const jointProbabilityTableCard = isSplitView(config) && showTable(config) && !isFreqs(config) && (
    <LearningResourceCard label={t('math:contingency-table')}>
      <div>{t('joint-probability-table-intro')}</div>
      <ProbabilityTable initialData={probabilities} showMarginals />
    </LearningResourceCard>
  );

  const eventsCard = (
    <LearningResourceCard label={t('events')}>
      {t('considered-events')}
      <VariablesAndCatsNameMapper variables={variables.long} renamedVariables={variables.short} />
    </LearningResourceCard>
  );

  const solutionCard = isSplitView(config) && config.answerInput === 'text-fields' && (
    <LearningResourceCard label={t('exercise:solution-attempt')}>
      {/* <div>{solutions.join('  ==== ')}</div> */}
      {!isDnD(config) && t('exercise:round-2')}
      <div>
        <NumberTaskTable type={config.inputMode} labels={data.cpLabels} format={formatProbability2} />
      </div>
      {config.inputMode === 'dnd' && (
        <NumericPossibleSolutions options={cpSolutionOptions} label={t('exercise:solution-drag-drop')} format={formatProbability2} />
      )}
      <Stack direction="row" justifyContent="center">
        <Button onClick={activateValidation}>{t('exercise:validate')}</Button>
        <Button onClick={presentSolutions}>{t('exercise:show-solution')}</Button>
      </Stack>
    </LearningResourceCard>
  );

  const solutionData = isFreqs(config) ? frequencies : probabilities;

  return (
    <AssessmentContext.Provider value={assessment}>
      <DndProvider backend={HTML5Backend}>
        <Stack spacing={2}>
          {situationCard}
          {contingencyTableCard || jointProbabilityTableCard}
          {eventsCard}
          {taskCard}
          {solutionCard}
          <LearningResourceCard label={t('exercise:help-tools')} optional>
            <TabbedContent panels={helpers} />
          </LearningResourceCard>
          <LearningResourceCard label={t('exercise:solution-path')} optional>
            <CPCalculation
              data={solutionData}
              mode={isFreqs(config) ? CPCalculationMode.Frequency : CPCalculationMode.Probability}
              showData
              showEquation
              displayVariables={variables.short}
              condProbsToCalculate={cpTargets}
            />
          </LearningResourceCard>
        </Stack>
      </DndProvider>
    </AssessmentContext.Provider>
  );
}

export function RandomCalculateCPsExercise1(props: IRandomCalculateCPsExercise1Props): React.ReactElement {
  const { inputType, repitition } = props;

  const learningResourceManager = useLearningResourceManager();
  const randomExperimentManager = useRandomExperimentManager();
  const { t } = useTranslation('conditional-probability');

  const [config] = useState(() => createRandomExerciseConfig(inputType, repitition));
  const [mathData] = useState(() => createRandomMathData(randomExperimentManager, config));

  const equationFromFreqs = learningResourceManager.getResource<string>('conditional-probability/from-frequencies-equation.json');
  const equationFromProbs = learningResourceManager.getResource<string>('conditional-probability/equation.md');

  const vennGraphData = useMemo(() => !isFreqs(config) && createVennGraphData(mathData.probabilities), [mathData.probabilities, config]);
  const vennGraphLabelConfig = useMemo(
    () => ({ ...SET_LABEL_CONFIG_PROBABILITY, rename: mathData.variables.short.map((variable) => t(variable.categories[0].toString())) }),
    [mathData.variables, t]
  );

  const helperTabs: TabPanelInfos = useMemo(
    () => ({
      equationFromFreqs: isFreqs(config) && [t('math:equation'), <Equation equation={equationFromFreqs} />],
      equationFromProbs: !isFreqs(config) && [t('math:equation'), <Equation equation={equationFromProbs} />],

      contingencyTable: isFreqs(config) &&
        !showTable(config) && [
          t('math:contingency-table'),
          <>
            <div>{t('contingency-table-freqs-intro')}</div>
            <ContingencyBarTable initialData={mathData.frequencies} showMarginals />
          </>,
        ],
      jointProbabilityTable: !isFreqs(config) &&
        !showTable(config) && [
          t('math:joint-probability-table'),
          <>
            <div>{t('joint-probability-table-intro')}</div>
            <ProbabilityTable initialData={mathData.probabilities} showMarginals />
          </>,
        ],
      vennProbs: vennGraphData && [
        t('math:venn-diagram'),
        <>
          <div>{t('venn-probs-intro')}</div>
          <VennDiagramWithCircles
            sets={vennGraphData.sets}
            combinations={vennGraphData.combinations}
            width={800}
            height={400}
            setLabelConfig={vennGraphLabelConfig}
          />
        </>,
      ],
    }),
    [mathData, equationFromFreqs, equationFromProbs, t, config, vennGraphData, vennGraphLabelConfig]
  );

  // eslint-disable-next-line react/jsx-props-no-spreading
  return <CalculateCPsExercise1 {...props} {...mathData} helpers={helperTabs} config={config} />;
}

export function RepeatableCalculateCPsFromFreqsExercise1(props: IRandomCalculateCPsExercise1Props): React.ReactElement {
  const { onFinished } = props;
  return (
    <RepeatableExercise numRepititions={3} onFinished={onFinished}>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <RandomCalculateCPsExercise1 inputType="frequencies" {...props} />
    </RepeatableExercise>
  );
}

export function RepeatableCalculateCPsFromProbsExercise1(props: IRandomCalculateCPsExercise1Props): React.ReactElement {
  const { onFinished } = props;
  return (
    <RepeatableExercise numRepititions={3} onFinished={onFinished}>
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <RandomCalculateCPsExercise1 inputType="probabilties" {...props} />
    </RepeatableExercise>
  );
}
