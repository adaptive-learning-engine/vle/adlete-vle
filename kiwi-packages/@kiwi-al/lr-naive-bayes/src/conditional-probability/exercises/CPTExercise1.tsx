// import { Definition } from '@adlete-vle/lr-core-components/Definition';
import { Stack } from '@mui/material';
import { extractFromExpression } from '@upsetjs/react';
import React, { useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { LearningResourceCard } from '@adlete-vle/lr-core-components/learning-resources';

// import { JointFrequencyGeneratorInput } from '@adlete-vle/lr-core/components/frequency/JointFrequencyGeneratorInput';
// import { Hint } from '@adlete-vle/lr-core/components/learning-resources/Hint';
import {
  createDescriptionFreqs,
  createRandomGenInput,
  generateJointFrequencyData,
  IBivarFreqRandomPopMarginalJointGenInput,
  IBivarFreqRET,
} from '@kiwi-al/math-core/frequencies';
import { getConditionalProbabilitiesFromJointFrequency, jointProbabilityDFFromFrequencyDF } from '@kiwi-al/math-core/probability';
import { RandomExperimentType } from '@kiwi-al/math-core/random-experiment';
import { Array2D } from '@kiwi-al/math-core/tabular';
import { RandomVarState } from '@kiwi-al/math-core/variables';
import { ContingencyTable } from '@kiwi-al/math-core-components/frequency';
import { ConditionalProbabilityTable, ProbabilityTable, validateProbabilities } from '@kiwi-al/math-core-components/probability';
import { convertFrequenciesToVennData, VennDiagramWithCircles } from '@kiwi-al/math-core-components/venn';

import { useRandomExperimentManager } from '../../contexts';
import { SET_LABEL_CONFIG_PROBABILITY } from '../shared/venn';

export function CPTExercise1(): React.ReactElement {
  const randomExperimentManager = useRandomExperimentManager();

  const { i18n, t } = useTranslation('conditional-probability');

  const [reTemplate] = useState<IBivarFreqRET>(
    randomExperimentManager.getTemplateByType<IBivarFreqRET>(RandomExperimentType.BivariateBinary)[0]
  );

  const genData = reTemplate.generation.random;

  const input = useMemo(() => createRandomGenInput(genData as IBivarFreqRandomPopMarginalJointGenInput), [genData]);

  // calculating the necessary data
  const data = useMemo(() => {
    const frequencies = generateJointFrequencyData(input, reTemplate.variables);
    const description = createDescriptionFreqs(reTemplate, frequencies, 'description-population-marginal-joint', i18n);
    const probs = jointProbabilityDFFromFrequencyDF(frequencies);
    const condProbs = getConditionalProbabilitiesFromJointFrequency(frequencies, true);

    const categories: [RandomVarState, RandomVarState] = [frequencies.variables[0].categories[0], frequencies.variables[1].categories[0]];

    const vennData = convertFrequenciesToVennData(probs, categories);
    const vennGraphData = extractFromExpression(vennData, { type: 'distinctIntersection' });

    return {
      frequencies,
      description,
      categories,
      condProbs,
      probs,
      vennGraphData,
    };
  }, [input, reTemplate, i18n]);

  const editableCells = new Array2D([
    [true, false],
    [true, false],
  ]);

  const initialProbs = useMemo(() => {
    const probs = data.condProbs.copy();
    probs.data.iset(0, 0, '');
    probs.data.iset(1, 0, '');
    return probs;
  }, [data.condProbs]);

  const validation = useMemo(
    () => ({
      validationData: data.condProbs,
      validate: validateProbabilities,
    }),
    [data.condProbs]
  );

  return (
    <Stack spacing={2}>
      <LearningResourceCard label={t('conditional-probability:situation')}>{data.description}</LearningResourceCard>
      <LearningResourceCard label={t('task-cpt-title')}>
        <div>{t('cpt-exercise-1-task')}</div>
        <ConditionalProbabilityTable initialData={initialProbs} editableCells={editableCells} validation={validation} />
      </LearningResourceCard>
      <LearningResourceCard label={t('math:contingency-table')} optional>
        <div>{t('contingency-table-freqs-intro')}</div>
        <ContingencyTable initialData={data.frequencies} showMarginals />
      </LearningResourceCard>
      <LearningResourceCard label={t('math:joint-probability-table')} optional>
        <div>{t('joint-probability-table-intro')}</div>
        <ProbabilityTable initialData={data.probs} showMarginals />
      </LearningResourceCard>
      <LearningResourceCard label={t('math:venn-diagram')} optional>
        <div>{t('conditional-probability:venn-probs-intro')}</div>
        <VennDiagramWithCircles
          sets={data.vennGraphData.sets}
          combinations={data.vennGraphData.combinations}
          width={800}
          height={400}
          setLabelConfig={SET_LABEL_CONFIG_PROBABILITY}
        />
      </LearningResourceCard>
      {/* <Hint hint={cotiHint} /> */}
    </Stack>
  );
}
