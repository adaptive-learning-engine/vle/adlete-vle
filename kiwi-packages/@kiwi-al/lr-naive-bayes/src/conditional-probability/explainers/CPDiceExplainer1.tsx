import { Button, Stack } from '@mui/material';
import React, { useCallback, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';

import type { RecursivePartial } from '@adlete-utils/typescript';
import { getMetaImage, HumanReadableTextualData } from '@adlete-vle/lr-core/learning-resources';
import { CustomMarkdown } from '@adlete-vle/lr-core-components/CustomMarkdown';
import { useLearningResourceManager } from '@adlete-vle/lr-core-components/contexts';
import { Hint, LearningResourceCard } from '@adlete-vle/lr-core-components/learning-resources';
import { MoveableContent } from '@adlete-vle/lr-core-components/misc/MoveableContent';
import { useMemoWith, useResettableState } from '@adlete-vle/utils/hooks';

import { IBivarFreqRET } from '@kiwi-al/math-core/frequencies';
import { RandomExperimentManager, RandomExperimentType, variablesToSampleSpace2 } from '@kiwi-al/math-core/random-experiment';
import { ICategoricalVarDef } from '@kiwi-al/math-core/variables';
import { ContingencyBarTable, IContingencyBarTableFormatting } from '@kiwi-al/math-core-components/frequency';
import { coloredEventsToColorMap } from '@kiwi-al/math-core-components/random-experiment';

import { useRandomExperimentManager } from '../../contexts';
import { DiceEventConfigurator, DiceSampleSpace } from '../../random-experiments/rolling-2-dice/components';
import {
  createDiceJointFrequencyDF,
  createJointEvent,
  createRandomEvents,
  IColoredDiceEvent,
} from '../../random-experiments/rolling-2-dice/functions';
import { StartInteractionMode } from '../../shared/components/StartInteractionMode';
import { CPCalculation, CPCalculationMode, ICPCalculationFormatting } from '../components/CPCalculation';
import { SituationWithImage } from '../shared/SituationWithImage';
import { useMakeLRCardWide } from '../shared/hooks';
import { TabbedContent } from '../shared/tabs';

function getDiceRET(rem: RandomExperimentManager): IBivarFreqRET {
  const polynaries = rem.getTemplateByType<IBivarFreqRET>(RandomExperimentType.BivariatePolynary);
  return polynaries.find((ret) => ret.id === 'rolling-2-dice-freq-random-experiment');
}

const CONTINGENCY_FORMAT: IContingencyBarTableFormatting = {
  labelFormat: { format: 'StateOnly' },
};

const CP_CALCULATION_FORMAT: RecursivePartial<ICPCalculationFormatting> = {
  condProb: { condProbFormat: 'Notation' },
};

export function CPDiceExplainer1(): React.ReactElement {
  const learningResourceManager = useLearningResourceManager();
  const randomExperimentManager = useRandomExperimentManager();

  const { t } = useTranslation('conditional-probability');

  // === handling the position of the task box
  const [taskPosition, setTaskPosition, taskRef] = useMakeLRCardWide();

  // === picking the random experiment
  const [reTemplate] = useState<IBivarFreqRET>(getDiceRET(randomExperimentManager));

  const imageURL = useMemo(() => getMetaImage(learningResourceManager.getMeta(reTemplate.id)), [reTemplate, learningResourceManager]);
  const hint = learningResourceManager.getResource<HumanReadableTextualData>('conditional-probability/calc-using-number-of-outcomes.json');

  // === calculating the data
  const sampleSpace = useMemo(
    () => variablesToSampleSpace2<number>(reTemplate.variables as ICategoricalVarDef<number>[]),
    [reTemplate.variables]
  );

  const [events, setEvents] = useResettableState(() => createRandomEvents(sampleSpace), [sampleSpace]);
  const [focusedEvent, setFocusedEvent] = useState('none');

  const frequencies = useMemoWith(createDiceJointFrequencyDF, [events]);

  const visibleEvents = useMemo(() => [...events, createJointEvent(events, frequencies.variables)], [events, frequencies.variables]);

  const colors = coloredEventsToColorMap(visibleEvents);
  colors.joint = colors[visibleEvents[2].name];

  const focusButtons = useMemo(
    () =>
      events.map((event) => (
        <Button onClick={() => setFocusedEvent(event.name)}>{t(`cp-dice-explainer:focus`, { condResult: event.name })}</Button>
      )),
    [events, t]
  );

  const setEventsAndReset = useCallback(
    (newEvents: IColoredDiceEvent[]) => {
      setFocusedEvent('none');
      setEvents(newEvents);
    },
    [setEvents]
  );

  // === rendering
  return (
    <MoveableContent targetIndex={4} position={taskPosition}>
      <SituationWithImage text={t('cp-dice-explainer:introduction')} imageURL={imageURL} />
      <LearningResourceCard label={t('results-events')}>
        <div>{t('cp-dice-explainer:combinations')}</div>
        <DiceSampleSpace
          variables={reTemplate.variables}
          events={visibleEvents}
          focusedEvent={focusedEvent}
          onFocusedEventChanged={setFocusedEvent}
        />
      </LearningResourceCard>
      <LearningResourceCard label={t('calculation')}>
        <div>
          <CPCalculation
            data={frequencies}
            mode={CPCalculationMode.Frequency}
            colors={colors}
            showEquation
            showData
            formatting={CP_CALCULATION_FORMAT}
            extraElems={focusButtons}
          />
        </div>
      </LearningResourceCard>
      <Hint hint={hint} />
      <LearningResourceCard ref={taskRef} label={t('task')}>
        {taskPosition === 'original' ? (
          <StartInteractionMode text={t('interaction-mode:maximise-browser')} onStart={() => setTaskPosition('left')} />
        ) : (
          <Stack spacing={2}>
            <div>
              <CustomMarkdown>
                {
                  // - In der oben dargestellten Visualisierung der Ereignismenge bzw. in der Berechnung kannst du auch eins der Ereignisse in den Fokus setzen um dessen Elemente besser zu sehen.
                  t('interaction-mode:dice-configure-events')
                }
              </CustomMarkdown>
            </div>
            <DiceEventConfigurator sampleSpace={sampleSpace} events={events} onChange={setEventsAndReset} />
            <div>
              <Button
                onClick={() => {
                  setFocusedEvent('none');
                  setEvents(createRandomEvents(sampleSpace));
                }}
              >
                {t('interaction-mode:random-events')}
              </Button>
            </div>
          </Stack>
        )}
      </LearningResourceCard>
      <LearningResourceCard label={t('more-visuals')} optional>
        <TabbedContent
          panels={[
            [
              t('math:contingency-table'),
              <>
                <div>{t('contingency-table-freqs-intro')}</div>
                <ContingencyBarTable initialData={frequencies} showMarginals formatting={CONTINGENCY_FORMAT} />
              </>,
            ],
          ]}
        />
      </LearningResourceCard>
    </MoveableContent>
  );
}
