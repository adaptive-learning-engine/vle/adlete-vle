import { Divider, Stack } from '@mui/material';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import { LearningResourceCard } from '@adlete-vle/lr-core-components/learning-resources';
import { MoveableContent } from '@adlete-vle/lr-core-components/misc/MoveableContent';
import { useMemoWith, useResettableState, useScrollOnState } from '@adlete-vle/utils/hooks';

import { IBivarFreqRET } from '@kiwi-al/math-core/frequencies';
import { getJointProbsFromProbsAndCPs } from '@kiwi-al/math-core/probability';
import {
  getAlternateVariableNamesFromRET,
  getShortCategoryNamesFromRET,
  RandomExperimentManager,
  RandomExperimentType,
} from '@kiwi-al/math-core/random-experiment';
import { BivarCategoricalVarDefs, copyVariablesWithNewNames } from '@kiwi-al/math-core/variables';
import { CPStackedBarChart, JointProbsFromCPsGeneratorInput, ProbabilityBarChart } from '@kiwi-al/math-core-components/probability';
import { VarCatNameMapper } from '@kiwi-al/math-core-components/shared/VarCatNameMapper';

import { useRandomExperimentManager } from '../../contexts';
import { MoveableContentInfo } from '../components/MoveableContentInfo';
import { SwitchRETCard } from '../components/SwitchRETCard';
import { TotalProbabilityCalculation } from '../components/TotalProbabilityCalculation';
import { createCPData } from '../shared/data';
import { useMakeLRCardWide } from '../shared/hooks';
import { getRETsOfType } from '../shared/rets';
import { TabbedContent } from '../shared/tabs';

function getRETs(randomExperimentManager: RandomExperimentManager): IBivarFreqRET[] {
  const polynaryRETs = getRETsOfType(randomExperimentManager, RandomExperimentType.BivariatePolynary);
  return polynaryRETs.filter((ret) => ret.generation && ret.generation.default);
}

export function TotalProbabilityExplainer(): React.ReactElement {
  const randomExperimentManager = useRandomExperimentManager();
  const { t } = useTranslation('conditional-probability');

  // === handling the position of the task box
  const [taskPosition, setTaskPosition, taskRef] = useMakeLRCardWide();

  const rets = useMemoWith(getRETs, [randomExperimentManager]);
  const [ret, setRET, ref] = useScrollOnState(rets[0]);

  const sharedData = useMemo(() => {
    const variables = ret.variables as BivarCategoricalVarDefs;
    const renamedNames = getAlternateVariableNamesFromRET(ret);
    const renamedCats = getShortCategoryNamesFromRET(ret);
    const shortVariables = copyVariablesWithNewNames(variables, renamedNames, renamedCats) as BivarCategoricalVarDefs;
    return { variables, shortVariables };
  }, [ret]);

  // creating default data used as initial state
  const defaultData = useMemo(() => {
    const cpData = createCPData(ret.generation.default, sharedData.shortVariables);
    const marginals = sharedData.shortVariables.map((variable) => cpData.probabilities.getMarginalFrequencies(variable));
    return { ...cpData, marginals };
  }, [sharedData, ret]);

  const [input, setInput] = useResettableState(
    () => ({
      condProbs: defaultData.condProbs[0],
      givenMarginalProbs: defaultData.marginals[1],
    }),
    [ret]
  );

  const data = useMemo(() => {
    const probabilities = getJointProbsFromProbsAndCPs(input);
    const condVarMarginals = probabilities.getMarginalFrequencies(input.condProbs.variables[0]);
    const description = t(ret.localeKeys['problem-description']);
    return { probabilities, description, condVarMarginals };
  }, [input, t, ret]);

  // calculating the necessary data
  // const data = useMemo(() => {
  //   const variables = ret.variables as BivarCategoricalVarDefs;
  //   const renamedNames = getAlternateVariableNamesFromRET(ret);
  //   const renamedCats = getShortCategoryNamesFromRET(ret);
  //   const shortVariables = copyVariablesWithNewNames(variables, renamedNames, renamedCats) as BivarCategoricalVarDefs;
  //   const cpData = createCPData(input, shortVariables);
  //   const marginals = shortVariables.map((variable) => cpData.probabilities.getMarginalFrequencies(variable));
  //   return { ...cpData, variables, shortVariables, marginals };
  // }, [ret, input]);

  return (
    <MoveableContent ref={ref} targetIndex={4} position={taskPosition}>
      <LearningResourceCard label={t('situation')}>
        <div>{data.description}</div>
      </LearningResourceCard>
      <LearningResourceCard label={t('Merkmale')}>
        {t('Folgende Merkmale werden betrachtet:')}
        <Stack spacing={2} direction="row" justifyContent="space-around">
          <VarCatNameMapper variable={sharedData.variables[0]} renamedVariable={sharedData.shortVariables[0]} showLabel />
          <VarCatNameMapper variable={sharedData.variables[1]} renamedVariable={sharedData.shortVariables[1]} showLabel />
        </Stack>
      </LearningResourceCard>
      <LearningResourceCard label={t('Wahrscheinlichkeiten')}>
        <div>{t('Folgende bedingten Wahrscheinlichkeiten und Randwahrscheinlichkeiten sind bekannt: ')}</div>
        <Stack spacing={2} direction="row" justifyContent="space-around" divider={<Divider orientation="vertical" flexItem />}>
          <CPStackedBarChart data={input.condProbs} />
          <ProbabilityBarChart probabilities={input.givenMarginalProbs} variable={sharedData.shortVariables[1]} />
        </Stack>
      </LearningResourceCard>
      <LearningResourceCard label={t('Berechnung')}>
        <div>
          <TotalProbabilityCalculation condProbs={input.condProbs} probabilities={data.probabilities} varFormat="StateOnly" />
        </div>
      </LearningResourceCard>
      <LearningResourceCard ref={taskRef} label={t('Aufgabe')}>
        <div>
          {t(
            'Verändere die gegebenen Wahrscheinlichkeiten und bedingten Wahrscheinlichkeiten. Beobachte wie sich die errechneten totalen Wahrscheinlichkeiten verändern. Was passiert bspw., wenn die bedingten Wahrscheinlickeiten ähnlich gesetzt sind?'
          )}
        </div>
        <JointProbsFromCPsGeneratorInput input={input} onChange={setInput} />
        <SwitchRETCard rets={rets} value={ret} onChange={setRET} />
        <MoveableContentInfo position={taskPosition} onChange={setTaskPosition} />
      </LearningResourceCard>
      <LearningResourceCard label={t('Weitere Visualisierungen')} optional>
        <TabbedContent
          panels={[
            [
              t('Wahrscheinlichkeitsverteilung'),
              <ProbabilityBarChart probabilities={data.condVarMarginals} variable={sharedData.shortVariables[0]} />,
            ],
          ]}
        />
      </LearningResourceCard>
    </MoveableContent>
  );
}
