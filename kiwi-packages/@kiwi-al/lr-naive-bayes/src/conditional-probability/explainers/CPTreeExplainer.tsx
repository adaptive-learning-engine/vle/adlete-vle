import { Alert } from '@mui/material';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import { getMetaImage, HumanReadableTextualData } from '@adlete-vle/lr-core/learning-resources';
import { useLearningResourceManager } from '@adlete-vle/lr-core-components/contexts';
import { Definition, LearningResourceCard } from '@adlete-vle/lr-core-components/learning-resources';
import { MoveableContent } from '@adlete-vle/lr-core-components/misc/MoveableContent';
import { useMemoWith, useResettableState, useScrollOnState } from '@adlete-vle/utils/hooks';

import {
  BivariateJointFrequencyDataFrame,
  createDescriptionFreqs,
  FrequencyGenInputProperty,
  generateJointFrequencyData,
} from '@kiwi-al/math-core/frequencies';
import {
  BivariateCPDataFrame,
  formatConditionalProbability,
  formatProbability,
  formatProbability2,
  getConditionalProbabilitiesFromJointFrequency,
  jointProbabilityDFFromFrequencyDF,
  probabilitiesFromVarFrequencies,
} from '@kiwi-al/math-core/probability';
import { formatVariableWithState, ICategoricalVarDef, RandomVarState } from '@kiwi-al/math-core/variables';
import { JointFrequencyGeneratorInput } from '@kiwi-al/math-core-components/frequency';
import { ConditionalProbabilityTable, ProbabilityTable } from '@kiwi-al/math-core-components/probability';
import {
  IProbabilityTreeDatum,
  ProbabilityTreeDiagram,
  ProbTreeLinkLabelType,
  ProbTreeNodeLabelType,
} from '@kiwi-al/math-core-components/probability-tree-diagram';

import { useRandomExperimentManager } from '../../contexts';
import { MoveableContentInfo } from '../components/MoveableContentInfo';
import { SwitchRETCard } from '../components/SwitchRETCard';
import { SituationWithImage } from '../shared/SituationWithImage';
import { useMakeLRCardWide } from '../shared/hooks';
import { getRETsOfType } from '../shared/rets';
import { TabbedContent } from '../shared/tabs';

interface ICustomProbabilityTreeDatum extends IProbabilityTreeDatum {
  context: string;
  frequency: number;
  condProbability: number;
  jointProbability: number;
  condProbNotation?: string;
  focusVariable?: ICategoricalVarDef;
  focusVariableState?: RandomVarState;
}

function convertFrequenciesToProbTreeData(
  freqs: BivariateJointFrequencyDataFrame,
  condProbs: BivariateCPDataFrame
): ICustomProbabilityTreeDatum {
  const context = 'laptops';

  const conditionalVar = condProbs.getConditionalVariable();
  // const conditionalVarFreqs = freqs.getMarginalFrequencies(conditionalVar);
  // const conditionalVarPriors = probabilitiesFromVarFrequencies(conditionalVarFreqs);
  const givenVar = condProbs.getGivenVariable();
  const givenVarFreqs = freqs.getMarginalFrequencies(givenVar);
  const givenVarPriors = probabilitiesFromVarFrequencies(givenVarFreqs);

  return {
    id: 'root',
    context,
    frequency: freqs.data.sum().sum(),
    condProbability: 1,
    jointProbability: 1,

    children: givenVar.categories.map((givenState, givenStateIndex) => {
      const prob = givenVarPriors[givenStateIndex];
      return {
        id: `${givenVar.name}-${givenState}`,
        context,
        frequency: givenVarFreqs[givenStateIndex],
        condProbability: prob,
        jointProbability: prob,
        focusVariable: givenVar,
        focusVariableState: givenState,
        condProbNotation: formatVariableWithState([givenVar, givenState], { format: 'Notation' }),
        children: conditionalVar.categories.map((conditionalState) => {
          const condProb = condProbs.getConditionalProbability(conditionalState, givenState);
          return {
            id: `${givenVar.name}-${givenState}-${conditionalVar.name}-${conditionalState}`,
            context,
            frequency: freqs.getFrequency(givenVar, givenState, conditionalVar, conditionalState),
            condProbability: condProb,
            jointProbability: prob * condProb,
            condProbNotation: formatConditionalProbability([conditionalVar, conditionalState], [givenVar, givenState], {
              condProbFormat: 'Notation',
              varFormat: 'Notation',
            }),
            focusVariable: conditionalVar,
            focusVariableState: conditionalState,
          };
        }),
      };
    }),
  };
}

function getNodeLabel(node: ICustomProbabilityTreeDatum, labelType: ProbTreeNodeLabelType): string {
  switch (labelType) {
    case ProbTreeNodeLabelType.Frequency:
      return `${node.frequency} ${node.context}`;
    case ProbTreeNodeLabelType.Probability:
      return formatProbability2(node.jointProbability);
    case ProbTreeNodeLabelType.ProbabilityPercent:
      return formatProbability(node.jointProbability, { format: 'Percent' });
    case ProbTreeNodeLabelType.Event:
      return node.focusVariable ? formatVariableWithState([node.focusVariable, node.focusVariableState], { format: 'Notation' }) : '';
    default:
      throw new Error(`Unknown node label type '${labelType}'`);
  }
}

function getLinkLabel(source: ICustomProbabilityTreeDatum, target: ICustomProbabilityTreeDatum, labelType: ProbTreeLinkLabelType): string {
  switch (labelType) {
    case ProbTreeLinkLabelType.FrequencyRatio:
      return `${target.frequency} out of ${source.frequency}`;
    case ProbTreeLinkLabelType.Probability:
      return formatProbability2(target.condProbability);
    case ProbTreeLinkLabelType.ProbabilityPercent:
      return formatProbability(target.condProbability, { format: 'Percent' });
    case ProbTreeLinkLabelType.Notation:
      return target.condProbNotation;
    default:
      throw new Error(`Unknown link label type '${labelType}'`);
  }
}

export function CPTreeExplainer(): React.ReactElement {
  const randomExperimentManager = useRandomExperimentManager();
  const learningResourceManager = useLearningResourceManager();

  const { i18n, t } = useTranslation('conditional-probability');

  // === handling the position of the task box
  const [taskPosition, setTaskPosition, taskRef] = useMakeLRCardWide();

  const rets = useMemoWith(getRETsOfType, [randomExperimentManager, 'BivariateBinary']);
  const [ret, setRET, ref] = useScrollOnState(rets[0]);
  const imageURL = useMemo(() => getMetaImage(learningResourceManager.getMeta(ret.id)), [ret, learningResourceManager]);

  const example = ret.generation.default;

  const [input, setInput] = useResettableState(example, [ret]);

  // calculating the necessary data
  const data = useMemo(() => {
    const frequencies = generateJointFrequencyData(input, ret.variables);
    const probs = jointProbabilityDFFromFrequencyDF(frequencies);
    const description = createDescriptionFreqs(ret, frequencies, 'description-population-marginal-joint', i18n);

    const condProbs = getConditionalProbabilitiesFromJointFrequency(frequencies, true);

    const treeData = convertFrequenciesToProbTreeData(frequencies, condProbs);

    return {
      condProbs,
      frequencies,
      probs,
      description,
      treeData,
    };
  }, [input, ret, i18n]);

  const definition = learningResourceManager.getResource<HumanReadableTextualData>('probability-tree-diagram&definition=1');

  return (
    <MoveableContent ref={ref} targetIndex={4} position={taskPosition}>
      <Alert severity="warning">TODO: Wahrscheinlichkeiten statt Häufigkeiten verwenden</Alert>
      <Definition definition={definition} />
      <SituationWithImage text={data.description} imageURL={imageURL} />
      <LearningResourceCard label={t('Bedingter Wahrscheinlichkeitsbaum')}>
        <ProbabilityTreeDiagram
          root={data.treeData}
          viewport={{ width: 700, margin: { right: 100 } }}
          getNodeLabel={getNodeLabel}
          getLinkLabel={getLinkLabel}
          nodeLabelTypes={[ProbTreeNodeLabelType.Event]}
          linkLabelTypes={[ProbTreeLinkLabelType.ProbabilityPercent]}
        />
      </LearningResourceCard>
      <LearningResourceCard ref={taskRef} label={t('Aufgabe')}>
        <div>{t('task-cptree-task')}</div>
        <JointFrequencyGeneratorInput
          input={input}
          onChange={setInput}
          variables={data.frequencies.variables}
          disabledSliders={[FrequencyGenInputProperty.Population]}
        />
        <SwitchRETCard rets={rets} value={ret} onChange={setRET} />
        <MoveableContentInfo position={taskPosition} onChange={setTaskPosition} />
      </LearningResourceCard>
      <LearningResourceCard label={t('Weitere Visualisierungen')} optional>
        <TabbedContent
          panels={[
            [
              t('math:joint-probability-table'),
              <>
                <div>{t('joint-probability-table-intro')}</div>
                <ProbabilityTable initialData={data.probs} showMarginals />
              </>,
            ],
            [t('math:conditional-probability-table'), <ConditionalProbabilityTable initialData={data.condProbs} />],
          ]}
        />
      </LearningResourceCard>
    </MoveableContent>
  );
}
