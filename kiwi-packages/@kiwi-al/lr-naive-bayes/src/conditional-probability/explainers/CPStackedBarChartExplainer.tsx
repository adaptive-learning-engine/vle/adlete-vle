import { Divider, Stack } from '@mui/material';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import { getMetaImage, HumanReadableTextualData } from '@adlete-vle/lr-core/learning-resources';
import { useLearningResourceManager } from '@adlete-vle/lr-core-components/contexts';
import { Definition, LearningResourceCard } from '@adlete-vle/lr-core-components/learning-resources';
import { MoveableContent } from '@adlete-vle/lr-core-components/misc/MoveableContent';
import { useMemoWith, useResettableState, useScrollOnState } from '@adlete-vle/utils/hooks';

import { createDescriptionPercent, IBivarFreqRET } from '@kiwi-al/math-core/frequencies';
import { getCPsFromJointProbs, getJointProbsFromProbsAndCPs } from '@kiwi-al/math-core/probability';
import { RandomExperimentManager } from '@kiwi-al/math-core/random-experiment';
import { CPStackedBarChart, JointProbsFromCPsGeneratorInput, ProbabilityTable } from '@kiwi-al/math-core-components/probability';
import { VariablesAndCatsNameMapper } from '@kiwi-al/math-core-components/shared/VariablesAndCatsNameMapper';

import { useRandomExperimentManager } from '../../contexts';
import { StartInteractionMode } from '../../shared/components/StartInteractionMode';
import { SwitchRETCard } from '../components/SwitchRETCard';
import { SituationWithImage } from '../shared/SituationWithImage';
import { createCPData } from '../shared/data';
import { useMakeLRCardWide } from '../shared/hooks';
import { getRETsOfTypes } from '../shared/rets';
import { TabbedContent } from '../shared/tabs';
import { getVariableSet } from '../shared/variables';

function getRETs(rem: RandomExperimentManager): IBivarFreqRET[] {
  const rets = getRETsOfTypes(rem, ['BivariateBinary', 'BivariatePolynary']);
  return rets.filter((ret) => ret.generation && ret.generation.default);
}

export function CPStackedBarChartExplainer(): React.ReactElement {
  const randomExperimentManager = useRandomExperimentManager();
  const learningResourceManager = useLearningResourceManager();

  const { i18n, t } = useTranslation('conditional-probability');

  // === handling the position of the task box
  const [taskPosition, setTaskPosition, taskRef] = useMakeLRCardWide();

  const rets = useMemoWith(getRETs, [randomExperimentManager]);
  const [ret, setRET, ref] = useScrollOnState(rets[0]);
  const imageURL = useMemo(() => getMetaImage(learningResourceManager.getMeta(ret.id)), [ret, learningResourceManager]);
  const variables = useMemo(() => getVariableSet(ret, false), [ret]);

  // creating default data used as initial state
  const defaultData = useMemo(() => {
    const cpData = createCPData(ret.generation.default, variables.long);
    const marginals = variables.long.map((variable) => cpData.probabilities.getMarginalFrequencies(variable));
    return { ...cpData, marginals };
  }, [variables, ret]);

  const [input, setInput] = useResettableState(
    () => ({
      condProbs: defaultData.condProbs[0],
      givenMarginalProbs: defaultData.marginals[1],
    }),
    [ret]
  );

  // calculating the necessary data
  const data = useMemo(() => {
    const jointProbs = getJointProbsFromProbsAndCPs(input);
    const descKey = 'description-marginal-joint-percent';
    const description = ret.localeKeys[descKey] && createDescriptionPercent(ret, jointProbs, descKey, i18n);
    const otherCondProbs = getCPsFromJointProbs(jointProbs, false);

    return {
      description,
      jointProbs,
      otherCondProbs,
    };
  }, [input, ret, i18n]);

  const definition = learningResourceManager.getResource<HumanReadableTextualData>('stacked-bar-chart-for-cps&definition=1');

  return (
    <MoveableContent ref={ref} targetIndex={4} position={taskPosition}>
      <Definition definition={definition} />
      {data.description && <SituationWithImage text={data.description} imageURL={imageURL} />}
      <LearningResourceCard label={t('events')}>
        {t('considered-events')}
        <VariablesAndCatsNameMapper variables={variables.long} renamedVariables={variables.short} />
      </LearningResourceCard>
      <LearningResourceCard label={t('math:stacked-bar-chart')}>
        <Stack direction="row" justifyContent="space-around" divider={<Divider orientation="vertical" flexItem />}>
          <CPStackedBarChart data={input.condProbs} /* viewport={{ width: 300, height: 200 }} */ displayVariables={variables.middle} />
          {/* <CPStackedBarChart data={data.otherCondProbs} viewport={{ width: 300, height: 200 }} /> */}
        </Stack>
      </LearningResourceCard>
      <LearningResourceCard ref={taskRef} label={t('task')}>
        {taskPosition === 'original' ? (
          <StartInteractionMode text={t('interaction-mode:maximise-browser-alternative')} onStart={() => setTaskPosition('left')} />
        ) : (
          <Stack spacing={2}>
            <div>{t('interaction-mode:change-prob')}</div>
            <JointProbsFromCPsGeneratorInput
              input={input}
              onChange={setInput}
              hiddenSliders={['marginal']}
              displayVariables={variables.middle}
            />
            <SwitchRETCard rets={rets} value={ret} onChange={setRET} />
          </Stack>
        )}
      </LearningResourceCard>
      <LearningResourceCard label={t('more-visuals')} optional>
        <TabbedContent
          panels={[
            [
              t('math:joint-probability-table'),
              <>
                <div>{t('joint-probability-table-intro')}</div>
                <ProbabilityTable initialData={data.jointProbs} showMarginals />
              </>,
            ],
          ]}
        />
      </LearningResourceCard>
    </MoveableContent>
  );
}
