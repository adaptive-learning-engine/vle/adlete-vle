import { FormControlLabel, Radio, RadioGroup, Stack } from '@mui/material';
import { extractFromExpression } from '@upsetjs/react';
import React, { useCallback, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { HumanReadableTextualData } from '@adlete-vle/lr-core/learning-resources';
import { useLearningResourceManager } from '@adlete-vle/lr-core-components/contexts';
import { Hint, LearningResourceCard } from '@adlete-vle/lr-core-components/learning-resources';

import { IVennSetStyle, VennDiagramWithRects } from '@kiwi-al/math-core-components/venn';

type FocusType = 'None' | 'A' | 'B';

const VENN_STYLES: Record<FocusType, Record<string, IVennSetStyle>> = {
  None: {
    A: { foreground: { stroke: 'black', strokeWidth: 1 }, text: { fill: 'black' } },
    B: { foreground: { stroke: 'black', strokeWidth: 1, strokeDasharray: 4 }, text: { fill: 'black' } },
    'A ∩ B': { background: { fill: '#eee', strokeWidth: 0 }, foreground: { strokeWidth: 0 }, text: { fill: 'black' } },
    'sample-space': { background: { border: '3px solid blueviolet' } },
  },
  A: {
    A: { foreground: { stroke: 'blueviolet', strokeWidth: 3 }, text: { fill: 'blueviolet' } },
    B: { foreground: { stroke: 'black', strokeWidth: 1, strokeDasharray: 4, opacity: 0.2 }, text: { fill: 'black', opacity: 0.2 } },
    'A ∩ B': { background: { fill: '#eee', strokeWidth: 0 }, foreground: { strokeWidth: 0 }, text: { fill: 'black' } },
    'sample-space': { background: { border: '3px solid lightgray' }, text: { opacity: 0.2 } },
  },
  B: {
    A: { foreground: { stroke: 'black', strokeWidth: 1, opacity: 0.2 }, text: { fill: 'black', opacity: 0.2 } },
    B: { foreground: { stroke: 'blueviolet', strokeWidth: 3 }, text: { fill: 'blueviolet' } },
    'A ∩ B': { background: { fill: '#eee', strokeWidth: 0 }, foreground: { strokeWidth: 0 }, text: { fill: 'black' } },
    'sample-space': { background: { border: '3px solid lightgray' }, text: { opacity: 0.2 } },
  },
};

export function CPVennSampleSpaceExplainer(): React.ReactElement {
  const learningResourceManager = useLearningResourceManager();

  const { t } = useTranslation('cp-venn-sample-explainer');

  const [focus, setFocus] = useState<FocusType>('None');
  const onFocusChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setFocus(event.target.value as FocusType);
  }, []);

  // calculating the necessary data
  const data = useMemo(() => {
    const testVennData = [
      {
        sets: ['A'],
        cardinality: 30,
        label: 'x',
      },
      {
        sets: ['B'],
        cardinality: 100,
        label: 'y',
      },
      {
        sets: ['A', 'B'],
        cardinality: 50,
        label: 'z',
      },
    ];

    const vennGraphData = extractFromExpression(testVennData, { type: 'distinctIntersection' });

    return {
      vennGraphData,
    };
  }, []);

  const sampleSpaceHint = learningResourceManager.getResource<HumanReadableTextualData>(
    'conditional-probability/reduced-sample-space.json'
  );

  const vennStyles = VENN_STYLES[focus];

  return (
    <Stack spacing={2}>
      <Hint hint={sampleSpaceHint} />
      <LearningResourceCard stackProps={{ direction: 'row' }} label={t('results-visual')}>
        <VennDiagramWithRects
          sets={data.vennGraphData.sets}
          combinations={data.vennGraphData.combinations}
          maxCardinality={400}
          viewport={{ width: 400, height: 400 }}
          setStyles={vennStyles}
        />
        <Stack spacing={2} flex={1}>
          <div>{t('calculated-values')}</div>
          <div>{t('choose-alpha')}</div>
          <RadioGroup value={focus} onChange={onFocusChange} name="focus-event">
            <FormControlLabel
              value="None"
              control={<Radio />}
              label={
                <Stack spacing={2} direction="row" alignItems="center">
                  <div style={{ fontSize: '1.2em' }}>Ω</div>
                  <div>{t('used-calc')}</div>
                </Stack>
              }
            />
            <FormControlLabel
              value="A"
              control={<Radio />}
              label={
                <Stack spacing={2} direction="row" alignItems="center">
                  <div style={{ fontSize: '1.2em' }}>
                    Ω<sub>A</sub> = A
                  </div>
                  <div>{t('used-calc')}</div>
                </Stack>
              }
            />
            <FormControlLabel
              value="B"
              control={<Radio />}
              label={
                <Stack spacing={2} direction="row" alignItems="center">
                  <div style={{ fontSize: '1.2em' }}>
                    Ω<sub>B</sub> = B
                  </div>
                  <div>{t('used-calc')}</div>
                </Stack>
              }
            />
          </RadioGroup>
        </Stack>
      </LearningResourceCard>
      {/* <LearningResourceCard label={t('Bedingte Wahrscheinlichkeiten und Schnittmenge')}>
        <div>
          {t(
            'Es ist ersichtlich, dass die Schnittmenge A ∩ B eine Rolle bei der Berechnung der bedingten Wahrscheinlichkeiten P(A|B) und P(B|A) spielt. Sie ist sowohl in ΩA als auch ΩB enthalten.'
          )}
        </div>
      </LearningResourceCard> */}
    </Stack>
  );
}
