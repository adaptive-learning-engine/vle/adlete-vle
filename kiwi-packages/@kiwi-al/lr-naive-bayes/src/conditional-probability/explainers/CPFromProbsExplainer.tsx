import { Stack } from '@mui/material';
import { extractFromExpression } from '@upsetjs/react';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import { getMetaImage } from '@adlete-vle/lr-core/learning-resources';
import { CustomMarkdown } from '@adlete-vle/lr-core-components/CustomMarkdown';
import { useLearningResourceManager } from '@adlete-vle/lr-core-components/contexts';
import { LearningResourceCard } from '@adlete-vle/lr-core-components/learning-resources';
import { MoveableContent } from '@adlete-vle/lr-core-components/misc/MoveableContent';
import { useResettableState, useScrollOnState } from '@adlete-vle/utils/hooks';
import { useMemoWith } from '@adlete-vle/utils/hooks/useMemoWith';

import { createDescriptionPercent, generateJointFrequencyData } from '@kiwi-al/math-core/frequencies';
import { jointProbabilityDFFromFrequencyDF } from '@kiwi-al/math-core/probability';
import { getAlternateVariableNamesFromRET } from '@kiwi-al/math-core/random-experiment';
import { BivarCategoricalVarDefs, RandomVarState } from '@kiwi-al/math-core/variables';
import { JointProbsGeneratorInput, ProbabilityTable } from '@kiwi-al/math-core-components/probability';
import { VariablesAndCatsNameMapper } from '@kiwi-al/math-core-components/shared/VariablesAndCatsNameMapper';
import { convertFrequenciesToVennData, VennDiagramWithCircles } from '@kiwi-al/math-core-components/venn';

import { useRandomExperimentManager } from '../../contexts';
import { StartInteractionMode } from '../../shared/components/StartInteractionMode';
import { CPCalculation, CPCalculationMode } from '../components/CPCalculation';
import { SwitchRETCard } from '../components/SwitchRETCard';
import { SituationWithImage } from '../shared/SituationWithImage';
import { createBivarMarginalJointColorMap } from '../shared/data';
import { useMakeLRCardWide } from '../shared/hooks';
import { getRETsOfType } from '../shared/rets';
import { TabbedContent } from '../shared/tabs';
import { getVariableSet } from '../shared/variables';
import { SET_LABEL_CONFIG_PROBABILITY } from '../shared/venn';

// TODO: fix typo
export function CPFromProbsExplaienr(): React.ReactElement {
  const randomExperimentManager = useRandomExperimentManager();
  const learningResourceManager = useLearningResourceManager();

  const { i18n, t } = useTranslation('conditional-probability');

  // === handling the position of the task box
  const [taskPosition, setTaskPosition, taskRef] = useMakeLRCardWide();

  const rets = useMemoWith(getRETsOfType, [randomExperimentManager, 'BivariateBinary']);
  const [ret, setRET, ref] = useScrollOnState(rets[0]);

  const imageURL = useMemo(() => getMetaImage(learningResourceManager.getMeta(ret.id)), [ret, learningResourceManager]);
  const variables = useMemo(() => getVariableSet(ret, false), [ret]);
  const colorMap = useMemoWith(createBivarMarginalJointColorMap, [ret.variables as BivarCategoricalVarDefs]);

  const [jointProbs, setJointProbs] = useResettableState(() => {
    const input = ret.generation.default;
    const frequencies = generateJointFrequencyData(input, ret.variables);
    return jointProbabilityDFFromFrequencyDF(frequencies);
  }, [ret]);

  // calculating the necessary data
  const data = useMemo(() => {
    const description = createDescriptionPercent(ret, jointProbs, 'description-marginal-joint-percent', i18n);

    // TODO: use variables
    const eventNames = getAlternateVariableNamesFromRET(ret, i18n);
    const categories: [RandomVarState, RandomVarState] = [jointProbs.variables[0].categories[0], jointProbs.variables[1].categories[0]];
    const vennData = convertFrequenciesToVennData(jointProbs, categories);
    const vennGraphData = extractFromExpression(vennData, { type: 'distinctIntersection' });
    const vennGraphLabelConfig = { ...SET_LABEL_CONFIG_PROBABILITY, rename: eventNames };

    return {
      // eventFrequencies,
      // frequencies,
      description,
      categories,
      vennGraphData,
      vennGraphLabelConfig,
    };
  }, [jointProbs, ret, i18n]);

  // const cotiHint = learningResourceManager.getResource<HumanReadableTextualData>(
  //   'conditional-probability-from-confusion-of-the-inverse-hint'
  // );

  return (
    <MoveableContent ref={ref} targetIndex={4} position={taskPosition}>
      <SituationWithImage text={data.description} imageURL={imageURL} />
      <LearningResourceCard label={t('math:joint-probability-table')}>
        <div>{t('joint-probability-table-intro')}</div>
        <ProbabilityTable initialData={jointProbs} showMarginals colors={colorMap} />
      </LearningResourceCard>
      <LearningResourceCard label={t('events')}>
        {t('considered-events')}
        <VariablesAndCatsNameMapper variables={variables.long} renamedVariables={variables.short} colors={colorMap} />
      </LearningResourceCard>
      <LearningResourceCard label={t('calculation')}>
        <div>
          <CPCalculation
            data={jointProbs}
            mode={CPCalculationMode.Probability}
            showData
            showEquation
            displayVariables={variables.short}
            colors={colorMap}
          />
        </div>
      </LearningResourceCard>
      <LearningResourceCard ref={taskRef} label={t('task')}>
        {taskPosition === 'original' ? (
          <StartInteractionMode text={t('interaction-mode:maximise-browser')} onStart={() => setTaskPosition('left')} />
        ) : (
          <Stack spacing={2}>
            <div>
              <CustomMarkdown>{t('cp-from-probs-explainer:change-probs')}</CustomMarkdown>
            </div>
            <JointProbsGeneratorInput jointProbs={jointProbs} onChange={setJointProbs} displayVars={variables.middle} colors={colorMap} />
            <SwitchRETCard rets={rets} value={ret} onChange={setRET} />
          </Stack>
        )}
        {/* <JointFrequencyGeneratorInput
          input={input}
          onChange={setInput}
          variables={data.frequencies.variables}
          disabledSliders={[FrequencyGenInputProperty.Population]}
        /> */}
      </LearningResourceCard>
      {/* <Hint hint={cotiHint} /> */}
      <LearningResourceCard label={t('more-visuals')} optional>
        <TabbedContent
          panels={[
            [
              t('math:venn-diagram'),
              <>
                <div>{t('venn-probs-intro')}</div>
                <VennDiagramWithCircles
                  sets={data.vennGraphData.sets}
                  combinations={data.vennGraphData.combinations}
                  width={800}
                  height={400}
                  setLabelConfig={data.vennGraphLabelConfig}
                />
              </>,
            ],
          ]}
        />
      </LearningResourceCard>
    </MoveableContent>
  );
}
