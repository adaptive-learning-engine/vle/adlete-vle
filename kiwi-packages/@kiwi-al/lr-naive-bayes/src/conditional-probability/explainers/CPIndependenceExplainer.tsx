import { Divider, Stack } from '@mui/material';
import React, { useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { getMetaImage } from '@adlete-vle/lr-core/learning-resources';
import { CustomMarkdown } from '@adlete-vle/lr-core-components/CustomMarkdown';
import { useLearningResourceManager } from '@adlete-vle/lr-core-components/contexts';
import { LearningResourceCard } from '@adlete-vle/lr-core-components/learning-resources';
import { MoveableContent } from '@adlete-vle/lr-core-components/misc/MoveableContent';
import { useResettableState } from '@adlete-vle/utils/hooks';

import {
  BivariateJointFrequencyDataFrame,
  createDescriptionFreqs,
  createPopMarginalJointFromFreqs,
  IBivarFreqRET,
} from '@kiwi-al/math-core/frequencies';
import {
  getConditionalProbabilitiesFromJointFrequency,
  getJointProbsFromProbsAndCPs,
  jointFreqDFFromJointProbDF,
} from '@kiwi-al/math-core/probability';
import { RandomExperimentType } from '@kiwi-al/math-core/random-experiment';
import { VariableWithStateFormat } from '@kiwi-al/math-core/variables';
import { CPStackedBarChart, JointProbsFromCPsGeneratorInput, ProbabilityBarChart } from '@kiwi-al/math-core-components/probability';
import { EventNameMapper } from '@kiwi-al/math-core-components/shared/EventNameMapper';

import { useRandomExperimentManager } from '../../contexts';
import { StartInteractionMode } from '../../shared/components/StartInteractionMode';
import { IndependenceCalculation } from '../components/IndependenceCalculation';
import { IndependenceCalculationSimple } from '../components/IndependenceCalculationSimple';
import { SituationWithImage } from '../shared/SituationWithImage';
import { createCPData } from '../shared/data';
import { useMakeLRCardWide } from '../shared/hooks';
import { TabbedContent } from '../shared/tabs';
import { getVariableSet } from '../shared/variables';

export interface ICPIndependenceExplainerProps {
  useEventVariables?: boolean;
}

export interface IIndependenceInfo {
  min: number;
  max: number;
  independent: number;
}

export function getIndependencePopulationInfo(frequencies: BivariateJointFrequencyDataFrame): IIndependenceInfo {
  const { jointFrequency, marginalFrequencies, population } = createPopMarginalJointFromFreqs(frequencies);

  const independentPopulation = (marginalFrequencies[0] * marginalFrequencies[1]) / jointFrequency;

  return {
    min: marginalFrequencies[0] + marginalFrequencies[1] - jointFrequency,
    max: Math.max(population, Math.round(independentPopulation * 2)),
    independent: independentPopulation,
  };
}

export function CPIndependenceExplainer(props: ICPIndependenceExplainerProps): React.ReactElement {
  const { useEventVariables = true } = props;

  const randomExperimentManager = useRandomExperimentManager();
  const learningResourceManager = useLearningResourceManager();

  const { i18n, t } = useTranslation('conditional-probability');

  // === handling the position of the task box
  const [taskPosition, setTaskPosition, taskRef] = useMakeLRCardWide();

  const [ret] = useState<IBivarFreqRET>(randomExperimentManager.getTemplateByType<IBivarFreqRET>(RandomExperimentType.BivariateBinary)[0]);
  const imageURL = useMemo(() => getMetaImage(learningResourceManager.getMeta(ret.id)), [ret, learningResourceManager]);

  const variables = useMemo(() => getVariableSet(ret, false), [ret]);

  // creating default data used as initial state
  const defaultData = useMemo(() => {
    const cpData = createCPData(ret.generation.default, variables.long);
    const marginals = variables.long.map((variable) => cpData.probabilities.getMarginalFrequencies(variable));
    return { ...cpData, marginals };
  }, [variables, ret]);

  const [input, setInput] = useResettableState(
    () => ({
      condProbs: defaultData.condProbs[0],
      givenMarginalProbs: defaultData.marginals[1],
    }),
    [ret]
  );

  // calculating the necessary data
  const data = useMemo(() => {
    const jointProbs = getJointProbsFromProbsAndCPs(input);
    const jointFreqs = jointFreqDFFromJointProbDF(jointProbs, defaultData.frequencies.getPopulation());
    const otherCondProbs = getConditionalProbabilitiesFromJointFrequency(jointFreqs, false);
    const description = createDescriptionFreqs(ret, jointFreqs, 'description-population-marginal-joint', i18n);

    // const vennData = convertFrequenciesToVennData(frequencies, categories);
    // const vennGraphData = extractFromExpression(vennData, { type: 'distinctIntersection' });

    const marginalProbs = [
      jointProbs.getMarginalFrequencies(jointProbs.variables[0]),
      jointProbs.getMarginalFrequencies(jointProbs.variables[1]),
    ];

    return {
      description,
      jointFreqs,
      marginalProbs,
      otherCondProbs,
      // eventCondProbs,
    };
  }, [input, ret, i18n, defaultData]);

  const varFormat: VariableWithStateFormat = useEventVariables ? 'StateOnly' : 'Notation';

  return (
    <MoveableContent targetIndex={5} position={taskPosition}>
      <SituationWithImage text={data.description} imageURL={imageURL} />
      {useEventVariables && (
        <LearningResourceCard label={t('events')}>
          {t('considered-events')}
          <EventNameMapper variables={variables.long} eventVariables={variables.short} />
        </LearningResourceCard>
      )}
      <LearningResourceCard label={t('cp-independence-explainer:stacked-barcharts')}>
        <Stack direction="row" justifyContent="space-around" divider={<Divider orientation="vertical" flexItem />}>
          <CPStackedBarChart data={input.condProbs} viewport={{ width: 300, height: 200 }} displayVariables={variables.middle} />
          <CPStackedBarChart data={data.otherCondProbs} viewport={{ width: 300, height: 200 }} displayVariables={variables.middle} />
        </Stack>
      </LearningResourceCard>
      <LearningResourceCard label={t('cp-independence-explainer:barcharts')}>
        <Stack direction="row" justifyContent="space-around" divider={<Divider orientation="vertical" flexItem />}>
          <ProbabilityBarChart probabilities={data.marginalProbs[0]} variable={variables.short[0]} viewport={{ width: 300, height: 200 }} />
          <ProbabilityBarChart probabilities={data.marginalProbs[1]} variable={variables.short[1]} viewport={{ width: 300, height: 200 }} />
        </Stack>
      </LearningResourceCard>
      <LearningResourceCard label={t('calculation')}>
        <TabbedContent
          panels={[
            [
              t('cp-independence-explainer:formula1'),
              <Stack direction="row" justifyContent="space-around" divider={<Divider orientation="vertical" flexItem />}>
                <IndependenceCalculationSimple
                  frequencies={data.jointFreqs}
                  condProbs={input.condProbs}
                  varFormat={varFormat}
                  displayVariables={variables.short}
                />
                <IndependenceCalculationSimple
                  frequencies={data.jointFreqs}
                  condProbs={data.otherCondProbs}
                  varFormat={varFormat}
                  displayVariables={variables.short}
                />
              </Stack>,
            ],
            [
              t('cp-independence-explainer:formula2'),
              <div>
                <IndependenceCalculation frequencies={data.jointFreqs} varFormat={varFormat} displayVariables={variables.short} />
              </div>,
            ],
          ]}
        />
      </LearningResourceCard>
      <LearningResourceCard ref={taskRef} label={t('task')}>
        {taskPosition === 'original' ? (
          <StartInteractionMode text={t('interaction-mode:maximise-browser-alternative')} onStart={() => setTaskPosition('left')} />
        ) : (
          <Stack spacing={2}>
            <div>
              <CustomMarkdown>{t('cp-independence-explainer:change-prob')}</CustomMarkdown>
            </div>
            <JointProbsFromCPsGeneratorInput
              input={input}
              onChange={setInput}
              hiddenSliders={['marginal']}
              displayVariables={variables.middle}
            />
          </Stack>
        )}
        {/* <MinMaxSliderWithDescription
          min={independenceInfo.min}
          max={independenceInfo.max}
          additionalSliderProps={{ marks: [{ value: independenceInfo.independent }] }}
          value={population}
          onChange={setPopulation}
          labelDescription="Population"
        /> */}
      </LearningResourceCard>
    </MoveableContent>
  );
}
