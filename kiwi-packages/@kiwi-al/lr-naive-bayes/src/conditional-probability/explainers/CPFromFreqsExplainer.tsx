import { FormControlLabel, FormGroup, Stack, Switch } from '@mui/material';
import React, { useCallback, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { getMetaImage, HumanReadableTextualData } from '@adlete-vle/lr-core/learning-resources';
import { CustomMarkdown } from '@adlete-vle/lr-core-components/CustomMarkdown';
import { useLearningResourceManager } from '@adlete-vle/lr-core-components/contexts';
import { Hint, LearningResourceCard } from '@adlete-vle/lr-core-components/learning-resources';
import { MoveableContent } from '@adlete-vle/lr-core-components/misc/MoveableContent';
import { useMemoWith, useResettableState, useScrollOnState } from '@adlete-vle/utils/hooks';

import { createDescriptionFreqs, FrequencyGenInputProperty, generateJointFrequencyData } from '@kiwi-al/math-core/frequencies';
import { BivarCategoricalVarDefs } from '@kiwi-al/math-core/variables';
import { ContingencyBarTable, JointFrequencyGeneratorInput } from '@kiwi-al/math-core-components/frequency';
import { EventNameMapper } from '@kiwi-al/math-core-components/shared/EventNameMapper';

import { useRandomExperimentManager } from '../../contexts';
import { StartInteractionMode } from '../../shared/components/StartInteractionMode';
import { CPCalculation, CPCalculationMode } from '../components/CPCalculation';
import { SwitchRETCard } from '../components/SwitchRETCard';
import { SituationWithImage } from '../shared/SituationWithImage';
import { createBivarMarginalJointColorMap } from '../shared/data';
import { useMakeLRCardWide } from '../shared/hooks';
import { getRETsOfType } from '../shared/rets';
import { getVariableSet } from '../shared/variables';

const HIDDEN_SLIDERS = {
  withAandB: [FrequencyGenInputProperty.Population],
  withoutAandB: [
    FrequencyGenInputProperty.Population,
    FrequencyGenInputProperty.MarginalFrequency1,
    FrequencyGenInputProperty.MarginalFrequency2,
  ],
};

export function CPFromFreqsExplainer(): React.ReactElement {
  const randomExperimentManager = useRandomExperimentManager();
  const learningResourceManager = useLearningResourceManager();

  const { i18n, t } = useTranslation('conditional-probability');

  // === handling the position of the task box
  const [taskPosition, setTaskPosition, taskRef] = useMakeLRCardWide();

  const rets = useMemoWith(getRETsOfType, [randomExperimentManager, 'BivariateBinary']);
  const [ret, setRET, ref] = useScrollOnState(rets[0]);

  const imageURL = useMemo(() => getMetaImage(learningResourceManager.getMeta(ret.id)), [ret, learningResourceManager]);
  const variables = useMemo(() => getVariableSet(ret, false), [ret]);
  const colorMap = useMemoWith(createBivarMarginalJointColorMap, [ret.variables as BivarCategoricalVarDefs]);

  const [input, setInput] = useResettableState(ret.generation.default, [ret]);

  // === calculating the necessary data
  const data = useMemo(() => {
    const frequencies = generateJointFrequencyData(input, ret.variables);
    const description = createDescriptionFreqs(ret, frequencies, 'description-population-marginal-joint', i18n);

    return {
      frequencies,
      description,
    };
  }, [input, ret, i18n]);

  const cotiHint = learningResourceManager.getResource<HumanReadableTextualData>(
    'conditional-probability/confusion-of-the-inverse-hint.json'
  );

  // === P(A) / P(B) Switch
  const [showAandBSliders, setShowAandBSliders] = useState(false);
  const onShowAandBSlidersChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setShowAandBSliders(event.target.checked);
  }, []);

  return (
    <MoveableContent ref={ref} targetIndex={4} position={taskPosition}>
      <SituationWithImage text={data.description} imageURL={imageURL} />
      <LearningResourceCard label={t('math:contingency-table')}>
        <div>{t('contingency-table-freqs-intro')}</div>
        <ContingencyBarTable initialData={data.frequencies} showMarginals colors={colorMap} />
      </LearningResourceCard>
      <LearningResourceCard label={t('events')}>
        {t('considered-events')}
        <EventNameMapper variables={variables.long} eventVariables={variables.short} colors={colorMap} />
      </LearningResourceCard>
      <LearningResourceCard label={t('calculation')} stackProps={{ direction: 'row', spacing: 20 }}>
        <div>
          <CPCalculation
            data={data.frequencies}
            mode={CPCalculationMode.Frequency}
            showData
            showEquation
            displayVariables={variables.short}
            colors={colorMap}
          />
        </div>
      </LearningResourceCard>
      <LearningResourceCard ref={taskRef} label={t('task')}>
        {taskPosition === 'original' ? (
          <StartInteractionMode text={t('interaction-mode:maximise-browser')} onStart={() => setTaskPosition('left')} />
        ) : (
          <Stack spacing={2}>
            <div>
              <CustomMarkdown>{t('cp-from-freqs-explainer:change-freqs')}</CustomMarkdown>
            </div>
            <JointFrequencyGeneratorInput
              input={input}
              onChange={setInput}
              variables={data.frequencies.variables}
              hiddenSliders={showAandBSliders ? HIDDEN_SLIDERS.withAandB : HIDDEN_SLIDERS.withoutAandB}
              colors={colorMap}
            />
            <FormGroup sx={{ alignContent: 'flex-end' }}>
              <FormControlLabel
                control={<Switch checked={showAandBSliders} onChange={onShowAandBSlidersChange} />}
                label={t('interaction-mode:intermediate') as string}
              />
            </FormGroup>
            <SwitchRETCard rets={rets} value={ret} onChange={setRET} />
          </Stack>
        )}
      </LearningResourceCard>
      <Hint hint={cotiHint} severity="warning" />
    </MoveableContent>
  );
}
