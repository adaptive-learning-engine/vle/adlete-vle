import { Stack } from '@mui/material';
import { extractFromExpression } from '@upsetjs/react';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import { getMetaImage } from '@adlete-vle/lr-core/learning-resources';
import { CustomMarkdown } from '@adlete-vle/lr-core-components/CustomMarkdown';
import { useLearningResourceManager } from '@adlete-vle/lr-core-components/contexts';
import { LearningResourceCard } from '@adlete-vle/lr-core-components/learning-resources';
import { MoveableContent } from '@adlete-vle/lr-core-components/misc/MoveableContent';
import { useMemoWith, useResettableState, useScrollOnState } from '@adlete-vle/utils/hooks';

import { convertToEventVariables } from '@kiwi-al/math-core/events';
import { createDescriptionFreqs, FrequencyGenInputProperty, generateJointFrequencyData } from '@kiwi-al/math-core/frequencies';
import { getAlternateVariableNamesFromRET } from '@kiwi-al/math-core/random-experiment';
import { ICategoricalVarDef, RandomVarState } from '@kiwi-al/math-core/variables';
import { JointFrequencyGeneratorInput } from '@kiwi-al/math-core-components/frequency';
import { convertFrequenciesToVennData, VennDiagramWithCircles } from '@kiwi-al/math-core-components/venn';

import { useRandomExperimentManager } from '../../contexts';
import { StartInteractionMode } from '../../shared/components/StartInteractionMode';
import { CPCalculation, CPCalculationMode } from '../components/CPCalculation';
import { SwitchRETCard } from '../components/SwitchRETCard';
import { SituationWithImage } from '../shared/SituationWithImage';
import { useMakeLRCardWide } from '../shared/hooks';
import { getRETsOfType } from '../shared/rets';
import { SET_LABEL_CONFIG_FREQUENCY } from '../shared/venn';

export interface ICPVennFreqsExplainerProps {
  useEventVariables?: boolean;
}

export function CPVennFreqsExplainer(props: ICPVennFreqsExplainerProps): React.ReactElement {
  const { useEventVariables = true } = props;

  const randomExperimentManager = useRandomExperimentManager();
  const learningResourceManager = useLearningResourceManager();

  const { i18n, t } = useTranslation('conditional-probability');

  // === handling the position of the task box
  const [taskPosition, setTaskPosition, taskRef] = useMakeLRCardWide();

  const rets = useMemoWith(getRETsOfType, [randomExperimentManager, 'BivariateBinary']);
  const [ret, setRET, ref] = useScrollOnState(rets[0]);
  const imageURL = useMemo(() => getMetaImage(learningResourceManager.getMeta(ret.id)), [ret, learningResourceManager]);

  const example = ret.generation.default;

  const [input, setInput] = useResettableState(example, [ret]);

  // calculating the necessary data
  const data = useMemo(() => {
    const frequencies = generateJointFrequencyData(input, ret.variables);
    const description = createDescriptionFreqs(ret, frequencies, 'description-population-marginal-joint', i18n);

    const categories: [RandomVarState, RandomVarState] = [frequencies.variables[0].categories[0], frequencies.variables[1].categories[0]];

    const vennData = convertFrequenciesToVennData(frequencies, categories);
    const vennGraphData = extractFromExpression(vennData, { type: 'distinctIntersection' });

    const eventNames = getAlternateVariableNamesFromRET(ret, i18n);
    const eventVars = (useEventVariables ? convertToEventVariables(ret.variables, eventNames) : ret.variables) as [
      ICategoricalVarDef,
      ICategoricalVarDef,
    ];
    const eventFrequencies = frequencies.copy({ variables: eventVars });

    const vennGraphLabelConfig = { ...SET_LABEL_CONFIG_FREQUENCY, rename: eventNames };

    return {
      frequencies,
      eventFrequencies,
      description,
      vennGraphData,
      vennGraphLabelConfig,
    };
  }, [input, ret, i18n, useEventVariables]);

  // const vennHint = learningResourceManager.getResource<HumanReadableTextualData>('conditional-probability/venn-area.json');

  return (
    <MoveableContent ref={ref} targetIndex={3} position={taskPosition}>
      <SituationWithImage text={data.description} imageURL={imageURL} />
      <LearningResourceCard label={t('math:venn-diagram')}>
        <div>{t('conditional-probability:venn-freqs-intro')}</div>
        {/* <VennDiagramWithRects sets={data.vennGraphData.sets} combinations={data.vennGraphData.combinations} maxCardinality={1000} /> */}
        <VennDiagramWithCircles
          sets={data.vennGraphData.sets}
          combinations={data.vennGraphData.combinations}
          width={600}
          height={300}
          setLabelConfig={data.vennGraphLabelConfig}
        />
      </LearningResourceCard>
      <LearningResourceCard label={t('calculation')}>
        <div>
          <CPCalculation data={data.eventFrequencies} mode={CPCalculationMode.Frequency} showData />
        </div>
      </LearningResourceCard>
      {/* <Hint hint={vennHint} /> */}
      <LearningResourceCard ref={taskRef} label={t('task')}>
        {taskPosition === 'original' ? (
          <StartInteractionMode text={t('interaction-mode:maximise-browser-alternative')} onStart={() => setTaskPosition('left')} />
        ) : (
          <Stack spacing={2}>
            <div>
              <CustomMarkdown>{t('cp-venn-freqs-explainer:task-cond-probs-from-areas')}</CustomMarkdown>
            </div>
            <JointFrequencyGeneratorInput
              input={input}
              onChange={setInput}
              variables={data.frequencies.variables}
              disabledSliders={[FrequencyGenInputProperty.Population]}
            />
            <SwitchRETCard rets={rets} value={ret} onChange={setRET} />
          </Stack>
        )}
      </LearningResourceCard>
    </MoveableContent>
  );
}
