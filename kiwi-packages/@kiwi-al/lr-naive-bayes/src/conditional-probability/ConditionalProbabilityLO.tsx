// import { DefinitionWithExamples } from '@adlete-vle/lr-core-components/DefinitionWithExamples';

import React, { useCallback } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';

import { useLRComponentCreator } from '@adlete-vle/lr-core-components/contexts';
import { ILearningSequence, NestedLRRenderer } from '@adlete-vle/lr-core-components/learning-resources';

const TEST_TREE_META: ILearningSequence = {
  id: 'conditional-probability',
  childEntries: [
    // {
    //   id: 'section-dev',
    //   childEntries: [
    //     {
    //       resources: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/dev/dev-1.json',
    //       rendering: 'LRComponent',
    //     },
    //   ],
    // },
    {
      id: 'welcome',
      name: {
        en: 'Welcome',
        de: 'Willkommen',
      },
      childEntries: [
        { resources: 'conditional-probability/welcome.json', rendering: 'AnyText' },
        { resources: 'http://localhost/data/content/kiwi-vle/learning-environment.json', rendering: 'AnyText' },
        { resources: 'conditional-probability/course-contents.json', rendering: 'AnyText' },
      ],
    },
    {
      id: 'introduction',
      name: {
        en: 'Introduction',
        de: 'Einführung',
      },
      childEntries: [
        { resources: 'conditional-probability/introduction.json', rendering: 'AnyTextWithImage' },
        {
          resources: ['conditional-probability&example=1', 'conditional-probability&example=2', 'conditional-probability&example=3'],
          rendering: 'Examples',
        },
        {
          resources: [
            'conditional-probability/random-experiments/online-dating.json&example-problem=0',
            'conditional-probability/random-experiments/laptops-with-virus.json&example-problem=0',
            'conditional-probability/random-experiments/personalized-advertisement.json&example-problem=0',
            'conditional-probability/random-experiments/students-in-classes.json&example-problem=0',
          ],
          rendering: 'ExampleProblems',
        },
        { resources: 'conditional-probability&definition=1', rendering: 'Definition' },
      ],
    },
    {
      id: 'notation',
      name: {
        en: 'Notation',
        de: 'Schreibweise',
      },
      childEntries: [
        { resources: 'conditional-probability/notation-full.json', rendering: 'AnyText' },
        { resources: 'conditional-probability/notation-given.json', rendering: 'AnyText' },
        // { resources: 'conditional-probability/notation-both-events-occured.json', rendering: 'AnyText' },
      ],
    },
    {
      id: 'vs-sample-spaces',
      name: {
        en: 'Conditional Probability and Sample Spaces',
        de: 'Bedingte Wahrscheinlichkeit und Ergebnismengen',
      },
      childEntries: [
        { resources: 'conditional-probability/focus-hint.json', rendering: 'Hint' },
        { resources: 'conditional-probability/explainers/venn-sample-space.json', rendering: 'LRComponent' },
      ],
    },
    {
      id: 'using-classical-probability',
      name: {
        en: 'Conditional Probability Using Classical Probability',
        de: 'Bedingte Wahrscheinlichkeit unter Nutzung klassischer Wahrscheinlichkeit',
      },
      childEntries: [
        { resources: 'conditional-probability/explainers/dice-1.json', rendering: 'LRComponent' },
        // { resources: 'conditional-probability/calc-using-number-of-outcomes.json', rendering: 'Hint' },
      ],
    },
    {
      id: 'using-classical-probability-exercise',
      name: {
        en: 'Exercise: Conditional Probability Using Classical Probability',
        de: 'Übung: Bedingte Wahrscheinlichkeit unter Nutzung klassischer Wahrscheinlichkeit',
      },
      childEntries: [{ resources: 'conditional-probability/exercises/cards-1.json', rendering: 'LRComponent' }],
    },
    {
      id: 'video-introduction',
      name: {
        en: 'Introductory Conditional Probability Video',
        de: 'Einführungsvideo zu bedingter Wahrscheinlichkeit',
      },
      childEntries: [{ resources: 'conditional-probability/studyflix-bedingte-wahrscheinlichkeit.json', rendering: 'EmbeddedVideo' }],
    },
    {
      id: 'using-frequencies',
      name: {
        en: 'Conditional Probability using Frequencies',
        de: 'Bedingte Wahrscheinlichkeit unter Nutzung von Häufigkeiten',
      },
      childEntries: [
        { resources: 'conditional-probability/from-frequencies.json', rendering: 'AnyText' },
        { resources: 'conditional-probability/from-frequencies-equation.json', rendering: 'Equation' },
        { resources: 'conditional-probability/explainers/from-freqs.json', rendering: 'LRComponent' },
      ],
    },
    {
      id: 'using-frequencies-exercise',
      name: {
        en: 'Exercise: Conditional Probability using Frequencies',
        de: 'Übung: Bedingte Wahrscheinlichkeit unter Nutzung von Häufigkeiten',
      },
      childEntries: [{ resources: 'conditional-probability/exercises/calculate-cps-from-freqs-1.json', rendering: 'LRComponent' }],
    },

    {
      id: 'using-probabilities',
      name: {
        en: 'Conditional Probability using Probabilities',
        de: 'Bedingte Wahrscheinlichkeit unter Nutzung von Wahrscheinlichkeiten',
      },
      childEntries: [
        { resources: 'conditional-probability/equation-in-words.json', rendering: 'AnyText' },
        { resources: 'conditional-probability/equation.md', rendering: 'Equation' },
        { resources: 'conditional-probability/explainers/from-probs.json', rendering: 'LRComponent' },
      ],
    },
    {
      id: 'using-probabilities-2',
      name: {
        en: 'Conditional Probability using Probabilities',
        de: 'Bedingte Wahrscheinlichkeit unter Nutzung von Wahrscheinlichkeiten',
      },
      childEntries: [{ resources: 'https://setosa.io/conditional/', rendering: 'IFrame' }],
    },
    {
      id: 'using-probabilties-exercise',
      name: {
        en: 'Exercise: Conditional Probability using Probabilities',
        de: 'Übung: Bedingte Wahrscheinlichkeit unter Nutzung von Wahrscheinlichkeiten',
      },
      childEntries: [{ resources: 'conditional-probability/exercises/calculate-cps-from-probs-1.json', rendering: 'LRComponent' }],
    },
    {
      id: 'cp-visualizations',
      name: {
        en: 'Visualizations for Conditional Probability',
        de: 'Visualisierungformen für bedingte Wahrscheinlichkeiten',
      },
      childEntries: [
        { resources: 'conditional-probability/explainers/cp-stacked-bar-chart.json', rendering: 'LRComponent' },
        // { resources: 'conditional-probability/explainers/cpt.json', rendering: 'LRComponent' },
        // { resources: 'conditional-probability/explainers/cptree.json', rendering: 'LRComponent' },
      ],
    },
    // {
    //   id: 'cpt-exercise',
    //   childEntries: [{ resources: 'conditional-probability/exercises/cpt-1.json', rendering: 'LRComponent' }],
    // },
    {
      id: 'multiplication-rule',
      name: {
        en: 'Multiplication Rule for Two Events',
        de: 'Multiplikationsregel für zwei beliebige Ereignisse',
      },
      childEntries: [
        { resources: 'events/multiplication-rule-from-conditional-probability-equation.json', rendering: 'Hint' },
        { resources: 'conditional-probability/equation.md', rendering: 'Equation' },
        { resources: 'events/multiplication-rule-equation.md', rendering: 'Equation' },
        { resources: 'events/multiplication-rule-from-conditional-probability-both-events.json', rendering: 'Hint' },
      ],
    },
    {
      id: 'multiplication-rule-exercise',
      name: {
        en: 'Exercise: Multiplication Rule for 2 Events',
        de: 'Übung: Multiplikationsregel für 2 beliebige Ereignisse',
      },
      childEntries: [{ resources: 'conditional-probability/exercises/calculate-freqs-from-cps-1.json', rendering: 'LRComponent' }],
    },
    // {
    //   id: 'total-probability',
    //   name: {
    //     en: 'Total Probability',
    //     de: 'Satz der totalen Wahrscheinlichkeit',
    //   },
    //   childEntries: [
    //     // TODO: Don't use AnyText
    //     { resources: 'conditional-probability/total-probability/introduction.json', rendering: 'AnyText' },
    //     { resources: 'conditional-probability/total-probability/conditions.json', rendering: 'AnyText' },
    //     { resources: 'events/disjoint-events.json', rendering: 'AnyText' },
    //     { resources: 'events/collectively-exhaustive-events.json', rendering: 'AnyText' },
    //     { resources: 'Disjunkte Zerlegung', rendering: 'ToDo' },
    //     { resources: 'conditional-probability/total-probability/disjointing-multiple.json', rendering: 'AnyText' },
    //     { resources: 'conditional-probability/total-probability/equation-from-multiplication-rule.json', rendering: 'AnyText' },
    //     { resources: 'conditional-probability/total-probability/equation-multiple.json', rendering: 'AnyText' },
    //     { resources: 'conditional-probability/total-probability/disjointing-complement.json', rendering: 'AnyText' },
    //     { resources: 'conditional-probability/total-probability/equation-complement.json', rendering: 'AnyText' },
    //   ],
    // },
    // {
    //   id: 'total-probability-2',
    //   name: {
    //     en: 'Total Probability',
    //     de: 'Satz der totalen Wahrscheinlichkeit',
    //   },
    //   childEntries: [{ resources: 'conditional-probability/explainers/total-probability.json', rendering: 'LRComponent' }],
    // },
    // {
    //   id: 'total-probability-exercise',
    //   name: {
    //     en: 'Exercise: Total Probability',
    //     de: 'Übung: Satz der totalen Wahrscheinlichkeit',
    //   },
    //   childEntries: [{ resources: 'conditional-probability/exercises/calculate-total-probabilities-1.json', rendering: 'LRComponent' }],
    // },
    {
      id: 'independence-1',
      name: {
        en: 'Dependent and Independent Events',
        de: 'Abhängige und unabhängige Ereignisse',
      },
      childEntries: [
        { resources: 'conditional-probability/independence-introduction.json', rendering: 'AnyText' },
        { resources: ['stochastic-independence&example=1', 'stochastic-independence&example=2'], rendering: 'Examples' },
        { resources: 'conditional-probability/independence-two-equations.json', rendering: 'AnyText' },
        { resources: 'conditional-probability/independence-equation-simple-long.json', rendering: 'Equation' },
        { resources: 'conditional-probability/independence-equation-simple-in-words.json', rendering: 'AnyText' },
        { resources: 'conditional-probability/independence-equation-long.json', rendering: 'Equation' },
        { resources: 'conditional-probability/independence-equation-long-from-simple.json', rendering: 'AnyText' },
      ],
    },
    {
      id: 'independence-2',
      name: {
        en: 'Dependent and Independent Events',
        de: 'Abhängige und unabhängige Ereignisse',
      },
      childEntries: [
        { resources: 'conditional-probability/explainers/independence.json', rendering: 'LRComponent' },
        { resources: 'conditional-probability/all-probabilities-are-conditional-hint.json', rendering: 'Hint' },
        { resources: 'conditional-probability/conditional-probabilities-bidirectional.json', rendering: 'Hint' },
      ],
    },
    {
      id: 'independence-exercise',
      name: {
        en: 'Exercise: Dependent and Independent Events',
        de: 'Übung: Abhängige und unabhängige Ereignisse',
      },
      childEntries: [{ resources: 'conditional-probability/exercises/calculate-independence-1.json', rendering: 'LRComponent' }],
    },
    {
      id: 'with-venn-diagrams',
      name: {
        en: 'Conditional Probability in Venn Diagrams',
        de: 'Bedingte Wahrscheinlichkeit in Venndiagrammen',
      },
      childEntries: [
        { resources: 'conditional-probability/venn-area.json', rendering: 'AnyText' },
        { resources: 'conditional-probability/venn-area-equation.json', rendering: 'Equation' },
        { resources: 'conditional-probability/explainers/venn-freqs.json', rendering: 'LRComponent' },
      ],
    },
    {
      id: 'good-bye',
      name: {
        en: 'Goodbye!',
        de: 'Auf Wiedersehen!',
      },
      childEntries: [{ resources: 'http://localhost/data/content/kiwi-vle/final-page.json', rendering: 'AnyText' }],
    },
    // {
    //   id: 'conditional-probability-trees',
    //   name: {
    //     en: 'Conditional Probability Trees',
    //     de: 'Bedingte Wahrscheinlichkeitsbäume',
    //   },
    //   childEntries: [{ resources: 'conditional-probability/explainers/cptree.json', rendering: 'LRComponent' }],
    // },
  ],
};

// const sharedProps = {};

export function ConditionalProbabilityLO(): React.ReactElement {
  const componentCreator = useLRComponentCreator();
  const location = useLocation();
  const navigate = useNavigate();
  const section = location.pathname === '/' ? 0 : Number.parseInt(location.pathname.slice(1), 10) - 1;

  const onSectionChanged = useCallback(
    (nextSection: number) => {
      navigate(`${nextSection + 1}`);
    },
    [navigate]
  );

  return (
    <NestedLRRenderer
      componentCreator={componentCreator}
      learningSequence={TEST_TREE_META}
      // sharedProps={sharedProps}
      section={section}
      onSectionChanged={onSectionChanged}
      // devVersion="V14"
    />
  );
}
