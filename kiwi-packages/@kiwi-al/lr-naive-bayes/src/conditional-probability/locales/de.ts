const messages = {
  'conditional-probability': {
    // shared
    calculation: 'Berechnung',
    'choose-scenario': 'Szenario auswählen',
    condition: 'Bedingung',
    'conditional-probability': 'Bedingte Wahrscheinlichkeit',
    'considered-events': 'Folgende Ereignisse werden betrachtet:',
    events: 'Ereignisse',
    focus: 'fokussieren',
    'more-visuals': 'Weitere Visualisierungen',
    'number-of-elements-in': 'Anzahl der Elemente in ',
    'results-events': 'Ergebnismenge und Ereignisse',
    situation: 'Situation',
    task: 'Aufgabe',
    'use-scenario-title': 'Optionale Aufgabe: Nutze ein anderes Szenario',
    'use-scenario': 'Nutze einen anderes Zufallsexperiment um ein anderes Szenario zu erkunden!',

    // these two arefound mostly in exercises, but also in a explainer
    'joint-probability-table-intro':
      'Diese Tabelle zeigt die gemeinsamen Wahrscheinlichkeiten verschiedener Schnitte der betrachteten Ereignisse und Komplementärereignisse.',
    'contingency-table-freqs-intro':
      'Diese Kreuztabelle zeigt die Häufigkeiten verschiedener Schnittmengen der betrachteten Ereignisse und Komplementärereignisse.',
    'venn-freqs-intro': 'Dieses Venndiagramm visualisiert die Frequenzen.',
    'venn-probs-intro': 'Dieses Venndiagramm visualisiert die Wahrscheinlichkeiten.',
  },
  exercise: {
    // exercises
    'calculate-prob': 'Berechne die bedingten Wahrscheinlichkeiten! ',
    'solution-attempt': 'Lösungsversuch',
    'round-2': 'Runde auf die zweite Nachkommastelle!',
    'solution-drag-drop': 'Wähle eine Lösung! (drag & drop)',
    'solution-drag-drop-round': 'Wähle eine Lösung (drag & drop) und achte bei der Berechnung darauf korrekt zu runden!',
    validate: 'Überprüfen',
    'show-solution': 'Zeige Lösungen',
    'help-tools': 'Hilfsmittel',
    'solution-path': 'Lösungsweg',
  },
  'repeatable-exercise': {
    // RepeatableExercise
    'next-exercise': 'Nächste Aufgabe',
    skip: 'Überspringen',
    'additional-exercise': 'Zusätzliche Aufgabe',
    next: 'Weiter',
  },
  'independence-exercise': {
    // IndependenceExercise1
    prob: 'Wahrscheinlichkeiten',
    'known-prob': 'Folgende bedingten Wahrscheinlichkeiten und Randwahrscheinlichkeiten sind bekannt: ',
    'generate-distribution': 'Erzeuge eine ungefähr unabhängige Verteilung!',
    correct: 'Diese Lösung ist richtig!',
    wrong: 'Diese Lösung ist falsch!',
  },
  'interaction-mode': {
    // interaction mode
    'maximise-browser':
      'Maximiere das Browser-Fenster und starte den Interaktionsmodus um eigene Ereignisse erzeugen zu können! Du findest die Interaktionselemente dann links.',
    'maximise-browser-short': 'Maximiere das Browser-Fenster und starte den Interaktionsmodus!',
    'maximise-browser-alternative':
      'Maximiere das Browser-Fenster und starte den Interaktionsmodus um die bedingten Wahrscheinlichkeiten selbst verändern zu können!',
    'dice-configure-events':
      'Konfiguriere die Ereignisse A und/oder B und beobachte oben in der Visualisierung und Berechnung, wie sich die bedingten Wahrscheinlichkeiten verändern.\n\n\n- Erstelle bspw. die Ereignisse so, dass eine bedingte Wahrscheinlichkeit von 0,00 / 1,00 oder "nicht definiert" entsteht.',
    'change-prob': 'Ändere die bedingten Wahrscheinlichkeiten und beobachte wie sich das Diagramm verändert.',
    'random-events': 'Zufällige Ereignisse erzeugen!',
    intermediate: 'Zusätzlich A und B verändern (für Fortgeschritte)',
    start: 'Interaktionsmodus starten',
  },
  'cp-dice-explainer': {
    // CPDiceExplainer
    combinations: 'Hier werden alle möglichen Kombinationen beider Würfel sowie zwei Ereignisse A und B visualisiert.',
    focus: 'Bedingung {condResult} fokussieren',
    introduction: 'Du würfelst mit zwei unterschiedlich gefärbten Würfeln.',
  },
  'cp-independence-explainer': {
    // CPIndependenceExplainer
    barcharts: 'Balkendiagramme (unbedingte Wahrscheinlichkeiten)',
    'change-prob':
      'Verändere die bedingten Wahrscheinlichkeiten. Beobachte, wie sich die Werte in den zwei Formeln zur Bestimmung der Unabhängigkeit zweier Ereignisse verändern.\n- Wann sind beide Seiten der Formel gleich?\n- Wie verhalten sich die bedingten Wahrscheinlichkeiten zu den Randwahrscheinlichkeiten bei stochastischer Unabhängigkeit?',
    formula1: 'Unabhängkeit mit 1. Formel',
    formula2: 'Unabhängkeit mit 2. Formel',
    'stacked-barcharts': 'Gestapelte Balkendiagramme (Bedingte Wahrscheinlichkeiten)',
  },
  'cp-venn-freqs-explainer': {
    'task-cond-probs-from-areas':
      'Ändere die Häufigkeiten und beobachte wie die Veränderung der Flächen im Venndiagramm sich zur Veränderung der bedingten Wahrscheinlichkeit verhält!\n\n- Wie sieht das Venndiagramm aus, wenn eine der bedingten Wahrscheinlichkeiten 0 oder 1 ist?\n- Was bewirkt die Veränderung der Schnittmenge?\n- Was bewirkt die Veränderung der anderen Häufigkeiten?',
  },
  'cp-venn-sample-explainer': {
    // CPVennSampleSpaceExplainer
    'calculated-values':
      'Bei der Berechnung der (bedingten) Wahrscheinlichkeiten werden nur die violett umrahmten Ergebnismengen verwendet. Ergebnisse die außerhalb liegen, werden außer Acht gelassen.',
    'choose-alpha':
      'Wähle ein entsprechendes Ω aus um die zur Berechnung der jeweiligen Wahrscheinlichkeit genutzte Ergebnismenge in den Fokus zu setzen:',
    'results-visual': 'Visualisierung der Ergebnismengen',
    'used-calc': 'genutzt zur Berechnung von P(A) und P(B)',
  },
  'cp-from-freqs-explainer': {
    // CPFromFreqsExplainer
    'change-freqs':
      'Verändere die Häufigkeiten und beobachte, wie sich die bedingte Wahrscheinlichkeit entsprechend der Formel ändert.\n\n- Versuche bspw. die berechneten bedingten Wahrscheinlichkeiten auf 0,00 oder 1,00 zu verändern.',
    intro: 'Dieses Venndiagramm visualisiert die Häufigkeiten.',
    'task-freq': 'Aufgabe: bedingte Wahrscheinlichkeiten von Häufigkeiten',
    'task-area': 'Aufgabe: Bedingte Wahrscheinlichkeiten und Flächen',
  },
  'cp-from-probs-explainer': {
    // CPFromProbsExplainer

    task: 'Aufgabe: Bedingte Wahrscheinlichkeiten von Wahrscheinlichkeiten',
    'change-probs':
      'Ändere die Wahrscheinlichkeiten und beobachte wie die bedingten Wahrscheinlichkeiten sich entsprechend der Formel verändern.\n\n- Versuche bspw. die berechneten bedingten Wahrscheinlichkeiten auf 0,00 oder 1,00 zu verändern.',
  },
  'cpt-explainer': {
    // CPTExplainer
    title: 'Aufgabe: Bedingte Wahrscheinlichkeitstabellen',
    task: 'Ändere die bedingten Wahrscheinlichkeiten und beobachte wie die Werte in den zwei Tabellen sich verändern.',
  },
  'cpt-tree-explainer': {
    // CPTreeExplainer
    title: 'Aufgabe: Bedingte Wahrscheinlichkeitsbäume',
    task: 'Ändere die Häufigkeiten und beobachte wie sich der bedingte Wahrscheinlichkeitsbaum verändert.',
  },
  'cpt-exercise-1': {
    // CPTExercise
    task: 'Berechne die fehlenden bedingten Wahrscheinlichkeiten und trage sie in die Tabelle ein!',
  },
  'cp-exercise-1': {
    // CPExercise1
    'cond-prob': 'Die bedingte Wahrscheinlichkeit {condProbNotation} ist {condProbValue}.',
  },
  'cp-cards-exercise': {
    // CPCardsExercise
    explanation:
      'Du ziehst eine Karte aus einem Standardkartenspiel mit 52 Karten. Die Karten im Deck haben verschiedene "Farben" (Karo ♦, Herz ♥, Pik ♠ und Kreuz ♣) und Symbole. Die Symbole sind unterteilt in Zahlen (2 bis 10) und "Bilder" (Bube, Dame, König, Ass).',
    'all-combinations':
      'Hier werden alle möglichen Kombinationen aus Farben und Symbolen sowie zwei zufällig generierte Ereignisse A und B visualisiert.',
    help: 'Wenn du alleine nicht weiterkommst, kannst unten den Abschnitt "Hilfsmittel" aufklappen und nutzen oder dir im Abschnitt "Lösungsweg" die Berechnung anschauen.',
  },
};

// eslint-disable-next-line import/no-default-export
export default messages;
