const messages = {
  'conditional-probability': {
    // shared
    calculation: 'Calculation',
    'choose-scenario': 'Choose Scenario',
    condition: 'Condition',
    'conditional-probability': 'Conditional Probability',
    'considered-events': 'The following events are considered:',
    events: 'Events',
    focus: 'focus',
    'more-visuals': 'Additional Visualizations',
    'number-of-elements-in': 'Number of elements in',
    'results-events': 'Sample space and events',
    situation: 'Situation',
    task: 'Task',
    'use-scenario-title': 'Optional Task: Use a different Scenario',
    'use-scenario': 'Choose another random experiment to explore a different scenario!',

    'joint-probability-table-intro':
      'This table shows the joint probabilities of different intersections of the regarded events and complementary events.',
    'contingency-table-freqs-intro':
      'This contingency table displays the frequency of different intersections between the considered and complementary events.',
    'venn-freqs-intro': 'This Venn diagramm visualizes the frequencies.',
    'venn-probs-intro': 'This Venn diagramm visualizes the probabilities.',
  },

  exercise: {
    // exercises
    'calculate-prob': 'Calculate the conditional probabilities! ',
    'solution-attempt': 'Solution attempt',
    'round-2': 'Round to the second decimal place!',
    'solution-drag-drop': 'Choose a solution! (drag & drop)',
    'solution-drag-drop-round': 'Choose a solution (drag & drop) and make sure you round correctly in your calculation!',
    validate: 'Validate',
    'show-solution': 'Show solution',
    'help-tools': 'Helpful tools',
    'solution-path': 'Solution approach',
  },
  'repeatable-exercise': {
    // RepeatableExercise
    'next-exercise': 'Next exercise',
    skip: 'Skip',
    'additional-exercise': 'Additional exercise',
    next: 'Next',
  },
  'independence-exercise': {
    // IndependenceExercise1
    prob: 'Probabilities',
    'known-prob': 'The following conditional and marginal probabilites are known: ',
    'generate-distribution': 'Create an approximately independant distribution!',
    correct: 'This solution is correct!',
    wrong: 'This solution is wrong!',
  },
  'interaction-mode': {
    // interaction mode
    'maximise-browser':
      'Maximise the browser window and start the interaction mode to generate your own events! You will find the interaction element on the left.',
    'maximise-browser-short': 'Maximise the browser window and start the interaction mode!',
    'maximise-browser-alternative':
      'Maximise the browser window and start the interaction mode to modify the conditional probability yourself!',
    'dice-configure-events':
      'Set up events A and/or B, and observe the visualization and calculations to see how the conditional probabilities change.\n\n- Construct, for example, the events in a way that results in a conditional probability of 0.00 / 1.00 or "not defined".',
    'change-prob': 'Change the conditional probability and observe how the diagram is changing.',
    'random-events': 'Generate random events!',
    intermediate: 'Configure A and B additionally (for intermediates)',
    start: 'Start interaction mode',
  },
  'cp-dice-explainer': {
    // CPDiceExplainer
    combinations:
      'Here, you can see visual representations of all potential combinations of both dice, along with two events denoted as A and B.',
    focus: 'Focus on condition {condResult}',
    introduction: 'You are rolling two dice of different colors.',
  },
  'cp-independence-explainer': {
    // CPIndependenceExplainer
    barcharts: 'Bar charts (non-conditional probabilities)',
    'change-prob':
      'Change the conditional probability. Observe how the values, in the two formulas for determening the independece of two events, are changing.\n-At which point are both sides of the formula the same?\n- How do the conditional probabilites behave in regard to the marginal probability with stochastic independence?',
    formula1: 'Independence using 1. formula',
    formula2: 'Independence using 2. formula',
    'stacked-barcharts': 'Stacked bar charts (conditional probabilities)',
  },
  'cp-venn-freqs-explainer': {
    'task-cond-probs-from-areas':
      'Change the frequencies and observe how the changes of the areas in the Venn diagramm relates towards the changes in the conditional probabilties!\n\n- How does the Venn diagramm look like, when the conditional probabilities are 0 or 1?\n- How does the change affect the intersection?\n- What effect does the changing of the other fequencies have?',
  },
  'cp-venn-sample-explainer': {
    // CPVennSampleSpaceExplainer
    'calculated-values':
      'For the calculation of the (conditional) probability, only the result set in the purple frame will be used. Results outside the frame will be ignored.',
    'choose-alpha':
      'Choose an appropriate Ω to focus the result set that has been used for the calculation of the corresponding probability',
    'results-visual': 'Visualization of the result set',
    'used-calc': 'used for the calculation of P(A) and P(B)',
  },
  'cp-from-freqs-explainer': {
    // CPFromFreqsExplainer
    'change-freqs':
      'Change the frequencies and observe how the conditional probabilities change according to the formula.\n\n- Try, for example, to create conditional probabilities of 0.00 or 1.00.',
    intro: 'This Venn diagramm visualizes the frequencies.',
    'task-freq': 'Task: conditional probabilities of frequencies',
    'task-area': 'Task: conditional probabilities and areas',
  },
  'cp-from-probs-explainer': {
    // CPFromProbsExplainer

    task: 'Task: conditional probabilities of probabilities',
    'change-probs':
      'Adjust the probabilities and observe how the conditional probabilities change according to the formula.\n\n- Try, for example, to create conditional probabilities of 0.00 or 1.00.',
  },
  'cpt-explainer': {
    // CPTExplainer
    title: 'Task: Conditional probability tables',
    task: 'Change the conditional probabilities and observe how the values in the two tables change.',
  },
  'cpt-tree-explainer': {
    // CPTreeExplainer
    title: 'Task: Conditional probability trees',
    task: 'Change the frequencies and observe how it affects the conditional probability tree.',
  },
  'cpt-exercise-1': {
    // CPTExercise
    task: 'Calculate the missing conditional probabilities and fill them in the table!',
  },
  'cp-exercise-1': {
    // CPExercise1
    'cond-prob': 'The conditional probability {condProbNotation} is {condProbValue}.',
  },
  'cp-cards-exercise': {
    // CPCardsExercise
    explanation:
      'You draw a card from a standard deck of cards with 52 cards. The cards in the deck have different "colors" (diamonds ♦, heart ♥, spade ♠ and clubs ♣) and symbols. The symbols are divided into numbers (2 to 10) and "faces" (Jack, Queen, King and Ace).',
    'all-combinations':
      'This visualizes all the possible combinations of colors and symbols aswell as two randomly generated events A and B.',
    help: 'If you can\'t figure it out on your own, you can expand and use the "Helpful tools" section below, or check the calculation in the "Solution Approach"',
  },
};

// eslint-disable-next-line import/no-default-export
export default messages;
