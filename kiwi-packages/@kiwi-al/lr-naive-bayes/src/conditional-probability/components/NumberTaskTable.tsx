import { TextField } from '@mui/material';
import React from 'react';
import { NumericFormat } from 'react-number-format';

import { useAssessmentContext, usePresentSolutions } from '@adlete-vle/lr-core-components/assessment';

import { NumericAnswerInput, NumericAnswerInputType } from '../exercises/NumericAnswerInput';

import './NumberTaskTable.css';

export interface INumberTaskTableProps {
  // TODO: pass inputIndices
  labels: string[];
  type: NumericAnswerInputType;
  itemType?: string;
  format?: (value: number) => string;
}

export function NumberTaskTable(props: INumberTaskTableProps): React.ReactElement {
  const { labels, type, format } = props;
  let { itemType } = props;

  const assessment = useAssessmentContext<number>();
  const presentSolutions = usePresentSolutions();

  itemType = itemType || 'exercise';

  const rows = labels.map((label, index) => (
    // eslint-disable-next-line react/no-array-index-key
    <tr key={index}>
      <td>{label}</td>
      <td>
        <NumericAnswerInput index={index} type={type} itemType={itemType} format={format} />
      </td>
      {assessment.solutions && presentSolutions && (
        <td>
          <NumericFormat
            value={format ? format(assessment.solutions[index]) : assessment.solutions[index]}
            customInput={TextField}
            decimalSeparator=","
            disabled
          />
        </td>
      )}
    </tr>
  ));
  return (
    <table className="number-task-table">
      <tbody>{rows}</tbody>
    </table>
  );
}
