import { Typography } from '@mui/material';
import { i18n } from 'i18next';
import merge from 'lodash.merge';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import type { RecursivePartial } from '@adlete-utils/typescript';
import { useMemoWith } from '@adlete-vle/utils/hooks';

import { BivariateJointFrequencyDataFrame } from '@kiwi-al/math-core/frequencies';
import {
  BivariateJointProbabilityDataFrame,
  formatConditionalProbability,
  formatProbability2,
  formatProbabilityExpression,
  formatProbabilitySetOperation,
  IFormatConditionalProbabilityOptions,
  IFormatProbabilityExpressionOptions,
  IFormatProbabilitySetOperationOptions,
} from '@kiwi-al/math-core/probability';
import { formatVariableSetOperation, IFormatVariableSetOperationOptions } from '@kiwi-al/math-core/sets';
import { toLocaleFixed } from '@kiwi-al/math-core/utils';
import {
  BivarCategoricalVarDefs,
  formatVariableWithState,
  getVarWithState,
  IFormatVariableWithStateOptions,
  VariableWithState,
} from '@kiwi-al/math-core/variables';
import { Expression } from '@kiwi-al/math-core-components/math';
import { getSingleVariableColor, SingleColorMap } from '@kiwi-al/math-core-components/shared/colors';
import './CPCalculation.css';

export enum CPCalculationMode {
  Frequency = 'Frequency',
  Probability = 'Probability',
}

export interface ICPCalculationFormatting {
  freqs?: {
    joint: IFormatVariableSetOperationOptions;
    given: IFormatVariableWithStateOptions;
  };
  probs?: {
    joint: IFormatProbabilitySetOperationOptions;
    given: IFormatProbabilityExpressionOptions;
  };
  condProb: IFormatConditionalProbabilityOptions;
}

const DEFAULT_FORMATS: ICPCalculationFormatting = {
  freqs: {
    joint: {
      setFormat: 'Notation',
      varFormat: 'StateOnly',
    },
    given: {
      format: 'StateOnly',
    },
  },
  probs: {
    joint: {
      setFormat: 'Notation',
      varFormat: 'StateOnly',
    },
    given: {
      probFormat: 'Notation',
      varFormat: 'StateOnly',
    },
  },
  condProb: {
    condProbFormat: 'Notation',
    varFormat: 'StateOnly',
  },
};

function mergeWithDefaultFormat(formatting: RecursivePartial<ICPCalculationFormatting>, i18n: i18n): ICPCalculationFormatting {
  const freqsPropsWithI18n = { joint: { i18n }, given: { i18n } };
  const withI18n: RecursivePartial<ICPCalculationFormatting> = { freqs: freqsPropsWithI18n, probs: freqsPropsWithI18n, condProb: { i18n } };
  return merge({}, withI18n, DEFAULT_FORMATS, formatting);
}

function getEquationText(
  conditionedInfo: VariableWithState,
  conditioningInfo: VariableWithState,
  mode: CPCalculationMode,
  formatting: ICPCalculationFormatting,
  jointColor: string,
  givenColor: string,
  i18n: i18n
): string {
  const numberOfElementsIn = i18n.t('conditional-probability:number-of-elements-in');
  switch (mode) {
    case CPCalculationMode.Frequency: {
      const aAndB = formatVariableSetOperation(conditionedInfo, conditioningInfo, 'Intersection', formatting.freqs.joint);
      const b = formatVariableWithState(conditioningInfo, formatting.freqs.given);
      // TODO: use t for localizing number of elements
      return `=\\frac{\\color{${jointColor}}\\text{${numberOfElementsIn} }${aAndB}}{\\color{${givenColor}}\\text{${numberOfElementsIn} }${b}}`;
    }
    case CPCalculationMode.Probability: {
      const aAndB = formatProbabilitySetOperation(conditionedInfo, conditioningInfo, 'Intersection', formatting.probs.joint);
      const b = formatProbabilityExpression(conditioningInfo, formatting.probs.given);
      return `=\\frac{\\color{${jointColor}}${aAndB}}{\\color{${givenColor}}${b}}`;
    }
    default:
      throw new Error(`Unknown Mode '${mode}'`);
  }
}

function getDefaultCondProbsToCalculate(variables: BivarCategoricalVarDefs): [VariableWithState, VariableWithState][] {
  const var0 = variables[0];
  const var1 = variables[1];
  return [
    [
      [var0, var0.categories[0]],
      [var1, var1.categories[0]],
    ],
    [
      [var1, var1.categories[0]],
      [var0, var0.categories[0]],
    ],
  ];
}

export interface ICPCalculationProps {
  data: BivariateJointFrequencyDataFrame | BivariateJointProbabilityDataFrame;
  showData?: boolean;
  showEquation?: boolean;
  mode: CPCalculationMode;
  condProbsToCalculate?: [VariableWithState, VariableWithState][];
  displayVariables?: BivarCategoricalVarDefs;
  colors?: SingleColorMap;
  formatting?: RecursivePartial<ICPCalculationFormatting>;
  extraElems?: React.ReactNode[];
}

// TODO: make columns flexible. Use CSS-Grids or tables instead?
export function CPCalculation(props: ICPCalculationProps): React.ReactElement {
  // === Incoming component data and defaults
  const { data, showData, showEquation, mode, colors, extraElems } = props;
  const displayVars = props.displayVariables || data.variables;
  const { i18n, t } = useTranslation();
  const formatting = useMemoWith(mergeWithDefaultFormat, [props.formatting, i18n]);

  const condProbsToCalculate = useMemo(
    () => props.condProbsToCalculate || getDefaultCondProbsToCalculate(data.variables),
    [props.condProbsToCalculate, data.variables]
  );

  // === Rows
  const rows = condProbsToCalculate.map((cpToCalc) => {
    const conditionedInfo = cpToCalc[0];
    const condVarIndex = data.getVariableIndex(cpToCalc[0][0]);
    const condCatIndex = conditionedInfo[0].categories.indexOf(conditionedInfo[1]);
    const givenInfo = cpToCalc[1];
    const givenVarIndex = data.getVariableIndex(cpToCalc[1][0]);
    const givenCatIndex = givenInfo[0].categories.indexOf(givenInfo[1]);
    const givenVar = data.variables[givenVarIndex];

    const conditionedInfoDisplay = getVarWithState(displayVars, condVarIndex, condCatIndex);
    const givenInfoDisplay = getVarWithState(displayVars, givenVarIndex, givenCatIndex);

    const jointFreq = data.getFrequency(givenInfo[0], givenInfo[1], conditionedInfo[0], conditionedInfo[1]);
    const marginalFreq = data.getMarginalFrequency(givenVarIndex, givenInfo[1]);
    const condProb = jointFreq / marginalFreq;
    const condProbFormatted = formatProbability2(condProb);

    // TODO: use varWithCatToColorKey
    const conditioningColor = getSingleVariableColor(givenVar, colors);
    const jointColor = colors && colors.joint;

    // const columns = [];
    // columns.push(formatConditionalProbability(conditionedInfo, conditioningInfo, condProbFormat));
    // if (showEquation) columns.push();

    const jointFreqStr = mode === CPCalculationMode.Frequency ? `${jointFreq}` : toLocaleFixed(jointFreq, 2);
    const marginalFreqStr = mode === CPCalculationMode.Frequency ? `${marginalFreq}` : toLocaleFixed(marginalFreq, 2);
    const expression = conditioningColor
      ? `=\\frac{\\color{${jointColor}}${jointFreqStr}}{\\color{${conditioningColor}}${marginalFreqStr}}`
      : `=\\frac{${jointFreqStr}}{${marginalFreqStr}}`;

    const condProbFormattedOrNaN = condProbFormatted !== 'NaN' ? condProbFormatted : `\\text{${t('nicht definiert')}}`;
    const partResult = `=${condProbFormattedOrNaN}`;
    const condProbStr = formatConditionalProbability(conditionedInfoDisplay, givenInfoDisplay, formatting.condProb);

    return (
      <tr key={condProbStr}>
        <td>{condProbStr}</td>
        {showEquation && (
          <td>
            <Typography fontSize="1.25em" component="div">
              <Expression
                expression={getEquationText(
                  conditionedInfoDisplay,
                  givenInfoDisplay,
                  mode,
                  formatting,
                  jointColor,
                  conditioningColor,
                  i18n
                )}
              />
            </Typography>
          </td>
        )}
        {showData && (
          <td>
            <Typography fontSize="1.25em" component="div">
              <Expression expression={expression} />
            </Typography>
          </td>
        )}
        <td>
          <Expression expression={partResult} />
        </td>
        {extraElems && <td>{extraElems[givenVarIndex]}</td>}
      </tr>
    );
  });

  return (
    <table className="cp-calculation">
      <tbody>{rows}</tbody>
    </table>
  );
}
