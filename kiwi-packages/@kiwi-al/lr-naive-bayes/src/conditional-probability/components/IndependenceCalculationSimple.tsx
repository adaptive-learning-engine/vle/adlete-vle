import { mapObjIndexed } from 'ramda';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import { BivariateJointFrequencyDataFrame, createPopMarginalJointFromFreqs } from '@kiwi-al/math-core/frequencies';
import {
  BivariateCPDataFrame,
  BivariateJointProbabilityDataFrame,
  formatConditionalProbability,
  formatProbability2,
  formatProbabilityExpression,
  IFormatConditionalProbabilityOptions,
} from '@kiwi-al/math-core/probability';
import { PhraseOrNotationFormat } from '@kiwi-al/math-core/shared/format';
import { BivarCategoricalVarDefs, VariableWithState, VariableWithStateFormat } from '@kiwi-al/math-core/variables';
import { Expression } from '@kiwi-al/math-core-components/math';
import { MultiColorMap } from '@kiwi-al/math-core-components/shared/colors';

import './IndependenceCalculation.css';

function formatCondProbTerm(
  condProbs: BivariateCPDataFrame,
  displayVariables: BivarCategoricalVarDefs,
  formatOptions: IFormatConditionalProbabilityOptions
): string {
  const condVar = displayVariables[condProbs.getConditionalVariableIndex()];
  const givenVar = displayVariables[condProbs.getGivenVariableIndex()];

  return formatConditionalProbability([condVar, condVar.categories[0]], [givenVar, givenVar.categories[0]], formatOptions);
}

export interface IIndependenceCalculationSimpleProps {
  frequencies?: BivariateJointFrequencyDataFrame;
  probabilities?: BivariateJointProbabilityDataFrame;
  displayVariables?: BivarCategoricalVarDefs;
  condProbs: BivariateCPDataFrame;
  probFormat?: PhraseOrNotationFormat;
  varFormat?: VariableWithStateFormat;
  // eslint-disable-next-line react/no-unused-prop-types
  colors?: MultiColorMap;
}

// TODO: make columns flexible. Use CSS-Grids or tables instead?
export function IndependenceCalculationSimple(props: IIndependenceCalculationSimpleProps): React.ReactElement {
  const { frequencies, probabilities, condProbs, probFormat = 'Notation', varFormat = 'Notation' } = props;

  const displayVariables = props.displayVariables || frequencies.variables || probabilities.variables;

  const { i18n } = useTranslation();

  const equations = useMemo(() => {
    const condVar = displayVariables[condProbs.getConditionalVariableIndex()];
    const condWithState: VariableWithState = [condVar, condVar.categories[0]];

    return mapObjIndexed((value) => <Expression expression={value} />, {
      equals: '=',
      equalsPotentially: '\\stackrel{?}{=}',
      condProb: formatCondProbTerm(condProbs, displayVariables, { condProbFormat: probFormat, varFormat, i18n }),
      condMarginalProb: formatProbabilityExpression(condWithState, {
        probFormat,
        varFormat,
        i18n,
      }),
    });
  }, [condProbs, probFormat, varFormat, i18n, displayVariables]);

  const calculations = useMemo(() => {
    const { marginalFrequencies, population } = createPopMarginalJointFromFreqs(frequencies);
    const condVar = condProbs.getConditionalVariable();
    const condVarIndex = condProbs.getConditionalVariableIndex();
    const condState = condVar.categories[0];
    const condMarginal = marginalFrequencies[condVarIndex];
    const givenVar = condProbs.getGivenVariable();
    const givenState = givenVar.categories[0];

    return mapObjIndexed((value) => <Expression expression={value} />, {
      condProb: formatProbability2(condProbs.getConditionalProbability(condState, givenState)),
      fracFreqs: `\\frac{${condMarginal}}{${population}}`,
      fracProbs: `${formatProbability2(condMarginal / population)}`,
    });
  }, [frequencies, condProbs]);

  return (
    <table className="independence-calculation">
      <tbody>
        <tr>
          <td>{equations.condProb}</td>
          <td>{equations.equals}</td>
          <td>{equations.condMarginalProb}</td>
        </tr>
        <tr>
          <td>{calculations.condProb}</td>
          <td>{equations.equalsPotentially}</td>
          <td>{calculations.fracFreqs}</td>
        </tr>
        <tr>
          <td>{calculations.condProb}</td>
          <td>{equations.equalsPotentially}</td>
          <td>{calculations.fracProbs}</td>
        </tr>
      </tbody>
    </table>
  );
}
