import React from 'react';
import { useTranslation } from 'react-i18next';

import { LearningResourceCard } from '@adlete-vle/lr-core-components/learning-resources';

import { ScenarioSelector, ScenarioSelectorProps } from '../../random-experiments/ScenarioSelector';

export type ISwitchRETCardProps = ScenarioSelectorProps;

export function SwitchRETCard(props: ISwitchRETCardProps): React.ReactElement {
  // TODO: move respective translations somewhere else
  const { t } = useTranslation('conditional-probability');
  return (
    <LearningResourceCard label={t('use-scenario-title')}>
      <div>{t('use-scenario')}</div>
      <ScenarioSelector
        // eslint-disable-next-line react/jsx-props-no-spreading
        {...props}
      />
    </LearningResourceCard>
  );
}
