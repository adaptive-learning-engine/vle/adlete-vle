import { Stack, ToggleButton, ToggleButtonGroup } from '@mui/material';
import { mapObjIndexed } from 'ramda';
import React, { useCallback, useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import { useResettableState } from '@adlete-vle/utils/hooks';

import {
  BivariateCPDataFrame,
  BivariateJointProbabilityDataFrame,
  formatConditionalProbability,
  formatProbability2,
  formatProbabilityExpression,
} from '@kiwi-al/math-core/probability';
import { PhraseOrNotationFormat } from '@kiwi-al/math-core/shared/format';
import { VariableWithState, VariableWithStateFormat, varToVarsWithState } from '@kiwi-al/math-core/variables';
import { Expression } from '@kiwi-al/math-core-components/math';

import './TotalProbabilityCalculation.css';

export interface ITotalProbabilityCalculationProps {
  probabilities: BivariateJointProbabilityDataFrame;
  condProbs: BivariateCPDataFrame;
  probFormat?: PhraseOrNotationFormat;
  varFormat?: VariableWithStateFormat;
}

// TODO: make columns flexible. Use CSS-Grids or tables instead?
export function TotalProbabilityCalculation(props: ITotalProbabilityCalculationProps): React.ReactElement {
  const { probabilities, /* colors, */ condProbs, probFormat = 'Notation', varFormat = 'Notation' } = props;

  const { i18n, t } = useTranslation();

  // === State
  const [focusCatIndex, setFocusCutIndex] = useResettableState(() => 0, [condProbs.getConditionalVariable()]);

  // === Equation row
  const equations = useMemo(() => {
    const condVar = condProbs.getConditionalVariable();
    const condVarWithState: VariableWithState = [condVar, condVar.categories[focusCatIndex]];
    const givenVar = condProbs.getGivenVariable();

    const givenVarsWithState = varToVarsWithState(givenVar);
    const leftSide = formatProbabilityExpression(condVarWithState, { varFormat, probFormat: 'Notation', i18n });
    const rightSide = givenVarsWithState
      .map((varWithState) => {
        const condProb = formatConditionalProbability(condVarWithState, varWithState, { condProbFormat: probFormat, varFormat, i18n });
        const prob = formatProbabilityExpression(varWithState, { varFormat, probFormat: 'Notation', i18n });
        return `${condProb} \\cdot ${prob}`;
      })
      .join(' + ');

    return mapObjIndexed((value) => <Expression expression={value} />, {
      equals: '=',
      leftSide,
      rightSide,
    });
  }, [condProbs, probFormat, varFormat, i18n, focusCatIndex]);

  // === Calculation Row
  const calculations = useMemo(() => {
    const condVar = condProbs.getConditionalVariable();
    const givenVar = condProbs.getGivenVariable();
    const givenVarsWithState = varToVarsWithState(givenVar);
    const condVarWithState: VariableWithState = [condVar, condVar.categories[focusCatIndex]];

    const numbers = givenVarsWithState.map((varWithState) => ({
      condProb: condProbs.getConditionalProbability(condVarWithState[1], varWithState[1]),
      prob: probabilities.getMarginalFrequency(varWithState[0], varWithState[1]),
    }));

    const rightSide1 = numbers
      .map((numberSet) => `${formatProbability2(numberSet.condProb)} \\cdot ${formatProbability2(numberSet.prob)}`)
      .join(' + ');

    const rightSide2 = numbers.map((numberSet) => `${formatProbability2(numberSet.condProb * numberSet.prob)}`).join(' + ');
    const rightSide3 = formatProbability2(numbers.reduce((sum, curr) => sum + curr.condProb * curr.prob, 0));

    return mapObjIndexed((value) => <Expression expression={value} />, {
      rightSide1,
      rightSide2,
      rightSide3,
    });
  }, [probabilities, condProbs, focusCatIndex]);

  // === Category ToggleButtonGroup
  const onFocusCatToggled = useCallback(
    (event: React.MouseEvent<HTMLElement>, newCatIndex: number) => {
      if (newCatIndex != null) setFocusCutIndex(newCatIndex);
    },
    [setFocusCutIndex]
  );

  const togglebuttonGroup = useMemo(
    () => (
      <ToggleButtonGroup value={focusCatIndex} exclusive onChange={onFocusCatToggled} aria-label="text alignment">
        {condProbs.getConditionalVariable().categories.map((cat, catIndex) => (
          <ToggleButton key={cat.toString()} value={catIndex} aria-label={cat.toString()}>
            {cat}
          </ToggleButton>
        ))}
      </ToggleButtonGroup>
    ),
    [onFocusCatToggled, condProbs, focusCatIndex]
  );

  // === Final Rendering
  return (
    <Stack className="total-probability-calculation" spacing={2}>
      <Stack spacing={2} direction="row" alignItems="center">
        <div>{t('Berechne: ')}</div>
        {togglebuttonGroup}
      </Stack>
      <table>
        <tbody>
          <tr>
            <td>{equations.leftSide}</td>
            <td>{equations.equals}</td>
            <td>{equations.rightSide}</td>
          </tr>
          <tr>
            {/* Seems like a bug in jsx-a11y */}
            {/* eslint-disable-next-line jsx-a11y/control-has-associated-label */}
            <td />
            <td>{equations.equals}</td>
            <td>{calculations.rightSide1}</td>
          </tr>
          <tr>
            {/* eslint-disable-next-line jsx-a11y/control-has-associated-label */}
            <td />
            <td>{equations.equals}</td>
            <td>{calculations.rightSide2}</td>
          </tr>
          <tr>
            {/* eslint-disable-next-line jsx-a11y/control-has-associated-label */}
            <td />
            <td>{equations.equals}</td>
            <td>{calculations.rightSide3}</td>
          </tr>
        </tbody>
      </table>
    </Stack>
  );
}
