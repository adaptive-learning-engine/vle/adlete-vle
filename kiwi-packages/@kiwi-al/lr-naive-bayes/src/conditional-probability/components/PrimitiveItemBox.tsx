import { Box, styled, TextField } from '@mui/material';
import React from 'react';

import { ValidationType } from '@adlete-vle/lr-core-components/assessment';
import { IContentItem } from '@adlete-vle/lr-core-components/dnd';

import './PrimitiveItemBox.css';

export interface IPrimitiveItemBoxProps {
  item: IContentItem<number | string>;
  validation?: ValidationType;
  format?: (value: number | string) => string;
  className?: string;
  endAdornment?: React.ReactElement;
}

interface IFakeInputProps {
  value: string;
}

// ref must be used in the function, but we don't really need it
// eslint-disable-next-line @typescript-eslint/no-unused-vars
const FakeInput = React.forwardRef((props: IFakeInputProps, ref: unknown) => <Box className="primitive-item-box-input">{props.value}</Box>);

export function PrimitiveItemBoxRaw(props: IPrimitiveItemBoxProps): React.ReactElement {
  const { format, className, endAdornment } = props;

  /* eslint-disable-next-line no-nested-ternary */
  const value = props.item.content == null ? 'X' : format ? format(props.item.content) : props.item.content;
  return (
    // <Box border={`1px solid ${color}`} padding={2}>
    //   {/* eslint-disable-next-line no-nested-ternary */}
    //   {props.item.content == null ? 'X' : format ? format(props.item.content) : props.item.content}
    //   {/* eslint-disable-next-line no-nested-ternary */}

    // </Box>
    <TextField
      className={`primitive-item-box ${className} ${props.validation}`}
      value={value}
      disabled
      InputProps={{
        inputComponent: FakeInput,
        endAdornment,
      }}
    />
  );
}

export const PrimitiveItemBox = styled(PrimitiveItemBoxRaw)(({ theme }) => ({
  '&.is-valid fieldset': {
    borderColor: `${theme.palette.success.main} !important`,
    borderWidth: '2px',
  },
  '&.is-invalid fieldset': {
    borderColor: `${theme.palette.error.main} !important`,
    borderWidth: '2px',
  },
  '.can-drop & fieldset': {
    borderColor: `${theme.palette.primary.main} !important`,
    borderWidth: '2px',
  },
  // '& fieldset': {
  //   borderColor: `${theme.palette.success.main} !important`,
  // },
}));
