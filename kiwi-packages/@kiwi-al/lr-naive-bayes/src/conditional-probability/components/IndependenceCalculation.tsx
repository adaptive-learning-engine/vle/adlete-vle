import { i18n } from 'i18next';
import { mapObjIndexed } from 'ramda';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import { BivariateJointFrequencyDataFrame, createPopMarginalJointFromFreqs } from '@kiwi-al/math-core/frequencies';
import {
  BivariateJointProbabilityDataFrame,
  formatProbability2,
  formatProbabilityExpression,
  formatProbabilitySetOperation,
} from '@kiwi-al/math-core/probability';
import { formatVariableSetOperation } from '@kiwi-al/math-core/sets';
import {
  BivarCategoricalVarDefs,
  formatVariableWithState,
  getVarWithState,
  ICategoricalVarDef,
  VariableWithStateFormat,
} from '@kiwi-al/math-core/variables';
import { Expression } from '@kiwi-al/math-core-components/math';
import { MultiColorMap } from '@kiwi-al/math-core-components/shared/colors';

import './IndependenceCalculation.css';

export enum IndependenceCalculationMode {
  Frequency = 'Frequency',
  Probability = 'Probability',
}

export function formatJointFrequencyTerm(
  variables: ICategoricalVarDef[],
  mode: IndependenceCalculationMode,
  varFormat: VariableWithStateFormat,
  i18n: i18n
): string {
  const var0State0 = getVarWithState(variables, 0, 0);
  const var1State0 = getVarWithState(variables, 1, 0);

  if (mode === IndependenceCalculationMode.Frequency) {
    return `|${formatVariableSetOperation(var0State0, var1State0, 'Intersection', {
      varFormat,
      setFormat: 'Notation',
      i18n,
    })}|`;
  }
  return formatProbabilitySetOperation(var0State0, var1State0, 'Intersection', {
    setFormat: 'Notation',
    varFormat,
    i18n,
  });
}

function formatProductTerm(
  variables: ICategoricalVarDef[],
  mode: IndependenceCalculationMode,
  varFormat: VariableWithStateFormat,
  i18n: i18n
): string {
  const var0State0 = getVarWithState(variables, 0, 0);
  const var1State0 = getVarWithState(variables, 1, 0);
  if (mode === IndependenceCalculationMode.Frequency) {
    const a = formatVariableWithState(var0State0, { format: 'Notation', i18n });
    const b = formatVariableWithState(var1State0, { format: 'Notation', i18n });
    return `|${a}| \\cdot |${b}|`;
  }
  const a = formatProbabilityExpression(var0State0, {
    probFormat: 'Notation',
    varFormat,
    i18n,
  });
  const b = formatProbabilityExpression(var1State0, {
    probFormat: 'Notation',
    varFormat,
    i18n,
  });
  return `${a} \\cdot ${b}`;
}

export interface IIndependenceCalculationProps {
  frequencies?: BivariateJointFrequencyDataFrame;
  probabilities?: BivariateJointProbabilityDataFrame;
  displayVariables?: BivarCategoricalVarDefs;
  // probFormat?: PhraseOrNotationFormat;
  varFormat?: VariableWithStateFormat;
  colors?: MultiColorMap;
}

// TODO: make columns flexible. Use CSS-Grids or tables instead?
export function IndependenceCalculation(props: IIndependenceCalculationProps): React.ReactElement {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { frequencies, probabilities, colors, varFormat = 'Notation' } = props;

  const displayVariables = props.displayVariables || frequencies.variables || probabilities.variables;

  const { i18n } = useTranslation();

  const equations = useMemo(
    () =>
      mapObjIndexed((value) => <Expression expression={value} />, {
        equals: '=',
        equalsPotentially: '\\stackrel{?}{=}',
        jointProbs: formatJointFrequencyTerm(displayVariables, IndependenceCalculationMode.Probability, varFormat, i18n),
        jointFreqs: formatJointFrequencyTerm(displayVariables, IndependenceCalculationMode.Frequency, varFormat, i18n),
        productProbs: formatProductTerm(displayVariables, IndependenceCalculationMode.Probability, varFormat, i18n),
        productFreqs: formatProductTerm(displayVariables, IndependenceCalculationMode.Frequency, varFormat, i18n),
      }),
    [displayVariables, i18n, varFormat]
  );

  const calculations = useMemo(() => {
    const { jointFrequency, marginalFrequencies, population } = createPopMarginalJointFromFreqs(frequencies);
    return mapObjIndexed((value) => <Expression expression={value} />, {
      jointProbs: formatProbability2(jointFrequency / population),
      jointFreqs: `\\frac{${jointFrequency}}{${population}}`,
      productFreqs: `\\frac{${marginalFrequencies[0]}}{${population}} \\cdot \\frac{${marginalFrequencies[1]}}{${population}}`,
      productProbs: `${formatProbability2(marginalFrequencies[0] / population)} \\cdot ${formatProbability2(
        marginalFrequencies[1] / population
      )}`,
      productValue: formatProbability2((marginalFrequencies[0] / population) * (marginalFrequencies[1] / population)),
    });
  }, [frequencies]);

  return (
    <table className="independence-calculation">
      <tbody>
        <tr>
          <td>{equations.jointProbs}</td>
          <td>{equations.equals}</td>
          <td>{equations.productProbs}</td>
        </tr>
        {/* <tr>
          <td>{equations.jointFreqs}</td>
          <td>{equations.equals}</td>
          <td>{equations.productFreqs}</td>
        </tr> */}
        <tr>
          <td>{calculations.jointFreqs}</td>
          <td>{equations.equalsPotentially}</td>
          <td>{calculations.productFreqs}</td>
        </tr>
        <tr>
          <td>{calculations.jointProbs}</td>
          <td>{equations.equalsPotentially}</td>
          <td>{calculations.productProbs}</td>
        </tr>
        <tr>
          <td>{calculations.jointProbs}</td>
          <td>{equations.equalsPotentially}</td>
          <td>{calculations.productValue}</td>
        </tr>
      </tbody>
    </table>
  );
}
