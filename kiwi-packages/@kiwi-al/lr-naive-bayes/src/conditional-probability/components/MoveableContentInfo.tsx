import { Alert, AlertTitle, Divider, Stack } from '@mui/material';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { MoveableContentButtons, MoveableContentPosition } from '@adlete-vle/lr-core-components/misc/MoveableContent';

export interface IMoveableContentInfoProps {
  position: MoveableContentPosition;
  onChange?: (pos: MoveableContentPosition) => void;
}

export function MoveableContentInfo({ position, onChange }: IMoveableContentInfoProps): React.ReactElement {
  const { t } = useTranslation();
  return (
    <>
      <Divider />
      <Alert severity="info">
        <Stack direction={position === 'left' ? 'column' : 'row'} spacing={2}>
          <Stack spacing={2}>
            <AlertTitle>{t('Ansicht')}</AlertTitle>
            <div>
              {t(
                'Wenn dein Bildschirm zu klein ist, um sowohl die Interaktionskomponenten, als auch die Visualisierungen zu sehen, kannst du diese Knöpfe nutzen um diese Box zu verschieben!'
              )}
            </div>
          </Stack>
          <div>
            <MoveableContentButtons value={position} onChange={onChange} />
          </div>
        </Stack>
      </Alert>
    </>
  );
}
