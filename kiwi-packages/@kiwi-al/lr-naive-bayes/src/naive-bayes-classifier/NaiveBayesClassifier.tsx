import { Box, TextField, Typography } from '@mui/material';
import React, { useCallback, useMemo, useState } from 'react';

import { getBivariateJointFrequenciesFromDataset } from '@kiwi-al/math-core/frequencies';
import { BernoulliNaiveBayesClassifier } from '@kiwi-al/math-core/naive-bayes/classifier';
import { getConditionalProbabilitiesFromJointFrequencies } from '@kiwi-al/math-core/probability';
import { createGenericDataFrame, TabularDataset } from '@kiwi-al/math-core/tabular';
import { ICategoricalVarDef, RandomVarState } from '@kiwi-al/math-core/variables';
import { DataTable } from '@kiwi-al/math-core-components/data-grid/DataTable';
import { ContingencyTable } from '@kiwi-al/math-core-components/frequency/ContingencyTable';
import { ConditionalProbabilityTable } from '@kiwi-al/math-core-components/probability/ConditionalProbabilityTable';

import { ISpamOccuranceDataRow, SPAM_WORD_OCCURANCE_DATASET } from '../datasets/SpamWordOccurence';

interface INaiveBayesTesterProps {
  classifier: BernoulliNaiveBayesClassifier;
}

function NaiveBayesTester(props: INaiveBayesTesterProps): React.ReactElement {
  const { classifier } = props;

  const [text, setText] = useState('');

  const prediction = useMemo(() => classifier.predictMax(text), [text, classifier]);

  // creating the dataset
  const dataset = useMemo(() => {
    const data = createGenericDataFrame<RandomVarState[]>([classifier.getPredictorStatesFromString(text)]);
    return new TabularDataset(classifier.predictorVariables, data);
  }, [classifier, text]);

  const onTextChanged = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setText(event.target.value);
  }, []);

  return (
    <Box>
      <TextField fullWidth multiline onChange={onTextChanged} />
      <DataTable initialData={dataset} />
      <Box>And the result is {prediction}</Box>
    </Box>
  );
}

const wordVariables = SPAM_WORD_OCCURANCE_DATASET.variables.slice(0, 4);
const words = wordVariables.map((variable) => variable.name);
const classVar = SPAM_WORD_OCCURANCE_DATASET.variables.find((variable) => variable.name === 'Class');

export function NaiveBayesClassifier(): React.ReactElement {
  const [dataset, setDataset] = useState(SPAM_WORD_OCCURANCE_DATASET);

  // const editableCells = new Array2D([
  //   [true, true, true, true, true],
  //   [false, false, false, false, true],
  //   [false, false, false, false, false],
  // ]);

  // doing the math
  const frequencies = useMemo(() => getBivariateJointFrequenciesFromDataset(dataset, words, [classVar.name]), [dataset]);
  const condProbs = useMemo(
    () => getConditionalProbabilitiesFromJointFrequencies(/* dataset, */ frequencies),
    [/* dataset, */ frequencies]
  );
  const classifier = useMemo(() => {
    const condProbsGivenClass = Object.entries(condProbs).map(([, probFrames]) => probFrames[0]);
    return new BernoulliNaiveBayesClassifier(classVar, wordVariables, condProbsGivenClass);
  }, [condProbs]);

  // preparing the tables
  const freqTables = Object.entries(frequencies).map(([varName, freqFrames]) => (
    <ContingencyTable key={varName} initialData={freqFrames[0]} />
  ));

  const condProbTables = Object.entries(condProbs).map(([varName, probFrames]) => (
    <ConditionalProbabilityTable key={varName} initialData={probFrames[0]} />
  ));

  // onUpdate
  const onDatasetUpdate = useCallback((updatedDataset: TabularDataset<ISpamOccuranceDataRow, ICategoricalVarDef>) => {
    setDataset(updatedDataset.copy());
  }, []);

  return (
    <Box>
      <Typography variant="h5">Data</Typography>
      <DataTable initialData={SPAM_WORD_OCCURANCE_DATASET} editableCells onUpdate={onDatasetUpdate} />
      <Typography variant="h5">Joint frequencies</Typography>
      {freqTables}
      <Typography variant="h5">Conditional probabilities</Typography>
      {condProbTables}
      <Typography variant="h5">Naive Bayes Classifier</Typography>
      <NaiveBayesTester classifier={classifier} />
    </Box>
  );
}
