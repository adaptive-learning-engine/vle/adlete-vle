import { createGenericDataFrame } from '@kiwi-al/math-core/tabular/GenericDataFrame';
import { TabularDataset } from '@kiwi-al/math-core/tabular/TabularDataset';
import {
  ICategoricalVarDef,
  INumericVarDef,
  IVarDef,
  VariableDataType,
  VariableType,
} from '@kiwi-al/math-core/variables/VariableDefinition';

export type NormalSpam = 'normal' | 'spam';
export type ISpamDataRow = [number, number, number, number, NormalSpam];

export const SPAM_WORD_FREQUENCY_DATA_RAW = [
  [1, 0, 1, 0, 'normal'],
  [2, 1, 1, 0, 'normal'],
  [0, 1, 1, 0, 'normal'],
  [1, 1, 0, 0, 'normal'],
  [1, 0, 0, 0, 'normal'],
  [1, 1, 0, 0, 'normal'],
  [1, 1, 0, 0, 'normal'],
  [1, 0, 0, 0, 'normal'],
  [0, 0, 1, 1, 'normal'],
  [0, 1, 0, 0, 'normal'],
  [0, 1, 0, 0, 'normal'],
  [0, 0, 1, 0, 'normal'],
  [1, 0, 1, 0, 'normal'],
  [0, 0, 1, 1, 'spam'],
  [0, 0, 0, 1, 'spam'],
  [1, 0, 0, 1, 'spam'],
  [1, 0, 0, 7, 'spam'],
  [1, 1, 0, 1, 'spam'],
  [1, 0, 0, 1, 'spam'],
  [0, 1, 0, 0, 'spam'],
];

const variables: IVarDef[] = [
  {
    name: 'Dear',
    type: VariableType.Discrete,
    dtype: VariableDataType.Integer,
    signed: true,
  } as INumericVarDef,
  {
    name: 'Friend',
    type: VariableType.Discrete,
    dtype: VariableDataType.Integer,
    signed: true,
  } as INumericVarDef,
  {
    name: 'Lunch',
    type: VariableType.Discrete,
    dtype: VariableDataType.Integer,
    signed: true,
  } as INumericVarDef,
  {
    name: 'Money',
    type: VariableType.Discrete,
    dtype: VariableDataType.Integer,
    signed: true,
  } as INumericVarDef,
  {
    name: 'Class',
    type: VariableType.Categorical,
    dtype: VariableDataType.String,
    categories: ['normal', 'spam'],
  } as ICategoricalVarDef<NormalSpam>,
];

export const SPAM_WORD_FREQUENCY_DATA = createGenericDataFrame<ISpamDataRow>(SPAM_WORD_FREQUENCY_DATA_RAW, {
  columns: variables.map((variable) => variable.name),
});

export const SPAM_WORD_FREQUENCY_DATASET = new TabularDataset(variables, SPAM_WORD_FREQUENCY_DATA);
