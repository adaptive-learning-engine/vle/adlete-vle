import { IGenericDataFrame } from '@kiwi-al/math-core/tabular';
import { TabularDataset } from '@kiwi-al/math-core/tabular/TabularDataset';
import { binarize } from '@kiwi-al/math-core/tabular/binarize';
import { ICategoricalVarDef, VariableDataType, VariableType } from '@kiwi-al/math-core/variables/VariableDefinition';
import { RandomVarState } from '@kiwi-al/math-core/variables/discrete-variable';

import { ISpamDataRow, NormalSpam, SPAM_WORD_FREQUENCY_DATA } from './SpamWordFrequency';

export type NoYes = 'no' | 'yes';
export type ISpamOccuranceDataRow = [NoYes, NoYes, NoYes, NoYes, NormalSpam];

const variables: ICategoricalVarDef<RandomVarState>[] = [
  {
    name: 'Dear',
    type: VariableType.Categorical,
    dtype: VariableDataType.String,
    categories: ['no', 'yes'],
  } as ICategoricalVarDef<NoYes>,
  {
    name: 'Friend',
    type: VariableType.Categorical,
    dtype: VariableDataType.String,
    categories: ['no', 'yes'],
  } as ICategoricalVarDef<NoYes>,
  {
    name: 'Lunch',
    type: VariableType.Categorical,
    dtype: VariableDataType.String,
    categories: ['no', 'yes'],
  } as ICategoricalVarDef<NoYes>,
  {
    name: 'Money',
    type: VariableType.Categorical,
    dtype: VariableDataType.String,
    categories: ['no', 'yes'],
  } as ICategoricalVarDef<NoYes>,
  {
    name: 'Class',
    type: VariableType.Categorical,
    dtype: VariableDataType.String,
    categories: ['normal', 'spam'],
  } as ICategoricalVarDef<NormalSpam>,
];

export const SPAM_WORD_OCCURANCE_DATA: IGenericDataFrame<ISpamOccuranceDataRow> = binarize<ISpamDataRow, ISpamOccuranceDataRow>(
  SPAM_WORD_FREQUENCY_DATA,
  ['no', 'yes']
);

export const SPAM_WORD_OCCURANCE_DATASET = new TabularDataset<ISpamOccuranceDataRow, ICategoricalVarDef<RandomVarState>>(
  variables,
  SPAM_WORD_OCCURANCE_DATA
);
