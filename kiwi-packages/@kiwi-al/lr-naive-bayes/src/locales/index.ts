import { type i18n } from 'i18next';

import { addLocales as addCondProbLocales } from '../conditional-probability/locales';
import { addLocales as addRandomExperimenLocales } from '../random-experiments/locales';

export function addLocales(i18nextInstance?: i18n): void {
  addCondProbLocales(i18nextInstance);
  addRandomExperimenLocales(i18nextInstance);
}
