import { Alert, Button, Stack } from '@mui/material';
import React from 'react';
import { useTranslation } from 'react-i18next';

export interface IStartInteractionModeProps {
  text?: string;
  buttonText?: string;
  onStart: () => void;
}

export function StartInteractionMode({ text, buttonText, onStart }: IStartInteractionModeProps): React.ReactElement {
  const { t } = useTranslation('interaction-mode');
  text = text || t('maximise-browser-short');
  buttonText = buttonText || t('start');

  return (
    <Alert severity="info">
      <Stack direction="row" spacing={4} alignItems="center">
        <div>{text}</div>
        <div>
          <Button variant="contained" onClick={onStart}>
            {buttonText}
          </Button>
        </div>
      </Stack>
    </Alert>
  );
}
