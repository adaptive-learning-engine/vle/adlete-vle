import type { ILearningResourceMeta } from '@adlete-semantics/amb';
import { HumanReadableTextualData, ILearningResource } from '@adlete-vle/lr-core/learning-resources';

const meta: ILearningResourceMeta = {
  '@context': ['https://w3id.org/kim/amb/draft/context.jsonld', { '@language': 'en' }],
  id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/welcome.json',
  type: ['LearningResource'],
  name: 'Welcome',
  localizedName: {
    en: 'Welcome',
    de: 'Willkommen',
  },
  learningResourceType: [{ id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/data' }],
  encoding: [{ encodingFormat: 'text/x.multi-lingual-markdown+json' }],
};

const content: HumanReadableTextualData = {
  de: 'Hallo und willkommen! \n\n Vielen Dank, dass du heute diese web-basierte Lernumgebung nutzt um dich zum Thema bedingte Wahrscheinlichkeit weiterzubilden!',
  en: 'Hello and welcome! \n\n Thank you for using this web-based learning environment today to learn more about conditional probability!',
};

export const WelcomeLR: ILearningResource<HumanReadableTextualData> = { meta, content };
