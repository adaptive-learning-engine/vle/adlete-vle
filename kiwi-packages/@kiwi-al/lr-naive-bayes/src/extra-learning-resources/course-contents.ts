import type { ILearningResourceMeta } from '@adlete-semantics/amb';
import { HumanReadableTextualData, ILearningResource } from '@adlete-vle/lr-core/learning-resources';

const meta: ILearningResourceMeta = {
  '@context': ['https://w3id.org/kim/amb/draft/context.jsonld', { '@language': 'en' }],
  id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/course-contents.json',
  type: ['LearningResource'],
  name: 'Course Contents',
  localizedName: {
    en: 'Course Contents',
    de: 'Kursinhalte',
  },
  learningResourceType: [{ id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/data' }],
  encoding: [{ encodingFormat: 'text/x.multi-lingual-markdown+json' }],
};

const content: HumanReadableTextualData = {
  de: 'Innerhalb dieses Kurses wirst du folgende Kompetenzen erwerben:\n- Grundlagenwissen zu bedingter Wahrscheinlichkeit\n- Rechnen mit bedingten Wahrscheinlichkeiten\n- Anwendung der Multiplikationsregel für die Wahrscheinlichkeiten zweier Ereignisse\n- Bestimmung der stochastischen Unabhängigkeit zweier Ereignisse',
  en: 'Within this course you will acquire the following competencies:\n- Basic knowledge of conditional probability\n- Calculating with conditional probabilities\n- Application of the multiplication rule for the probabilities of two events\n- Determination of the stochastic independence of two events',
};

export const CourseContentsLR: ILearningResource<HumanReadableTextualData> = { meta, content };
