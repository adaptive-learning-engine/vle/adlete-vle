import type { ILearningResourceMeta } from '@adlete-semantics/amb';
import { HumanReadableTextualData, ILearningResource } from '@adlete-vle/lr-core/learning-resources';

const meta: ILearningResourceMeta = {
  '@context': ['https://w3id.org/kim/amb/draft/context.jsonld', { '@language': 'en' }],
  id: 'http://localhost/data/content/kiwi-vle/learning-environment.json',
  type: ['LearningResource'],
  name: 'Personalization',
  localizedName: {
    en: 'Personalization',
    de: 'Personalisierung',
  },
  learningResourceType: [{ id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/data' }],
  encoding: [{ encodingFormat: 'text/x.multi-lingual-markdown+json' }],
};

const content: HumanReadableTextualData = {
  de: 'Die Benutzeroberfläche dieser Lernumgebung wurde so entworfen, dass du sie auf verschiedene Weisen auf deine Bedürfnisse anpassen kannst. Du kannst bspw. einzelne Blöcke durch Klick auf den Titel ein- und ausklappen.\n\nNutze gerne die verschiedenen Interaktionselemente in den begleitenden Beispielen. Du kannst die Interaktionselemente auch nach links verschieben um deren Effekte besser sehen zu können. Keine Sorge - du kannst dadurch nichts kaputt machen!',
  en: "The user interface of this learning environment was designed so that you can customize it to your needs in various ways. For example, you can expand and collapse individual blocks by clicking on the title.\n\nFeel free to use the various interaction elements in the accompanying examples. You can also move the interaction elements to the left to better see their effects. Don't worry - you can't break anything by doing this!",
};

export const LearningEnvironmentLR: ILearningResource<HumanReadableTextualData> = { meta, content };
