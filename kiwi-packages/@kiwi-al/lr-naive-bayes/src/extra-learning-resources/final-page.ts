import type { ILearningResourceMeta } from '@adlete-semantics/amb';
import { HumanReadableTextualData, ILearningResource } from '@adlete-vle/lr-core/learning-resources';

const meta: ILearningResourceMeta = {
  '@context': ['https://w3id.org/kim/amb/draft/context.jsonld', { '@language': 'en' }],
  id: 'http://localhost/data/content/kiwi-vle/final-page.json',
  type: ['LearningResource'],
  name: 'Thank you!',
  localizedName: {
    en: 'Thank you!',
    de: 'Danke!',
  },
  learningResourceType: [{ id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/data' }],
  encoding: [{ encodingFormat: 'text/x.multi-lingual-markdown+json' }],
};

const content: HumanReadableTextualData = {
  de: 'Du hast es geschafft!',
  en: 'You did it!',
};

export const FinalPageLR: ILearningResource<HumanReadableTextualData> = { meta, content };
