import { Box } from '@mui/material';
import React from 'react';

import { BivariateJointFrequencyDataFrame } from '@kiwi-al/math-core/frequencies';
import { Array2D, IBivariateDataFrameOptions } from '@kiwi-al/math-core/tabular';
import { VariableDataType, VariableType } from '@kiwi-al/math-core/variables';
import { ContingencyTable } from '@kiwi-al/math-core-components/frequency';

export function ReadContingencyTable(): React.ReactElement {
  const sharedOptions: IBivariateDataFrameOptions = {
    variables: [
      {
        name: 'bar',
        type: VariableType.Categorical,
        dtype: VariableDataType.String,
        categories: ['yes', 'no', 'maybe'],
      },
      {
        name: 'foo',
        type: VariableType.Categorical,
        dtype: VariableDataType.String,
        categories: ['yes', 'no', 'maybe'],
      },
    ],
  };

  const initialData = new BivariateJointFrequencyDataFrame(
    [
      ['', '', ''],
      [3, 32, 7],
      [5, 3, 4],
    ],
    sharedOptions
  );

  const validationData = new BivariateJointFrequencyDataFrame(
    [
      [6, 3, 12],
      [3, 32, 7],
      [5, 3, 4],
    ],
    sharedOptions
  );

  const editableCells = new Array2D([
    [true, true, true],
    [false, false, false],
    [false, false, false],
  ]);

  // const somedata = [
  //   [6, 3, 12],
  //   [3, 32, 7],
  //   [5, 3, 4],
  //   [5, 3, 'sdfdf'],
  // ];

  return (
    <Box>
      <ContingencyTable initialData={initialData} validationData={validationData} editableCells={editableCells} />
      <Box>TODO: What is the frequency of A and Z appearing together?</Box>
    </Box>
  );
}
