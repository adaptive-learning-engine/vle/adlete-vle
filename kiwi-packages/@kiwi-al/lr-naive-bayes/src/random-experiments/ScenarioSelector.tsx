import { MenuItem, Stack, TextField } from '@mui/material';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { IRandomExperimentTemplate } from '@kiwi-al/math-core/random-experiment';

export interface ScenarioSelectorProps {
  rets: IRandomExperimentTemplate[];
  value: IRandomExperimentTemplate;
  onChange?: (ret: IRandomExperimentTemplate) => void;
}

export function ScenarioSelector(props: ScenarioSelectorProps): React.ReactElement {
  const { rets, value, onChange } = props;

  const { t } = useTranslation('conditional-probability');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (onChange) {
      const ret = rets.find((aRET) => aRET.id === event.target.value);
      onChange(ret);
    }
  };

  const items = rets.map((ret) => (
    <MenuItem key={ret.id} value={ret.id}>
      {t(ret.localeKeys.name)}
    </MenuItem>
  ));

  return (
    <Stack direction="row" alignItems="center" spacing={2}>
      <span>{t('choose-scenario')}:</span>
      <TextField value={value ? value.id : ''} label="Scenario" onChange={handleChange} select name="Scenario">
        {items}
      </TextField>
    </Stack>
  );
}
