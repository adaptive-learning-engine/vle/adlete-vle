import { getIntersection, getIntersectionSize, IEvent } from '@kiwi-al/math-core/events';
import { BivariateJointFrequencyDataFrame } from '@kiwi-al/math-core/frequencies';
import { Outcome } from '@kiwi-al/math-core/random-experiment';
import { getRandomElement } from '@kiwi-al/math-core/randomization';
import { formatSetOperationSymbol } from '@kiwi-al/math-core/sets';
import { addAfterEachChar } from '@kiwi-al/math-core/shared/strings';
import { getRandomCategory, ICategoricalVarDef } from '@kiwi-al/math-core/variables';
import { IColoredEvent } from '@kiwi-al/math-core-components/random-experiment';

import { EVENT_COLORS, EVENT_NAMES } from '../../conditional-probability/shared/data';

type CardsEventType = 'color-is' | 'symbol-is' | 'symbol-is-type' | 'joint';
type CardsSymbolType = 'number' | 'face';

const SYMBOL_TYPES: CardsSymbolType[] = ['number', 'face'];

export const SYMBOL_TO_SYMBOL_TYPE: Record<string, CardsSymbolType> = {
  'drawing-a-card:2': 'number',
  'drawing-a-card:3': 'number',
  'drawing-a-card:4': 'number',
  'drawing-a-card:5': 'number',
  'drawing-a-card:6': 'number',
  'drawing-a-card:7': 'number',
  'drawing-a-card:8': 'number',
  'drawing-a-card:9': 'number',
  'drawing-a-card:10': 'number',
  'drawing-a-card:jack': 'face',
  'drawing-a-card:queen': 'face',
  'drawing-a-card:king': 'face',
  'drawing-a-card:ace': 'face',
};

export interface ICardsEventConfig {
  type: CardsEventType;
  symbolType?: CardsSymbolType;
  value?: string;
}

export interface ICardsEvent extends IEvent {
  config: ICardsEventConfig;
}

export interface IColoredCardsEvent extends IColoredEvent, ICardsEvent {}

export function createCardEvent(sampleSpace: Outcome<string>[], config: ICardsEventConfig): Outcome[] {
  const { value, symbolType, type } = config;
  return sampleSpace.filter((outcome) => {
    switch (type) {
      case 'symbol-is':
        return value === outcome[0][1];
      case 'symbol-is-type':
        return SYMBOL_TO_SYMBOL_TYPE[outcome[0][1]] === symbolType;
      case 'color-is':
        return value === outcome[1][1];
      default:
        throw new Error(`Unknown CardsEventType ${type}!`);
    }
  });
}

export function createOverlappingRandomCardEvents(sampleSpace: Outcome<string>[], numTries = 100000): IColoredCardsEvent[] {
  const symbolVar = sampleSpace[0][0][0];
  const colorVar = sampleSpace[0][1][0];

  for (let i = 0; i < numTries; ++i) {
    const colorConfig: ICardsEventConfig = { value: getRandomCategory(colorVar), type: 'color-is' };
    const colorOutcomes = createCardEvent(sampleSpace, colorConfig);

    const symbolConfig: ICardsEventConfig = { value: getRandomCategory(symbolVar), type: 'symbol-is' };
    const symbolOutcomes = createCardEvent(sampleSpace, symbolConfig);

    const symbolTypeConfig: ICardsEventConfig = { symbolType: getRandomElement(SYMBOL_TYPES), type: 'symbol-is-type' };
    const symbolTypeOutcomes = createCardEvent(sampleSpace, symbolTypeConfig);

    // eslint-disable-next-line no-continue
    if (getIntersectionSize(colorOutcomes, symbolOutcomes) === 0) continue;

    return [
      { config: colorConfig, outcomes: colorOutcomes, color: EVENT_COLORS[0], name: EVENT_NAMES[0] },
      // { config: symbolConfig, outcomes: symbolOutcomes, color: EVENT_COLORS[1], name: EVENT_NAMES[1] },
      { config: symbolTypeConfig, outcomes: symbolTypeOutcomes, color: EVENT_COLORS[1], name: EVENT_NAMES[1] },
    ];
  }

  throw new Error(`Unable to create random overlapping events within ${numTries} tries!`);
}

export function createCardsJointFrequencyDF(events: ICardsEvent[]): BivariateJointFrequencyDataFrame {
  const baseDiceVar = {
    type: 'Categorical',
    dtype: 'String',
    // categories: ['drawing-a-card:yes', 'drawing-a-card:no'],
  };

  const usedEvents = events.slice(0, 2);

  const variables = usedEvents.map((event) => ({
    ...baseDiceVar,
    name: event.name,
    categories: [event.name, addAfterEachChar(event.name, '\u0305')],
  })) as [ICategoricalVarDef, ICategoricalVarDef];

  const marginal0 = usedEvents[0].outcomes.length;
  const marginal1 = usedEvents[1].outcomes.length;
  const yesYesSize = getIntersectionSize(usedEvents[0].outcomes, usedEvents[1].outcomes);
  const noNoSize = 52 - marginal0 - marginal1 + yesYesSize;
  const data = [
    [yesYesSize, marginal0 - yesYesSize],
    [marginal1 - yesYesSize, noNoSize],
  ];

  return new BivariateJointFrequencyDataFrame(data, { variables });
}

// TODO: merge with Dice function
export function createJointEventCards(events: IColoredCardsEvent[], variables: ICategoricalVarDef[]): IColoredCardsEvent {
  const opSymbol = formatSetOperationSymbol('Intersection', { format: 'Notation' });
  return {
    config: { type: 'joint' },
    name: `${variables[0].name} ${opSymbol} ${variables[1].name}`,
    color: EVENT_COLORS[2],
    outcomes: getIntersection(events[0].outcomes, events[1].outcomes),
  };
}
