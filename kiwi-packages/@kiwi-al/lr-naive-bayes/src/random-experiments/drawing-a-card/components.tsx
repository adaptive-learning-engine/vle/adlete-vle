import { Box, FormControlLabel, FormGroup, Radio, RadioGroup, Stack } from '@mui/material';
import { i18n } from 'i18next';
import React, { useCallback, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { ICategoricalVarDef } from '@kiwi-al/math-core/variables';
import { IOutcomeProps, SampleSpaceTable } from '@kiwi-al/math-core-components/random-experiment';

import { IColoredCardsEvent } from './functions';

import './components.css';

export function CardsOutcome(props: IOutcomeProps): React.ReactElement {
  const outcome = props.outcome;

  const { t } = useTranslation();

  const symbol = outcome[0][1] as string;
  const color = outcome[1][1] as string;
  const visibleSymbol = t(symbol);

  // switch (symbol) {
  //   case 'drawing-a-card:jack':
  //   case 'drawing-a-card:queen':
  //   case 'drawing-a-card:king':
  //   case 'drawing-a-card:ace':
  //     visibleSymbol = t(symbol)[0].toUpperCase();
  //     break;
  //   default:
  // }

  let visibleColor;
  switch (color) {
    case 'drawing-a-card:hearts':
      visibleColor = '\u2665';
      break;
    case 'drawing-a-card:diamonds':
      visibleColor = '\u2666';
      break;
    case 'drawing-a-card:clubs':
      visibleColor = '\u2663';
      break;
    case 'drawing-a-card:spades':
      visibleColor = '\u2660';
      break;
    default:
  }

  return (
    <Stack className="cards-card-root" spacing={1} direction="row">
      <Box className="cards-color">{visibleColor}</Box>
      <Box className="cards-symbol">{visibleSymbol}</Box>
    </Stack>
  );
}

export interface IEventListProps {
  events: IColoredCardsEvent[];
}

function getEventTypeLabel(event: IColoredCardsEvent, i18n: i18n): string {
  switch (event.config.type) {
    case 'color-is':
    case 'symbol-is':
      return i18n.t(`drawing-a-card:event-${event.config.type}`, { value: i18n.t(event.config.value) });
    case 'symbol-is-type':
      return i18n.t(`drawing-a-card:event-${event.config.type}`, { value: i18n.t(`drawing-a-card:${event.config.symbolType}`) });
    default:
      return '';
  }
}

export function EventList({ events }: IEventListProps): React.ReactElement {
  const { i18n } = useTranslation('drawing-a-card');
  return (
    <Stack>
      {events.map((event) => (
        <Box key={event.name} sx={{ color: event.color }}>
          {event.name}
          {event.config.type !== 'joint' && `: ${getEventTypeLabel(event, i18n)}`}
        </Box>
      ))}
    </Stack>
  );
}

export interface ICardsSampleSpaceProps {
  variables: ICategoricalVarDef[];
  events: IColoredCardsEvent[];
}

export function CardsSampleSpace(props: ICardsSampleSpaceProps): React.ReactElement {
  const { variables, events } = props;
  const { t } = useTranslation('random-experiments');

  const [focusEventName, setFocusEventName] = useState('none');

  const focusEvent = events.find((event) => event.name === focusEventName);

  const onFocusEventChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    setFocusEventName((event.target as HTMLInputElement).value);
  }, []);

  const noEventText = t('none');
  // const sortText = t('Sortiere nach Ereignissen');
  return (
    <Stack spacing={2} direction="row">
      <SampleSpaceTable variables={variables} OutcomeComponent={CardsOutcome} events={events} focusEvent={focusEvent} />
      <Stack spacing={2}>
        <EventList events={events} />
        <FormGroup>
          {/* <FormControlLabel disabled control={<Checkbox />} label={sortText} /> */}
          {t('focus-event')}
          <RadioGroup value={focusEventName} onChange={onFocusEventChange} row name="focus-event">
            <FormControlLabel value="none" control={<Radio />} label={noEventText} />
            <FormControlLabel value={events[0].name} control={<Radio />} label={events[0].name} />
            <FormControlLabel value={events[1].name} control={<Radio />} label={events[1].name} />
          </RadioGroup>
        </FormGroup>
      </Stack>
    </Stack>
  );
}
