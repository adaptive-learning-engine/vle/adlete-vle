import type { ILearningResourceMeta } from '@adlete-semantics/amb';
import { ILearningResource } from '@adlete-vle/lr-core/learning-resources';

import { IBivarFreqRET } from '@kiwi-al/math-core/frequencies';

const meta: ILearningResourceMeta = {
  '@context': ['https://w3id.org/kim/amb/draft/context.jsonld', { '@language': 'en' }],
  id: 'drawing-a-card-freq-random-experiment',
  type: ['LearningResource', 'RandomExperiment'],
  name: 'Drawing a Card',
  learningResourceType: [{ id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/data' }],
  encoding: [{ encodingFormat: 'application/x.bivar-freq-random-experiment' }],
  image: 'https://al.fki.htw-berlin.de/conditional-probability/images/card_game.jpeg',
};

const ret: IBivarFreqRET = {
  id: 'drawing-a-card-freq-random-experiment',
  type: 'BivariatePolynary',
  variables: [
    {
      name: 'drawing-a-card:symbol',
      type: 'Categorical',
      dtype: 'String',
      categories: [
        'drawing-a-card:2',
        'drawing-a-card:3',
        'drawing-a-card:4',
        'drawing-a-card:5',
        'drawing-a-card:6',
        'drawing-a-card:7',
        'drawing-a-card:8',
        'drawing-a-card:9',
        'drawing-a-card:10',
        'drawing-a-card:jack',
        'drawing-a-card:queen',
        'drawing-a-card:king',
        'drawing-a-card:ace',
      ],
    },
    {
      name: 'drawing-a-card:color',
      type: 'Categorical',
      dtype: 'String',
      categories: ['drawing-a-card:diamonds', 'drawing-a-card:hearts', 'drawing-a-card:spades', 'drawing-a-card:clubs'],
    },
  ],
  generation: {},
  localeKeys: {
    name: 'drawing-a-card:name',
    'description-population-marginal-joint': 'drawing-a-card:description-population-marginal-joint',
  },
  locale: {
    en: {
      'drawing-a-card': {
        name: 'Drawing a Card',
        'description-population-marginal-joint': 'XXX',
        color: 'Color',
        symbol: 'Symbol',
        '2': '2',
        '3': '3',
        '4': '4',
        '5': '5',
        '6': '6',
        '7': '7',
        '8': '8',
        '9': '9',
        '10': '10',
        jack: 'Jack',
        queen: 'Queen',
        king: 'King',
        ace: 'Ace',
        hearts: 'hearts',
        diamonds: 'diamonds',
        clubs: 'clubs',
        spades: 'spades',
        'event-color-is': 'Color is {value}.',
        'event-symbol-is': 'Symbol is {value}.',
        'event-symbol-is-type': 'Symbol is of type "{value}".',
        yes: 'yes',
        no: 'no',
        number: 'number',
        face: 'face',
      },
    },
    de: {
      'drawing-a-card': {
        name: 'Eine Karte ziehen',
        'description-population-marginal-joint': 'XXX',
        color: 'Farbe',
        symbol: 'Symbol',
        jack: 'Bube',
        queen: 'Dame',
        king: 'König',
        ace: 'Ass',
        hearts: 'Herz',
        diamonds: 'Karo',
        clubs: 'Kreuz',
        spades: 'Pik',
        'event-color-is': 'Farbe ist {value}.',
        'event-symbol-is': 'Symbol ist {value}.',
        'event-symbol-is-type': 'Symbol ist vom Typ "{value}".',
        yes: 'ja',
        no: 'nein',
        number: 'Zahl',
        face: 'Bild',
      },
    },
  },
};

export const DrawingACardRETLearningResource: ILearningResource<IBivarFreqRET> = { meta, content: ret };
