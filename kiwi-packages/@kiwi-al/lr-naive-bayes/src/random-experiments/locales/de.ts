const messages = {
  'random-experiments': {
    // components
    none: 'keins',
    'focus-event': 'Fokussiere Ereignis:',
    type: 'Typ',
    value: 'Wert',
  },
};

// eslint-disable-next-line import/no-default-export
export default messages;
