const messages = {
  'random-experiments': {
    // components
    none: 'none',
    'focus-event': 'Focus on event',
    type: 'Type',
    value: 'Value',
  },
};

// eslint-disable-next-line import/no-default-export
export default messages;
