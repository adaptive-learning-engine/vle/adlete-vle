import { Box, FormControlLabel, FormGroup, MenuItem, Radio, RadioGroup, Stack, TextField } from '@mui/material';
import React, { useCallback } from 'react';
import { useTranslation } from 'react-i18next';

import { Outcome } from '@kiwi-al/math-core/random-experiment';
import { ICategoricalVarDef } from '@kiwi-al/math-core/variables';
import { IOutcomeProps, SampleSpaceTable } from '@kiwi-al/math-core-components/random-experiment';

import { create2DiceEvent, DiceEventType, DiceEventTypeEnum, IColoredDiceEvent, IDiceEventConfig } from './functions';

import './components.css';

const DICE_SYMBOLS: Record<number, string> = {
  1: '\u2680',
  2: '\u2681',
  3: '\u2682',
  4: '\u2683',
  5: '\u2684',
  6: '\u2685',
};

export function DiceOutcome(props: IOutcomeProps): React.ReactElement {
  const outcome = props.outcome;
  return (
    <Stack className="outcome" direction="row">
      <Box className="dice-1" sx={{ padding: '2px' }}>
        {DICE_SYMBOLS[outcome[0][1] as number]}
      </Box>
      <Box className="dice-2" sx={{ padding: '2px' }}>
        {DICE_SYMBOLS[outcome[1][1] as number]}
      </Box>
    </Stack>
  );
}

interface IEventListProps {
  events: IColoredDiceEvent[];
}

function EventList({ events }: IEventListProps): React.ReactElement {
  const { t } = useTranslation('rolling-2-dice');
  return (
    <Stack>
      {events.map((event) => (
        <Box key={event.name} sx={{ color: event.color }}>
          {event.name}
          {event.config.type !== 'joint' && `: ${t(`event-${event.config.type}-with-value`, { value: event.config.value })}`}
        </Box>
      ))}
    </Stack>
  );
}

export interface IDiceSampleSpaceProps {
  variables: ICategoricalVarDef[];
  events: IColoredDiceEvent[];
  focusedEvent?: string;
  onFocusedEventChanged?: (focusedEvent: string) => void;
}

export function DiceSampleSpace(props: IDiceSampleSpaceProps): React.ReactElement {
  const { variables, events, focusedEvent, onFocusedEventChanged } = props;
  const { t } = useTranslation('random-experiments');

  const focusEvent = events.find((event) => event.name === focusedEvent);

  const onFocusEventChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      onFocusedEventChanged(event.target.value);
    },
    [onFocusedEventChanged]
  );

  const noEventText = t('none');
  // const sortText = t('Sortiere nach Ereignissen');
  return (
    <Stack spacing={2} direction="row">
      <SampleSpaceTable variables={variables} OutcomeComponent={DiceOutcome} events={events} focusEvent={focusEvent} minWraps={3} />
      <Stack spacing={2}>
        <EventList events={events} />
        <FormGroup>
          {/* <FormControlLabel disabled control={<Checkbox />} label={sortText} /> */}
          {t('focus-event')}
          <RadioGroup value={focusedEvent} onChange={onFocusEventChange} row name="focus-event">
            <FormControlLabel value="none" control={<Radio />} label={noEventText} />
            <FormControlLabel value={events[0].name} control={<Radio />} label={events[0].name} />
            <FormControlLabel value={events[1].name} control={<Radio />} label={events[1].name} />
          </RadioGroup>
        </FormGroup>
      </Stack>
    </Stack>
  );
}

const VALUE_ITEMS = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map((value) => (
  <MenuItem key={value} value={value}>
    {value}
  </MenuItem>
));

export interface IDiceEventConfigProps {
  label: string;
  config: IDiceEventConfig;
  color?: string;
  onChange: (config: IDiceEventConfig) => void;
}

export function DiceEventConfig({ config, label, color, onChange }: IDiceEventConfigProps): React.ReactElement {
  const { t } = useTranslation('rolling-2-dice');

  const typeItems = Object.keys(DiceEventTypeEnum)
    .filter((type) => type !== 'joint')
    .map((type) => (
      <MenuItem key={type} value={type}>
        {t(`event-${type}`)}
      </MenuItem>
    ));

  const onTypeChanged = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      onChange({ ...config, type: event.target.value as DiceEventType });
    },
    [config, onChange]
  );
  const onValueChanged = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      onChange({ ...config, value: Number(event.target.value) });
    },
    [config, onChange]
  );

  return (
    <Stack spacing={2} direction="row" alignItems="center">
      <div style={{ color }}>{label}</div>
      <TextField label={t('type')} variant="outlined" value={config.type} select onChange={onTypeChanged}>
        {typeItems}
      </TextField>
      <TextField label={t('value')} variant="outlined" value={config.value} select onChange={onValueChanged}>
        {VALUE_ITEMS}
      </TextField>
    </Stack>
  );
}

export interface IDiceEventConfiguratorProps {
  sampleSpace: Outcome<number>[];
  events: IColoredDiceEvent[];
  onChange: (events: IColoredDiceEvent[]) => void;
}

export function DiceEventConfigurator(props: IDiceEventConfiguratorProps): React.ReactElement {
  const { t } = useTranslation();
  const { events, onChange, sampleSpace } = props;

  const onConfigChange = useCallback(
    (index: number, config: IDiceEventConfig) => {
      const eventsCopy = [...events];
      const outcomes = create2DiceEvent(sampleSpace, config);
      eventsCopy[index] = { ...eventsCopy[index], config, outcomes };
      onChange(eventsCopy);
    },
    [events, onChange, sampleSpace]
  );

  return (
    <Stack spacing={2}>
      {events.map((event, index) => (
        <DiceEventConfig
          key={event.name}
          label={t(event.name)}
          config={event.config}
          onChange={(config) => onConfigChange(index, config)}
          color={event.color}
        />
      ))}
    </Stack>
  );
}
