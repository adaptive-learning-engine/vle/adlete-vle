import type { ILearningResourceMeta } from '@adlete-semantics/amb';
import { ILearningResource } from '@adlete-vle/lr-core/learning-resources';

import { IBivarFreqRET } from '@kiwi-al/math-core/frequencies';

const meta: ILearningResourceMeta = {
  '@context': ['https://w3id.org/kim/amb/draft/context.jsonld', { '@language': 'en' }],
  id: 'rolling-2-dice-freq-random-experiment',
  type: ['LearningResource', 'RandomExperiment'],
  name: 'Rolling 2 Dice',
  learningResourceType: [{ id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/data' }],
  encoding: [{ encodingFormat: 'application/x.bivar-freq-random-experiment' }],
  image: 'https://al.fki.htw-berlin.de/conditional-probability/images/dice_throw.jpeg',
};

const ret: IBivarFreqRET = {
  id: 'rolling-2-dice-freq-random-experiment',
  type: 'BivariatePolynary',
  variables: [
    {
      name: 'rolling-2-dice:dice-1',
      type: 'Categorical',
      dtype: 'String',
      categories: [1, 2, 3, 4, 5, 6],
    },
    {
      name: 'rolling-2-dice:dice-2',
      type: 'Categorical',
      dtype: 'String',
      categories: [1, 2, 3, 4, 5, 6],
    },
  ],
  generation: {},
  localeKeys: {
    name: 'rolling-2-dice:name',
    'description-population-marginal-joint': 'rolling-2-dice:description-population-marginal-joint',
  },
  locale: {
    en: {
      'rolling-2-dice': {
        name: 'Rolling 2 Dice',
        'description-population-marginal-joint': 'XXX',
        'dice-1': 'Die 1',
        'dice-2': 'Die 2',
        '1': '1',
        '2': '2',
        '3': '3',
        '4': '4',
        '5': '5',
        '6': '6',
        'event-first': 'The first die is showing ...',
        'event-second': 'The second die is showing ...',
        'event-any': 'At least one of the dice is showing ...',
        'event-every': 'Both dice are showing ...',
        'event-sum': 'The sum of both dice is ...',
        'event-minSum': 'The sum of both dice is at least ...',
        'event-first-with-value': 'The first die is showing {value}.',
        'event-second-with-value': 'The second die is showing {value}.',
        'event-any-with-value': 'At least one of the dice is showing {value}.',
        'event-every-with-value': 'Both dice are showing {value}.',
        'event-sum-with-value': 'The sum of both dice is {value}.',
        'event-minSum-with-value': 'The sum of both dice is at least {value}.',
        yes: 'yes',
        no: 'no',
      },
    },
    de: {
      'rolling-2-dice': {
        name: 'Mit 2 Würfeln würfeln',
        'description-population-marginal-joint': 'XXX',
        'dice-1': 'Würfel 1',
        'dice-2': 'Würfel 2',
        'event-first': 'Der erste Würfel zeigt ...',
        'event-second': 'Der zweite Würfel zeigt ...',
        'event-any': 'Mindestens einer der Würfel zeigt ...',
        'event-every': 'Beide Würfel zeigen ...',
        'event-sum': 'Die Summe der Würfel ist ...',
        'event-minSum': 'Die Summe der Würfel ist mindestens ...',
        'event-first-with-value': 'Der erste Würfel zeigt {value}.',
        'event-second-with-value': 'Der zweite Würfel zeigt {value}.',
        'event-any-with-value': 'Mindestens einer der Würfel zeigt {value}.',
        'event-every-with-value': 'Beide Würfel zeigen {value}.',
        'event-sum-with-value': 'Die Summe der Würfel ist {value}.',
        'event-minSum-with-value': 'Die Summe der Würfel ist mindestens {value}.',
        yes: 'ja',
        no: 'nein',
      },
    },
  },
};

export const Rolling2DiceRETLearningResource: ILearningResource<IBivarFreqRET> = { meta, content: ret };
