import { getIntersection, getIntersectionSize, IEvent } from '@kiwi-al/math-core/events';
import { BivariateJointFrequencyDataFrame } from '@kiwi-al/math-core/frequencies';
import { BivariateJointProbabilityDataFrame } from '@kiwi-al/math-core/probability';
import { Outcome } from '@kiwi-al/math-core/random-experiment';
import { getRandomInt } from '@kiwi-al/math-core/randomization';
import { formatSetOperationSymbol } from '@kiwi-al/math-core/sets';
import { addAfterEachChar } from '@kiwi-al/math-core/shared/strings';
import { ICategoricalVarDef } from '@kiwi-al/math-core/variables';
import { IColoredEvent } from '@kiwi-al/math-core-components/random-experiment';

import { EVENT_COLORS, EVENT_NAMES } from '../../conditional-probability/shared/data';

export enum DiceEventTypeEnum {
  sum = 'sum',
  minSum = 'minSum',
  first = 'first',
  second = 'second',
  any = 'any',
  every = 'every',
  joint = 'joint',
}

export type DiceEventType = keyof typeof DiceEventTypeEnum;

export interface IDiceEventConfig {
  type: DiceEventType;
  value?: number;
}

export interface IDiceEvent extends IEvent {
  config: IDiceEventConfig;
}

export interface IColoredDiceEvent extends IColoredEvent, IDiceEvent {}

export function create2DiceEvent(sampleSpace: Outcome<number>[], config: IDiceEventConfig): Outcome[] {
  const { value, type } = config;
  return sampleSpace.filter((outcome) => {
    switch (type) {
      case 'first':
        return value === outcome[0][1];
      case 'second':
        return value === outcome[1][1];
      case 'any':
        return value === outcome[0][1] || value === outcome[1][1];
      case 'every':
        return value === outcome[0][1] && value === outcome[1][1];
      case 'sum':
        return value === outcome[0][1] + outcome[1][1];
      case 'minSum':
        return value <= outcome[0][1] + outcome[1][1];
      default:
        throw new Error(`Unknown DiceEventType ${type}!`);
    }
  });
}

export function createDiceJointFrequencyDF(events: IDiceEvent[]): BivariateJointFrequencyDataFrame {
  const baseDiceVar = {
    type: 'Categorical',
    dtype: 'String',
    // categories: ['rolling-2-dice:yes', 'rolling-2-dice:no'],
  };

  const usedEvents = events.slice(0, 2);

  const variables = usedEvents.map((event) => ({
    ...baseDiceVar,
    name: event.name,
    categories: [event.name, addAfterEachChar(event.name, '\u0305')],
  })) as [ICategoricalVarDef, ICategoricalVarDef];

  const marginal0 = usedEvents[0].outcomes.length;
  const marginal1 = usedEvents[1].outcomes.length;
  const yesYesSize = getIntersectionSize(usedEvents[0].outcomes, usedEvents[1].outcomes);
  const noNoSize = 36 - marginal0 - marginal1 + yesYesSize;
  const data = [
    [yesYesSize, marginal0 - yesYesSize],
    [marginal1 - yesYesSize, noNoSize],
  ];

  return new BivariateJointFrequencyDataFrame(data, { variables });
}

export function createDiceJointProbabilityDF(events: IDiceEvent[]): BivariateJointProbabilityDataFrame {
  const baseDiceVar = {
    type: 'Categorical',
    dtype: 'String',
    // categories: ['rolling-2-dice:yes', 'rolling-2-dice:no'],
  };

  const variables = events.map((event) => ({
    ...baseDiceVar,
    name: event.name,
    categories: [event.name, addAfterEachChar(event.name, '\u0305')],
  })) as [ICategoricalVarDef, ICategoricalVarDef];

  const marginal0 = events[0].outcomes.length;
  const marginal1 = events[1].outcomes.length;
  const yesYesSize = getIntersectionSize(events[0].outcomes, events[1].outcomes);
  const noNoSize = 36 - marginal0 - marginal1 + yesYesSize;
  const data = [
    [yesYesSize / 36, (marginal0 - yesYesSize) / 36],
    [(marginal1 - yesYesSize) / 36, noNoSize / 36],
  ];

  return new BivariateJointProbabilityDataFrame(data, { variables });
}

export function createRandomEvents(sampleSpace: Outcome<number>[], overlapping = true, numTries = 100000): IColoredDiceEvent[] {
  for (let i = 0; i < numTries; ++i) {
    const anyConfig: IDiceEventConfig = { value: getRandomInt(1, 6), type: 'any' };
    const anyOutcomes = create2DiceEvent(sampleSpace, anyConfig);

    const sumConfig: IDiceEventConfig = { value: getRandomInt(2, 12), type: 'sum' };
    const sumOutcomes = create2DiceEvent(sampleSpace, sumConfig);

    // eslint-disable-next-line no-continue
    if (overlapping && getIntersectionSize(anyOutcomes, sumOutcomes) === 0) continue;

    return [
      { config: anyConfig, outcomes: anyOutcomes, color: EVENT_COLORS[0], name: EVENT_NAMES[0] },
      { config: sumConfig, outcomes: sumOutcomes, color: EVENT_COLORS[1], name: EVENT_NAMES[1] },
    ];
  }

  throw new Error(`Unable to create random overlapping events within ${numTries} tries!`);
}

// TODO: merge with Cards function
export function createJointEvent(events: IColoredDiceEvent[], variables: ICategoricalVarDef[]): IColoredDiceEvent {
  const opSymbol = formatSetOperationSymbol('Intersection', { format: 'Notation' });
  return {
    config: { type: 'joint' },
    name: `${variables[0].name} ${opSymbol} ${variables[1].name}`,
    color: EVENT_COLORS[2],
    outcomes: getIntersection(events[0].outcomes, events[1].outcomes),
  };
}
