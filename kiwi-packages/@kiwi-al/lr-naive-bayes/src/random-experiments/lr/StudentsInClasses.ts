import type { ILearningResourceMeta } from '@adlete-semantics/amb';
import { ILearningResource } from '@adlete-vle/lr-core/learning-resources';

import { IBivarFreqRET } from '@kiwi-al/math-core/frequencies';

const meta: ILearningResourceMeta = {
  '@context': ['https://w3id.org/kim/amb/draft/context.jsonld', { '@language': 'en' }],
  id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/random-experiments/students-in-classes.json',
  type: ['LearningResource', 'RandomExperiment'],
  name: 'Students in Classes',
  learningResourceType: [{ id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/data' }],
  encoding: [{ encodingFormat: 'application/x.bivar-freq-random-experiment' }],
  about: [
    {
      id: 'https://kiwi-al.edu/vocab/conditional-probability',
      type: 'Concept',
      prefLabel: {
        en: 'Conditional Probability',
        de: 'Bedingte Wahrscheinlichkeit',
      },
    },
  ],
  image:
    'https://adaptive-learning-engine.gitlab.io/naive-bayes/lr/learning-resources/kiwi-vle/probability/random-experiments/students-in-classes.jpg',
  imageEx: {
    '@type': 'ImageObject',
    contentUrl:
      'https://adaptive-learning-engine.gitlab.io/naive-bayes/lr/learning-resources/kiwi-vle/probability/random-experiments/students-in-classes.jpg',
    url: 'https://www.pexels.com/photo/students-with-their-hands-raised-5428003/',
    name: 'Students with their Hands Raised',
    license: 'https://www.pexels.com/license/',
    copyrightHolder: {
      name: 'pexels.com',
    },
    creator: {
      '@type': 'Person',
      name: 'Tima Miroshnichenko',
    },
  },
};

const ret: IBivarFreqRET = {
  id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/random-experiments/students-in-classes.json',
  type: 'BivariateBinary',
  variables: [
    {
      name: 'students-in-classes:has-chemistry',
      type: 'Categorical',
      dtype: 'String',
      categories: ['students-in-classes:has-chemistry-yes', 'students-in-classes:has-chemistry-no'],
    },
    {
      name: 'students-in-classes:has-algebra',
      type: 'Categorical',
      dtype: 'String',
      categories: ['students-in-classes:has-algebra-yes', 'students-in-classes:has-algebra-no'],
    },
  ],
  shortVariableLocaleKeys: {
    'students-in-classes:has-chemistry': 'students-in-classes:has-chemistry-short',
    'students-in-classes:has-chemistry-yes': 'students-in-classes:has-chemistry-yes-short',
    'students-in-classes:has-chemistry-no': 'students-in-classes:has-chemistry-no-short',
    'students-in-classes:has-algebra': 'students-in-classes:has-algebra-short',
    'students-in-classes:has-algebra-yes': 'students-in-classes:has-algebra-yes-short',
    'students-in-classes:has-algebra-no': 'students-in-classes:has-algebra-no-short',
  },
  generation: {
    default: {
      format: 'joint-freqs',
      data: [
        [30, 50],
        [120, 300],
      ],
    },
    random: {
      format: 'random-population-marginal-joint',
      data: {
        populationMinMax: [500, 1000],
        marginalProbsMinMax: [
          [0.1, 0.9],
          [0.1, 0.9],
        ],
        conditionalProbMinMax: [0.1, 0.6],
      },
    },
  },
  localeKeys: {
    name: 'students-in-classes:name',
    introduction: 'students-in-classes:introduction',
    'problem-description': 'students-in-classes:problem-description',
    'description-population-marginal-joint': 'students-in-classes:description-population-marginal-joint',
    'description-marginal-joint-percent': 'students-in-classes:description-marginal-joint-percent',
  },
  locale: {
    en: {
      'students-in-classes': {
        name: 'Students in Classes',
        introduction:
          'A school provides several classes like Algebra and Chemistry. To optimize the class schedule, the school administration wants to determine if there is a statistical dependency between enrollment in Algebra and enrollment in Chemistry.',
        'problem-description':
          'A school provides several classes like Algebra and Chemistry. What is the probability that a student is enrolled in Chemistry given that they are also studying Algebra?',
        'description-population-marginal-joint':
          'There are {population} students in a certain school. {marginal0} students are enrolled in a Chemistry course and {marginal1} students are enrolled in an Algebra course. There are {joint} students, who are taking both Algebra and Chemistry.',
        'description-marginal-joint-percent':
          'A school offers Chemistry and Algebra courses. {marginal0} of students are enrolled in a Chemistry course and {marginal1} of students are enrolled in an Algebra course. {joint} of the students are taking both Algebra and Chemistry.',
        'has-algebra': 'Algebra',
        'has-algebra-short': 'A',
        'has-algebra-yes': 'yes',
        'has-algebra-yes-short': 'A',
        'has-algebra-no': 'no',
        'has-algebra-no-short': 'A\u0305',
        'has-chemistry': 'Chemistry',
        'has-chemistry-short': 'C',
        'has-chemistry-yes': 'yes',
        'has-chemistry-yes-short': 'C',
        'has-chemistry-no': 'no',
        'has-chemistry-no-short': 'C\u0305',
      },
    },
    de: {
      'students-in-classes': {
        name: 'Schüler_innen in Schulfächern',
        introduction:
          'Eine Schule bietet verschiedene Fächer wie Mathematik und Chemie an. Zur Optimierung des Stundenplans, möchte die Schulleitung ermitteln, ob es eine statistische Abhängigkeit zw. der Belegung von Mathematik und der Belegung von Chemie gibt.',
        'problem-description':
          'Eine Schule bietet verschiedene Fächer wie Mathematik und Chemie an. Zur Optimierung des Stundenplans, möchte die Schulleitung ermitteln, ob es eine statistische Abhängigkeit zw. der Belegung von Mathematik und der Belegung von Chemie gibt. Wird Chemie bspw. häufiger von den Schüler_innen gewählt, wenn sie im gleichen Schuljahr auch Mathematik belegen?',
        'description-population-marginal-joint':
          'Eine Schule bietet verschiedene Fächer wie Mathematik und Chemie an. Zur Optimierung des Stundenplans, möchte die Schulleitung ermitteln, ob es eine statistische Abhängigkeit zw. der Belegung von Mathematik und der Belegung von Chemie gibt. Es gibt {population} Schüler_innen in der Schule. {marginal0} Schüler_innen besuchen einen Chemiekurs und {marginal1} Schüler_innen besuchen einen Mathematikkurs. Es gibt {joint} Schüler_innen, die sowohl Chemie als auch Mathematik besuchen.',
        'description-marginal-joint-percent':
          'Eine Schule bietet verschiedene Fächer wie Mathematik und Chemie an. Zur Optimierung des Stundenplans, möchte die Schulleitung ermitteln, ob es eine statistische Abhängigkeit zw. der Belegung von Mathematik und der Belegung von Chemie gibt. {marginal0} der Schüler_innen besuchen einen Chemiekurs und {marginal1} der Schüler_innen besuchen einen Mathematikkurs. {joint} der Schüler_innen besuchen sowohl Chemie als auch Mathematik.',
        'has-algebra': 'Mathematik',
        'has-algebra-short': 'M',
        'has-algebra-yes': 'ja',
        'has-algebra-yes-short': 'M',
        'has-algebra-no': 'nein',
        'has-algebra-no-short': 'M\u0305',
        'has-chemistry': 'Chemie',
        'has-chemistry-short': 'C',
        'has-chemistry-yes': 'ja',
        'has-chemistry-yes-short': 'C',
        'has-chemistry-no': 'nein',
        'has-chemistry-no-short': 'C\u0305',
      },
    },
  },
};

export const StudentsInClassesRETLearningResource: ILearningResource<IBivarFreqRET> = { meta, content: ret };
