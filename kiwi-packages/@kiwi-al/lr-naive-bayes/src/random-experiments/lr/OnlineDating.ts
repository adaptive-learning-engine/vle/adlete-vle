import type { ILearningResourceMeta } from '@adlete-semantics/amb';
import { ILearningResource } from '@adlete-vle/lr-core/learning-resources';

import { IBivarFreqRET } from '@kiwi-al/math-core/frequencies';

const meta: ILearningResourceMeta = {
  '@context': ['https://w3id.org/kim/amb/draft/context.jsonld', { '@language': 'en' }],
  id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/random-experiments/online-dating.json',
  type: ['LearningResource', 'RandomExperiment'],
  name: 'Online Dating',
  localizedName: {
    en: 'Online Dating',
    de: 'Onlinedating',
  },
  learningResourceType: [{ id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/data' }],
  encoding: [{ encodingFormat: 'application/x.bivar-freq-random-experiment' }],
  about: [
    {
      id: 'https://kiwi-al.edu/vocab/conditional-probability',
      type: 'Concept',
      prefLabel: {
        en: 'Conditional Probability',
        de: 'Bedingte Wahrscheinlichkeit',
      },
    },
  ],
  image:
    'https://adaptive-learning-engine.gitlab.io/naive-bayes/lr/learning-resources/kiwi-vle/probability/random-experiments/online-dating.jpg',
  imageEx: {
    '@type': 'ImageObject',
    contentUrl:
      'https://adaptive-learning-engine.gitlab.io/naive-bayes/lr/learning-resources/kiwi-vle/probability/random-experiments/online-dating.jpg',
    url: 'https://www.pexels.com/photo/couple-sitting-on-rock-beside-lake-238622/',
    name: 'Couple Sitting on Rock Beside Lake',
    license: 'https://www.pexels.com/license/',
    copyrightHolder: {
      name: 'pexels.com',
    },
    creator: {
      '@type': 'Person',
      name: 'Flo Maderebner',
    },
  },
};

const ret: IBivarFreqRET = {
  id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/random-experiments/online-dating.json',
  type: 'BivariateBinary',
  variables: [
    {
      name: 'online-dating:interest-culture',
      type: 'Categorical',
      dtype: 'String',
      categories: ['online-dating:interest-culture-yes', 'online-dating:interest-culture-no'],
    },
    {
      name: 'online-dating:interest-travel',
      type: 'Categorical',
      dtype: 'String',
      categories: ['online-dating:interest-travel-yes', 'online-dating:interest-travel-no'],
    },
  ],
  shortVariableLocaleKeys: {
    'online-dating:interest-culture': 'online-dating:interest-culture-short',
    'online-dating:interest-travel': 'online-dating:interest-travel-short',
    'online-dating:interest-culture-yes': 'online-dating:interest-culture-yes-short',
    'online-dating:interest-culture-no': 'online-dating:interest-culture-no-short',
    'online-dating:interest-travel-yes': 'online-dating:interest-travel-yes-short',
    'online-dating:interest-travel-no': 'online-dating:interest-travel-no-short',
  },
  generation: {
    default: {
      format: 'joint-freqs',
      data: [
        [106, 64],
        [40, 173],
      ],
    },
    random: {
      format: 'random-population-marginal-joint',
      data: {
        populationMinMax: [200, 1000],
        marginalProbsMinMax: [
          [0.1, 0.9],
          [0.1, 0.9],
        ],
        conditionalProbMinMax: [0.1, 0.6],
      },
    },
  },
  localeKeys: {
    name: 'online-dating:name',
    introduction: 'online-dating:introduction',
    'problem-description': 'online-dating:problem-description',
    'description-population-marginal-joint': 'online-dating:description-population-marginal-joint',
    'description-marginal-joint-percent': 'online-dating:description-marginal-joint-percent',
  },
  locale: {
    en: {
      'online-dating': {
        name: 'Online Dating Interests',
        introduction:
          'In an online dating portal, users can indicate whether they are interested in travel and culture. In order to improve the suggestion of possible partners, we want to investigate whether there is a correlation between these two.',
        'problem-description':
          'In an online dating portal, users can indicate whether they are interested in travel and culture. In order to improve the suggestion of possible partners, we want to investigate whether there is a correlation between these two. Does interest in culture influence interest in travel?',
        'description-population-marginal-joint':
          'In an online dating portal, users can indicate whether they are interested in travel and culture. In order to improve the suggestion of possible partners, we want to investigate whether there is a correlation between these two. Of {population} users on the platform, {marginal0} are interested in culture and {marginal1} are interested in travel. {joint} users indicate being interested both in travel as well as culture.',
        'description-marginal-joint-percent':
          'In an online dating portal, users can indicate whether they are interested in travel and culture. In order to improve the suggestion of possible partners, we want to investigate whether there is a correlation between these two. {marginal0} of the users are interested in culture and {marginal1} of the users are interested in travel. {joint} of the users are interested in both travel as well as culture.',
        'interest-culture': 'Interested in culture',
        'interest-culture-short': 'C',
        'interest-culture-yes': 'yes',
        'interest-culture-yes-short': 'C',
        'interest-culture-no': 'no',
        'interest-culture-no-short': 'C\u0305',
        'interest-travel': 'Interested in travel',
        'interest-travel-short': 'T',
        'interest-travel-yes': 'yes',
        'interest-travel-yes-short': 'T',
        'interest-travel-no': 'no',
        'interest-travel-no-short': 'T\u0305',
      },
    },
    de: {
      'online-dating': {
        name: 'Onlinedating Interessen',
        introduction:
          'In einem Onlinedatingportal können Nutzer_innen angeben, ob sie sich für Reisen und Kultur interessieren. Um das Vorschlagen von möglichen Partnern zu verbessern, soll untersucht werden, ob hier eine Korrelation besteht.',
        'problem-description':
          'In einem Onlinedatingportal können Nutzer_innen angeben, ob sie sich für Reisen und Kultur interessieren. Um das Vorschlagen von möglichen Partnern zu verbessern, soll untersucht werden, ob hier eine Korrelation besteht. Beinflusst ein Interesse für Kultur auch das Interesse für Reisen?',
        'description-population-marginal-joint':
          'In einem Onlinedatingportal können Nutzer_innen angeben, ob sie sich für Reisen und Kultur interessieren. Um das Vorschlagen von möglichen Partnern zu verbessern, soll untersucht werden, ob hier eine Korrelation besteht. Von {population} Nutzer_innen auf der Platform haben {marginal0} Interesse für Kultur und {marginal1} Interesse für Reisen. {joint} Nutzer_innen geben ein Interesse für Reisen sowie Kultur an.',
        'description-marginal-joint-percent':
          'In einem Onlinedatingportal können Nutzer_innen angeben, ob sie sich für Reisen und Kultur interessieren. Um das Vorschlagen von möglichen Partnern zu verbessern, soll untersucht werden, ob hier eine Korrelation besteht. {marginal0} der Nutzer_innen geben ein Interesse für Kultur an und {marginal1} Nutzer_innen ein Interesse für Reisen. {joint} der Nutzer_innen interssieren sich für Reisen sowie Kultur.',
        'interest-culture': 'Interesse für Kultur',
        'interest-culture-short': 'K',
        'interest-culture-yes': 'ja',
        'interest-culture-yes-short': 'K',
        'interest-culture-no': 'nein',
        'interest-culture-no-short': 'K\u0305',
        'interest-travel': 'Interesse für Reisen',
        'interest-travel-short': 'R',
        'interest-travel-yes': 'ja',
        'interest-travel-yes-short': 'R',
        'interest-travel-no': 'nein',
        'interest-travel-no-short': 'R\u0305',
      },
    },
  },
};

export const OnlineDatingRETLearningResource: ILearningResource<IBivarFreqRET> = { meta, content: ret };
