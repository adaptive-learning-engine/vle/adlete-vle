import type { ILearningResourceMeta } from '@adlete-semantics/amb';
import { ILearningResource } from '@adlete-vle/lr-core/learning-resources';

import { IBivarFreqRET } from '@kiwi-al/math-core/frequencies';

const meta: ILearningResourceMeta = {
  '@context': ['https://w3id.org/kim/amb/draft/context.jsonld', { '@language': 'en' }],
  id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/random-experiments/laptops-with-virus.json',
  type: ['LearningResource', 'RandomExperiment'],
  name: 'Laptops with Virus',
  localizedName: {
    en: 'Laptops with Virus',
    de: 'Laptops mit Virus',
  },
  learningResourceType: [{ id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/data' }],
  encoding: [{ encodingFormat: 'application/x.bivar-freq-random-experiment' }],
  about: [
    {
      id: 'https://kiwi-al.edu/vocab/conditional-probability',
      type: 'Concept',
      prefLabel: {
        en: 'Conditional Probability',
        de: 'Bedingte Wahrscheinlichkeit',
      },
    },
  ],
  image:
    'https://adaptive-learning-engine.gitlab.io/naive-bayes/lr/learning-resources/kiwi-vle/probability/random-experiments/laptops-with-virus.jpg',
  imageEx: {
    '@type': 'ImageObject',
    contentUrl:
      'https://adaptive-learning-engine.gitlab.io/naive-bayes/lr/learning-resources/kiwi-vle/probability/random-experiments/laptops-with-virus.jpg',
    url: 'https://www.pexels.com/photo/crop-cyber-spy-hacking-system-while-typing-on-laptop-5935794/',
    name: 'Crop cyber spy hacking system while typing on laptop',
    license: 'https://www.pexels.com/license/',
    copyrightHolder: {
      name: 'pexels.com',
    },
    creator: {
      '@type': 'Person',
      name: 'Sora Shimazaki',
    },
  },
};

const ret: IBivarFreqRET = {
  id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/random-experiments/laptops-with-virus.json',
  type: 'BivariateBinary',
  variables: [
    {
      name: 'laptops-with-virus:has-virus',
      type: 'Categorical',
      dtype: 'String',
      categories: ['laptops-with-virus:yes', 'laptops-with-virus:no'],
    },
    {
      name: 'laptops-with-virus:testresult',
      type: 'Categorical',
      dtype: 'String',
      categories: ['laptops-with-virus:positive', 'laptops-with-virus:negative'],
    },
  ],
  shortVariableLocaleKeys: {
    'laptops-with-virus:has-virus': 'laptops-with-virus:has-virus-short',
    'laptops-with-virus:testresult': 'laptops-with-virus:testresult-short',
    'laptops-with-virus:yes': 'laptops-with-virus:yes-short',
    'laptops-with-virus:no': 'laptops-with-virus:no-short',
    'laptops-with-virus:positive': 'laptops-with-virus:positive-short',
    'laptops-with-virus:negative': 'laptops-with-virus:negative-short',
  },
  generation: {
    default: {
      format: 'joint-freqs',
      data: [
        [99, 1],
        [99, 9801],
      ],
    },
    random: {
      format: 'random-population-marginal-joint',
      data: {
        populationMinMax: [500, 1000],
        marginalProbsMinMax: [
          [0.1, 0.9],
          [0.1, 0.9],
        ],
        conditionalProbMinMax: [0.1, 0.6],
      },
    },
  },
  localeKeys: {
    name: 'laptops-with-virus:name',
    introduction: 'laptops-with-virus:introduction',
    'problem-description': 'laptops-with-virus:problem-description',
    'description-population-marginal-joint': 'laptops-with-virus:description-population-marginal-joint',
    'description-marginal-joint-percent': 'laptops-with-virus:description-marginal-joint-percent',
  },
  locale: {
    en: {
      'laptops-with-virus': {
        name: 'Laptops with Virus',
        introduction:
          'In a cyberattack on a large corporation, hackers infect various laptops with a virus. To bring the situation under control as quickly as possible, the IT department develops a test that can determine if a laptop is infected. Unfortunately, the test cannot detect all infected laptops as positive. Sometimes, it also identifies uninfected laptops as false positives!',
        'problem-description':
          'In a cyberattack on a large corporation, hackers infect 1% of all laptops with a virus. To bring the situation under control as quickly as possible, the IT department develops a test that can determine if a laptop is infected. The initial investigations by the IT department show that the test is positive in 99% of cases when a laptop is infected with the virus. 99% of the tested laptops without the virus are correctly identified as negative by the test. The IT department now tests your laptop for the virus. The result: positive! But what is the probability that your laptop is truly infected?',
        'description-population-marginal-joint':
          'In a cyberattack on a large corporation, hackers infect 1% of all laptops with a virus. To bring the situation under control as quickly as possible, the IT department develops a test that can determine if a laptop is infected. {population} laptops have been tested. {marginal0} laptops have a virus, while {marginal1} have been tested positive for a virus. There are {joint} laptops, that both have a virus and tested positive.',
        'description-marginal-joint-percent':
          'In a cyberattack on a large corporation, hackers infect 1% of all laptops with a virus. To bring the situation under control as quickly as possible, the IT department develops a test that can determine if a laptop is infected. {marginal0} of the laptops are infected with the virus, while {marginal1} have been tested positive for the virus. {joint} of the laptops both have a virus and tested positive.',
        'has-virus': 'Has Virus',
        'has-virus-short': 'V',
        testresult: 'Testresult',
        'testresult-short': 'T',
        yes: 'yes',
        'yes-short': 'Y',
        no: 'no',
        'no-short': 'N',
        positive: 'positive',
        'positive-short': 'Pos',
        negative: 'negative',
        'negative-short': 'Neg',
      },
    },
    de: {
      'laptops-with-virus': {
        name: 'Laptops mit Virus',
        introduction:
          'Bei einer Cyberattacke auf ein Großunternehmen infizieren Hacker verschiedenste Laptops mit einem Virus. Um das Problem schnellstmöglich unter Kontrolle zu bringen, entwickelt die IT-Abteilung einen Test, welcher feststellen kann, ob ein Laptop infiziert ist. Der Test kann leider nicht alle infizierten Laptops als positiv erkennen. Manchmal erkennt er auch nicht infizierte Laptops als falsch-positiv!',
        'problem-description':
          'Bei einer Cyberattacke auf ein Großunternehmen infizieren Hacker 1% aller Laptops mit einem Virus. Um das Problem schnellstmöglich unter Kontrolle zu bringen, entwickelt die IT-Abteilung einen Test, welcher feststellen kann, ob ein Laptop infiziert ist. Die ersten Untersuchungen der IT-Abteilung zeigen, dass der Test in 99%  der Fälle positiv ist, wenn ein Laptop mit dem Virus infiziert ist. 99% der getesteten Laptops ohne Virus werden vom Test als negativ erkannt. Die IT-Abteilung prüft nun deinen Laptop auf das Virus. Das Ergebnis: positiv! Doch wie hoch ist die Wahrscheinlichkeit, dass dein Laptop wirklich infiziert ist?',
        'description-population-marginal-joint':
          'Bei einer Cyberattacke auf ein Großunternehmen infizieren Hacker verschiedenste Laptops mit einem Virus. Um das Problem schnellstmöglich unter Kontrolle zu bringen, entwickelt die IT-Abteilung einen Test, welcher feststellen kann, ob ein Laptop infiziert ist. {population} Laptops wurden auf Viren überprüft. {marginal0} haben wirklich einen Virus, während {marginal1} positiv getestet wurden. Es existieren {joint} Laptops, die sowohl tatsächlich einen Virus haben und positiv auf Viren getestet wurden.',
        'description-marginal-joint-percent':
          'Bei einer Cyberattacke auf ein Großunternehmen infizieren Hacker verschiedenste Laptops mit einem Virus. Um das Problem schnellstmöglich unter Kontrolle zu bringen, entwickelt die IT-Abteilung einen Test, welcher feststellen kann, ob ein Laptop infiziert ist. {marginal0} der Laptops haben den Virus, während {marginal1} der Laptops positiv getestet wurden. {joint} der Laptops haben tatsächlich den Virus und wurden positiv auf den Virus getestet.',
        'has-virus': 'Hat Virus',
        'has-virus-short': 'V',
        testresult: 'Test',
        'testresult-short': 'T',
        yes: 'ja',
        'yes-short': 'V',
        no: 'nein',
        'no-short': 'V\u0305',
        positive: 'positiv',
        'positive-short': 'Pos',
        negative: 'negativ',
        'negative-short': 'Neg',
      },
    },
  },
};

export const LaptopsWithVirusRETLearningResource: ILearningResource<IBivarFreqRET> = { meta, content: ret };
