import type { ILearningResourceMeta } from '@adlete-semantics/amb';
import { ILearningResource } from '@adlete-vle/lr-core/learning-resources';

import { IBivarFreqRET } from '@kiwi-al/math-core/frequencies';

const meta: ILearningResourceMeta = {
  '@context': ['https://w3id.org/kim/amb/draft/context.jsonld', { '@language': 'en' }],
  id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/random-experiments/personalized-advertisement.json',
  type: ['LearningResource', 'RandomExperiment'],
  name: 'Personalized Advertisement',
  localizedName: {
    en: 'Personalized Advertisement',
    de: 'Personalisierte Werbung',
  },
  learningResourceType: [{ id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/data' }],
  encoding: [{ encodingFormat: 'application/x.bivar-freq-random-experiment' }],
  about: [
    {
      id: 'https://kiwi-al.edu/vocab/conditional-probability',
      type: 'Concept',
      prefLabel: {
        en: 'Conditional Probability',
        de: 'Bedingte Wahrscheinlichkeit',
      },
    },
  ],
  image:
    'https://adaptive-learning-engine.gitlab.io/naive-bayes/lr/learning-resources/kiwi-vle/probability/random-experiments/personalized-ads.jpg',
  imageEx: {
    '@type': 'ImageObject',
    contentUrl:
      'https://adaptive-learning-engine.gitlab.io/naive-bayes/lr/learning-resources/kiwi-vle/probability/random-experiments/personalized-ads.jpg',
    url: 'https://www.pexels.com/photo/working-macbook-computer-keyboard-34577/',
    name: 'Black and Gray Laptop Computer With Turned-on Screen Beside Person Holding Red Smart Card in Selective-focus Photography',
    license: 'https://creativecommons.org/publicdomain/zero/1.0',
    creator: {
      '@type': 'Person',
      name: 'Negative Space',
    },
  },
};

const ret: IBivarFreqRET = {
  id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/random-experiments/personalized-advertisement.json',
  type: 'BivariateBinary',
  variables: [
    {
      name: 'personalized-advertisement:conversion',
      type: 'Categorical',
      dtype: 'String',
      categories: ['personalized-advertisement:conversion-yes', 'personalized-advertisement:conversion-no'],
    },
    {
      name: 'personalized-advertisement:personalization',
      type: 'Categorical',
      dtype: 'String',
      categories: ['personalized-advertisement:personalized', 'personalized-advertisement:not-personalized'],
    },
  ],
  shortVariableLocaleKeys: {
    'personalized-advertisement:conversion': 'personalized-advertisement:conversion-short',
    'personalized-advertisement:personalization': 'personalized-advertisement:personalization-short',
    'personalized-advertisement:conversion-yes': 'personalized-advertisement:conversion-yes-short',
    'personalized-advertisement:conversion-no': 'personalized-advertisement:conversion-no-short',
    'personalized-advertisement:personalized': 'personalized-advertisement:personalized-short',
    'personalized-advertisement:not-personalized': 'personalized-advertisement:not-personalized-short',
  },
  generation: {
    default: {
      format: 'joint-freqs',
      data: [
        [1660, 528],
        [3566, 4814],
      ],
    },
    random: {
      format: 'random-population-marginal-joint',
      data: {
        populationMinMax: [500, 10000],
        marginalProbsMinMax: [
          [0.1, 0.9],
          [0.1, 0.9],
        ],
        conditionalProbMinMax: [0.1, 0.6],
      },
    },
  },
  localeKeys: {
    name: 'personalized-advertisement:name',
    introduction: 'personalized-advertisement:introduction',
    'problem-description': 'personalized-advertisement:problem-description',
    'description-population-marginal-joint': 'personalized-advertisement:description-population-marginal-joint',
    'description-marginal-joint-percent': 'personalized-advertisement:description-marginal-joint-percent',
  },
  locale: {
    en: {
      'personalized-advertisement': {
        name: 'Personalied Advertisement',
        introduction:
          "In online retail, the goal is to encourage website visitors to make purchases by displaying product ads along their customer journey. Different marketing strategies and their effectiveness are tested, and one of these strategies is personalized advertising. An essential aspect is how people respond to ads, known as 'conversion'.",
        'problem-description':
          'In online retail, the goal is to encourage website visitors to make purchases by displaying product ads along their customer journey. Different marketing strategies, such as personalized advertising, and their effectiveness are tested. Do visitors interact more with personalized advertising than with non-personalized advertising?',
        'description-population-marginal-joint':
          'In online retail, the goal is to encourage website visitors to make purchases by displaying product ads along their customer journey. Different marketing strategies, such as personalized advertising, and their effectiveness are tested. In a survey, data was gathered from {population} users. Out of them, {marginal0} users engaged with the advertisements (conversion). {marginal1} were exposed to personalized advertising. Altogether, {joint} users were exposed to and interacted with personalized advertising.',
        'description-marginal-joint-percent':
          'In online retail, the goal is to encourage website visitors to make purchases by displaying product ads along their customer journey. Different marketing strategies, such as personalized advertising, and their effectiveness are tested. As part of a survey, data was gathered. {marginal0} of the users engaged with the advertisements (conversion), whereas {marginal1} were exposed to personalized advertising. {joint} of the users received personalized ads and interacted with them.',
        conversion: 'Conversion',
        'conversion-short': 'C',
        personalization: 'Personalized',
        'personalization-short': 'P',
        'conversion-yes': 'yes',
        'conversion-yes-short': 'C',
        'conversion-no': 'no',
        'conversion-no-short': 'C\u0305',
        personalized: 'personalized',
        'personalized-short': 'P',
        'not-personalized': 'not personalized',
        'not-personalized-short': 'N',
      },
    },
    de: {
      'personalized-advertisement': {
        name: 'Personalisierte Werbung',
        introduction:
          'Im Online-Handel versucht man, Besucher_innen der Websites auf ihrer Customer Journey durch eingeblendete Produktwerbung zum Kaufen zu animieren. Dabei werden unterschiedliche Marketingstrategien und deren Wirksamkeit überprüft. Eine dieser Strategien ist personalisierte Werbung. Dabei ist ein wichtiges Merkmal wie Menschen mit Werbung interagieren - die sogenannte Conversion.',
        'problem-description':
          'Im Online-Handel versucht man, Besucher_innen der Websites auf ihrer Customer Journey durch eingeblendete Produktwerbung zum Kaufen zu animieren. Dabei werden unterschiedliche Marketingstrategien wie personalisierte Werbung eingesetzt und deren Wirksamkeit überprüft. Interagieren Besucher_innen eher mit personalisierter Werbung als mit nicht personalisierter Werbung?',
        'description-population-marginal-joint':
          'Im Online-Handel versucht man, Besucher_innen der Websites auf ihrer Customer Journey durch eingeblendete Produktwerbung zum Kaufen zu animieren. Dabei werden unterschiedliche Marketingstrategien eingesetzt und deren Wirksamkeit überprüft. Bei einer Stichprobe wurden Daten von {population} Nutzer_innen gesammelt. Davon haben {marginal0} Nutzer_innen mit der Werbung interagiert (Conversion). {marginal1} bekamen personalisierte Werbung angezeigt. Insgesamt haben {joint} Nutzer_innen personalisierte Werbung bekommen und mit dieser interagiert.',
        'description-marginal-joint-percent':
          'Im Online-Handel versucht man, Besucher_innen der Websites auf ihrer Customer Journey durch eingeblendete Produktwerbung zum Kaufen zu animieren. Dabei werden unterschiedliche Marketingstrategien eingesetzt und deren Wirksamkeit überprüft. Im Zuge der Evaluation wurden Daten gesammelt. {marginal0} der Nutzer_innen haben mit der Werbung interagiert (Conversion), während {marginal1} personalisierte Werbung bekamen. {joint} der Nutzer_innen bekamen personalisierte Werbung und haben mit dieser interagiert.',
        conversion: 'Conversion',
        'conversion-short': 'C',
        personalization: 'Personalisierung',
        'personalization-short': 'P',
        'conversion-yes': 'ja',
        'conversion-yes-short': 'C',
        'conversion-no': 'nein',
        'conversion-no-short': 'C\u0305',
        personalized: 'personalisiert',
        'personalized-short': 'P',
        'not-personalized': 'nicht personalisiert',
        'not-personalized-short': 'P\u0305',
      },
    },
  },
};

export const PersonalizedAdvertisementRETLearningResource: ILearningResource<IBivarFreqRET> = { meta, content: ret };
