import type { ILearningResourceMeta } from '@adlete-semantics/amb';
import { ILearningResource } from '@adlete-vle/lr-core/learning-resources';

import { IBivarFreqRET } from '@kiwi-al/math-core/frequencies';

const meta: ILearningResourceMeta = {
  '@context': ['https://w3id.org/kim/amb/draft/context.jsonld', { '@language': 'en' }],
  id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/random-experiments/lectures-and-exams.json',
  type: ['LearningResource', 'RandomExperiment'],
  name: 'Students in Classes',
  learningResourceType: [{ id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/data' }],
  encoding: [{ encodingFormat: 'application/x.bivar-freq-random-experiment' }],
  about: [
    {
      id: 'https://kiwi-al.edu/vocab/total-probability',
      type: 'Concept',
      prefLabel: {
        en: 'Total Probability',
        de: 'Totale Wahrscheinlichkeit',
      },
    },
  ],
};

const ret: IBivarFreqRET = {
  id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/random-experiments/lectures-and-exams.json',
  type: 'BivariatePolynary',
  variables: [
    {
      name: 'lectures-and-exams:repeated-exams',
      type: 'Categorical',
      dtype: 'String',
      categories: ['lectures-and-exams:two-or-more', 'lectures-and-exams:one', 'lectures-and-exams:none'],
    },
    {
      name: 'lectures-and-exams:intensity-lecture-visit',
      type: 'Categorical',
      dtype: 'String',
      categories: ['lectures-and-exams:low', 'lectures-and-exams:middle', 'lectures-and-exams:high'],
    },
  ],
  shortVariableLocaleKeys: {
    'lectures-and-exams:repeated-exams': 'lectures-and-exams:repeated-exams-short',
    'lectures-and-exams:intensity-lecture-visit': 'lectures-and-exams:intensity-lecture-visit-short',
    'lectures-and-exams:none': 'lectures-and-exams:none-short',
    'lectures-and-exams:one': 'lectures-and-exams:one-short',
    'lectures-and-exams:two-or-more': 'lectures-and-exams:two-or-more-short',
    'lectures-and-exams:low': 'lectures-and-exams:low-short',
    'lectures-and-exams:middle': 'lectures-and-exams:middle-short',
    'lectures-and-exams:high': 'lectures-and-exams:high-short',
  },
  generation: {
    default2: {
      format: 'joint-freqs',
      data: [
        [536, 614, 1299],
        [591, 1208, 3166],
        [164, 496, 1926],
      ],
    },
    default: {
      format: 'joint-freqs',
      data: [
        [536, 591, 164],
        [614, 1208, 496],
        [1299, 3166, 1926],
      ],
    },
    // random: {
    //   format: 'random-population-marginal-joint',
    //   data: {
    //     populationMinMax: [500, 1000],
    //     marginalProbsMinMax: [
    //       [0.1, 0.9],
    //       [0.1, 0.9],
    //     ],
    //     conditionalProbMinMax: [0.1, 0.6],
    //   },
    // },
  },
  localeKeys: {
    name: 'lectures-and-exams:name',
    introduction: 'lectures-and-exams:introduction',
    'problem-description': 'lectures-and-exams:problem-description',
    // 'description-population-marginal-joint': 'lectures-and-exams:description-population-marginal-joint',
    // 'description-marginal-joint-percent': 'lectures-and-exams:description-marginal-joint-percent',
  },
  locale: {
    en: {
      'lectures-and-exams': {
        name: 'Lecture Visits and Exams',
        // 'problem-description':
        //   'A school provides several classes like Algebra and Chemistry. What is the probability that a student is enrolled in Chemistry given that they are also studying Algebra?',
        // 'description-population-marginal-joint':
        //   'There are {population} students in a certain school. {marginal0} students are enrolled in a Chemistry course and {marginal1} students are enrolled in an Algebra course. There are {joint} students, who are taking both Algebra and Chemistry.',
        // 'description-marginal-joint-percent':
        //   'A school offers Chemistry and Algebra courses. {marginal0} of students are enrolled in a Chemistry course and {marginal1} of students are enrolled in an Algebra course. {joint} of the students are taking both Algebra and Chemistry.',
        'repeated-exams': 'Number of exam repititions (grouped)',
        'repeated-exams-short': 'E',
        'intensity-lecture-visit': 'Intensity Lecture Visit (grouped)',
        'intensity-lecture-visit-short': 'L',
        none: 'None',
        'none-short': 'N',
        one: 'One',
        'one-short': 'O',
        'two-or-more': 'Two or more',
        'two-or-more-short': 'T',
        low: 'low',
        'low-short': 'L',
        middle: 'middle',
        'middle-short': 'M',
        high: 'high',
        'high-short': 'H',
      },
    },
    de: {
      'lectures-and-exams': {
        name: 'Vorlesungsbesuche und Prüfungswiederholungen',
        introduction:
          'Eine Hochschule sammelt Daten über ihre Studierenden um die Lehre zu optimieren. Unter anderem werden Daten zur Anzahl an durchgeführten Prüfungsversuchen sowie zur Intensität des Vorlesungsbesuches der einzelnen Studierenden gesammelt.',
        'problem-description':
          'Eine Hochschule sammelt Daten über ihre Studierenden um die Lehre zu optimieren. Unter anderem werden Daten zur Anzahl an durchgeführten Prüfungsversuchen sowie zur Intensität des Vorlesungsbesuches der einzelnen Studierenden gesammelt.',
        // 'description-population-marginal-joint':
        //   'Es gibt {population} Schüler_innen in der Schule. {marginal0} Schüler_innen besuchen einen Chemiekurs und {marginal1} Schüler_innen besuchen einen Algebrakurs. Es gibt {joint} Schüler_innen, die sowohl Chemie als auch Algebra besuchen.',
        // 'description-marginal-joint-percent':
        //   'Eine Schule unterrichtet sowohl Chemie als auch Algebra. {marginal0} der Schüler_innen besuchen einen Chemiekurs und {marginal1} der Schüler_innen besuchen einen Algebrakurs. {joint} der Schüler_innen besuchen sowohl Chemie als auch Algebra.',
        'repeated-exams': 'Anzahl der Prüfungswiederholungen Klassiert',
        'repeated-exams-short': 'Prüfung',
        'intensity-lecture-visit': 'Intensität Vorlesungsbesuch Klassiert',
        'intensity-lecture-visit-short': 'Vorlesung',
        none: 'Keine',
        'none-short': 'K',
        one: 'Eine',
        'one-short': 'E',
        'two-or-more': 'Zwei oder mehr',
        'two-or-more-short': 'Z',
        low: 'niedrig',
        'low-short': 'N',
        middle: 'mittel',
        'middle-short': 'M',
        high: 'hoch',
        'high-short': 'H',
      },
    },
  },
};

export const LecturesAndExamsRETLearningResource: ILearningResource<IBivarFreqRET> = { meta, content: ret };
