import { ILearningResource } from '@adlete-vle/lr-core/learning-resources/learning-resource';

const content: ILearningResource[] = [
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/dev/dev-1.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/kiwi-vle/probability/conditional-probability/dev/dev-1.json',
        },
      ],
      type: ['LearningResource', 'InteractiveExample'],
      name: 'Development: Remove for Production',
      localizedName: {
        en: 'Development: Remove for Production',
        de: 'Entwicklung: Für Production entfernen',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/assessment',
        },
      ],
      encoding: [
        {
          encodingFormat: 'application/x.react-component',
        },
      ],
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/exercises/calculate-cps-from-freqs-1.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/kiwi-vle/probability/conditional-probability/exercises/calculate-cps-from-freqs-1.json',
        },
      ],
      type: ['LearningResource', 'Exercise'],
      name: 'Exercise: Calculate Conditional Probability From Frequencies',
      localizedName: {
        en: 'Exercise: Calculate Conditional Probability From Frequencies',
        de: 'Übung: Berechnung bedingter Wahrscheinlichkeiten anhand von Häufigkeiten',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/assessment',
        },
      ],
      encoding: [
        {
          encodingFormat: 'application/x.react-component',
        },
      ],
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/exercises/calculate-cps-from-probs-1.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/kiwi-vle/probability/conditional-probability/exercises/calculate-cps-from-probs-1.json',
        },
      ],
      type: ['LearningResource', 'Exercise'],
      name: 'Exercise: Calculate Conditional Probability From Probabilities',
      localizedName: {
        en: 'Exercise: Calculate Conditional Probability From Probabilities',
        de: 'Übung: Berechnung bedingter Wahrscheinlichkeiten anhand von Wahrscheinlichkeiten',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/assessment',
        },
      ],
      encoding: [
        {
          encodingFormat: 'application/x.react-component',
        },
      ],
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/exercises/calculate-freqs-from-cps-1.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/kiwi-vle/probability/conditional-probability/exercises/calculate-freqs-from-cps-1.json',
        },
      ],
      type: ['LearningResource', 'Exercise'],
      name: 'Exercise: Calculate Using Conditional Probabilities',
      localizedName: {
        en: 'Exercise: Calculate Using Conditional Probabilities',
        de: 'Übung: Rechnen mit bedingten Wahrscheinlichkeiten',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/assessment',
        },
      ],
      encoding: [
        {
          encodingFormat: 'application/x.react-component',
        },
      ],
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/exercises/calculate-independence-1.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/kiwi-vle/probability/conditional-probability/exercises/calculate-independence-1.json',
        },
      ],
      type: ['LearningResource', 'Exercise'],
      name: 'Exercise: Dependent and Independent Events',
      localizedName: {
        en: 'Exercise: Dependent and Independent Events',
        de: 'Übung: Abhängige und unabhängige Ereignisse',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/assessment',
        },
      ],
      encoding: [
        {
          encodingFormat: 'application/x.react-component',
        },
      ],
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/exercises/calculate-total-probabilities-1.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/kiwi-vle/probability/conditional-probability/exercises/calculate-total-probabilities-1.json',
        },
      ],
      type: ['LearningResource', 'Exercise'],
      name: 'Exercise: Calculate Total Probabilities',
      localizedName: {
        en: 'Exercise: Calculate Total Probabilities',
        de: 'Übung: Berechnung totaler Wahrscheinlichkeiten',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/assessment',
        },
      ],
      encoding: [
        {
          encodingFormat: 'application/x.react-component',
        },
      ],
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/exercises/cards-1.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/kiwi-vle/probability/conditional-probability/exercises/cards-1.json',
        },
      ],
      type: ['LearningResource', 'Exercise'],
      name: 'Exercise: Calculate Conditional Probabilities using Cards',
      localizedName: {
        en: 'Exercise: Calculate Conditional Probabilities using Cards',
        de: 'Übung: Berechnung bedingter Wahrscheinlichkeiten mit Karten',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/assessment',
        },
      ],
      encoding: [
        {
          encodingFormat: 'application/x.react-component',
        },
      ],
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/exercises/cpt-1.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/kiwi-vle/probability/conditional-probability/exercises/cpt-1.json',
        },
      ],
      type: ['LearningResource', 'Exercise'],
      name: 'Exercise: Conditional Probability Tables',
      localizedName: {
        en: 'Exercise: Conditional Probability Tables',
        de: 'Übung: Bedingte Wahrscheinlichkeitstabellen',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/assessment',
        },
      ],
      encoding: [
        {
          encodingFormat: 'application/x.react-component',
        },
      ],
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/explainers/cp-stacked-bar-chart.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/kiwi-vle/probability/conditional-probability/explainers/cp-stacked-bar-chart.json',
        },
      ],
      type: ['LearningResource', 'InteractiveExample'],
      name: 'Interactive Example: Stacked Bar Charts for Conditional Probabilities',
      localizedName: {
        en: 'Interactive Example: Stacked Bar Charts for Conditional Probabilities',
        de: 'Interaktives Beispiel: Gestapelte Balkendiagramme für bedingte Wahrscheinlichkeiten',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/simulation',
        },
      ],
      encoding: [
        {
          encodingFormat: 'application/x.react-component',
        },
      ],
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/explainers/cpt.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/kiwi-vle/probability/conditional-probability/explainers/cpt.json',
        },
      ],
      type: ['LearningResource', 'InteractiveExample'],
      name: 'Interactive Example: Conditional Probability Tables',
      localizedName: {
        en: 'Interactive Example: Conditional Probability Tables',
        de: 'Interaktives Beispiel: Bedingte Wahrscheinlichkeitstabellen',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/simulation',
        },
      ],
      encoding: [
        {
          encodingFormat: 'application/x.react-component',
        },
      ],
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/explainers/cptree.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/kiwi-vle/probability/conditional-probability/explainers/cptree.json',
        },
      ],
      type: ['LearningResource', 'InteractiveExample'],
      name: 'Interactive Example: Conditional Probability Tree Diagrams',
      localizedName: {
        en: 'Interactive Example: Conditional Probability Tree Diagrams',
        de: 'Interaktives Beispiel: Bedingte Wahrscheinlichkeitsbäume',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/simulation',
        },
      ],
      encoding: [
        {
          encodingFormat: 'application/x.react-component',
        },
      ],
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/explainers/dice-1.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/kiwi-vle/probability/conditional-probability/explainers/dice-1.json',
        },
      ],
      type: ['LearningResource', 'InteractiveExample'],
      name: 'Random Experiment: A single throw of two differently colored dice',
      localizedName: {
        en: 'Random Experiment: A single throw of two differently colored dice',
        de: 'Zufallsexperiment: Einmaliger Wurf von zwei verschiedenfarbigen Würfeln',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/simulation',
        },
      ],
      encoding: [
        {
          encodingFormat: 'application/x.react-component',
        },
      ],
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/explainers/from-freqs.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/kiwi-vle/probability/conditional-probability/explainers/from-freqs.json',
        },
      ],
      type: ['LearningResource', 'InteractiveExample'],
      name: 'Interactive Example: Calculating Conditional Probabilities from Frequencies',
      localizedName: {
        en: 'Interactive Example: Calculating Conditional Probabilities from Frequencies',
        de: 'Interaktives Beispiel: Berechnung bedingter Wahrscheinlichkeiten anhand von Häufigkeiten',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/simulation',
        },
      ],
      encoding: [
        {
          encodingFormat: 'application/x.react-component',
        },
      ],
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/explainers/from-probs.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/kiwi-vle/probability/conditional-probability/explainers/from-probs.json',
        },
      ],
      type: ['LearningResource', 'InteractiveExample'],
      name: 'Interactive Example: Calculating Conditional Probabilities from Probabilities',
      localizedName: {
        en: 'Interactive Example: Calculating Conditional Probabilities from Probabilities',
        de: 'Interaktives Beispiel: Berechnung bedingter Wahrscheinlichkeiten anhand von Wahrscheinlichkeiten',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/simulation',
        },
      ],
      encoding: [
        {
          encodingFormat: 'application/x.react-component',
        },
      ],
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/explainers/independence.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/kiwi-vle/probability/conditional-probability/explainers/independence.json',
        },
      ],
      type: ['LearningResource', 'InteractiveExample'],
      name: 'Interactive Example: Conditional Probability and Independent Events',
      localizedName: {
        en: 'Interactive Example: Conditional Probability and Independent Events',
        de: 'Interaktives Beispiel: Bedingte Wahrscheinlichkeit und unabhängige Ereignisse',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/simulation',
        },
      ],
      encoding: [
        {
          encodingFormat: 'application/x.react-component',
        },
      ],
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/explainers/total-probability.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/kiwi-vle/probability/conditional-probability/explainers/total-probability.json',
        },
      ],
      type: ['LearningResource', 'InteractiveExample'],
      name: 'Interactive Example: Total Probability',
      localizedName: {
        en: 'Interactive Example: Total Probability',
        de: 'Interaktives Beispiel: Satz der totalen Wahrscheinlichkeit',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/simulation',
        },
      ],
      encoding: [
        {
          encodingFormat: 'application/x.react-component',
        },
      ],
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/explainers/venn-freqs.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/kiwi-vle/probability/conditional-probability/explainers/venn-freqs.json',
        },
      ],
      type: ['LearningResource', 'InteractiveExample'],
      name: 'Interactive Example: Conditional Probabilities and Venn Diagrams',
      localizedName: {
        en: 'Interactive Example: Conditional Probabilities and Venn Diagrams',
        de: 'Interaktives Beispiel: Bedingte Wahrscheinlichkeiten und Venndiagramme',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/simulation',
        },
      ],
      encoding: [
        {
          encodingFormat: 'application/x.react-component',
        },
      ],
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/kiwi-vle/probability/conditional-probability/explainers/venn-sample-space.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/kiwi-vle/probability/conditional-probability/explainers/venn-sample-space.json',
        },
      ],
      type: ['LearningResource', 'InteractiveExample'],
      name: 'Interactive Example: Conditional Probability Sample Spaces',
      localizedName: {
        en: 'Interactive Example: Conditional Probability Sample Spaces',
        de: 'Interaktives Beispiel: Bedingte Wahrscheinlichkeiten und Ergebnismengen',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/simulation',
        },
      ],
      encoding: [
        {
          encodingFormat: 'application/x.react-component',
        },
      ],
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/all-probabilities-are-conditional-hint.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/all-probabilities-are-conditional-hint.json',
        },
      ],
      type: ['LearningResource', 'Hint'],
      name: 'All Probabilities Are Conditional',
      localizedName: {
        en: 'All Probabilities Are Conditional',
        de: 'Alle Wahrscheinlichkeiten sind abhängig',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-text+json',
        },
      ],
    },
    content: {
      en: 'Nearly all probabilities are conditional in some way or another. Real-world problems are usually simplified, resulting in unconditional probabilities. For example: the probability of throwing heads with a coin depends on the coin being fair.',
      de: 'Fast alle Wahrscheinlichkeiten sind auf eine gewisse Weise abhängig. Unabhängige Ereignisse werden häufig nur geschaffen um die komplexe reale Welt zu vereinfachen. Bspw. ist die Wahrscheinlichkeit mit einer Münze Kopf zu werfen abhängig davon, ob die Münze gezinkt ist oder nicht.',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/both-events-occured.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/both-events-occured.json',
        },
      ],
      type: ['LearningResource', 'Hint'],
      name: 'Both events did occur or will have occured!',
      localizedName: {
        en: 'Both events did occur or will have occured!',
        de: 'Beide Ereignisse sind eingetreten oder werden eingetreten sein!',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-text+json',
        },
      ],
    },
    content: {
      en: 'Conditional probability looks at a possible future, where both events will have occured.',
      de: 'Die bedingte Wahrscheinlichkeit betracht eine mögliche Zukunft, innerhalb derer beide Ereignisse eingetreten sind.',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/calc-using-number-of-outcomes.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/calc-using-number-of-outcomes.json',
        },
      ],
      type: ['LearningResource', 'Explanation', 'Hint'],
      name: 'Calculating Conditional Probabilities Using the Number of Outcomes',
      localizedName: {
        en: 'Calculating Conditional Probabilities Using the Number of Outcomes',
        de: 'Berechnung bedingter Wahrscheinlichkeiten anhand der Anzahl gleich wahrscheinlicher Ergebnisse',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-text+json',
        },
      ],
    },
    content: {
      en: 'For random experiments that have a finite number of equally likely outcomes (classical probability), probabilities and conditional probabilities can be easily determined by counting. The probability P(A) of an event A is calculated from the ratio of the outcomes of A to all possible outcomes. Similarly, the conditional probability P(A|B) is calculated using the ratio of the outcomes of the intersection A ∩ B to the outcomes of B.',
      de: 'Bei Zufallsexperimenten, die eine endliche Anzahl gleich wahrscheinlicher Ergebnisse haben (klassische Wahrscheinlichkeit), kann man Wahrscheinlichkeiten und bedingte Wahrscheinlichkeiten leicht durch Auszählen bestimmen. Die Wahrscheinlichkeit P(A) eines Ereignisses A errechnet sich anhand des Verhältnisses der Ergebnisse von A zu allen möglichen Ergebnissen. Ähnlich errechnet sich die bedingte Wahrscheinlichkeit P(A|B) anhand des Verhältnisses der Ergebnisse der Schnittmenge A ∩ B zu den Ergebnissen von B.',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/conditional-probabilities-bidirectional.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/conditional-probabilities-bidirectional.json',
        },
      ],
      type: ['LearningResource', 'Hint'],
      name: 'Conditional Probabilities are Bidirectional',
      localizedName: {
        en: 'Conditional Probabilities are Bidirectional',
        de: 'Bedingte Wahrscheinlichkeiten sind bidirektional',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-markdown+json',
        },
      ],
    },
    content: {
      en: "Even though the conditional probabilities P(A|B) and P(B|A) are rarely exactly the same, a relationship exists between them. If we change a distribution by changing P(A|B), then P(B|A) will also change. The mathematical equation that connects the two conditional probabilities is called 'Bayes' theorem'. This equation will be discussed later.",
      de: "Auch wenn die bedingten Wahrscheinlichkeiten P(A|B) und P(B|A) selten exakt gleich sind, existiert eine Beziehung zwischen ihnen. Verändern wir eine Verteilung durch Änderung von P(A|B), dann wird sich P(B|A) auch verändern. Die mathematische Gleichung, die beide bedingten Wahrscheinlichkeiten miteinander verbindet, heißt 'Satz von Bayes'. Diese Gleichung wird später noch behandelt.",
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/confusion-of-the-inverse-hint.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/confusion-of-the-inverse-hint.json',
        },
      ],
      type: ['LearningResource', 'Hint'],
      name: 'Confusion of the Inverse',
      localizedName: {
        en: 'Confusion of the Inverse',
        de: 'Verwechslung der Umkehrung',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-text+json',
        },
      ],
    },
    content: {
      en: 'The conditional probability P(A|B) and its inverse P(B|A) may not be the same. This wrong assumption is also known as "Confusion of the Inverse".',
      de: 'Die bedingte Wahrscheinlichkeit P(A|B) und ihre Umkehrung P(B|A) haben meist nicht den gleichen Wert. Bedingte Verteilungen sind also nicht symmetrisch. Diese falsche Annahme ist auch unter dem Begriff „Confusion of the Inverse“ oder zu deutsch „Verwechslung der Umkehrung“ bekannt.',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/equation-in-words.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/equation-in-words.json',
        },
      ],
      type: ['LearningResource', 'Explanation'],
      name: 'Conditional Probabilities Equation in Words',
      localizedName: {
        en: 'Conditional Probabilities Equation in Words',
        de: 'Formel für bedingte Wahrscheinlichkeit in Worten',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-text+json',
        },
      ],
    },
    content: {
      en: 'The conditional probability P(A|B) can be calculated by dividing the joint probability of A and B - written as P(A∩B) - by the probability of B - written as P(B).',
      de: 'Zur Berechnung der bedingten Wahrscheinlichkeit P(A|B) wird die gemeinsame Wahrscheinlichkeit von A und B - geschrieben P(A∩B) - durch die Wahrscheinlichkeit von B - geschrieben P(B) - geteilt.',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/equation.md',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/equation.json',
        },
      ],
      type: ['LearningResource', 'Equation'],
      name: 'Equation of Conditional Probability',
      localizedName: {
        en: 'Equation of Conditional Probability',
        de: 'Formel für bedingte Wahrscheinlichkeit',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/markdown',
        },
      ],
      about: [
        {
          id: 'https://kiwi-al.edu/vocab/conditional-probability',
          type: 'Concept',
          prefLabel: {
            en: 'Conditional Probability',
            de: 'Bedingte Wahrscheinlichkeit',
          },
        },
      ],
      teaches: [
        {
          id: 'https://cass.fki.htw-berlin.de/api/data/schema.cassproject.org.0.4.Competency/7beeca89-c455-4f22-81b3-ad929868e01c',
          prefLabel: {
            en: 'Define Conditional Probability (Mathematically, 2 Events)',
          },
        },
      ],
    },
    content: '$$P\\left(A\\middle|B\\right)=\\frac{P\\left(A \\cap B\\right)}{P\\left(B\\right)}$$\r\n',
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'https://setosa.io/conditional/',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/explainers/interactive-levels.json',
        },
      ],
      type: ['LearningResource', 'InteractiveExample'],
      name: 'Interactive Example: Conditional Probabilities using Colored Balls',
      localizedName: {
        en: 'Interactive Example: Conditional Probabilities using Colored Balls',
        de: 'Interaktives Beispiel: Bedingte Wahrscheinlichkeiten visualisiert durch eingefärbte Bälle',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/simulation',
        },
      ],
      encoding: [
        {
          encodingFormat: 'application/x.react-component',
        },
      ],
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/focus-hint.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/focus-hint.json',
        },
      ],
      type: ['LearningResource', 'Hint'],
      name: 'Focussing on a Feature',
      localizedName: {
        en: 'Focussing on a Feature',
        de: 'Fokussierung auf eine Ausprägung eines Merkmals',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-text+json',
        },
      ],
    },
    content: {
      en: 'In the case of conditional probabilities, one characteristic is set (the condition) and the probabilities of the characteristics of another characteristic under this condition are calculated.',
      de: 'Bei bedingten Wahrscheinlichkeiten wird quasi eine Ausprägung eines Merkmals festgesetzt (die Bedingung) und die Wahrscheinlichkeiten der Ausprägungen eines anderen Merkmals unter dieser Bedingung berechnet.',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/from-frequencies-equation.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/from-frequencies-equation.json',
        },
      ],
      type: ['LearningResource', 'Equation'],
      name: 'Equation of Conditional Probability (Frequencies)',
      localizedName: {
        en: 'Equation of Conditional Probability (Frequencies)',
        de: 'Formel für bedingte Wahrscheinlichkeit (Häufigkeiten)',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-markdown+json',
        },
      ],
      about: [
        {
          id: 'https://kiwi-al.edu/vocab/conditional-probability',
          type: 'Concept',
          prefLabel: {
            en: 'Conditional Probability (Frequencies)',
            de: 'Bedingte Wahrscheinlichkeit (Häufigkeiten)',
          },
        },
      ],
    },
    content: {
      en: '$$P\\left(A\\middle|B\\right)=\\ \\frac{\\text{Number of elements in }A \\cap B}{\\text{Number of elements in }B}$$',
      de: '$$P\\left(A\\middle|B\\right)=\\ \\frac{\\text{Anzahl der Elemente in }A \\cap B}{\\text{Anzahl der Elemente in } B}$$',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/from-frequencies.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/from-frequencies.json',
        },
      ],
      type: ['LearningResource', 'Explanation'],
      name: 'Conditional Probabilities Can Be Calculated from Frequencies',
      localizedName: {
        en: 'Conditional Probabilities Can Be Calculated from Frequencies',
        de: 'Bedingte Wahrscheinlichkeiten können mithilfe von Häufigkeiten berechnet werden',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-text+json',
        },
      ],
    },
    content: {
      en: 'Given the marginal frequencies of two events A and B and their joint frequency |A∩B|, the conditional probabilities can be calculated.',
      de: 'Die bedingten Wahrscheinlichkeiten zweier Ereignisse A und B können berechnet werden, wenn die Häufigkeiten beider Ereignisse sowie ihre gemeinsame Häufigkeit A∩B bekannt ist.',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/independence-equation-long-from-simple.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/independence-equation-long-from-simple.json',
        },
      ],
      type: ['LearningResource', 'Hint'],
      name: 'Explanation for the 2. Equation of Independence',
      localizedName: {
        en: 'Explanation for the 2. Equation of Independence',
        de: 'Erläuterung der 2. Formel für stochastische Unabhängigkeit',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-markdown+json',
        },
      ],
    },
    content: {
      en: 'The second formula for determining stochastic independence follows from the multiplication rule and the first formula. In the multiplication rule $$P\\left(A \\cap B\\right) = P\\left(A|B\\right) \\cdot P\\left(B\\right)$$ the conditional probability $$P(A|B)$$ is replaced by $$P(A)$$ according to the first formula.',
      de: 'Die zweite Formel zur Bestimmung der stochastischen Unabhängigkeit folgt aus der Multiplikationsregel und der ersten Formel. Dabei wird in der Multiplikationsregel $$P\\left(A \\cap B\\right) = P\\left(A|B\\right) \\cdot P\\left(B\\right)$$ die bedingte Wahrscheinlichkeit $$P(A|B)$$ entsprechend der ersten Formel durch $$P(A)$$ ersetzt.',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/independence-equation-long.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/independence-equation-long.json',
        },
      ],
      type: ['LearningResource', 'Equation'],
      name: '2. Equation for Checking Independence of 2 Events',
      localizedName: {
        en: '2. Equation for Checking Independence of 2 Events',
        de: '2. Formel zur Bestimmung der Unabhängigkeit zweier Ereignisse',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-markdown+json',
        },
      ],
    },
    content: {
      en: 'Two events are independent, if $$P\\left(A \\cap B\\right) = P(A) \\cdot P(B)$$',
      de: 'Zwei Ereignisse sind stochastisch unabhängig, wenn gilt $$P\\left(A \\cap B\\right) = P(A) \\cdot P(B)$$',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/independence-equation-simple-in-words.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/independence-equation-simple-in-words.json',
        },
      ],
      type: ['LearningResource', 'Hint'],
      name: 'Explanation for the Equation of Independence',
      localizedName: {
        en: 'Explanation for the Equation of Independence',
        de: 'Erläuterung der 1. Formel für stochastische Unabhängigkeit',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-text+json',
        },
      ],
    },
    content: {
      en: 'Two events A and B are stochastically independent if the conditional probability P(A|B) is equal to the unconditional probability P(A). Since the probabilities are equal, condition B has no effect on the probability of occurrence of A. The same applies to P(B|A) and P(B).',
      de: 'Zwei Ereignisse A und B sind stochastisch unabhängig, wenn die bedingte Wahrscheinlichkeit P(A|B) gleich der unbedingten Wahrscheinlichkeit P(A) ist. Da die Wahrscheinlichkeiten gleich sind, hat die Bedingung B keinen Effekt auf die Eintrittswahrscheinlichkeit von A. Gleiches gilt für P(B|A) und P(B).',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/independence-equation-simple-long.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/independence-equation-simple-long.json',
        },
      ],
      type: ['LearningResource', 'Equation'],
      name: '1. Equation for Checking Independence of 2 Events',
      localizedName: {
        en: '1. Equation for Checking Independence of 2 Events',
        de: '1. Formel zur Bestimmung der Unabhängigkeit zweier Ereignisse',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-markdown+json',
        },
      ],
    },
    content: {
      en: 'Two events are independent, if \n\n$$P\\left(A | B\\right) = P\\left(A\\right), \\quad P\\left(B\\right) \\neq 0$$\n\n$$P\\left(B | A\\right) = P\\left(B\\right), \\quad P\\left(A\\right) \\neq 0$$',
      de: 'Zwei Ereignisse sind stochastisch unabhängig, wenn gilt \n\n$$P\\left(A | B\\right) = P\\left(A\\right), \\quad P\\left(B\\right) \\neq 0$$\n\n$$P\\left(B | A\\right) = P\\left(B\\right), \\quad P\\left(A\\right) \\neq 0$$',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/independence-equation-simple.md',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/independence-equation-simple.json',
        },
      ],
      type: ['LearningResource', 'Equation'],
      name: '1. Equation for Checking Independence of 2 Events',
      localizedName: {
        en: '1. Equation for Checking Independence of 2 Events',
        de: '1. Formel zur Bestimmung der Unabhängigkeit zweier Ereignisse',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/markdown',
        },
      ],
      about: [
        {
          id: 'https://kiwi-al.edu/vocab/conditional-probability',
          type: 'Concept',
          prefLabel: {
            en: 'Independence of 2 Events',
            de: 'Unabhängigkeit zweier Ereignisse',
          },
        },
      ],
    },
    content:
      '$$P\\left(A | B\\right) = P\\left(A\\right), \\quad P\\left(B\\right) \\neq 0$$\r\n\r\n$$P\\left(B | A\\right) = P\\left(B\\right), \\quad P\\left(A\\right) \\neq 0$$\r\n',
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/independence-equation.md',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/independence-equation.json',
        },
      ],
      type: ['LearningResource', 'Equation'],
      name: 'Equation for Checking Independence of 2 Events',
      localizedName: {
        en: 'Equation for Checking Independence of 2 Events',
        de: 'Formel zur Bestimmung der Unabhängigkeit zweier Ereignisse',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/markdown',
        },
      ],
      about: [
        {
          id: 'https://kiwi-al.edu/vocab/conditional-probability',
          type: 'Concept',
          prefLabel: {
            en: 'Independence of 2 Events',
            de: 'Unabhängigkeit zweier Ereignisse',
          },
        },
      ],
    },
    content: '$$P\\left(A \\cap B\\right) = P(A) \\cdot P(B)$$\r\n',
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/independence-introduction.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/independence-introduction.json',
        },
      ],
      type: ['LearningResource', 'Introduction'],
      name: 'Introduction to Dependent and Independent Events',
      localizedName: {
        en: 'Introduction to Dependent and Independent Events',
        de: 'Einführung Abhängigkeit und Unabhängigkeit von Ereignissen',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-text+json',
        },
      ],
    },
    content: {
      en: 'So far, we have used conditional probability to describe how one event can affect another. However, some events do not affect each other in the sense of probabilities: The fact that one event has occurred does not affect the probability that another event will occur. Such events are called (stochastically) independent.',
      de: 'Bisher haben wir mithilfe der bedingten Wahrscheinlichkeit beschrieben, wie sich ein Ereignis auf ein anderes auswirken kann. Manche Ereignisse haben aber keinen Einfluss aufeinander im Sinne von Wahrscheinlichkeiten: Die Tatsache, das ein Ereignis eingetreten ist, beeinflusst nicht die Wahrscheinlichkeit dafür, dass ein anderes Ereignis eintritt. Solche Ereignisse heißen (stochastisch) unabhängig.',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/independence-two-equations.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/independence-two-equations.json',
        },
      ],
      type: ['LearningResource', 'Hint'],
      name: 'Two Equations for Independence',
      localizedName: {
        en: 'Two Equations for Independence',
        de: 'Zwei Formeln zur Bestimmung stochastischer Unabhängigkeit',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-text+json',
        },
      ],
    },
    content: {
      en: 'There are two different formulas to determine whether two events are stochastically dependent or independent.',
      de: 'Es gibt zwei verschiedene Formeln um zu bestimmen, ob zwei Ereignisse stochastisch abhängig bzw. unabhängig sind.',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/introduction.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/introduction.json',
        },
      ],
      type: ['LearningResource', 'Introduction'],
      name: 'Introduction to Conditional Probability',
      localizedName: {
        en: 'Introduction to Conditional Probability',
        de: 'Einführung in die bedingte Wahrscheinlichkeit',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      image: 'https://al.fki.htw-berlin.de/conditional-probability/images/dice_throw.jpeg',
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-markdown+json',
        },
      ],
    },
    content: {
      en: 'Consider a simple die with 6 sides. The probability of rolling a 4 is $$\\frac{1}{6}$$. However, this probability changes if we know whether an even or odd number was rolled:\n- If an even number was rolled, the probability that this number is a 4 is only $$\\frac{1}{3}$$.\n- On the other hand, if it is known that the number rolled is odd, then the 4 is excluded - the probability is 0. \n\nWe say: the conditional probability of rolling a 4 conditional on the event that an even number was rolled is $$\\frac{1}{3}$$.\n\n\nConditional probability thus plays a role when multiple events occur. Knowing the conditional probabilities of two events, helps us determine if one of the events is dependent on the other. Conditional probabilities therefore allow us to predict, for example, how likely it is that one event will occur, assuming that another event has already occurred.',
      de: 'Betrachten wir einen einfachen Würfel mit 6 Seiten. Die Wahrscheinlichkeit damit eine 4 zu würfeln ist $$\\frac{1}{6}$$. Diese Wahrscheinlichkeit verändert sich jedoch, wenn wir wissen, ob eine gerade oder ungerade Zahl gewürfelt wurde:\n- Wurde eine gerade Zahl gewürfelt, ist die Wahrscheinlichkeit, dass diese Zahl eine 4 ist, nur noch $$\\frac{1}{3}$$.\n- Ist hingegen bekannt, dass die gewürfelte Zahl ungerade ist, dann ist die 4 ausgeschlossen - die Wahrscheinlichkeit liegt bei 0.\n\nWir sagen: Die bedingte Wahrscheinlichkeit eine 4 zu würfeln, bedingt auf das Ereignis, dass eine gerade Zahl gewürfelt wurde, ist $$\\frac{1}{3}$$.\n\n\n\nBedingte Wahrscheinlichkeit spielt also eine Rolle, wenn mehrere Ereignisse eintreten. Das Wissen über die bedingten Wahrscheinlichkeiten zweier Ereignisse, hilft uns dabei zu bestimmen, ob eins der Ereignisse vom anderen abhängig ist. Bedingte Wahrscheinlichkeiten erlauben uns daher bspw. vorherzusagen, wie wahrscheinlich der Eintritt eines Ereignisses ist, unter der Annahme dass ein anderes Ereignis bereits eingetreten ist.',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/notation-both-events-occured.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/notation-both-events-occured.json',
        },
      ],
      type: ['LearningResource', 'Hint'],
      name: 'Both events did occur or will have occured!',
      localizedName: {
        en: 'Both events did occur or will have occured!',
        de: 'Beide Ereignisse sind eingetreten oder werden eingetreten sein!',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-markdown+json',
        },
      ],
    },
    content: {
      en: 'Conditional probability looks at a possible future, where both events will have occured. The notation $P\\left(A\\middle|B\\right)$ is thus only a shortened version of $P\\left(A\\cap B\\middle|B\\right)!',
      de: 'Die bedingte Wahrscheinlichkeit betrachtet eine Realität, innerhalb derer beide Ereignisse eingetreten sind oder eintreten werden. Die Schreibweise $P\\left(A\\middle|B\\right)$ ist daher eigentlich nur eine verkürzte Schreibweise für $P\\left(A\\cap B\\middle|B\\right)$!',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/notation-full.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/notation-full.json',
        },
      ],
      type: ['LearningResource', 'Notation'],
      name: 'Notation of Conditional Probability',
      localizedName: {
        en: 'Notation of Conditional Probability',
        de: 'Schreibweise für bedingte Wahrscheinlichkeit',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-markdown+json',
        },
      ],
    },
    content: {
      en: 'The probability of the event A given B is notated as $P\\left(A\\middle|B\\right)$.',
      de: 'Die Wahrscheinlichkeit des Ereignisses A unter der Voraussetzung B hat die Schreibweise $P\\left(A\\middle|B\\right)$.',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/notation-given.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/notation-given.json',
        },
      ],
      type: ['LearningResource', 'Notation'],
      name: 'Notation for Conditional Probability: Given',
      localizedName: {
        en: 'Notation for Conditional Probability: Given',
        de: 'Schreibweise für bedingte Wahrscheinlichkeit: "Vorausgesetzt"',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-text+json',
        },
      ],
    },
    content: {
      en: 'The vertical line within the conditional probability notation is pronounced "given" or "conditionally given".',
      de: 'Die vertikale Linie innerhalb der Schreibweise der bedingten Wahrscheinlichkeit wird als "vorausgesetzt" oder "bedingt durch" ausgesprochen.',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/probability-given-cp-equation.md',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/probability-given-cp-equation.json',
        },
      ],
      type: ['LearningResource', 'Equation'],
      name: 'Equation for Calculating Probability from Conditional Probability',
      localizedName: {
        en: 'Equation for Calculating Probability from Conditional Probability',
        de: 'Formel zur Berechnung einer Wahrscheinlichkeit anhand bedingter Wahrscheinlichkeit',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/markdown',
        },
      ],
    },
    content:
      '$$P\\left(A\\right)=\\frac{P\\left(B \\cap A\\right)}{P\\left(B\\middle|A\\right)}$$\r\n\r\n$$P\\left(B\\right)=\\frac{P\\left(A \\cap B\\right)}{P\\left(A\\middle|B\\right)}$$\r\n',
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/reduced-sample-space.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/reduced-sample-space.json',
        },
      ],
      type: ['LearningResource', 'Explanation', 'Hint'],
      name: 'Reduction of the sample space',
      localizedName: {
        en: 'Reduction of the sample space',
        de: 'Reduktion der Ergebnismenge bei bedingten Wahrscheinlichkeiten',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-markdown+json',
        },
      ],
    },
    content: {
      en: 'For the calculation of the conditional probability, one can imagine that the total set of possible outcomes Ω is reduced.\n\nIf, for example, one wants to calculate the conditional probability for event A with condition B - i.e. P(A|B) - only all elements of event B are considered. Since condition B has occurred, all elements outside of B (i.e., all elements in B̅) are impossible. For the elements of A this means: all elements of A, which are not also in B, play no role for the calculation of P(A|B).',
      de: 'Für die Berechnung der bedingten Wahrscheinlichkeit kann man sich vorstellen, dass die Gesamtmenge der möglichen Ergebnisse Ω reduziert wird.\n\nMöchte man bspw. die bedingte Wahrscheinlichkeit für das Ereignis A mit der Bedingung B - also P(A|B) - errechnen, so werden nur noch alle Elemente des Ereignisses B betrachtet. Da die Bedingung B eingetreten ist, sind alle Elemente außerhalb von B (also alle Elemente in B̅) unmöglich. Für die Elemente von A bedeutet dies: alle Elemente von A, die nicht auch in B sind, spielen für die Berechnung von P(A|B) keine Rolle.',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/studyflix-bedingte-wahrscheinlichkeit.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/studyflix-bedingte-wahrscheinlichkeit.json',
        },
      ],
      type: ['LearningResource', 'VideoObject'],
      name: 'Conditional Probability',
      localizedName: {
        en: 'Conditional Probability',
        de: 'Bedingte Wahrscheinlichkeit',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/video',
        },
      ],
      encoding: [
        {
          encodingFormat: 'application/x.embedded-video-script',
        },
      ],
    },
    content: {
      src: 'https://studyflix.de/embed',
      'data-id': '1110',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/total-probability/conditions.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/total-probability/conditions.json',
        },
      ],
      type: ['LearningResource', 'Hint'],
      name: 'Conditions for Total Probability',
      localizedName: {
        en: 'Conditions for Total Probability',
        de: 'Bedingungen für den Satz der totalen Wahrscheinlichkeit',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-text+json',
        },
      ],
    },
    content: {
      en: 'A prerequisite for the use of the total probability theorem is that the conditional events are disjoint on the one hand and exhaustive in total on the other hand.',
      de: 'Voraussetzung für die Verwendung des Satzes der totalen Wahrscheinlichkeit ist, dass die bedingenden Ereignisse einerseits disjunkt und andererseits insgesamt erschöpfend sind.',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/total-probability/disjointing-complement.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/total-probability/disjointing-complement.json',
        },
      ],
      type: ['LearningResource', 'Hint'],
      name: 'Total Probability from Joint Probabilities (Complementary Events)',
      localizedName: {
        en: 'Total Probability from Complementary Events (Complementary Events)',
        de: 'Satz der totalen Wahrscheinlichkeit und gemeinsame Wahrscheinlichkeiten (Komplementärereignis)',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-markdown+json',
        },
      ],
    },
    content: {
      en: 'An event B and its complementary event B̅ form a disjoint decomposition. Therefore, the following holds for the total probability of another event A: \n\n\n\n$$P(A) = P(A \\cap B) + P(A \\cap B̅)$$',
      de: 'Ein Ereignis B und dessen Komplementärereignis B̅ bilden eine disjunkte Zerlegung. Daher gilt für die Gesamtwahrscheinlichkeit eines anderen Ereignisses A: \n\n\n\n$$P(A) = P(A \\cap B) + P(A \\cap B̅)$$',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/total-probability/disjointing-multiple.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/total-probability/disjointing-multiple.json',
        },
      ],
      type: ['LearningResource', 'Hint'],
      name: 'Total Probability from Joint Probabilities (Disjoint Events)',
      localizedName: {
        en: 'Total Probability from Joint Probabilities (Disjoint Events)',
        de: 'Satz der totalen Wahrscheinlichkeit und gemeinsame Wahrscheinlichkeiten (disjunkte Ereignisse)',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-markdown+json',
        },
      ],
    },
    content: {
      en: 'If several events X, Y and Z form a disjoint decomposition then the following holds for the total probability of another event A: \n\n\n$$P(A) = P(A \\cap X) + P(A \\cap Y) + P(A \\cap Z)$$',
      de: 'Bilden mehrere Ereignisse X, Y und Z eine disjunkte Zerlegung so gilt für die Gesamtwahrscheinlichkeit eines anderen Ereignisses A: \n\n\n\n$$P(A) = P(A \\cap X) + P(A \\cap Y) + P(A \\cap Z)$$',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/total-probability/equation-complement.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/total-probability/equation-complement.json',
        },
      ],
      type: ['LearningResource', 'Equation'],
      name: 'Equation for Total Probability (Complementary Events)',
      localizedName: {
        en: 'Equation for Total Probability (Complementary Events)',
        de: 'Formel für Satz der totalen Wahrscheinlichkeit (Komplementärereignis)',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-markdown+json',
        },
      ],
    },
    content: {
      en: '$$P(A) = P(A \\cap B) + P(A \\cap B̅) = P(A|B) \\cdot P(B) + P(A|B̅) \\cdot P(B̅)$$',
      de: '$$P(A) = P(A \\cap B) + P(A \\cap B̅) = P(A|B) \\cdot P(B) + P(A|B̅) \\cdot P(B̅)$$',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/total-probability/equation-from-multiplication-rule.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/total-probability/equation-from-multiplication-rule.json',
        },
      ],
      type: ['LearningResource', 'Hint'],
      name: 'Total Probability from Conditional Probabilities',
      localizedName: {
        en: 'Total Probability from Conditional Probabilities',
        de: 'Satz der totalen Wahrscheinlichkeit und bedingte Ereignisse',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-markdown+json',
        },
      ],
    },
    content: {
      en: 'By using the multiplication rule for any 2 events, conditional probabilities can be used instead of joint probabilities.',
      de: 'Durch die Verwendung der Multiplikationsregel für 2 beliebige Ereignisse können statt der gemeinsamen Wahrscheinlichkeiten auch bedingte Wahrscheinlichkeiten verwendet werden.',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/total-probability/equation-multiple.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/total-probability/equation-multiple.json',
        },
      ],
      type: ['LearningResource', 'Equation'],
      name: 'Equation for Total Probability (Disjunct Events)',
      localizedName: {
        en: 'Equation for Total Probability (Disjunct Events)',
        de: 'Formel für Satz der totalen Wahrscheinlichkeit (disjunkte Ereignisse)',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-markdown+json',
        },
      ],
    },
    content: {
      en: '$$P(A) = P(A \\cap X) + P(A \\cap Y) + P(A \\cap Z) = P(A|X) \\cdot P(X) + P(A|Y) \\cdot P(Y) + P(A|Z) \\cdot P(Z)$$',
      de: '$$P(A) = P(A \\cap X) + P(A \\cap Y) + P(A \\cap Z) = P(A|X) \\cdot P(X) + P(A|Y) \\cdot P(Y) + P(A|Z) \\cdot P(Z)$$',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/total-probability/introduction.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/total-probability/introduction.json',
        },
      ],
      type: ['LearningResource', 'Introduction'],
      name: 'Total Probability',
      localizedName: {
        en: 'Total Probability',
        de: 'Satz der totalen Wahrscheinlichkeit',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-markdown+json',
        },
      ],
    },
    content: {
      en: 'In some situations, the probability of an event P(A) is not known, but a set of conditional or joint probabilities of the event is known. Under certain conditions, the total probability P(A) (also called "total probability") can be calculated from the conditional or joint probabilities using the so-called total probability theorem.',
      de: 'In manchen Situationen ist die Wahrscheinlichkeit eines Ereignisses P(A) nicht bekannt, dafür aber eine Gruppe bedingter oder gemeinsamer Wahrscheinlichkeiten des Ereignisses. Unter bestimmten Voraussetzungen kann man die Gesamtwahrscheinlichkeit P(A) (auch "totale Wahrscheinlichkeit" genannt) anhand der bedingten bzw. gemeinsamen Wahrscheinlichkeiten mithilfe des sogenannten Satzes der totalen Wahrscheinlichkeit berechnen.',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/venn-area-equation.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/venn-area-equation.json',
        },
      ],
      type: ['LearningResource', 'Equation'],
      name: 'Equation of Conditional Probability and Areas in Venn Diagrams',
      localizedName: {
        en: 'Equation of Conditional Probability and Areas in Venn Diagrams',
        de: 'Formel für bedingte Wahrscheinlichkeit und Flächen in Venndiagrammen',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-markdown+json',
        },
      ],
      about: [
        {
          id: 'https://kiwi-al.edu/vocab/conditional-probability',
          type: 'Concept',
          prefLabel: {
            en: 'Conditional Probability',
            de: 'Bedingte Wahrscheinlichkeit',
          },
        },
      ],
    },
    content: {
      en: '$$P\\left(A\\middle|B\\right)=\\frac{P\\left(A \\cap B\\right)}{P\\left(B\\right)}=\\frac{\\text{area of }A \\cap B}{\\text{area of }B}$$',
      de: '$$P\\left(A\\middle|B\\right)=\\frac{P\\left(A \\cap B\\right)}{P\\left(B\\right)}=\\frac{\\text{Flaeche von }A \\cap B}{\\text{Flaeche von }B}$$',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/venn-area.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/venn-area.json',
        },
      ],
      type: ['LearningResource', 'Explanation', 'Hint'],
      name: 'Conditional Probability and Area Ratios in Venn Diagrams',
      localizedName: {
        en: 'Conditional Probability and Area Ratios in Venn Diagrams',
        de: 'Bedingte Wahrscheinlichkeit und Flächenverhältnisse in Venndiagrammen',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-text+json',
        },
      ],
    },
    content: {
      en: 'In an area-proportional Venn diagram, the conditional probability of an event A (represented by a full circle) represents the ratio of the intersection (the area, where both circles overlap) to the frequency of the other event B (the area of the other circle). The smaller the intersection area is in comparison to the full circle of B, the smaller is the chance of A happening when B occured, in other words: the smaller is the conditional probability of A given B.',
      de: 'Bedingte Wahrscheinlichkeiten können grob in flächengetreuen Venndiagrammen identifiziert werden. Die bedingte Wahrscheinlichkeit P(A|B) entspricht dem Verhältnis aus der Fläche der Schnittmenge von A und B (die Fläche, in der sich die zwei Kreise von A und B überschneiden) zur Fläche von B (repräsentiert durch die Fläche des Kreises von B). Je kleiner die Schnittfläche im Vergleich zur Fläche des Kreises von B, desto geringer ist die bedingte Wahrscheinlichkeit P(A|B).',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/conditional-probability/vocabulary.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/conditional-probability/vocabulary.json',
        },
      ],
      name: 'Conditional Probability Vocabulary',
      localizedName: {
        en: 'Conditional Probability Vocabulary',
        de: 'Vokabular für bedingte Wahrscheinlichkeit',
      },
      inLanguage: ['de', 'en'],
      type: ['LearningResource', 'Vocabulary'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/data',
        },
      ],
      encoding: [
        {
          encodingFormat: 'application/x.jskos',
        },
      ],
    },
    content: {
      uri: 'https://kiwi-al.edu/vocab/',
      concepts: [
        {
          '@context': 'https://gbv.github.io/jskos/context.json',
          uri: 'https://kiwi-al.edu/vocab/conditional-probability',
          inScheme: [
            {
              uri: 'https://kiwi-al.edu/vocab/',
            },
          ],
          type: ['http://www.w3.org/2004/02/skos/core#Concept'],
          definition: {
            en: [
              'A conditional probability is the probability of an event, given some other event has already occurred. As such, the probability of the event depends on the other event. In other words: it is conditioned on it.',
            ],
            de: [
              'Eine bedingte Wahrscheinlichkeit ist die Wahrscheinlichkeit eines Ereignisses unter der Voraussetzung, dass ein anderes Ereignis eingetreten ist.',
            ],
          },
          prefLabel: {
            en: 'Conditional Probability',
            de: 'Bedingte Wahrscheinlichkeit',
          },
          example: {
            en: [
              'The probability of a person leaving the house may depend on the probability of having sunny weather.',
              'The probability of a person leaving the house given that it is storming outside, is very low.',
            ],
            de: [
              'Tim mag es sonnig. Die Wahrscheinlichkeit, dass er aus dem Haus geht, ist höher, wenn es gutes Wetter gibt.',
              'Die Wahrscheinlichkeit an Diabetis zu erkranken hängt von verschiedenen Faktoren ab. So haben bspw. Menschen mit übermäßigem Zuckerkonsum ein größeres Risiko an Diabetis zu erkranken.',
              'Sarah Beatson, die Starspielerin einer Amateurvolleyballmannschaft, hat sich verletzt. Die lokalen Sportexpert_innen schätzen die Chancen, dass ihr Team am kommenden Samstag gewinnen könnte, nun geringer ein.',
            ],
          },
        },
        {
          '@context': 'https://gbv.github.io/jskos/context.json',
          uri: 'https://kiwi-al.edu/vocab/conditional-probability-given',
          inScheme: [
            {
              uri: 'https://kiwi-al.edu/vocab/',
            },
          ],
          type: ['http://www.w3.org/2004/02/skos/core#Concept'],
          definition: {
            en: [
              "The term 'given', when used in a conditional probability, states that a probability of an event occuring depends on the occurance of another event.",
            ],
            de: [
              "Bei bedingten Wahrscheinlichkeiten sagt das Wort 'vorausgesetzt' aus, dass die Wahrscheinlichkeit des Eintretens eines Ereignisses vom Eintritt eines anderen Ereignisses abhängig ist.",
            ],
          },
          prefLabel: {
            en: 'Given',
            de: 'Vorausgesetzt',
          },
          altLabel: {
            en: ['|'],
            de: ['|'],
          },
          example: {
            en: ['The probability of a person leaving the house given that it is storming outside, is very low.'],
          },
        },
        {
          '@context': 'https://gbv.github.io/jskos/context.json',
          uri: 'https://kiwi-al.edu/vocab/probability-tree-diagram',
          inScheme: [
            {
              uri: 'https://kiwi-al.edu/vocab/',
            },
          ],
          type: ['http://www.w3.org/2004/02/skos/core#Concept'],
          definition: {
            en: [
              'A probability tree diagram is a visualization that shows sequences of events in the form of a tree, where the conditioning event is displayed as the parent of the conditioned event.',
            ],
            de: [
              'Ein Wahrscheinlichkeitsbaum ist eine Visualisierung, welche die Sequenz bedingter Ereignisse in Form eines Baumes darstellt. Vorausgesetzte Ereignisse sind als Eltern der bedingten Ereignisse visualisiert.',
            ],
          },
          prefLabel: {
            en: 'Probability Tree Diagram',
            de: 'Wahrscheinlichkeitsbaum',
          },
        },
        {
          '@context': 'https://gbv.github.io/jskos/context.json',
          uri: 'https://kiwi-al.edu/vocab/conditional-probability-table',
          inScheme: [
            {
              uri: 'https://kiwi-al.edu/vocab/',
            },
          ],
          type: ['http://www.w3.org/2004/02/skos/core#Concept'],
          definition: {
            en: [
              'A conditional probability table (CPT) is a tabular visualization that displays the conditional probabilities of a discrete  variable with respect to one or more other variables.',
            ],
            de: [
              'Eine bedingte Wahrscheinlichkeitstabelle ist eine tabellenartige Visualisierung, welche die bedingten Wahrscheinlichkeiten eines diskreten Merkmals unter der Voraussetzung einer oder mehrerer anderer Merkmale darstellt.',
            ],
          },
          prefLabel: {
            en: 'Conditional Probability Table',
            de: 'Bedingte Wahrscheinlichkeitstabelle',
          },
          altLabel: {
            en: ['CPT'],
          },
        },
        {
          '@context': 'https://gbv.github.io/jskos/context.json',
          uri: 'https://kiwi-al.edu/vocab/stacked-bar-chart-for-cps',
          inScheme: [
            {
              uri: 'https://kiwi-al.edu/vocab/',
            },
          ],
          type: ['http://www.w3.org/2004/02/skos/core#Concept'],
          definition: {
            en: [
              'A stacked bar chart is a form of visualizing conditional probabilities. It shows one barstack per given feature, where every barstack is made up of the probabilities conditional on that feature, adding up to 100%.',
            ],
            de: [
              'Ein gestapeltes Balkendiagramm ist eine grafische Visualisierung für bedingte Wahrscheinlichkeiten. Es zeigt einen Balken für jede Ausprägung des bedingenden Merkmals. Jeder Balken ist in die entsprechendenden bedingten Wahrscheinlichkeiten unterteilt, welche sich auf 100% summieren.',
            ],
          },
          prefLabel: {
            en: 'Stacked Bar Chart for Conditional Probabilities',
            de: 'Gestapeltes Balkendiagramm für bedingte Wahrscheinlichkeiten',
          },
          altLabel: {
            en: ['CPT'],
          },
        },
        {
          '@context': 'https://gbv.github.io/jskos/context.json',
          uri: 'https://kiwi-al.edu/vocab/stochastic-independence',
          inScheme: [
            {
              uri: 'https://kiwi-al.edu/vocab/',
            },
          ],
          type: ['http://www.w3.org/2004/02/skos/core#Concept'],
          definition: {
            en: ['Two events are independent, if the occurrance of one event does not effect the probability of another and vice versa.'],
            de: [
              'Zwei Ereignisse sind stochastisch unabhängig, wenn die Eintrittswahrscheinlichkeit des einen Ereignisses keinen Einfluss auf die Eintrittswahrscheinlichkeit des anderen Ereignisses hat und andersrum.',
            ],
          },
          prefLabel: {
            en: 'Stochastic Independence',
            de: 'Stochastische Unabhängigkeit',
          },
          example: {
            en: [
              'The probability of rolling a 3 with a dice is independent of putting down a card with a hearts king.',
              'The probability of winning the lottery in the evening does not depend on showering in the morning.',
            ],
            de: [
              'Die Wahrscheinlichkeit eine 3 zu würfeln ist unabhängig von der Wahrscheinlichkeit eine Herz-König-Karte zu legen.',
              'Die Wahrscheinlichkeit bei der Abendziehung im Lotto zu gewinnen ist unabhängig davon, ob du morgens geduscht hast oder nicht.',
            ],
          },
        },
      ],
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/events/collectively-exhaustive-events.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/events/collectively-exhaustive-events.json',
        },
      ],
      type: ['LearningResource', 'Definition'],
      name: 'Collectively Exhaustive Events',
      localizedName: {
        en: 'Collectively Exhaustive Events',
        de: 'Insgesamt erschöpfende Ereignisse',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-text+json',
        },
      ],
    },
    content: {
      en: 'Several events are "collectively exhaustive" if at least one of the events must occur. The probability that one of the events occurs is 100%.',
      de: 'Mehrere Ereignisse sind "insgesamt erschöpfend" (engl. collectively exhaustive), wenn mindestens eins der Ereignisse eintreten muss. Die Wahrscheinlichkeit, dass eins der Ereignisse auftritt, ist entsprechend 100%.',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/events/disjoint-events.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/events/disjoint-events.json',
        },
      ],
      type: ['LearningResource', 'Definition'],
      name: 'Disjoint Events',
      localizedName: {
        en: 'Disjoint Events',
        de: 'Disjunkte Ereignisse',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-text+json',
        },
      ],
    },
    content: {
      en: 'Two events are disjoint or mutually exclusive if they are mutually exclusive. Accordingly, they have no common elements.',
      de: 'Zwei Ereignisse sind disjunkt (engl. disjoint oder mutually exclusive), wenn sie sich gegenseitich ausschließen. Sie haben entsprechend keine gemeinsamen Elemente.',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/events/multiplication-rule-equation.md',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/events/multiplication-rule-equation.json',
        },
      ],
      type: ['LearningResource', 'Equation'],
      name: 'Multiplication rule for two events',
      localizedName: {
        en: 'Multiplication rule for two events',
        de: 'Multiplikationsregel für zwei beliebige Ereignisse',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/markdown',
        },
      ],
      about: [
        {
          id: 'https://kiwi-al.edu/vocab/conditional-probability',
          type: 'Concept',
          prefLabel: {
            en: 'Multiplication of Two Events',
            de: 'Multiplikation zweier Ereignisse',
          },
        },
      ],
    },
    content:
      '$$P\\left(A \\cap B\\right) = P\\left(A|B\\right) \\cdot P\\left(B\\right)$$\r\n\r\n$$P\\left(A \\cap B\\right) = P\\left(B|A\\right) \\cdot P\\left(A\\right)$$\r\n',
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/events/multiplication-rule-from-conditional-probability-both-events.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/events/multiplication-rule-from-conditional-probability-both-events.json',
        },
      ],
      type: ['LearningResource', 'Explanation', 'Hint'],
      name: 'Multiplication rule can be derived from the definition of conditional probability of both events',
      localizedName: {
        en: 'Multiplication rule can be derived from the definition of conditional probability of both events',
        de: 'Multiplikationsregel und Formeln für bedingte Wahrscheinlichkeit beider Ereignisse',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-markdown+json',
        },
      ],
    },
    content: {
      en: 'Since the intercepts $$A \\cap B$$ and $$B \\cap A$$ and thus their probabilities $$P(A \\cap B)$$ and $$P(B \\cap A)$$ are identical (commutativity), the multiplication rule can be derived from the definitions of the conditional probabilities for both $$P(A|B)$$ and $$P(B|A)$$.',
      de: 'Da die Schnitte $$A \\cap B$$ und $$B \\cap A$$ und somit ihre Wahrscheinlichkeiten $$P(A \\cap B)$$ und $$P(B \\cap A)$$ identisch sind (Kommutativität), kann die Multiplikationsregel sowohl aus den Definitionen der bedingten Wahrscheinlichkeiten für $$P(A|B)$$ als auch $$P(B|A)$$ hergeleitet werden.',
    },
  },
  {
    meta: {
      '@context': [
        'https://w3id.org/kim/amb/draft/context.jsonld',
        {
          '@language': 'en',
        },
      ],
      id: 'http://localhost/data/content/math/probability/events/multiplication-rule-from-conditional-probability-equation.json',
      mainEntityOfPage: [
        {
          id: 'http://localhost/data/meta/math/probability/events/multiplication-rule-from-conditional-probability-equation.json',
        },
      ],
      type: ['LearningResource', 'Explanation', 'Hint'],
      name: 'Relation Between Multiplication Rule for 2 Events and Conditional Probability',
      localizedName: {
        en: 'Relation Between Multiplication Rule for 2 Events and Conditional Probability',
        de: 'Zusammenhang von Multiplikationsregel für zwei beliebige Ereignisse und bedingter Wahrscheinlichkeit',
      },
      inLanguage: ['de', 'en'],
      learningResourceType: [
        {
          id: 'http://w3id.org/openeduhub/vocabs/learningResourceType/text',
        },
      ],
      encoding: [
        {
          encodingFormat: 'text/x.multi-lingual-markdown+json',
        },
      ],
    },
    content: {
      en: 'From the formula for conditional probability follows directly the multiplication rule for any 2 events.\n\nTo do this, the formula $$P\\left(A\\middle|B\\right)=\\frac{P\\left(A \\cap B\\right)}{P\\left(B\\right)}$$ just needs to be converted to $$P(A \\cap B)$$ by multiplying $$P\\left(B\\right)$$.',
      de: 'Aus der Formel für bedingte Wahrscheinlichkeit folgt direkt die Multiplikationsregel für 2 beliebige Ereignisse.\n\nDafür muss die Formel $$P\\left(A\\middle|B\\right)=\\frac{P\\left(A \\cap B\\right)}{P\\left(B\\right)}$$ lediglich durch Multiplikation von $$P\\left(B\\right)$$ nach $$P(A \\cap B)$$ umgestellt werden.',
    },
  },
];

export default content;
