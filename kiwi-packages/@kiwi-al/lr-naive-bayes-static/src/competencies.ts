import type { ICassNodeObject } from '@adlete-semantics/cass-json-ld';

const content: ICassNodeObject[] = [
  {
    "@context": "https://schema.cassproject.org/0.4",
    "@id": "https://cass.fki.htw-berlin.de/api/data/schema.cassproject.org.0.4.Framework/e8ac5941-c455-4a59-9658-9916e0bf8a05/1690381744206",
    "@type": "Framework",
    "competency": [
      "https://cass.fki.htw-berlin.de/api/data/schema.cassproject.org.0.4.Competency/987ba064-c455-48d5-8502-0d3f32835c41",
      "https://cass.fki.htw-berlin.de/api/data/schema.cassproject.org.0.4.Competency/13a1d893-c455-4b31-ab5a-5fdd4bd2f670"
    ],
    "directory": "https://cass.fki.htw-berlin.de/api/data/schema.cassproject.org.0.4.Directory/48a54145-c455-45fe-8bb6-c41c9b841924",
    "level": [
      "https://cass.fki.htw-berlin.de/api/data/schema.cassproject.org.0.4.Level/41c448aa-c455-4f87-9669-9b2063c59039"
    ],
    "name": {
      "@language": "de",
      "@value": "Probability Theory"
    },
    "relation": [
      "https://cass.fki.htw-berlin.de/api/data/schema.cassproject.org.0.4.Relation/58248545-c455-428d-b95e-c8edf4df2b2c"
    ],
    "schema:dateCreated": "2023-07-19T07:22:16.008Z",
    "schema:dateModified": "2023-07-26T14:29:04.206Z"
  },
  {
    "@context": "https://schema.cassproject.org/0.4",
    "@id": "https://cass.fki.htw-berlin.de/api/data/schema.cassproject.org.0.4.Competency/987ba064-c455-48d5-8502-0d3f32835c41/1689752293508",
    "@type": "Competency",
    "name": [
      {
        "@language": "en",
        "@value": "Define Probability (Layman)"
      },
      {
        "@language": "de",
        "@value": "Definiere Wahrscheinlichkeit (Laie)"
      }
    ],
    "schema:dateCreated": "2023-07-19T07:32:59.312Z",
    "schema:dateModified": "2023-07-19T07:38:13.508Z"
  },
  {
    "@context": "https://schema.cassproject.org/0.4",
    "@id": "https://cass.fki.htw-berlin.de/api/data/schema.cassproject.org.0.4.Competency/13a1d893-c455-4b31-ab5a-5fdd4bd2f670/1689754855526",
    "@type": "Competency",
    "name": {
      "@language": "de",
      "@value": "Define Conditional Probability (simple)"
    },
    "schema:dateCreated": "2023-07-19T08:20:39.320Z",
    "schema:dateModified": "2023-07-19T08:20:55.526Z"
  },
  {
    "@context": "https://schema.cassproject.org/0.4",
    "@id": "https://cass.fki.htw-berlin.de/api/data/schema.cassproject.org.0.4.Relation/58248545-c455-428d-b95e-c8edf4df2b2c/1689755028271",
    "@type": "Relation",
    "relationType": "requires",
    "schema:dateCreated": "2023-07-19T08:23:48.270Z",
    "source": "https://cass.fki.htw-berlin.de/api/data/schema.cassproject.org.0.4.Competency/13a1d893-c455-4b31-ab5a-5fdd4bd2f670",
    "target": "https://cass.fki.htw-berlin.de/api/data/schema.cassproject.org.0.4.Competency/987ba064-c455-48d5-8502-0d3f32835c41"
  }
];

export default content;