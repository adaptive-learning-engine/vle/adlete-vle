module.exports = {
  extends: ['@adlete/eslint-config/eslint'],
  parserOptions: {
    project: './tsconfig.json',
    tsconfigRootDir: __dirname,
  },
  rules: {
    'import/no-default-export': 'off',
  },
};
