// eslint-disable-next-line import/no-extraneous-dependencies
import { beforeEach, describe, expect, jest, test } from '@jest/globals';
import type { Statement } from '@xapi/xapi';
import type { Mock } from 'jest-mock';

import { XAPIBackendManager } from '../../src/xapi';

const STATEMENT: Statement = {
  id: 'd6e4b410-c376-470d-a347-c4ebad0cf232',
  actor: {
    name: 'Sally Glider',
    mbox: 'mailto:sally@example.com',
    objectType: 'Agent',
  },
  verb: {
    id: 'http://adlnet.gov/expapi/verbs/experienced',
    display: { 'en-US': 'experienced' },
  },
  object: {
    id: 'http://example.com/activities/solo-hang-gliding',
    objectType: 'Activity',
    definition: {
      name: { 'en-US': 'Solo Hang Gliding' },
      type: 'http://adlnet.gov/expapi/activities/objective',
    },
  },
};

describe('XAPIBackendManager', () => {
  let xAPIBackendManager: XAPIBackendManager;
  let cbReceived: Mock<(statements: Statement[]) => void>;

  beforeEach(() => {
    xAPIBackendManager = new XAPIBackendManager();

    // events
    cbReceived = jest.fn<(statements: Statement[]) => void>();
    xAPIBackendManager.events.addListener('statementsReceived', cbReceived);
  });

  test('emits `statementsReceived` event, after `receive` was called', async () => {
    await xAPIBackendManager.receive([STATEMENT]);
    expect(cbReceived).toBeCalledWith([STATEMENT]);
  });
});
