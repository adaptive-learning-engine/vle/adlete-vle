// eslint-disable-next-line import/no-extraneous-dependencies
import { beforeAll, describe, expect, jest, test } from '@jest/globals';
import { ElasticsearchContainer, StartedElasticsearchContainer } from '@testcontainers/elasticsearch';

import { type Statement, XAPIElasticStorage } from '../../../src/xapi';

const STATEMENT: Statement = {
  id: 'd6e4b410-c376-470d-a347-c4ebad0cf232',
  actor: {
    name: 'Sally Glider',
    mbox: 'mailto:sally@example.com',
    objectType: 'Agent',
  },
  verb: {
    id: 'http://adlnet.gov/expapi/verbs/experienced',
    display: { 'en-US': 'experienced' },
  },
  object: {
    id: 'http://example.com/activities/solo-hang-gliding',
    objectType: 'Activity',
    definition: {
      name: { 'en-US': 'Solo Hang Gliding' },
      type: 'http://adlnet.gov/expapi/activities/objective',
    },
  },
};

describe('XAPIElasticStorage', () => {
  jest.setTimeout(60000);
  let elastic: StartedElasticsearchContainer;
  let xapiStorage: XAPIElasticStorage;
  beforeAll(async () => {
    elastic = await new ElasticsearchContainer().start();
    xapiStorage = new XAPIElasticStorage({ node: elastic.getHttpUrl() });
  });

  test('if statements can be stored', async () => {
    const ids = await xapiStorage.store([STATEMENT]);
    expect(ids).toEqual([STATEMENT.id]);
  });

  test('if statements can be retrieved', async () => {
    const statement = await xapiStorage.get({ statementId: STATEMENT.id });
    expect(statement).toEqual(STATEMENT);
  });
});
