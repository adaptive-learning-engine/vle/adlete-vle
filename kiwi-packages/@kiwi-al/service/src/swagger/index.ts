import fastifySwagger from '@fastify/swagger';
import fastifySwaggerUi from '@fastify/swagger-ui';

import type { IPlugin, IPluginManager, IPluginMeta } from '@fki/plugin-system/specs';

import type { IWithService } from '../core/Service';
import { ServiceLogger } from '../utils/logger';

export interface IWithSwagger extends IWithService {
  swagger: SwaggerPlugin;
}

export const SwaggerMeta: IPluginMeta<keyof IWithSwagger, 'swagger'> = {
  id: 'swagger',
  dependencies: ['service'],
};

export class SwaggerPlugin implements IPlugin<IWithSwagger> {
  public meta: IPluginMeta<keyof IWithSwagger, 'swagger'> = SwaggerMeta;

  public async initialize(pluginMan: IPluginManager<IWithSwagger>): Promise<void> {
    ServiceLogger.info('Initializing Swagger plugin');

    const service = pluginMan.get('service');
    const host = `localhost:${service.port}`;

    const swaggerOptions = {
      swagger: {
        info: {
          title: 'My Title',
          description: 'My Description.',
          version: '1.0.0',
        },
        host,
        schemes: ['http', 'https'],
        consumes: ['application/json'],
        produces: ['application/json'],
        tags: [
          { name: 'Default', description: 'Default' },
          { name: 'swagger', description: 'swagger' },
          { name: 'Recommendation', description: 'Recommendation' },
        ],
      },
    };

    const swaggerUiOptions = {
      routePrefix: '/docs',
      exposeRoute: true,
    };

    await service.server.register(fastifySwagger, swaggerOptions);
    await service.server.register(fastifySwaggerUi, swaggerUiOptions);
  }

  // eslint-disable-next-line class-methods-use-this
  public terminate(): Promise<void> {
    return Promise.resolve();
  }
}
