import { cassObjectsToGraph } from '@adlete-semantics/cass-graph';

import CASS_OBJECTS_ARR from '@kiwi-al/lr-naive-bayes-static/competencies';

// TODO: don't use global variable
export const COMPETENCY_GRAPH = cassObjectsToGraph(CASS_OBJECTS_ARR);
