import { createConsola } from 'consola';

export const ServiceLogger = createConsola({ defaults: { tag: 'Service' } });
