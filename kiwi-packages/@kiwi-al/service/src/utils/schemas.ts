import { FastifySchema } from 'fastify';

export type HTTPMethod = 'get' | 'put' | 'post' | 'delete';

export type RESTSchemaCollection = Record<string, Partial<Record<HTTPMethod, FastifySchema>>>;
