import type { Statement } from '@xapi/xapi';
import { flatten, groupBy } from 'ramda';

import { getActorIFIasString } from '@adlete-semantics/xapi';

import {
  createAssertionsFromStatement,
  isAssertionStatement,
  updateCompetencyLearnerModel,
} from '@kiwi-al/pl-engine/competencies/assertions';

import { ServiceLogger } from '../utils/logger';
import { XAPIBackendManager } from '../xapi/XAPIBackendManager';

import { LearnerModelManager } from './LearnerModelManager';
import { ILearnerModels } from './create-learner-models';

export class LearnerModelUpdateManager {
  protected xAPIMan: XAPIBackendManager;

  protected learnerModelMan: LearnerModelManager<ILearnerModels>;

  public constructor(learnerModelMan: LearnerModelManager<ILearnerModels>, xAPIMan: XAPIBackendManager) {
    this.learnerModelMan = learnerModelMan;
    this.xAPIMan = xAPIMan;
    this.onStatementsReceived = this.onStatementsReceived.bind(this);
    this.xAPIMan.events.on('statementsReceived', this.onStatementsReceived);
  }

  public destruct() {
    this.xAPIMan.events.off('statementsReceived', this.onStatementsReceived);
  }

  protected async onStatementsReceived(statements: Statement[]) {
    const learnerStatements = groupBy((statement) => getActorIFIasString(statement.actor), statements);

    const learnerIds = Object.keys(learnerStatements);
    const learnerStatementsArr = Object.values(learnerStatements);

    const learnerModels = await Promise.all(learnerIds.map((learnerId) => this.learnerModelMan.getLearnerModels(learnerId)));

    // update using assertion statements
    learnerIds.forEach((learnerId, i) => {
      const competencyModel = learnerModels[i].competencies;
      const assertionStatements = learnerStatementsArr[i].filter(isAssertionStatement);
      const assertions = flatten(assertionStatements.map((statement) => createAssertionsFromStatement(statement)));

      if (assertions.length > 0) {
        ServiceLogger.info(`Updating learner models for learner with id '${learnerId}'`);
        updateCompetencyLearnerModel(competencyModel, assertions);
      }
    });
  }
}
