import type { IPlugin, IPluginManager, IPluginMeta } from '@fki/plugin-system/specs';

import { ServiceLogger } from '../utils/logger';
import type { IWithXAPI } from '../xapi';

import { LearnerModelManager } from './LearnerModelManager';
import { LearnerModelUpdateManager } from './LearnerModelUpdateManager';
import { createLearnerModels, ILearnerModels } from './create-learner-models';

export async function retrieveLearnerModels(learnerId: string): Promise<ILearnerModels> {
  return createLearnerModels(learnerId);
}

export interface IWithAnalytics extends IWithXAPI {
  analytics: AnalyticsPlugin;
}

export const AnalyticsMeta: IPluginMeta<keyof IWithAnalytics, 'analytics'> = {
  id: 'analytics',
  dependencies: ['xAPI'],
};

export class AnalyticsPlugin implements IPlugin<IWithAnalytics> {
  public meta: IPluginMeta<keyof IWithAnalytics, 'analytics'> = AnalyticsMeta;

  public learnerModelManager: LearnerModelManager<ILearnerModels>;

  public learnerModelUpdateManager: LearnerModelUpdateManager;

  public initialize(pluginMan: IPluginManager<IWithAnalytics>): Promise<void> {
    ServiceLogger.info('Initializing analytics plugin');

    const xAPIBackend = pluginMan.get('xAPI').backend;

    this.learnerModelManager = new LearnerModelManager({ retrieveLearnerModels });
    this.learnerModelUpdateManager = new LearnerModelUpdateManager(this.learnerModelManager, xAPIBackend);

    return Promise.resolve();
  }

  // eslint-disable-next-line class-methods-use-this
  public terminate(): Promise<void> {
    return Promise.resolve();
  }
}
