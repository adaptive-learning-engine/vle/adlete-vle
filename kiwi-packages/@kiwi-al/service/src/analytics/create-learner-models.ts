import { CompetencyLearnerModel } from '@kiwi-al/pl-engine/analytics';

import { COMPETENCY_GRAPH } from '../shared/competencies';
import { ServiceLogger } from '../utils/logger';

export interface ILearnerModels {
  competencies: CompetencyLearnerModel<number>;
}

export function createLearnerModels(learnerId: string): ILearnerModels {
  ServiceLogger.info(`Creating learner models for learner with id '${learnerId}'`);
  return { competencies: new CompetencyLearnerModel<number>(COMPETENCY_GRAPH) };
}
