export type RetrieveLearnerModelsCB<TLearnerModels> = (learnerId: string) => Promise<TLearnerModels>;

export interface ILearnerModelManagerConfig<TLearnerModels> {
  retrieveLearnerModels: RetrieveLearnerModelsCB<TLearnerModels>;
}

export class LearnerModelManager<TLearnerModels> {
  protected _learnerModels: Record<string, TLearnerModels>;

  protected _config: ILearnerModelManagerConfig<TLearnerModels>;

  public constructor(config: ILearnerModelManagerConfig<TLearnerModels>) {
    this._config = config;
    this._learnerModels = {};
  }

  public async getLearnerModels(learnerId: string): Promise<TLearnerModels> {
    // TODO: retrieve old user model from server - make sure that other calls to this
    // function will wait for the same promise!
    if (!this._learnerModels[learnerId]) {
      this._learnerModels[learnerId] = await this._config.retrieveLearnerModels(learnerId);
    }

    return this._learnerModels[learnerId];
  }
}
