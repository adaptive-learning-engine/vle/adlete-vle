import type { IPlugin, IPluginManager, IPluginMeta } from '@fki/plugin-system/specs';

import type { IWithService } from '../core/Service';
import { ServiceLogger } from '../utils/logger';

import { XAPIBackendManager } from './XAPIBackendManager';
import { IXAPIElasticStorageOptions, XAPIElasticStorage } from './XAPIStorage';
import { registerRoutes } from './routes';

export interface IWithXAPI extends IWithService {
  xAPI: XAPIPlugin;
}

export const XAPIMeta: IPluginMeta<keyof IWithXAPI, 'xAPI'> = {
  id: 'xAPI',
  dependencies: ['service'],
};

export interface IXAPIPluginOptions {
  elasticStorage: IXAPIElasticStorageOptions;
}

export class XAPIPlugin implements IPlugin<IWithXAPI> {
  public meta: IPluginMeta<keyof IWithXAPI, 'xAPI'> = XAPIMeta;

  public storage: XAPIElasticStorage;

  public backend: XAPIBackendManager;

  public constructor(protected xapiOptions: IXAPIPluginOptions) {}

  public async initialize(pluginMan: IPluginManager<IWithXAPI>): Promise<void> {
    ServiceLogger.info('Initializing xAPI plugin');

    this.storage = new XAPIElasticStorage(this.xapiOptions.elasticStorage);
    this.backend = new XAPIBackendManager();

    const server = pluginMan.get('service').server;

    await registerRoutes(server, this.storage, this.backend);
  }

  // eslint-disable-next-line class-methods-use-this
  public terminate(): Promise<void> {
    return Promise.resolve();
  }
}
