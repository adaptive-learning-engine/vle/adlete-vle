import type { JSONSchema7 } from 'json-schema';
import xapiSchemaOriginal from 'tin-can-json-schema/1.0.1.json' assert { type: 'json' };
import xapiFormatsOriginal from 'tin-can-json-schema/formats.json' assert { type: 'json' };

// ============================= Schema

export const XAPI_SCHEMA: JSONSchema7 = xapiSchemaOriginal as unknown;

XAPI_SCHEMA.$id = 'http://kiwi-xapi/';
delete XAPI_SCHEMA.$schema;

function recursiveReplaceId(obj: JSONSchema7) {
  if ('id' in obj && typeof obj.id === 'string') {
    obj.$id = obj.id;
    delete obj.id;
  }

  Object.values(obj).forEach((prop) => {
    if (typeof prop === 'object') {
      recursiveReplaceId(prop);
    }
  });
}

recursiveReplaceId(XAPI_SCHEMA.properties);

Object.values(XAPI_SCHEMA.properties).forEach((prop) => {
  if (typeof prop === 'object' && prop.patternProperties) {
    delete prop.patternProperties;
    // const oldPP = prop['patternProperties'];
    // const newPP = {};

    // for (let key in oldPP) {
    //   console.log('=================');
    //   console.log(key);
    //   // const newKey = key.replace(/\\\\/g, '\\');
    //   const newKey = 'foo';
    //   new RegExp(newKey);
    //   newPP[newKey] = oldPP[key];
    // }

    // prop['patternProperties'] = newPP;

    // console.log(prop['patternProperties']);
  }
});

// ============================= Formats

export type JSONSchemaFormats = Record<string, string>;

export const XAPI_SCHEMA_FORMATS: JSONSchemaFormats = xapiFormatsOriginal;

// eslint-disable-next-line no-restricted-syntax, guard-for-in
for (const format in XAPI_SCHEMA_FORMATS) {
  XAPI_SCHEMA_FORMATS[format] = XAPI_SCHEMA_FORMATS[format].replace(/\\\\/g, '\\');
}
