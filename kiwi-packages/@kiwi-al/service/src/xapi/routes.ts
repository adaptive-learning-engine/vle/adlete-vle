import type { GetStatementParams, Statement } from '@xapi/xapi';
import type { FastifyInstance } from 'fastify';

import { addMissingIds } from '@adlete-semantics/xapi';

import { ServiceLogger } from '../utils/logger';
import { RESTSchemaCollection } from '../utils/schemas';

import { XAPIBackendManager } from './XAPIBackendManager';
import { XAPIElasticStorage } from './XAPIStorage';
import { XAPI_SCHEMA } from './json-schema';

export const SCHEMAS: RESTSchemaCollection = {
  '/xAPI/statements': {
    get: {
      tags: ['xAPI'],
      querystring: {
        type: 'object',
        properties: {
          statementId: { type: 'string' },
        },
      },
      // TODO: proper response validation ?
      response: {
        // 200: {
        //   $ref: 'http://kiwi-xapi/#statement',
        // },
      },
    },
    post: {
      tags: ['xAPI'],
      // body: {
      //   $ref: 'http://kiwi-xapi/#statement',
      // },
      response: {
        200: {
          description: 'Successfully added statement',
          type: 'array',
          items: {
            type: 'string',
          },
        },
      },
    },
  },
};

export async function registerRoutes(
  server: FastifyInstance,
  xAPIStorage: XAPIElasticStorage,
  xAPIBackend: XAPIBackendManager
): Promise<void> {
  ServiceLogger.info('Registering xAPI routes');
  server.addSchema(XAPI_SCHEMA);

  await server.register(async () => {
    server.get('/xAPI/statements', { schema: SCHEMAS['/xAPI/statements'].get }, async (req) => {
      const query = req.query as GetStatementParams;
      return xAPIStorage.get(query);
    });

    server.post('/xAPI/statements', { schema: SCHEMAS['/xAPI/statements'].post }, async (req) => {
      const statements = (Array.isArray(req.body) ? req.body : [req.body]) as Statement[];
      const statementsWithId = addMissingIds(statements);

      xAPIBackend.receive(statementsWithId);
      return xAPIStorage.store(statementsWithId);
    });
  });
}
