import type { Statement } from '@xapi/xapi';

import { ITypedEventEmitter, ITypedEventEmitterNoEmit, TypedEventEmitter } from '@adlete-utils/events';

export interface IXAPIBackendEvents {
  statementsReceived: Statement[];
}

export class XAPIBackendManager {
  protected _events: ITypedEventEmitter<IXAPIBackendEvents>;

  public get events(): ITypedEventEmitterNoEmit<IXAPIBackendEvents> {
    return this._events;
  }

  public constructor() {
    this._events = new TypedEventEmitter();
  }

  public receive(statements: Statement[]): void {
    this._events.emit('statementsReceived', statements);
  }
}
