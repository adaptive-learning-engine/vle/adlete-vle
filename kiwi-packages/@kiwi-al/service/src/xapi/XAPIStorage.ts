import { Client, type ClientOptions } from '@elastic/elasticsearch';
import type { GetStatementParams, Statement, StatementsResponse } from '@xapi/xapi';

const INDEX_STATEMENTS = 'xapi-statements';

export function ensureStatementIds(statements: Statement[]): void {
  if (!statements.every((statement) => 'id' in statement)) throw new Error('Not all statements have an id set!');
}

export interface IXAPIElasticStorageOptions extends ClientOptions {}

export class XAPIElasticStorage {
  protected client: Client;

  public constructor(options: IXAPIElasticStorageOptions) {
    this.client = new Client(options);
  }

  // protected storePreprocess(statements: Statement | Statement[]): [Statement[], string[]] {
  //   if (Array.isArray(statements)) {
  //     const uuids = statements.map(() => uuidV4());
  //     return [statements, uuids];
  //   }

  //   return [[statements], [uuidV4()]];
  // }

  public async store(statements: Statement[]): Promise<string[]> {
    ensureStatementIds(statements);

    // TODO: handle bulk response
    /* const response = */ await this.client.helpers.bulk({
      datasource: statements,
      onDocument(doc) {
        return {
          index: { _index: INDEX_STATEMENTS, _id: doc.id },
        };
      },
    });

    return statements.map((statement) => statement.id);
  }

  public async get(options: GetStatementParams): Promise<Statement | StatementsResponse> {
    if (options.statementId) {
      const response = await this.client.getSource<Statement>({ index: INDEX_STATEMENTS, id: options.statementId });
      return response;
    }

    throw new Error('Unsupported GetStatementParams');
  }
}
