import { cassObjectsToGraph } from '@adlete-semantics/cass-graph';

import CASS_OBJECTS_ARR from '@kiwi-al/lr-naive-bayes-static/competencies';
import { CompetencyRecommender } from '@kiwi-al/pl-engine/recommendation';

export function createCompetencyRecommender(): CompetencyRecommender {
  const graph = cassObjectsToGraph(CASS_OBJECTS_ARR);
  return new CompetencyRecommender(graph);
}
