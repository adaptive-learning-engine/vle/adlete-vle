import type { IPlugin, IPluginManager, IPluginMeta } from '@fki/plugin-system/specs';

import { CompetencyRecommender } from '@kiwi-al/pl-engine/recommendation';

import type { IWithService } from '../core/Service';
import { COMPETENCY_GRAPH } from '../shared/competencies';
import { ServiceLogger } from '../utils/logger';

import { registerRoutes } from './routes';

export interface IWithRecommendation extends IWithService {
  recommendation: RecommendationPlugin;
}

export const RecommendationMeta: IPluginMeta<keyof IWithRecommendation, 'recommendation'> = {
  id: 'recommendation',
  dependencies: ['service'],
};

export class RecommendationPlugin implements IPlugin<IWithRecommendation> {
  public meta: IPluginMeta<keyof IWithRecommendation, 'recommendation'> = RecommendationMeta;

  public competencyRecommender: CompetencyRecommender;

  public async initialize(pluginMan: IPluginManager<IWithRecommendation>): Promise<void> {
    ServiceLogger.info('Initializing Recommendation plugin');
    this.competencyRecommender = new CompetencyRecommender(COMPETENCY_GRAPH);

    const server = pluginMan.get('service').server;

    await registerRoutes(server, this.competencyRecommender);
  }

  // eslint-disable-next-line class-methods-use-this
  public terminate(): Promise<void> {
    return Promise.resolve();
  }
}
