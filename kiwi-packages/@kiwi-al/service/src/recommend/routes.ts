import type { FastifyInstance } from 'fastify';

import type { CompetencyRecommender } from '@kiwi-al/pl-engine/recommendation';

import { ServiceLogger } from '../utils/logger';
import { RESTSchemaCollection } from '../utils/schemas';

export const SCHEMAS: RESTSchemaCollection = {
  '/recommendation/competency': {
    get: {
      tags: ['Recommendation'],
      querystring: {
        type: 'object',
        properties: {
          userId: { type: 'string' },
        },
      },
      // TODO: proper response validation ?
      response: {
        // 200: {
        //   $ref: 'http://kiwi-xapi/#statement',
        // },
      },
    },
  },
};

export async function registerRoutes(server: FastifyInstance, competencyRecommender: CompetencyRecommender): Promise<void> {
  ServiceLogger.info('Registering recommendation routes');

  await server.register(async () => {
    server.get('/recommendation/competency', { schema: SCHEMAS['/recommendation/competency'].get }, async () =>
      competencyRecommender.recommend()
    );
  });
}
