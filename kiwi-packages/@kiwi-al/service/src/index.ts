import { Service } from './core/Service';

const service = new Service({ port: 2999 });

await service.start();

// TODO: move to second initialization phase of plugin system
await service.listen();
