import { createInstantFactory } from '@fki/plugin-system';
import type { IPluginFactory } from '@fki/plugin-system/specs';

import { AnalyticsMeta, AnalyticsPlugin } from '../analytics';
import { RecommendationMeta, RecommendationPlugin } from '../recommend';
import { SwaggerMeta, SwaggerPlugin } from '../swagger';
import { IXAPIPluginOptions, XAPIMeta, XAPIPlugin } from '../xapi';

import type { IServicePlugins } from './Service';

// TODO: retrieve options from somewhere else, e.g. ENV
const xapiOptions: IXAPIPluginOptions = {
  elasticStorage: { node: 'http://localhost:9200' },
};

export const FACTORIES: IPluginFactory<IServicePlugins, keyof IServicePlugins>[] = [
  createInstantFactory(AnalyticsMeta, () => new AnalyticsPlugin()),
  createInstantFactory(RecommendationMeta, () => new RecommendationPlugin()),
  createInstantFactory(SwaggerMeta, () => new SwaggerPlugin()),
  createInstantFactory(XAPIMeta, () => new XAPIPlugin(xapiOptions)),
];
