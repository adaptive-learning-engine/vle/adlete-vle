import Fastify, { FastifyInstance } from 'fastify';

import PluginBasedApp from '@fki/plugin-based-app';
import { PluginFactoryRegistry } from '@fki/plugin-system';
import type { IPluginMeta, Plugins } from '@fki/plugin-system/specs';

import type { IWithAnalytics } from '../analytics';
import type { IWithRecommendation } from '../recommend';
import type { IWithSwagger } from '../swagger';
import { ServiceLogger } from '../utils/logger';
import { IWithXAPI, XAPI_SCHEMA_FORMATS } from '../xapi';

import { FACTORIES } from './factories';

export interface IWithService {
  // adding the plugins here only makes things complicated, so we'll leave it
  service: Service<unknown>;
}

// eslint-disable-next-line @typescript-eslint/no-explicit-any
export interface IServicePlugins extends IWithService, IWithAnalytics, IWithRecommendation, IWithSwagger, IWithXAPI {}

// eslint-disable-next-line @typescript-eslint/ban-types
export const ServiceMeta: IPluginMeta<keyof IWithService, 'service'> = {
  id: 'service',
};

export interface IServiceOptions {
  port: number;
}

export class Service<TPlugins extends Plugins<TPlugins>> extends PluginBasedApp<TPlugins & IWithService> {
  public server: FastifyInstance;

  protected options: IServiceOptions;

  public get port(): number {
    return this.options.port;
  }

  public constructor(options: IServiceOptions) {
    const registry = new PluginFactoryRegistry<TPlugins & IWithService>();

    FACTORIES.forEach((factory) => {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      registry.add(factory.meta.id as any, factory as any);
    });

    super(ServiceMeta, registry);
    this.options = options;
  }

  public initialize(): Promise<void> {
    this.server = Fastify({
      logger: true,
      ajv: {
        customOptions: {
          formats: XAPI_SCHEMA_FORMATS,
        },
      },
    });
    return Promise.resolve();
  }

  public async listen(): Promise<void> {
    await this.server.listen({ port: this.options.port });
    ServiceLogger.info(`Server listening on port ${this.options.port}`);
  }
}
