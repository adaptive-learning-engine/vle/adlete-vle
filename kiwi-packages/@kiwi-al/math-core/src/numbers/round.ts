export function roundTo(n: number, digits: number): number {
  // eslint-disable-next-line no-restricted-properties
  const multiplicator = 10 ** digits;
  n = parseFloat((n * multiplicator).toFixed(11));
  return Math.round(n) / multiplicator;
}

interface IRoundedInfo {
  index: number;
  diff: number;
  roundedValue: number;
}

export function roundAllKeepSum(values: number[], targetSum: number, digits: number): number[] {
  let roundedSum = 0;
  const roundedInfo: IRoundedInfo[] = [];
  for (let i = 0; i < values.length; ++i) {
    const value = values[i];
    const roundedValue = roundTo(value, digits);
    roundedInfo.push({
      index: i,
      diff: roundedValue - value,
      roundedValue,
    });
    roundedSum += roundedValue;
  }

  const diffSum = roundedSum - targetSum;
  const changeVal = 10 ** -digits;
  const numChanges = Math.abs(Math.round(diffSum / changeVal));

  if (numChanges > 0) {
    const dir = diffSum > 0 ? 1 : -1;
    const filteredRounded = roundedInfo.filter((info) => info.diff * diffSum > 0);
    filteredRounded.sort((a, b) => (diffSum > 0 ? b.diff - a.diff : a.diff - b.diff));
    const maxI = Math.min(filteredRounded.length, numChanges);
    for (let i = 0; i < maxI; i++) {
      const roundInfo = filteredRounded[i];
      roundInfo.roundedValue -= dir * changeVal;
    }
  }

  const results = new Array(values.length);
  roundedInfo.forEach((roundInfo) => {
    results[roundInfo.index] = roundInfo.roundedValue;
  });

  return results;
}
