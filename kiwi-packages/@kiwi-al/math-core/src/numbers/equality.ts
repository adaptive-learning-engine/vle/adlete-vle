import { roundTo } from './round';

export function isNumberEqual(a: number, b: number, precision: number): boolean {
  return roundTo(a, precision) === roundTo(b, precision);
}

export function isNumberEqual2(a: number, b: number): boolean {
  return isNumberEqual(a, b, 2);
}

export function isStrictEqual(a: number, b: number): boolean {
  return a === b;
}
