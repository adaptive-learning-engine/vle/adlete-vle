const messages = {
  math: {
    and: 'und',
    'conditional-probability-of': 'Bedingte Wahrscheinlichkeit von {conditionedVariable} vorausgesetzt {conditioningVariable}',
    'conditional-probability-table': 'Bedingte Wahrscheinlichkeitstabelle',
    'contingency-table': 'Kreuztabelle',
    equation: 'Formel',
    given: 'vorausgesetzt',
    is: 'ist',
    'joint-probability-table': 'Gemeinsame Wahrscheinlichkeitstabelle',
    'joint-probability-of-variables-2': 'Wahrscheinlichkeit von {variable1} und {variable2}',
    or: 'oder',
    probability: 'Wahrscheinlichkeit',
    'probability-of-variable': 'Wahrscheinlichkeit von {variable}',
    'stacked-bar-chart': 'gestapeltes Balkendiagramm',
    sum: 'Summe',
    'variable-is-state': '{variable} ist {state}',
    'venn-diagram': 'Venndiagramm',
  },
};

// eslint-disable-next-line import/no-default-export
export default messages;
