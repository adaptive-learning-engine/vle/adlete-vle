const messages = {
  math: {
    and: 'and',
    'conditional-probability-of': 'Conditional probability of {conditionedVariable} given {conditioningVariable}',
    'conditional-probability-table': 'conditional probability table',
    'contingency-table': 'Contingency Table',
    equation: 'equation',
    given: 'given',
    is: 'is',
    'joint-probability-table': 'Joint Probability Table',
    'joint-probability-of-variables-2': 'Probability of {variable1} and {variable2}',
    or: 'or',
    probability: 'probability',
    'probability-of-variable': 'Probability of {variable}',
    'stacked-bar-chart': 'Stacked Bar Chart',
    sum: 'Sum',
    'variable-is-state': '{variable} is {state}',
    'venn-diagram': 'Venn diagram',
  },
};

// eslint-disable-next-line import/no-default-export
export default messages;
