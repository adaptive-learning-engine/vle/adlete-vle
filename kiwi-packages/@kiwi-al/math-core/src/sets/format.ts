import i18next, { i18n } from 'i18next';

import { PhraseOrNotationFormat, PhraseOrNotationFormatEnum } from '../shared/format';
import { formatVariableWithState, VariableWithState, VariableWithStateFormat } from '../variables';

export enum SetOperationEnum {
  Intersection = 'Intersection',
  Union = 'Union',
}

export type SetOperation = keyof typeof SetOperationEnum;

export interface IFormatSetOperationSymbolOptions {
  format: PhraseOrNotationFormat;
  i18n?: i18n;
}

export function formatSetOperationSymbol(op: SetOperation, { format, i18n }: IFormatSetOperationSymbolOptions): string {
  i18n = i18n || i18next;
  switch (op) {
    case SetOperationEnum.Intersection:
      return format === PhraseOrNotationFormatEnum.Phrase ? i18n.t('math:and') : '∩';
    case SetOperationEnum.Union:
      return format === PhraseOrNotationFormatEnum.Phrase ? i18n.t('math:or') : '∪';
    default:
      throw new Error(`Unknown set operation '${op}'`);
  }
}

export interface IFormatVariableSetOperationOptions {
  setFormat: PhraseOrNotationFormat;
  varFormat: VariableWithStateFormat;
  i18n?: i18n;
}

export function formatVariableSetOperation(
  var1: VariableWithState,
  var2: VariableWithState,
  op: SetOperation,
  { setFormat, varFormat, i18n }: IFormatVariableSetOperationOptions
): string {
  const symbol = formatSetOperationSymbol(op, { format: setFormat, i18n });
  const var1Text = formatVariableWithState(var1, { format: varFormat, i18n });
  const var2Text = formatVariableWithState(var2, { format: varFormat, i18n });

  return `${var1Text} ${symbol} ${var2Text}`;
}
