import { Outcome } from '../random-experiment';
import { addAfterEachChar } from '../shared/strings';
import { ICategoricalVarDef, RandomVarState, VariableDataType, VariableType } from '../variables';

export * from './operations';

export interface IEvent<TCategory extends RandomVarState = RandomVarState> {
  name: string;
  // cellColor?: string;
  outcomes: Outcome<TCategory>[];
}

// TODO: handle cases where first element is not the event happening
export function convertToEventVariables<Elems extends ICategoricalVarDef[]>(
  variables: [...Elems],
  eventNames: string[],
  variableNames?: string[]
): [...Elems] {
  if (variables.length > eventNames.length) throw new Error(`Must have at least ${variables.length} event names!`);

  return variables.map((variable, index) => {
    if (variable.categories.length !== 2) throw new Error(`Cannot convert ${variable.name} to event variable, because it is not binary!`);

    const eventName = eventNames[index];
    const notEventName = addAfterEachChar(eventName, '\u0305');

    const eventVar: ICategoricalVarDef = {
      name: variableNames ? variableNames[index] : eventNames[index],
      dtype: VariableDataType.String,
      type: VariableType.Discrete,
      categories: [eventName, notEventName],
    };

    return eventVar;
  }) as unknown as [...Elems];
}
