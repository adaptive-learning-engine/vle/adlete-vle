import { Outcome, outcomesToIdArray, outcomesToIdSet } from '../random-experiment';

// TODO: add ID function
export function getIntersectionSize(setA: Outcome[], setB: Outcome[]): number {
  // intersection test
  const setBIds = outcomesToIdSet(setB);
  return outcomesToIdArray(setA).filter((id) => setBIds.has(id)).length;
}

export function getIntersection(setA: Outcome[], setB: Outcome[]): Outcome[] {
  const bAsSet = new Set(setB);
  return setA.filter((outcome) => bAsSet.has(outcome));
}
