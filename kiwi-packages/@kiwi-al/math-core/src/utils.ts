export function arrayToIndexMap<T>(arr: T[]): Map<T, number> {
  const map: Map<T, number> = new Map();
  for (let i = 0; i < arr.length; ++i) {
    map.set(arr[i], i);
  }
  return map;
}

export function toLocaleFixed(value: number, fractionDigits?: number): string {
  // adding epsilon value for better rounding
  // see https://stackoverflow.com/questions/11832914/how-to-round-to-at-most-2-decimal-places-if-necessary#comment24719818_11832950
  value += Number.EPSILON;

  // TODO: use toLocaleString
  // TODO: option for removing trailing 0s

  return value.toFixed(fractionDigits);
}
