import { i18n } from 'i18next';
import { mapObjIndexed } from 'ramda';

import { formatProbability } from '../probability/format';
import { IRandomExperimentGenInput, IRandomExperimentTemplate } from '../random-experiment';
import { getVarWithState } from '../variables';

import { BivariateJointFrequencyDataFrame } from './BivariateJointFrequencyDataFrame';

// === Frequencies

// TODO: make version that works without true/false values
export interface IBivarFreqDataPopMarginalJoint {
  population: number;

  // only for True
  marginalFrequencies: [number, number];
  jointFrequency: number;
}

// TODO: move somewhere else
export function createPopMarginalJointFromFreqs(frequencies: BivariateJointFrequencyDataFrame): IBivarFreqDataPopMarginalJoint {
  const var0State0 = getVarWithState(frequencies.variables, 0, 0);
  const var1State0 = getVarWithState(frequencies.variables, 1, 0);

  return {
    jointFrequency: frequencies.getFrequency(var0State0[0], var0State0[1], var1State0[0], var1State0[1]),
    marginalFrequencies: [
      frequencies.getMarginalFrequency(var0State0[0], var0State0[1]),
      frequencies.getMarginalFrequency(var1State0[0], var1State0[1]),
    ],
    population: frequencies.getPopulation(),
  };
}

export interface IBivarFreqDataRandomPopMarginalJoint {
  populationMinMax: [number, number];
  marginalProbsMinMax: [[number, number], [number, number]];
  conditionalProbMinMax: [number, number];
}

export type BivarFreqREInputFormat = 'joint-freqs' | 'population-marginal-joint' | 'random-population-marginal-joint';
export type IBivarFreqJointFreqGenInput = IRandomExperimentGenInput<number[][], 'joint-freqs'>;
export type IBivarFreqPopMarginalJointGenInput = IRandomExperimentGenInput<IBivarFreqDataPopMarginalJoint, 'population-marginal-joint'>;
export type IBivarFreqRandomPopMarginalJointGenInput = IRandomExperimentGenInput<
  IBivarFreqDataRandomPopMarginalJoint,
  'random-population-marginal-joint'
>;
export type IBivarFreqREGenInput =
  | IBivarFreqJointFreqGenInput
  | IBivarFreqPopMarginalJointGenInput
  | IBivarFreqRandomPopMarginalJointGenInput;

// === Probabilities

export interface IBivarProbDataMarginalJoint {
  // only for True
  marginalProbs: [number, number];
  jointProb: number;
}

export interface IBivarProbDataMarginalCP {
  marginalProbs: number[];
  condProbs: number[][];
}

export type BivarProbREInputFormat = 'joint-probs' | 'marginal-joint' | 'marginal-cps';
export type IBivarProbJointProbGenInput = IRandomExperimentGenInput<number[][], 'joint-probs'>;
export type IBivarProbMarginalJointGenInput = IRandomExperimentGenInput<IBivarProbDataMarginalJoint, 'marginal-cps'>;
export type IBivarProbMarginalCPsGenInput = IRandomExperimentGenInput<IBivarProbDataMarginalCP, 'marginal-joint'>;
export type IBivarProbREGenInput = IBivarProbJointProbGenInput | IBivarProbMarginalCPsGenInput | IBivarProbMarginalJointGenInput;

export interface IBivarFreqRET extends IRandomExperimentTemplate {
  generation?: Record<PropertyKey, IBivarFreqREGenInput>;
}

// TODO: better infer
export function asGenInput<T extends IRandomExperimentGenInput<TData, TFormat>, TData, TFormat>(data: TData, format: TFormat): T {
  return {
    format,
    data,
  } as T;
}

export function convertBivarFreqREGenInput<T extends IBivarFreqREGenInput>(
  input: IBivarFreqREGenInput,
  newFormat: BivarFreqREInputFormat
): T {
  if (input.format === newFormat) return input as T;

  switch (input.format) {
    case 'joint-freqs': {
      switch (newFormat) {
        case 'population-marginal-joint':
          return {
            format: newFormat,
            data: {
              jointFrequency: input.data[0][0],
              marginalFrequencies: [input.data[0][0] + input.data[0][1], input.data[0][0] + input.data[1][0]],
              population: input.data[0][0] + input.data[0][1] + input.data[1][0] + input.data[1][1],
            },
          } as T;
        default:
          throw new Error(`Unsupported conversion from ${input.format} to ${newFormat}`);
      }
    }
    case 'population-marginal-joint':
      switch (newFormat) {
        case 'joint-freqs': {
          const data = input.data;
          const marginalRow = data.marginalFrequencies[0];
          const marginalColumn = data.marginalFrequencies[1];
          const a11 = data.jointFrequency;
          const a12 = marginalRow - a11;
          const a21 = marginalColumn - a11;
          const a22 = data.population - a11 - a12 - a21;
          return {
            format: newFormat,
            data: [
              [a11, a12],
              [a21, a22],
            ],
          } as T;
        }
        default:
          throw new Error(`Unsupported conversion from ${input.format} to ${newFormat}`);
      }
    default:
      throw new Error(`Unsupported conversion from ${input.format} to ${newFormat}`);
  }
}

export interface IBivarFreqPopMarginalJointDescriptionData {
  population: number;
  marginal0: number;
  marginal1: number;
  joint: number;
}

// TODO: use variables from frequencies directly
export function createBivarFreqPopMarginalJointDescriptionData(
  ret: IBivarFreqRET,
  frequencies: BivariateJointFrequencyDataFrame
): IBivarFreqPopMarginalJointDescriptionData {
  const var0 = ret.variables[0];
  const var1 = ret.variables[1];

  return {
    population: frequencies.getPopulation(),
    marginal0: frequencies.getMarginalFrequencies(var0)[0],
    marginal1: frequencies.getMarginalFrequencies(var1)[0],
    joint: frequencies.getFrequency(var0, var0.categories[0], var1, var1.categories[0]),
  };
}

export function getTemplateLocaleKey(ret: IBivarFreqRET, entry: string): string {
  return ret.localeKeys[entry];
}

export function createDescriptionFreqs(
  ret: IBivarFreqRET,
  frequencies: BivariateJointFrequencyDataFrame,
  key: string,
  i18next: i18n
): string {
  const values = createBivarFreqPopMarginalJointDescriptionData(ret, frequencies);
  return i18next.t(ret.localeKeys[key], values);
}

export function createDescriptionPercent(
  ret: IBivarFreqRET,
  frequencies: BivariateJointFrequencyDataFrame,
  key: string,
  i18next: i18n
): string {
  const values = createBivarFreqPopMarginalJointDescriptionData(ret, frequencies);
  const asPercents = mapObjIndexed((value) => formatProbability(value, { format: 'Percent', fractionDigits: 0 }), values);
  return i18next.t(ret.localeKeys[key], asPercents);
}
