import cloneDeep from 'lodash.clonedeep';

import { getRandomBetween, getRandomInt } from '../randomization';
import { ICategoricalVarDef } from '../variables';

import { BivariateJointFrequencyDataFrame } from './BivariateJointFrequencyDataFrame';
import {
  asGenInput,
  convertBivarFreqREGenInput,
  IBivarFreqDataPopMarginalJoint,
  IBivarFreqDataRandomPopMarginalJoint,
  IBivarFreqPopMarginalJointGenInput,
  IBivarFreqRandomPopMarginalJointGenInput,
  IBivarFreqREGenInput,
} from './random-experiment';

export enum FrequencyGenInputProperty {
  Population = 'Population',
  MarginalFrequency1 = 'MarginalFrequency1',
  MarginalFrequency2 = 'MarginalFrequency2',
  JointFrequency = 'JointFrequency',
}

export function validateFrequencyGenInput(
  input: IBivarFreqDataPopMarginalJoint,
  fixed: FrequencyGenInputProperty
): IBivarFreqDataPopMarginalJoint {
  // putting the numbers into range
  input.jointFrequency = Math.min(input.jointFrequency, input.population);
  input.marginalFrequencies[0] = Math.min(input.marginalFrequencies[0], input.population);
  input.marginalFrequencies[1] = Math.min(input.marginalFrequencies[1], input.population);

  if (fixed === FrequencyGenInputProperty.JointFrequency) {
    // make marginal freqs at least jointFreq
    input.marginalFrequencies[0] = Math.max(input.jointFrequency, input.marginalFrequencies[0]);
    input.marginalFrequencies[1] = Math.max(input.jointFrequency, input.marginalFrequencies[1]);
  } else {
    // make jointFrequency max marginal
    input.jointFrequency = Math.min(input.jointFrequency, input.marginalFrequencies[0], input.marginalFrequencies[1]);
  }

  const aAndB = input.jointFrequency;
  const aAndNotB = input.marginalFrequencies[0] - aAndB;
  const notAAndB = input.marginalFrequencies[1] - aAndB;
  const notAAndNotB = input.population - aAndB - aAndNotB - notAAndB;
  switch (fixed) {
    case FrequencyGenInputProperty.Population:
    // eslint-disable-next-line no-fallthrough
    case FrequencyGenInputProperty.JointFrequency:
      if (notAAndNotB < 0) {
        const extra = -notAAndNotB;
        const leverageA = input.marginalFrequencies[0] - input.jointFrequency;
        const leverageB = input.marginalFrequencies[1] - input.jointFrequency;
        const factor = leverageB === 0 ? 1 : leverageA / leverageB;
        const removeFromA = Math.floor(extra * factor);
        const removeFromB = extra - removeFromA;
        input.marginalFrequencies[0] -= removeFromA;
        input.marginalFrequencies[1] -= removeFromB;
      }
      break;
    case FrequencyGenInputProperty.MarginalFrequency1:
    case FrequencyGenInputProperty.MarginalFrequency2: {
      // const marginalIndex = fixed === FrequencyGenInputProperty.MarginalFrequency1 ? 0 : 1;
      const marginalOtherIndex = fixed === FrequencyGenInputProperty.MarginalFrequency1 ? 1 : 0;
      if (notAAndNotB < 0) {
        const extra = -notAAndNotB;
        const leverageB = input.marginalFrequencies[marginalOtherIndex] - input.jointFrequency;

        let removeFromJF = 0;
        let removeFromB = 0;
        if (extra <= leverageB) {
          // remove everything from B if possible
          removeFromB = extra;
        } else {
          const extraExtra = extra - leverageB;
          const diff = Math.floor(extraExtra / 2);
          removeFromJF = extraExtra - diff; // removing bigger part
          removeFromB = leverageB + diff; // removing smaller part
        }

        input.jointFrequency -= removeFromJF;
        input.marginalFrequencies[marginalOtherIndex] -= removeFromB;
      }
      break;
    }
    default:
      throw new Error(`Unsupported fixed property ${fixed}`);
  }
  return input;
}

export type BivariateMinMaxProbability = [[number, number], [number, number]];

export function createRandomInputFromData(
  genData: IBivarFreqDataRandomPopMarginalJoint,
  validateBy: FrequencyGenInputProperty = FrequencyGenInputProperty.JointFrequency
): IBivarFreqDataPopMarginalJoint {
  const { marginalProbsMinMax, populationMinMax, conditionalProbMinMax } = genData;

  const pA = getRandomBetween(marginalProbsMinMax[0][0], marginalProbsMinMax[0][1]);
  const pB = getRandomBetween(marginalProbsMinMax[1][0], marginalProbsMinMax[1][1]);

  const population = getRandomInt(populationMinMax[0], populationMinMax[1]);
  const freqA = Math.round(pA * population);
  const freqB = Math.round(pB * population);

  const maxFreq = Math.min(freqA, freqB);
  const conditionalProb = getRandomBetween(conditionalProbMinMax[0], conditionalProbMinMax[1]);
  const jointFreq = Math.round(conditionalProb * maxFreq);

  return validateFrequencyGenInput(
    {
      population,
      marginalFrequencies: [freqA, freqB],
      jointFrequency: jointFreq,
    },
    validateBy
  );
}

export function createRandomGenInput(
  randomGenInput: IBivarFreqRandomPopMarginalJointGenInput,
  validateBy: FrequencyGenInputProperty = FrequencyGenInputProperty.JointFrequency
): IBivarFreqPopMarginalJointGenInput {
  const { marginalProbsMinMax, populationMinMax, conditionalProbMinMax } = randomGenInput.data;

  let numTries = 0;
  while (numTries < 1000) {
    ++numTries;

    const pA = getRandomBetween(marginalProbsMinMax[0][0], marginalProbsMinMax[0][1]);
    const pB = getRandomBetween(marginalProbsMinMax[1][0], marginalProbsMinMax[1][1]);

    const population = getRandomInt(populationMinMax[0], populationMinMax[1]);
    const freqA = Math.round(pA * population);
    const freqB = Math.round(pB * population);

    const maxFreq = Math.min(freqA, freqB);
    const conditionalProb = getRandomBetween(conditionalProbMinMax[0], conditionalProbMinMax[1]);
    const jointFreq = Math.round(conditionalProb * maxFreq);

    const validatedInput = validateFrequencyGenInput(
      {
        population,
        marginalFrequencies: [freqA, freqB],
        jointFrequency: jointFreq,
      },
      validateBy
    );

    // preventing distributions with 0 frequencies
    if (
      validatedInput.marginalFrequencies[0] + validatedInput.marginalFrequencies[1] - validatedInput.jointFrequency ===
      validatedInput.population
    ) {
      // eslint-disable-next-line no-continue
      continue;
    }

    return asGenInput(validatedInput, 'population-marginal-joint');
  }

  throw new Error('Could not create random distribution!');
}

export function generateJointFrequencyData(input: IBivarFreqREGenInput, variables: ICategoricalVarDef[]): BivariateJointFrequencyDataFrame {
  let newData: number[][];
  switch (input.format) {
    case 'population-marginal-joint': {
      const newInput = convertBivarFreqREGenInput(input, 'joint-freqs');
      newData = newInput.data as number[][];
      break;
    }
    case 'joint-freqs': {
      // clone
      newData = cloneDeep(input.data);
      break;
    }
    default:
      throw new Error(`Unsupported input format ${input.format}`);
  }

  return new BivariateJointFrequencyDataFrame(newData, {
    variables: variables as [ICategoricalVarDef, ICategoricalVarDef],
  });
}
