import { ArrayType2D } from 'danfojs/dist/danfojs-base/shared/types';

import { TabularDataset } from '../tabular';
import { arrayToIndexMap } from '../utils';
import { ICategoricalVarDef, toVarDefRecord } from '../variables';

import { BivariateJointFrequencyDataFrame } from './BivariateJointFrequencyDataFrame';

// TODO: remove
/**
 * Calculates the joint frequency of all state combinations two variables of the given dataset
 *
 * @param dataset   Dataset to extract information from
 * @param columnVar Variable to use for the columns of the resulting DataFrame
 * @param rowVar    Variable to use for the rows of the resulting DataFrame
 * @returns
 */
export function getBivariateJointFrequencyFromDataset<
  TData = number,
  TColVar extends ICategoricalVarDef = ICategoricalVarDef,
  TRowVar extends ICategoricalVarDef = ICategoricalVarDef,
>(
  dataset: TabularDataset<unknown, ICategoricalVarDef>,
  columnVar: TColVar,
  rowVar: TRowVar
): BivariateJointFrequencyDataFrame<TData, TColVar, TRowVar> {
  const freqs = rowVar.categories.map(() => columnVar.categories.map(() => 0));
  const columnVarStateToIndex = arrayToIndexMap(columnVar.categories);
  const rowVarStateToIndex = arrayToIndexMap(rowVar.categories);
  const data = dataset.data;

  const values = data.values as ArrayType2D;

  // TODO: verify taht variables are part of the dataset

  values.forEach((row, rowI) => {
    const columnState = data.at(rowI, columnVar.name);
    const rowState = data.at(rowI, rowVar.name);

    // TODO error handling
    if (!columnVarStateToIndex.has(columnState)) throw new Error(`Unknown column state '${columnState}'!`);
    const x = columnVarStateToIndex.get(columnState);

    if (!rowVarStateToIndex.has(rowState)) throw new Error(`Unknown row state '${rowState}'!`);
    const y = rowVarStateToIndex.get(rowState);

    freqs[y][x] += 1;
  });

  return new BivariateJointFrequencyDataFrame(freqs, {
    variables: [rowVar, columnVar],
  });
}

/**
 * Calculates the joint frequencies of all state combinations of the given column and row variables.
 *
 * Will return columnVariables * rowVariables DataFrames with joint frequency information about the two
 * participating variables
 *
 * @param dataset
 * @param columnVariables Variables to use for the columns of the resulting DataFrame
 * @param rowVariables    Variables to use for the rows of the resulting DataFrame
 * @returns
 */
export function getBivariateJointFrequenciesFromDataset(
  dataset: TabularDataset<unknown, ICategoricalVarDef>,
  columnVariables: string[],
  rowVariables: string[]
): Record<string, BivariateJointFrequencyDataFrame[]> {
  const variables = toVarDefRecord(dataset.variables);
  const frequencies: Record<string, BivariateJointFrequencyDataFrame[]> = {};

  // calculate joint frequencies for all variable combinations that we are interested in
  for (let columnIndex = 0; columnIndex < columnVariables.length; ++columnIndex) {
    const columnVar = variables[columnVariables[columnIndex]];
    frequencies[columnVar.name] = [];
    for (let rowIndex = 0; rowIndex < rowVariables.length; ++rowIndex) {
      const rowVar = variables[rowVariables[rowIndex]];
      const freqs = getBivariateJointFrequencyFromDataset(dataset, columnVar, rowVar);
      frequencies[columnVar.name].push(freqs);
    }
  }

  return frequencies;
}
