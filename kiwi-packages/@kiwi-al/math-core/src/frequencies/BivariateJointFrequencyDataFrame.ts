import { BivariateDataFrame, IBivariateDataFrameOptions } from '../tabular/BivariateDataframe';
import { ICategoricalVarDef, RandomVarState } from '../variables';

/**
 * A 2-dimensional DataFrame containing joint frequency information of two categorical variables
 */
export class BivariateJointFrequencyDataFrame<
  TValue = number,
  TColVar extends ICategoricalVarDef = ICategoricalVarDef,
  TRowVar extends ICategoricalVarDef = ICategoricalVarDef,
> extends BivariateDataFrame<TValue, TColVar, TRowVar> {
  public print(): void {
    const getColumnNames = () => this.columnVariable.categories.map((state) => `${this.columnVariable.name}: ${state}`);
    const getIndex = () => this.rowVariable.categories.map((state) => `${this.rowVariable.name}: ${state}`);
    this.data.customPrint(getColumnNames, getIndex);
  }

  public copy(
    extraOptions?: Partial<IBivariateDataFrameOptions<TColVar, TRowVar>>
  ): BivariateJointFrequencyDataFrame<TValue, TColVar, TRowVar> {
    const copy = new BivariateJointFrequencyDataFrame<TValue, TColVar, TRowVar>(this.copyData(), { ...this.options, ...extraOptions });
    return copy;
  }

  /**
   * Returns the frequency for the combination of the given variable states. Order (row, column) is not important
   * @param var1
   * @param state1
   * @param var2
   * @param state2
   * @returns
   */
  public getFrequency(var1: TColVar | TRowVar, state1: RandomVarState, var2: TColVar | TRowVar, state2: RandomVarState): number {
    if (this.columnVariable !== var1 && this.rowVariable !== var1)
      throw new Error(`Variable 1 '${var1.name}' is not a variable of this DataFrame`);
    if (this.columnVariable !== var2 && this.rowVariable !== var2)
      throw new Error(`Variable 2 '${var2.name}' is not a variable of this DataFrame`);
    if (var1 === var2) throw new Error(`Variable 1 '${var1.name}' and variable 2 '${var2.name}' cannot be the same!`);

    const rowState = var1 === this.rowVariable ? state1 : state2;
    const columnState = var1 === this.columnVariable ? state1 : state2;

    return this.data.at(rowState as string | number, columnState as string) as number;
  }

  /**
   * Gets the sums of the frequencies of all states of the given variable (e.g. summing across rows / columns)
   * @param variable
   * @returns
   */
  public getMarginalFrequencies(variable: TRowVar | TColVar): number[];
  public getMarginalFrequencies(varIndex: number): number[];
  public getMarginalFrequencies(varIndexOrVar: number | TRowVar | TColVar): number[];
  public getMarginalFrequencies(varIndexOrVar: number | TRowVar | TColVar): number[] {
    if (typeof varIndexOrVar !== 'number') varIndexOrVar = this.getVariableIndex(varIndexOrVar);

    if (varIndexOrVar === 1) return this.data.sum({ axis: 0 }).values as unknown[] as number[];
    return this.data.sum({ axis: 1 }).values as unknown[] as number[];
  }

  public getMarginalFrequency(variable: TRowVar | TColVar, state: RandomVarState): number;
  public getMarginalFrequency(varIndex: number, state: RandomVarState): number;
  public getMarginalFrequency(varIndexOrVar: number | TRowVar | TColVar, state: RandomVarState): number {
    const varIndex = typeof varIndexOrVar === 'number' ? varIndexOrVar : this.getVariableIndex(varIndexOrVar);
    const mfs = this.getMarginalFrequencies(varIndex);
    return mfs[this.variables[varIndex].categories.indexOf(state)];
  }

  public getPopulation(): number {
    return this.data.sum().sum();
  }
}
