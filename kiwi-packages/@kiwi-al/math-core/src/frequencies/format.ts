import i18next, { i18n } from 'i18next';

import { PhraseOrNotationFormat, PhraseOrNotationFormatEnum } from '../shared/format';
import { formatVariableWithState, VariableWithState, VariableWithStateFormat } from '../variables';

export interface IFormatJointFrequencyOptions {
  varFormat: VariableWithStateFormat;
  jointFormat: PhraseOrNotationFormat;
  jointSeparator?: string;
  i18n?: i18n;
}

export function formatJointFrequency(
  var1: VariableWithState,
  var2: VariableWithState,
  { varFormat, jointFormat, jointSeparator = '∩', i18n = i18next }: IFormatJointFrequencyOptions
): string {
  const variable1 = formatVariableWithState(var1, { format: varFormat, i18n });
  const variable2 = formatVariableWithState(var2, { format: varFormat, i18n });
  switch (jointFormat) {
    case PhraseOrNotationFormatEnum.Phrase: {
      const and = i18next.t('math:and');
      return `${variable1} ${and} ${variable2}`;
    }
    case PhraseOrNotationFormatEnum.Notation:
      return `${variable1}${jointSeparator}${variable2}`;
    default:
      throw new Error(`Unknown ProbabilityFormat '${jointFormat}'`);
  }
}
