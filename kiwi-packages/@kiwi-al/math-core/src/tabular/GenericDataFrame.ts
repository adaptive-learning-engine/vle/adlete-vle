import { DataFrame } from 'danfojs';
import { ArrayType2D } from 'danfojs/dist/danfojs-base/shared/types';

import type { ConstructorArg2 } from '@adlete-utils/typescript';

export declare type GenericArrayType1D<TRow> = TRow;
export declare type GenericArrayType2D<TRow> = TRow[];

export type BaseDataOptionType = ConstructorArg2<typeof DataFrame>;

/**
 * A generic DataFrame with type information about the data stored in rows
 */
export interface IGenericDataFrame<TRowData> extends DataFrame {
  customPrint(getColumnNames: () => string[], getIndex: () => (string | number)[]): void;
  iset(row: number, column: number, value: string | number | boolean): void;
  get typedValues(): GenericArrayType2D<TRowData> | GenericArrayType1D<TRowData>;
}

function customPrint(getColumnNames: () => string[], getIndex: () => (string | number)[]): void {
  const columns = this.columns;
  const index = this.index;
  this.$setColumnNames(getColumnNames());
  this.$setIndex(getIndex());
  this.print();
  this.$setColumnNames(columns);
  this.$setIndex(index);
}

function iset(row: number, column: number, value: string | number | boolean): void {
  (this.$data as ArrayType2D)[row][column] = value;
  if (this.$dataIncolumnFormat) {
    (this.$dataIncolumnFormat as ArrayType2D)[column][row] = value;
  }
}

export function makeGenericDataFrame<TRowData>(frame: DataFrame): IGenericDataFrame<TRowData> {
  const genericDataFrameProperties = {
    customPrint,
    iset,
    get typedValues(): GenericArrayType2D<TRowData> | GenericArrayType1D<TRowData> {
      // eslint-disable-next-line @typescript-eslint/no-explicit-any
      return this.values as any;
    },
  };
  Object.assign(frame, genericDataFrameProperties);
  return frame as IGenericDataFrame<TRowData>;
}

export function createGenericDataFrame<TRowData>(data: unknown, options?: BaseDataOptionType): IGenericDataFrame<TRowData> {
  return makeGenericDataFrame(new DataFrame(data, options));
}
