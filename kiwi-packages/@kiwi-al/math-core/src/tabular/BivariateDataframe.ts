import cloneDeep from 'lodash.clonedeep';

import { ICategoricalVarDef } from '../variables';

import { BaseDataOptionType, createGenericDataFrame, IGenericDataFrame, makeGenericDataFrame } from './GenericDataFrame';

export interface IBivariateDataFrameOptions<
  TColVar extends ICategoricalVarDef = ICategoricalVarDef,
  TRowVar extends ICategoricalVarDef = ICategoricalVarDef,
> extends BaseDataOptionType {
  variables: [TRowVar, TColVar];
}

/**
 * A 2-dimensional DataFrame with information about the 2 involved categorical variables (row / coloumn)
 */
export class BivariateDataFrame<
  TValue = number,
  TColVar extends ICategoricalVarDef = ICategoricalVarDef,
  TRowVar extends ICategoricalVarDef = ICategoricalVarDef,
> {
  public options: IBivariateDataFrameOptions<TColVar, TRowVar>;

  public data: IGenericDataFrame<TValue[]>;

  // must be any to conformt to danfojs DataFrame
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types, @typescript-eslint/no-explicit-any
  public constructor(data: any, options: IBivariateDataFrameOptions<TColVar, TRowVar>) {
    const optionsCopy = { ...options };
    optionsCopy.columns = options.variables[1].categories.map((cat) => cat.toString());
    optionsCopy.index = options.variables[0].categories.map((cat) => cat.toString());

    if (data != null) this.data = createGenericDataFrame<TValue[]>(data, optionsCopy);

    // TODO: throw error if states < 2

    this.options = optionsCopy;
  }

  public getVariableIndex(variable: TRowVar | TColVar): number {
    this.assertContainsVariable(variable);
    return this.variables.indexOf(variable);
  }

  public get columnVariable(): TColVar {
    return this.options.variables[1];
  }

  public get rowVariable(): TRowVar {
    return this.options.variables[0];
  }

  public get variables(): [TRowVar, TColVar] {
    return this.options.variables;
  }

  public getOtherVariable(variable: TColVar | TRowVar): TColVar | TRowVar {
    this.assertContainsVariable(variable);

    return this.columnVariable === variable ? this.rowVariable : this.columnVariable;
  }

  public getOtherVariableIndex(variable: TColVar | TRowVar): number {
    this.assertContainsVariable(variable);

    return this.columnVariable === variable ? 0 : 1;
  }

  protected copyDataFrame(): IGenericDataFrame<TValue[]> {
    const data = makeGenericDataFrame<TValue[]>(this.data.copy());
    // TODO: remove, once this issue was fixed: https://github.com/javascriptdata/danfojs/issues/475
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    (this.data as any).$data = this.copyData();
    return data;
  }

  protected copyData(): TValue[] {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return cloneDeep((this.data as any).$data);
  }

  public copy(extraOptions?: Partial<IBivariateDataFrameOptions<TColVar, TRowVar>>): BivariateDataFrame<TValue, TColVar, TRowVar> {
    const copy = new BivariateDataFrame<TValue, TColVar, TRowVar>(this.copyData(), { ...this.options, ...extraOptions });
    return copy;
  }

  public get values(): TValue[][] {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return this.data.values as any;
  }

  public assertContainsVariable(variable: TColVar | TRowVar): void {
    if (this.columnVariable !== variable && this.rowVariable !== variable)
      throw new Error(`Variable '${variable.name}' is not a variable of this DataFrame`);
  }
}
