export function to1DArray(values: number[][]): number[] {
  const result = [];
  for (let y = 0; y < values.length; y++) {
    const row = values[y];
    for (let x = 0; x < row.length; x++) {
      result.push(row[x]);
    }
  }
  return result;
}

export function to2DArray(values: number[], numColumns: number): number[][] {
  const result = [];
  for (let i = 0; i < values.length; i += numColumns) {
    result.push(values.slice(i, i + numColumns));
  }
  return result;
}
