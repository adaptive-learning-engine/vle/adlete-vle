import cloneDeep from 'lodash.clonedeep';

export class Array2D<TValue> {
  protected data: TValue[][];

  public readonly shape: number[];

  // TODO: support for tensors
  public constructor(data: TValue[][], clone = true) {
    if (clone) this.data = cloneDeep(data);
    else this.data = data;
    this.shape = [data.length, data[0].length];
  }

  public get values(): TValue[][] {
    return this.data;
  }

  public has(x: number, y: number): boolean {
    return y >= 0 && y < this.shape[0] && x >= 0 && x < this.shape[1];
  }

  public get(x: number, y: number): TValue {
    return this.data[y][x];
  }

  public set(x: number, y: number, value: TValue): void {
    this.data[y][x] = value;
  }

  public forEachRow(cb: (row: TValue[], y?: number) => void): void {
    this.data.forEach(cb);
  }

  public map<U>(cb: (value: TValue, x: number, y: number) => U): Array2D<U> {
    const rows = [];
    for (let y = 0; y < this.shape[0]; ++y) {
      const row = [];
      for (let x = 0; x < this.shape[1]; ++x) {
        row.push(cb(this.get(x, y), x, y));
      }
      rows.push(row);
    }
    return new Array2D(rows);
  }

  public mapRow<U>(cb: (value: TValue[], index: number) => U): U[] {
    return this.data.map(cb);
  }

  public copy(): Array2D<TValue> {
    return new Array2D(this.data, true);
  }

  public static fill<T>(shape: number[], value: T): Array2D<T> {
    const rows = [];
    for (let y = 0; y < shape[0]; ++y) {
      const row = [];
      for (let x = 0; x < shape[1]; ++x) {
        row.push(value);
      }
      rows.push(row);
    }
    return new Array2D<T>(rows);
  }
}
