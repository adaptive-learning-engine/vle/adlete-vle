import { IVarDef } from '../variables';

import { IGenericDataFrame, makeGenericDataFrame } from './GenericDataFrame';

/**
 * A 2-dimensional DataFrame with information about the involved variables (columns)
 */
export class TabularDataset<TRow, TVarDef extends IVarDef = IVarDef> {
  public variables: TVarDef[];

  public data: IGenericDataFrame<TRow>;

  public constructor(variables: TVarDef[], data: IGenericDataFrame<TRow>) {
    this.variables = variables;
    this.data = data;
  }

  public copy(): TabularDataset<TRow, TVarDef> {
    const data = makeGenericDataFrame<TRow>(this.data.copy());
    return new TabularDataset([...this.variables], data);
  }
}
