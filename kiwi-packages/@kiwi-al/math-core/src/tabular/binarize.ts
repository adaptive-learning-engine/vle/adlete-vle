import { RandomVarState } from '../variables';

import { IGenericDataFrame, makeGenericDataFrame } from './GenericDataFrame';

export function binarize<TRowIn, TRowOut>(data: IGenericDataFrame<TRowIn>, binValues: RandomVarState[]): IGenericDataFrame<TRowOut> {
  const copy = makeGenericDataFrame(data.copy()) as unknown as IGenericDataFrame<TRowOut>;
  for (let y = 0; y < data.shape[0]; ++y) {
    for (let x = 0; x < data.shape[1]; ++x) {
      const value = data.iat(y, x);
      if (typeof value === 'number') copy.iset(y, x, value <= 0 ? binValues[0] : binValues[1]);
    }
  }
  return copy;
}
