import { BivariateCPDataFrame } from '../probability';
import { ICategoricalVarDef, RandomVarState } from '../variables';

// function getStateCountsFromFreqs(distribution: BivariateJointFrequencyDistribution, forRowVariable: boolean): number[] {
//   const xLength = distribution[0].length;
//   for (let y = 0; y < distribution.length; ++y) {
//     for (let x = 0; x < xLength; ++x) {}
//   }
// }

// function getConditionalProbabilityFromFreqs();

export class BernoulliNaiveBayesClassifier<TCategory extends RandomVarState = RandomVarState> {
  protected condProbs: BivariateCPDataFrame[];

  public predictorVariables: ICategoricalVarDef[];

  public targetVariable: ICategoricalVarDef<TCategory>;

  protected isCondititionedOnColumn: boolean;

  public constructor(
    targetVariable: ICategoricalVarDef<TCategory>,
    predictorVariables: ICategoricalVarDef[],
    condProbs: BivariateCPDataFrame[]
  ) {
    this.targetVariable = targetVariable;
    this.predictorVariables = predictorVariables;
    this.condProbs = condProbs;
    this.isCondititionedOnColumn = this.condProbs[0].isConditionedOnColumnVariable;
  }

  public getPredictorStatesFromString(str: string): RandomVarState[] {
    const lcStr = str.toLowerCase();
    return this.predictorVariables.map((predictor) => (lcStr.includes(predictor.name.toLowerCase()) ? 'yes' : 'no'));
  }

  public predictAll(str: string): Map<TCategory, number> {
    const result = new Map<TCategory, number>();
    const givenStates = this.getPredictorStatesFromString(str);
    this.targetVariable.categories.forEach((cat) => {
      const res = this.condProbs.reduce((product, frame, index) => {
        const conditionalProb = frame.getConditionalProbability(givenStates[index], cat);
        // TODO: add prior!
        return product * conditionalProb;
      }, 1); // TODO: prior!
      result.set(cat, res);
    });

    return result;
  }

  public predictMax(str: string): TCategory {
    const predictions = this.predictAll(str);
    const entries = Array.from(predictions.entries());
    let maxEntry = entries[0];
    entries.forEach((entry) => {
      if (entry[1] > maxEntry[1]) maxEntry = entry;
    });
    return maxEntry[0];
  }
}
