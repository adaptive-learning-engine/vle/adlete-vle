import i18next, { i18n } from 'i18next';

import { MultiLangResourceBundleWithNS } from '@adlete-vle/lr-core/locale';

import { ICategoricalVarDef } from '../variables';

export const BivariateFrequencyRandomExperimentEncoding = 'application/x.bivar-freq-random-experiment';

export interface IRandomExperimentTemplate {
  id: string;
  variables: ICategoricalVarDef[];
  type: string;
  shortVariableLocaleKeys?: Record<string, string>;
  localeKeys: Record<string, string>;
  locale: MultiLangResourceBundleWithNS;
}

export interface IRandomExperimentGenInput<TData, TFormat = string> {
  format: TFormat;
  data: TData;
}

export const BACKUP_VARIABLE_NAMES = ['A', 'B', 'C', 'D', 'E'];

export function getAlternateVariableNamesFromRET(ret: IRandomExperimentTemplate, i18n: i18n = i18next): string[] {
  if (ret.shortVariableLocaleKeys) {
    return ret.variables.map((variable) =>
      variable.name in ret.shortVariableLocaleKeys ? i18n.t(ret.shortVariableLocaleKeys[variable.name]) : i18n.t(variable.name)
    );
  }
  return ret.variables.map((variable, index) => BACKUP_VARIABLE_NAMES[index]);
}

export function getShortCategoryNamesFromRET(ret: IRandomExperimentTemplate, i18n: i18n = i18next): string[][] {
  if (ret.shortVariableLocaleKeys) {
    return ret.variables.map((variable) => variable.categories.map((cat) => i18n.t(ret.shortVariableLocaleKeys[cat.toString()])));
  }
  throw new Error('Could not find alternate names!');
}
