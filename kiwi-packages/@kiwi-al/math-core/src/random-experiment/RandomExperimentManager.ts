import { IRandomExperimentTemplate } from './IRandomExperimentTemplate';

export enum RandomExperimentType {
  BivariateBinary = 'BivariateBinary',
  BivariatePolynary = 'BivariatePolynary',
}

export class RandomExperimentManager {
  protected templates: Record<string, IRandomExperimentTemplate> = {};

  public addTemplate(ds: IRandomExperimentTemplate): void {
    this.templates[ds.id] = ds;
  }

  public getTemplate<T extends IRandomExperimentTemplate>(id: string): T {
    return this.templates[id] as T;
  }

  public getTemplateByType<T extends IRandomExperimentTemplate>(type: string): T[] {
    return Object.values(this.templates).filter((ds) => ds.type === type) as T[];
  }

  protected validateTemplate(id: string): void {
    if (!this.templates[id]) {
      throw new Error(`Unknown random experiment template '${id}'`);
    }
  }
}
