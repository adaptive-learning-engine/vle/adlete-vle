import { RandomVarState, VariableWithState } from '../variables';

export type Outcome<TCategory extends RandomVarState = RandomVarState> = VariableWithState<TCategory>[];

export function compareOutcome(a: Outcome, b: Outcome): boolean {
  return a.every((varWithState, i) => varWithState.every((elem, j) => elem === b[i][j]));
}

export function outcomeToId(outcome: Outcome): string {
  return outcome.map((varWithState) => `${varWithState[0].name}-${varWithState[1]}`).join('-');
}

export function outcomesToIdSet(outcomes: Outcome[]): Set<string> {
  return new Set(outcomes.map(outcomeToId));
}

export function outcomesToIdArray(outcomes: Outcome[]): string[] {
  return outcomes.map(outcomeToId);
}
