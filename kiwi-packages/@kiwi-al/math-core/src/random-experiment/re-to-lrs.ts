import { ILearningResource, IMultiLingualRawText, LR_CONTEXT_EN } from '@adlete-vle/lr-core/learning-resources';
import { MultiLangResourceBundleWithNS } from '@adlete-vle/lr-core/locale';

import { IRandomExperimentTemplate } from './IRandomExperimentTemplate';

function localeToMultilingualContent(locale: MultiLangResourceBundleWithNS, key: string): IMultiLingualRawText {
  const [namespace, shortKey] = key.split(':');
  const result: IMultiLingualRawText = {};
  Object.entries(locale).forEach(([lang, resourceBundle]) => {
    if (namespace in resourceBundle && shortKey in resourceBundle[namespace]) {
      result[lang] = resourceBundle[namespace][shortKey];
    }
  });
  return result;
}

export function learningResourcesFromRET(lr: ILearningResource<IRandomExperimentTemplate>): ILearningResource[] {
  const resources: ILearningResource[] = [];

  // itself
  resources.push(lr);

  // example problems
  const contentKey = lr.content.localeKeys['problem-description'];
  const about = lr.meta.about?.[0];
  if (contentKey && about) {
    resources.push({
      meta: {
        '@context': LR_CONTEXT_EN,
        id: `${lr.meta.id}&example-problem=0`,
        name: `Example Problem for ${about.prefLabel.en}`,
        localizedName: {
          en: `Example Problem for ${about.prefLabel.en}`,
          de: `Beispiel Problem für ${about.prefLabel.de}`,
        },
        type: ['LearningResource', 'ExampleProblem'],
        encoding: [{ encodingFormat: 'text/x.multi-lingual-text+json' }],
        about: [about],
        image: lr.meta.image,
      },
      content: localeToMultilingualContent(lr.content.locale, contentKey),
    } as ILearningResource<IMultiLingualRawText>);
  }
  return resources;
}
