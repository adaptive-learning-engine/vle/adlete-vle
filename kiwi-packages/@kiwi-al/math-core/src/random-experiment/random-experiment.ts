import { ICategoricalVarDef, RandomVarState } from '../variables';

import { Outcome } from './outcomes';

export function variablesToSampleSpace2<TCategory extends RandomVarState = RandomVarState>(
  variables: ICategoricalVarDef<TCategory>[]
): Outcome<TCategory>[] {
  const [var0, var1] = variables;

  const outcomes: Outcome<TCategory>[] = [];
  for (let i = 0; i < var0.categories.length; ++i) {
    for (let j = 0; j < var1.categories.length; ++j) {
      outcomes.push([
        [var0, var0.categories[i]],
        [var1, var1.categories[j]],
      ]);
    }
  }
  return outcomes;
}
