// TODO: performance
export function addAfterEachChar(str: string, toAdd: string): string {
  const result = [];
  for (let i = 0; i < str.length; ++i) {
    result.push(str.at(i));
    result.push(toAdd);
  }

  return result.join('');
}
