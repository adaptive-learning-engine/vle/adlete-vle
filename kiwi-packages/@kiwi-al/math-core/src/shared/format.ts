export enum PhraseOrNotationFormatEnum {
  Phrase = 'Phrase',
  Notation = 'Notation',
}

export type PhraseOrNotationFormat = keyof typeof PhraseOrNotationFormatEnum;
