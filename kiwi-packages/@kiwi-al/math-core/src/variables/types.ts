import { INumericVarDef, VariableDataType, VariableType } from './VariableDefinition';

export function createUnsignedIntegerVarDef(name: string): INumericVarDef {
  return {
    name,
    type: VariableType.Continuous,
    dtype: VariableDataType.Integer,
    isUnsigned: true,
  };
}

export function createProbabilityVarDef(name: string): INumericVarDef {
  return {
    name,
    type: VariableType.Continuous,
    dtype: VariableDataType.Float,
    isUnsigned: true,
    range: [0, 1],
  };
}
