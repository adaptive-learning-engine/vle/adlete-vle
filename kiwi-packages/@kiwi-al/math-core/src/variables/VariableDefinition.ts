import { getRandomElement } from '../randomization';

import { RandomVarState } from './discrete-variable';

export enum VariableType {
  Discrete = 'Discrete',
  Continuous = 'Continuous',
  Categorical = 'Categorical',
}

export enum VariableDataType {
  Integer = 'Integer',
  Float = 'Float',
  String = 'String',
}

export interface IVarDef {
  name: string;
  type: VariableType | keyof typeof VariableType;
  dtype: VariableDataType | keyof typeof VariableDataType;
}

export interface INumericVarDef extends IVarDef {
  isUnsigned?: boolean;
  range?: [number, number];
}

export interface ICategoricalVarDef<TCategory extends RandomVarState = RandomVarState> extends IVarDef {
  categories: TCategory[];
}

export type BivarCategoricalVarDefs<TCat1 extends RandomVarState = RandomVarState, TCat2 extends RandomVarState = RandomVarState> = [
  ICategoricalVarDef<TCat1>,
  ICategoricalVarDef<TCat2>,
];

export type VariableWithState<TCategory extends RandomVarState = RandomVarState> = [ICategoricalVarDef<TCategory>, TCategory];

export function toVarDefRecord<T extends IVarDef>(defs: T[]): Record<string, T> {
  const record: Record<string, T> = {};
  defs.forEach((def) => {
    record[def.name] = def;
  });
  return record;
}

export function isBinaryVariable<TCategory extends RandomVarState = RandomVarState>(variable: ICategoricalVarDef<TCategory>): boolean {
  return variable.categories.length === 2;
}

export function assertIsBinaryVariable<TCategory extends RandomVarState = RandomVarState>(variable: ICategoricalVarDef<TCategory>): void {
  if (variable.categories.length !== 2) throw new Error(`Variable '${variable.name}' is not a binary variable!`);
}

export function hasCategory<TCategory extends RandomVarState = RandomVarState>(
  variable: ICategoricalVarDef<TCategory>,
  cat: TCategory
): boolean {
  return variable.categories.includes(cat);
}

export function assertHasCategory<TCategory extends RandomVarState = RandomVarState>(
  variable: ICategoricalVarDef<TCategory>,
  cat: TCategory
): void {
  if (!variable.categories.includes(cat)) throw new Error(`Variable '${variable.name}' does not contain category '${cat}'!`);
}

export function getOtherCategory<TCategory extends RandomVarState>(variable: ICategoricalVarDef<TCategory>, cat: TCategory): TCategory {
  assertIsBinaryVariable(variable);
  assertHasCategory(variable, cat);

  return variable.categories[0] === cat ? variable.categories[1] : variable.categories[0];
}

// TODO: rename createVarWithState
export function getVarWithState(variables: ICategoricalVarDef[], varIndex: number, stateIndex: number): VariableWithState {
  const variable = variables[varIndex];
  return [variable, variable.categories[stateIndex]];
}

export function varToVarsWithState(variable: ICategoricalVarDef): VariableWithState[] {
  return variable.categories.map((cat) => [variable, cat]);
}

export function jointVarsToString(var0: string, var1: string, separator = '|'): string {
  return `${var0}${separator}${var1}`;
}

export function varWithCatToString(varWithCat: VariableWithState, separator = ':'): string {
  return `${varWithCat[0].name}${separator}${varWithCat[1]}`;
}

export function variableToVarCatStrings(variable: ICategoricalVarDef, separator = ':'): string[] {
  return variable.categories.map((cat) => `${variable.name}${separator}${cat}`);
}

export function getRandomCategory<TCategory extends RandomVarState = RandomVarState>(variable: ICategoricalVarDef<TCategory>): TCategory {
  return getRandomElement(variable.categories);
}
