import i18next, { i18n } from 'i18next';

import { VariableWithState } from './VariableDefinition';

export enum VariableWithStateFormatEnum {
  Phrase = 'Phrase',
  Notation = 'Notation',
  VarOnly = 'VarOnly',
  StateOnly = 'StateOnly',
}

export type VariableWithStateFormat = keyof typeof VariableWithStateFormatEnum;

export interface IFormatVariableWithStateOptions {
  format: VariableWithStateFormat;
  separator?: string;
  i18n?: i18n;
}

export function formatVariableWithState(
  varInfo: VariableWithState,
  { format, separator = ' = ', i18n }: IFormatVariableWithStateOptions
): string {
  i18n = i18n || i18next;

  const variable = i18n.t(varInfo[0].name);
  const state = i18n.t(varInfo[1].toString());
  switch (format) {
    case VariableWithStateFormatEnum.Phrase:
      return i18n.t('math:variable-is-state', { variable, state });
    case VariableWithStateFormatEnum.Notation:
      return `${variable}${separator}${state}`;
    case VariableWithStateFormatEnum.VarOnly:
      return variable;
    case VariableWithStateFormatEnum.StateOnly:
      return state;
    default:
      throw new Error(`Unknown VariableWithStateFormat '${format}'`);
  }
}
