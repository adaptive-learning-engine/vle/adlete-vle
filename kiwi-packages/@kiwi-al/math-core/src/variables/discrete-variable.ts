export type RandomVarState = boolean | number | string;

export function createFieldStateByNameRecord(variableNames: string[], fieldStates: RandomVarState[][]): Record<string, RandomVarState[]> {
  const record: Record<string, RandomVarState[]> = {}; // TODO: fix typing
  variableNames.forEach((name, i) => {
    record[name] = fieldStates[i];
  });
  return record;
}
