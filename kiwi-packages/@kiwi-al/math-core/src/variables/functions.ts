import { ICategoricalVarDef } from './VariableDefinition';

// TODO: handle tuples
export function copyVariablesWithNewNames(
  variables: ICategoricalVarDef[],
  variableNames: string[],
  categoryNames: string[][]
): ICategoricalVarDef[] {
  return variables.map((variable, index) => ({
    name: variableNames ? variableNames[index] : variable.name,
    dtype: variable.dtype,
    type: variable.type,
    categories: [...(categoryNames ? categoryNames[index] : variable.categories)],
  }));
}
