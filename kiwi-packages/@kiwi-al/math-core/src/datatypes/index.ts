export function isInteger(value: string): boolean {
  return value !== '' && Number.isInteger(Number(value));
}

export function isPositiveInteger(value: string): boolean {
  const numValue = Number(value);
  return value !== '' && Number.isInteger(numValue) && numValue >= 0;
}

export function isFloat(value: string): boolean {
  const numValue = Number(value);
  return value !== '' && !Number.isNaN(numValue);
}
