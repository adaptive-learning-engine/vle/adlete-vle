import { BivariateJointFrequencyDataFrame } from '../frequencies/BivariateJointFrequencyDataFrame';
import { roundAllKeepSum } from '../numbers/round';
import { to1DArray, to2DArray } from '../tabular/arrays';

import { BivariateJointProbabilityDataFrame } from './BivariateJointProbabilityDataFrame';

export function probabilitiesFromVarFrequencies(freqs: number[]): number[] {
  const sum = freqs.reduce((pv, cv) => pv + cv, 0);
  return freqs.map((freq) => freq / sum);
}

export interface IJointProbsDFFromFreqsDFOptions {
  fractionDigits: number;
}

export function jointProbabilityDFFromFrequencyDF(
  freqs: BivariateJointFrequencyDataFrame,
  options?: IJointProbsDFFromFreqsDFOptions
): BivariateJointProbabilityDataFrame {
  const fractionDigits = options?.fractionDigits;

  const sum = freqs.getPopulation();
  const probs = freqs.data.div(sum);

  let values = probs.values;

  if (fractionDigits != null) {
    const origValues = probs.values as number[][];
    values = to2DArray(roundAllKeepSum(to1DArray(origValues), 1, fractionDigits), origValues[0].length);
  }

  return new BivariateJointProbabilityDataFrame(values, freqs.options);
}

export function jointFreqDFFromJointProbDF(
  probs: BivariateJointProbabilityDataFrame,
  population: number
): BivariateJointFrequencyDataFrame {
  const freqs = probs.values.map((row) => row.map((val) => Math.round(val * population)));
  return new BivariateJointProbabilityDataFrame(freqs, probs.options);
}
