import cloneDeep from 'lodash.clonedeep';
import * as R from 'ramda';

import { BivariateJointFrequencyDataFrame } from '../frequencies';
import { roundTo } from '../numbers';
import { ICategoricalVarDef } from '../variables';
// import { RandomVarState } from '../variables/discrete-variable';

import { BivariateCPDataFrame } from './BivariateCPDataframe';
import { BivariateJointProbabilityDataFrame } from './BivariateJointProbabilityDataFrame';

// export function getConditionalProbabilityFromJointFrequency<TColVar extends ICategoricalVarDef, TRowVar extends ICategoricalVarDef>(
//   freqs: BivariateJointFrequencyDataFrame<number, TColVar, TRowVar>,
//   conditionOnVar: TColVar | TRowVar,
//   conditionOnState: RandomVarState,
//   state: RandomVarState
// ): number {
//   freqs.assertContainsVariable(conditionOnVar);

//   const other = freqs.getOtherVariable(conditionOnVar);
//   const jointFreq = freqs.getFrequency(conditionOnVar, conditionOnState, other, state);
//   const marginalFreq = freqs.getMarginalFrequency(freqs.getVariableIndex(conditionOnVar), conditionOnState);

//   // TODO: throw Error or return NaN?
//   return jointFreq / marginalFreq;
// }

export interface IGetCPsFromJointXOptions {
  fractionDigits?: number;
}

/**
 * Calcualates the conditional probabilites of all states of a variable given all states of another variable
 *
 * @param freqs
 * @param conditionOnColumnVar Whether the column or row variable is the conditioning variable
 * @returns
 */
export function getConditionalProbabilitiesFromJointFrequency<TColVar extends ICategoricalVarDef, TRowVar extends ICategoricalVarDef>(
  freqs: BivariateJointFrequencyDataFrame<number, TColVar, TRowVar>,
  conditionOnColumnVar: boolean,
  options?: IGetCPsFromJointXOptions
): BivariateCPDataFrame<number, TColVar, TRowVar> {
  const fractionDigits = options?.fractionDigits;

  const sums = freqs.data.sum({ axis: conditionOnColumnVar ? 0 : 1 });
  const probs = cloneDeep(freqs.values);

  // const probs = freqs.copy();
  const sumValues = sums.values as number[];
  for (let x = 0; x < freqs.data.columns.length; ++x) {
    for (let y = 0; y < freqs.data.index.length; ++y) {
      const value = freqs.values[y][x] / sumValues[conditionOnColumnVar ? x : y];
      probs[y][x] = fractionDigits == null ? value : roundTo(value, fractionDigits);
    }
  }

  return new BivariateCPDataFrame(probs, {
    variables: freqs.variables,
    isConditionedOnColumnVariable: conditionOnColumnVar,
  });
}

/**
 * Calcualates the conditional probabilites of all states of a variable given all states of another variable
 *
 * @param jointProbs
 * @param conditionOnColumnVar Whether the column or row variable is the conditioning variable
 * @returns
 */
export function getCPsFromJointProbs<TColVar extends ICategoricalVarDef, TRowVar extends ICategoricalVarDef>(
  jointProbs: BivariateJointProbabilityDataFrame<number, TColVar, TRowVar>,
  conditionOnColumnVar: boolean,
  options?: IGetCPsFromJointXOptions
): BivariateCPDataFrame<number, TColVar, TRowVar> {
  const fractionDigits = options?.fractionDigits;

  const sums = jointProbs.data.sum({ axis: conditionOnColumnVar ? 0 : 1 });
  const probs = cloneDeep(jointProbs.values);

  const sumValues = sums.values as number[];
  for (let x = 0; x < jointProbs.data.columns.length; ++x) {
    for (let y = 0; y < jointProbs.data.index.length; ++y) {
      const value = jointProbs.values[y][x] / sumValues[conditionOnColumnVar ? x : y];
      probs[y][x] = fractionDigits == null ? value : roundTo(value, fractionDigits);
    }
  }

  return new BivariateCPDataFrame(probs, {
    variables: jointProbs.variables,
    isConditionedOnColumnVariable: conditionOnColumnVar,
  });
}

// TODO: make generic
export function getConditionalProbabilitiesFromJointFrequencies(
  // dataset: TabularDataset<unknown, ICategoricalVarDef>,
  frequencies: Record<string, BivariateJointFrequencyDataFrame[]>
): Record<string, BivariateCPDataFrame[]> {
  return R.mapObjIndexed(
    (freqFrames) => freqFrames.map((freqs) => getConditionalProbabilitiesFromJointFrequency(freqs, false)),
    frequencies
  );
}

export interface ICondProbsWithMarginals {
  givenMarginalProbs: number[];
  condProbs: BivariateCPDataFrame;
}

export function getJointProbsFromProbsAndCPs(cpsWithMarginals: ICondProbsWithMarginals): BivariateJointProbabilityDataFrame {
  const { condProbs, givenMarginalProbs } = cpsWithMarginals;

  const conditionOnColumnVar = condProbs.isConditionedOnColumnVariable;

  const probs = cloneDeep(condProbs.values);

  // const probs = freqs.copy();
  for (let x = 0; x < condProbs.data.columns.length; ++x) {
    for (let y = 0; y < condProbs.data.index.length; ++y) {
      probs[y][x] = condProbs.values[y][x] * givenMarginalProbs[conditionOnColumnVar ? x : y];
    }
  }

  return new BivariateJointProbabilityDataFrame(probs, {
    variables: cpsWithMarginals.condProbs.variables,
  });
}
