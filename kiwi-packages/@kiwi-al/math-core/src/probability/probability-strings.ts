import { RandomVarState } from '../variables';

export function varSetToState(varName: string, state: RandomVarState): string {
  return `${varName}=${state}`;
}

export function stateAsProbability(varName: string, state: RandomVarState): string {
  return `P(${varName}=${state})`;
}

export function stateAsConditionalProbability(varName: string, state: RandomVarState, condVarName: string): string {
  return `P(${varName}=${state}|${condVarName})`;
}
