import i18next, { i18n } from 'i18next';

import { formatSetOperationSymbol, SetOperation } from '../sets';
import { PhraseOrNotationFormat, PhraseOrNotationFormatEnum } from '../shared/format';
import { toLocaleFixed } from '../utils';
import { formatVariableWithState, ICategoricalVarDef, VariableWithState, VariableWithStateFormat } from '../variables';

export enum ProbabilityFormatEnum {
  Percent = 'Percent',
  Decimal = 'Decimal',
}
export type ProbabilityFormat = keyof typeof ProbabilityFormatEnum;

export interface IFormatProbabilityOptions {
  format: ProbabilityFormat;
  fractionDigits?: number;
}

export function formatProbability(prob: number, { format, fractionDigits }: IFormatProbabilityOptions): string {
  fractionDigits = fractionDigits == null ? 2 : fractionDigits;
  switch (format) {
    case ProbabilityFormatEnum.Decimal:
      return toLocaleFixed(prob, fractionDigits);
    case ProbabilityFormatEnum.Percent:
      return `${toLocaleFixed(prob * 100, fractionDigits)}%`;
    default:
      throw new Error(`Unknown ProbabilityFormat '${format}'`);
  }
}

export function formatProbability2(probability: number): string {
  // TODO: proper decimalSeparator
  return formatProbability(probability, { format: 'Decimal', fractionDigits: 2 }).replace('.', ',');
}

export function formatPercent0(probability: number): string {
  // TODO: proper decimalSeparator
  return formatProbability(probability, { format: 'Percent', fractionDigits: 0 }).replace('.', ',');
}

export function formatPercent2(probability: number): string {
  // TODO: proper decimalSeparator
  return formatProbability(probability, { format: 'Percent', fractionDigits: 2 }).replace('.', ',');
}

export interface IFormatConditionalProbabilityOptions {
  varFormat?: VariableWithStateFormat;
  varFormatCond?: VariableWithStateFormat;
  varFormatGiven?: VariableWithStateFormat;
  condProbFormat: PhraseOrNotationFormat;
  givenSymbol?: string;
  i18n?: i18n;
}

export function formatConditionalProbability(
  conditionedVar: VariableWithState,
  conditioningVar: VariableWithState | ICategoricalVarDef,
  { varFormat, varFormatCond, varFormatGiven, condProbFormat, givenSymbol = '|', i18n }: IFormatConditionalProbabilityOptions
): string {
  i18n = i18n || i18next;

  const conditionedVariable = formatVariableWithState(conditionedVar, { format: varFormatCond || varFormat, i18n });
  const conditioningVariable = Array.isArray(conditioningVar)
    ? formatVariableWithState(conditioningVar, { format: varFormatGiven || varFormat, i18n })
    : i18n.t(conditioningVar.name);
  switch (condProbFormat) {
    case PhraseOrNotationFormatEnum.Phrase:
      return i18n.t('math:conditional-probability-of', { conditionedVariable, conditioningVariable });
    case PhraseOrNotationFormatEnum.Notation:
      return `P(${conditionedVariable}${givenSymbol}${conditioningVariable})`;
    default:
      throw new Error(`Unknown ProbabilityFormat '${condProbFormat}'`);
  }
}

export interface IFormatJointProbabilityOptions {
  varFormat: VariableWithStateFormat;
  jointProbFormat: PhraseOrNotationFormat;
  i18n?: i18n;
}

export function formatJointProbability(
  var1: VariableWithState,
  var2: VariableWithState,
  { varFormat, jointProbFormat, i18n }: IFormatJointProbabilityOptions
): string {
  i18n = i18n || i18next;
  const variable1 = formatVariableWithState(var1, { format: varFormat, i18n });
  const variable2 = formatVariableWithState(var2, { format: varFormat, i18n });
  switch (jointProbFormat) {
    case PhraseOrNotationFormatEnum.Phrase:
      return i18n.t('math:joint-probability-of-variables-2', { variable1, variable2 });
    case PhraseOrNotationFormatEnum.Notation:
      return `P(${variable1}∩${variable2})`;
    default:
      throw new Error(`Unknown ProbabilityFormat '${jointProbFormat}'`);
  }
}

export interface IFormatProbabilityExpressionOptions {
  varFormat: VariableWithStateFormat;
  probFormat: PhraseOrNotationFormat;
  i18n?: i18n;
}

export function formatProbabilityExpression(
  varInfo: VariableWithState,
  { varFormat, probFormat, i18n }: IFormatProbabilityExpressionOptions
): string {
  i18n = i18n || i18next;
  const variable = formatVariableWithState(varInfo, { format: varFormat, i18n });
  switch (probFormat) {
    case PhraseOrNotationFormatEnum.Phrase:
      return i18n.t('math:probability-of-variable', { variable });
    case PhraseOrNotationFormatEnum.Notation:
      return `P(${variable})`;
    default:
      throw new Error(`Unknown ProbabilityFormat '${probFormat}'`);
  }
}

export interface IFormatProbabilitySetOperationOptions {
  varFormat: VariableWithStateFormat;
  setFormat: PhraseOrNotationFormat;
  i18n?: i18n;
}

export function formatProbabilitySetOperation(
  var1: VariableWithState,
  var2: VariableWithState,
  op: SetOperation,
  { varFormat, setFormat, i18n }: IFormatProbabilitySetOperationOptions
): string {
  const symbol = formatSetOperationSymbol(op, { format: setFormat, i18n });
  const var1Text = formatVariableWithState(var1, { format: varFormat, i18n });
  const var2Text = formatVariableWithState(var2, { format: varFormat, i18n });

  return `P(${var1Text} ${symbol} ${var2Text})`;
}
