import { BivariateJointProbabilityDataFrame } from './BivariateJointProbabilityDataFrame';

export function calcIndependenceValues(jointProbs: BivariateJointProbabilityDataFrame): [number, number] {
  const marginals0 = jointProbs.getMarginalFrequencies(jointProbs.variables[0])[0];
  const marginals1 = jointProbs.getMarginalFrequencies(jointProbs.variables[1])[0];
  return [jointProbs.values[0][0], marginals0 * marginals1];
}

export function isDistributionIndependent(jointProbs: BivariateJointProbabilityDataFrame, acceptenceThreshold = 0.01): boolean {
  const indiValues = calcIndependenceValues(jointProbs);
  return Math.abs(indiValues[0] - indiValues[1]) < acceptenceThreshold;
}
