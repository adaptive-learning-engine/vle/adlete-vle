import { BivariateDataFrame, IBivariateDataFrameOptions } from '../tabular/BivariateDataframe';
import {
  BivarCategoricalVarDefs,
  formatVariableWithState,
  ICategoricalVarDef,
  IFormatVariableWithStateOptions,
  RandomVarState,
} from '../variables';

import { formatConditionalProbability, IFormatConditionalProbabilityOptions } from './format';

export interface ICPFormatting {
  given?: IFormatVariableWithStateOptions;
  conditional?: IFormatConditionalProbabilityOptions;
}

export interface BivariateCPDataFrameOptions<TColVar extends ICategoricalVarDef, TRowVar extends ICategoricalVarDef>
  extends IBivariateDataFrameOptions<TColVar, TRowVar> {
  // TODO: use index instead of column
  isConditionedOnColumnVariable: boolean;
  formats?: ICPFormatting;
}

/**
 * A 2-dimensional DataFrame containing the conditional probability of one variable given the other P(A|B).
 *
 * Conditioning variable is configurable to be column or row.
 */
export class BivariateCPDataFrame<
  TValue = number,
  TColVar extends ICategoricalVarDef = ICategoricalVarDef,
  TRowVar extends ICategoricalVarDef = ICategoricalVarDef,
> extends BivariateDataFrame<TValue, TColVar, TRowVar> {
  public declare options: BivariateCPDataFrameOptions<TColVar, TRowVar>;

  // must be any to conformt to danfojs DataFrame
  // eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types, @typescript-eslint/no-explicit-any
  public constructor(data: any, options: BivariateCPDataFrameOptions<TColVar, TRowVar>) {
    super(data, options);
  }

  public get isConditionedOnColumnVariable(): boolean {
    return this.options.isConditionedOnColumnVariable;
  }

  public getFormattedColumnNames(formats: ICPFormatting, displayVars?: BivarCategoricalVarDefs): string[] {
    const columnVarIndex = this.getVariableIndex(this.columnVariable);
    const columnVar = displayVars ? displayVars[columnVarIndex] : this.variables[columnVarIndex];
    const rowVarIndex = this.getVariableIndex(this.rowVariable);
    const rowVar = displayVars ? displayVars[rowVarIndex] : this.variables[rowVarIndex];

    if (this.isConditionedOnColumnVariable) {
      return columnVar.categories.map((cat) => formatVariableWithState([this.columnVariable, cat], formats.given));
    }
    return columnVar.categories.map((cat) => formatConditionalProbability([this.columnVariable, cat], rowVar, formats.conditional));
  }

  public getFormattedRowNames(formats: ICPFormatting, displayVars?: BivarCategoricalVarDefs): string[] {
    const columnVarIndex = this.getVariableIndex(this.columnVariable);
    const columnVar = displayVars ? displayVars[columnVarIndex] : this.variables[columnVarIndex];
    const rowVarIndex = this.getVariableIndex(this.rowVariable);
    const rowVar = displayVars ? displayVars[rowVarIndex] : this.variables[rowVarIndex];
    if (this.isConditionedOnColumnVariable) {
      return rowVar.categories.map((cat) => formatConditionalProbability([rowVar, cat], columnVar, formats.conditional));
    }
    return rowVar.categories.map((cat) => formatVariableWithState([rowVar, cat], formats.given));
  }

  public print(): void {
    this.data.customPrint(this.getFormattedColumnNames.bind(this), this.getFormattedRowNames.bind(this));
  }

  public copy(extraOptions?: Partial<BivariateCPDataFrameOptions<TColVar, TRowVar>>): BivariateCPDataFrame<TValue, TColVar, TRowVar> {
    const copy = new BivariateCPDataFrame<TValue, TColVar, TRowVar>(this.copyData(), { ...this.options, ...extraOptions });
    return copy;
  }

  public getConditionalVariable(): TColVar | TRowVar {
    const isConditionedOnColumn = this.options.isConditionedOnColumnVariable;
    return isConditionedOnColumn ? this.rowVariable : this.columnVariable;
  }

  public getGivenVariable(): TColVar | TRowVar {
    const isConditionedOnColumn = this.options.isConditionedOnColumnVariable;
    return isConditionedOnColumn ? this.columnVariable : this.rowVariable;
  }

  public getConditionalVariableIndex(): number {
    const isConditionedOnColumn = this.options.isConditionedOnColumnVariable;
    return isConditionedOnColumn ? 0 : 1;
  }

  public getGivenVariableIndex(): number {
    const isConditionedOnColumn = this.options.isConditionedOnColumnVariable;
    return isConditionedOnColumn ? 1 : 0;
  }

  /**
   * Returns the conditional probability for the conditional variable at state `state` given
   * the conditioning variables state `given`
   * @param state
   * @param given
   * @returns
   */
  public getConditionalProbability(state: RandomVarState, given: RandomVarState): number {
    const isConditionedOnColumn = this.options.isConditionedOnColumnVariable;

    const conditionalVar = isConditionedOnColumn ? this.rowVariable : this.columnVariable;
    const givenVar = isConditionedOnColumn ? this.columnVariable : this.rowVariable;

    const conditionalIndex = conditionalVar.categories.indexOf(state);
    const givenIndex = givenVar.categories.indexOf(given);

    if (conditionalIndex === -1) throw new Error(`Variable '${conditionalVar.name}' has no category '${state}'`);

    if (givenIndex === -1) throw new Error(`Variable '${givenVar.name}' has no category '${given}'`);

    return isConditionedOnColumn
      ? (this.data.iat(conditionalIndex, givenIndex) as number)
      : (this.data.iat(givenIndex, conditionalIndex) as number);
  }

  public setConditionalProbability(state: RandomVarState, given: RandomVarState, value: number): void {
    const isConditionedOnColumn = this.options.isConditionedOnColumnVariable;

    const conditionalVar = isConditionedOnColumn ? this.rowVariable : this.columnVariable;
    const givenVar = isConditionedOnColumn ? this.columnVariable : this.rowVariable;

    const conditionalIndex = conditionalVar.categories.indexOf(state);
    const givenIndex = givenVar.categories.indexOf(given);

    if (conditionalIndex === -1) throw new Error(`Variable '${conditionalVar.name}' has no category '${state}'`);

    if (givenIndex === -1) throw new Error(`Variable '${givenVar.name}' has no category '${given}'`);

    if (isConditionedOnColumn) this.data.iset(conditionalIndex, givenIndex, value);
    else this.data.iset(givenIndex, conditionalIndex, value);
  }

  public getConditionalProbabilitiesForCond(state: RandomVarState): number[] {
    const isConditionedOnColumn = this.options.isConditionedOnColumnVariable;

    const conditionalVar = isConditionedOnColumn ? this.rowVariable : this.columnVariable;
    const givenVar = isConditionedOnColumn ? this.columnVariable : this.rowVariable;

    const conditionalIndex = conditionalVar.categories.indexOf(state);

    if (conditionalIndex === -1) throw new Error(`Variable '${conditionalVar.name}' has no category '${state}'`);
    return isConditionedOnColumn
      ? givenVar.categories.map((cat, index) => this.data.iat(conditionalIndex, index) as number)
      : givenVar.categories.map((cat, index) => this.data.iat(index, conditionalIndex) as number);
  }

  public setConditionalProbabilitiesForCond(state: RandomVarState, probs: number[]): void {
    const isConditionedOnColumn = this.options.isConditionedOnColumnVariable;

    const conditionalVar = isConditionedOnColumn ? this.rowVariable : this.columnVariable;
    const givenVar = isConditionedOnColumn ? this.columnVariable : this.rowVariable;

    const conditionalIndex = conditionalVar.categories.indexOf(state);

    if (conditionalIndex === -1) throw new Error(`Variable '${conditionalVar.name}' has no category '${state}'`);

    if (isConditionedOnColumn) givenVar.categories.map((cat, index) => this.data.iset(conditionalIndex, index, probs[index]));
    else givenVar.categories.map((cat, index) => this.data.iset(index, conditionalIndex, probs[index]));
  }

  public getConditionalProbabilitiesForGiven(given: RandomVarState): number[] {
    const isConditionedOnColumn = this.options.isConditionedOnColumnVariable;

    const conditionalVar = isConditionedOnColumn ? this.rowVariable : this.columnVariable;
    const givenVar = isConditionedOnColumn ? this.columnVariable : this.rowVariable;

    const givenIndex = givenVar.categories.indexOf(given);
    if (givenIndex === -1) throw new Error(`Variable '${givenVar.name}' has no category '${given}'`);

    return isConditionedOnColumn
      ? conditionalVar.categories.map((cat, index) => this.data.iat(index, givenIndex) as number)
      : conditionalVar.categories.map((cat, index) => this.data.iat(givenIndex, index) as number);
  }

  public setConditionalProbabilitiesForGiven(given: RandomVarState, probs: number[]): void {
    const isConditionedOnColumn = this.options.isConditionedOnColumnVariable;

    const conditionalVar = isConditionedOnColumn ? this.rowVariable : this.columnVariable;
    const givenVar = isConditionedOnColumn ? this.columnVariable : this.rowVariable;

    const givenIndex = givenVar.categories.indexOf(given);
    if (givenIndex === -1) throw new Error(`Variable '${givenVar.name}' has no category '${given}'`);

    if (isConditionedOnColumn) conditionalVar.categories.map((cat, index) => this.data.iset(index, givenIndex, probs[index]));
    else conditionalVar.categories.map((cat, index) => this.data.iset(givenIndex, index, probs[index]));
  }
}
