import { BivariateJointFrequencyDataFrame } from '../frequencies';
import { ICategoricalVarDef } from '../variables';

export class BivariateJointProbabilityDataFrame<
  TValue = number,
  TColVar extends ICategoricalVarDef = ICategoricalVarDef,
  TRowVar extends ICategoricalVarDef = ICategoricalVarDef,
> extends BivariateJointFrequencyDataFrame<TValue, TColVar, TRowVar> {}
