import { IBivarProbDataMarginalJoint } from '../frequencies';
import { BivarCategoricalVarDefs } from '../variables';

import { BivariateJointProbabilityDataFrame } from './BivariateJointProbabilityDataFrame';

export enum ProbabilityGenInputProperty {
  MarginalProb1 = 'MarginalProb1',
  MarginalProb2 = 'MarginalProb2',
  JointProb = 'JointProb',
}

export function validateProbGenInput(input: IBivarProbDataMarginalJoint, fixed: ProbabilityGenInputProperty): IBivarProbDataMarginalJoint {
  // putting the numbers into range
  input.jointProb = Math.min(input.jointProb, 1);
  input.marginalProbs[0] = Math.min(input.marginalProbs[0], 1);
  input.marginalProbs[1] = Math.min(input.marginalProbs[1], 1);

  if (fixed === ProbabilityGenInputProperty.JointProb) {
    // make marginal probs at least jointFreq
    input.marginalProbs[0] = Math.max(input.jointProb, input.marginalProbs[0]);
    input.marginalProbs[1] = Math.max(input.jointProb, input.marginalProbs[1]);
  } else {
    // make.jointProb max marginal
    input.jointProb = Math.min(input.jointProb, input.marginalProbs[0], input.marginalProbs[1]);
  }

  const aAndB = input.jointProb;
  const aAndNotB = input.marginalProbs[0] - aAndB;
  const notAAndB = input.marginalProbs[1] - aAndB;
  const notAAndNotB = 1 - aAndB - aAndNotB - notAAndB;
  switch (fixed) {
    case ProbabilityGenInputProperty.JointProb:
      if (notAAndNotB < 0) {
        const extra = -notAAndNotB;
        const leverageA = input.marginalProbs[0] - input.jointProb;
        const leverageB = input.marginalProbs[1] - input.jointProb;
        const factor = leverageB === 0 ? 1 : leverageA / leverageB;
        const removeFromA = extra * factor;
        const removeFromB = extra - removeFromA;
        input.marginalProbs[0] -= removeFromA;
        input.marginalProbs[1] -= removeFromB;
      }
      break;
    case ProbabilityGenInputProperty.MarginalProb1:
    case ProbabilityGenInputProperty.MarginalProb2: {
      // const marginalIndex = fixed === ProbabilityGenInputProperty.MarginalProb1 ? 0 : 1;
      const marginalOtherIndex = fixed === ProbabilityGenInputProperty.MarginalProb1 ? 1 : 0;
      if (notAAndNotB < 0) {
        const extra = -notAAndNotB;
        const leverageB = input.marginalProbs[marginalOtherIndex] - input.jointProb;

        let removeFromJF = 0;
        let removeFromB = 0;
        if (extra <= leverageB) {
          // remove everything from B if possible
          removeFromB = extra;
        } else {
          const extraExtra = extra - leverageB;
          const diff = extraExtra / 2;
          removeFromJF = extraExtra - diff; // removing bigger part
          removeFromB = leverageB + diff; // removing smaller part
        }

        input.jointProb -= removeFromJF;
        input.marginalProbs[marginalOtherIndex] -= removeFromB;
      }
      break;
    }
    default:
      throw new Error(`Unsupported fixed property ${fixed}`);
  }
  return input;
}

export function jointProbDFToProbDataMarginalJoint(jointProbs: BivariateJointProbabilityDataFrame): IBivarProbDataMarginalJoint {
  const variables = jointProbs.variables;
  const marginals0 = jointProbs.getMarginalFrequencies(variables[0]);
  const marginals1 = jointProbs.getMarginalFrequencies(variables[1]);
  return {
    marginalProbs: [marginals0[0], marginals1[0]],
    jointProb: jointProbs.values[0][0],
  };
}

export function probDataMarginalJointToJointProbDF(
  input: IBivarProbDataMarginalJoint,
  variables: BivarCategoricalVarDefs
): BivariateJointProbabilityDataFrame {
  const marginalRow = input.marginalProbs[0];
  const marginalColumn = input.marginalProbs[1];
  const a11 = input.jointProb;
  const a12 = marginalRow - a11;
  const a21 = marginalColumn - a11;
  const a22 = 1 - a11 - a12 - a21;
  const data = [
    [a11, a12],
    [a21, a22],
  ];
  return new BivariateJointProbabilityDataFrame(data, { variables });
}
