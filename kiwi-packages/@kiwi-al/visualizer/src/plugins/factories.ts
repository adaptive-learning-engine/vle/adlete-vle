import type { IPluginFactory, IPluginMeta } from '@fki/plugin-system/specs';

import { LayoutManagerMeta, LayoutManagerPlugin } from '@adlete/visualizer-core/plugin-layout/LayoutManagerPlugin';
import { ThemeManagerMeta, ThemeManagerPlugin } from '@adlete/visualizer-core/plugin-theme/ThemeManagerPlugin';

import { BUILTIN_LAYOUTS } from './layout/builtinLayouts';
import { IEditorPlugins } from './plugins';
import { KiwiVLEMeta, KiwiVLEPlugin } from './vle/VLEPlugin';

// helper function to create non-async factories
function createInstantFactory<K extends keyof IEditorPlugins>(
  meta: IPluginMeta<keyof IEditorPlugins, K>,
  create: () => IEditorPlugins[K]
): IPluginFactory<IEditorPlugins, K> {
  return {
    meta,
    create: () => Promise.resolve(create()),
  };
}

export const FACTORIES: IPluginFactory<IEditorPlugins, keyof IEditorPlugins>[] = [
  createInstantFactory(LayoutManagerMeta, () => new LayoutManagerPlugin(BUILTIN_LAYOUTS)),
  createInstantFactory(ThemeManagerMeta, () => new ThemeManagerPlugin()),
  createInstantFactory(KiwiVLEMeta, () => new KiwiVLEPlugin()),
];
