import AccountTree from '@mui/icons-material/AccountTree';
import React from 'react';

import { IWithLayoutManager } from '@adlete/visualizer-core/plugin-layout/LayoutManagerPlugin';

import { VLE } from '@adlete-vle/vle-core/VLE';

import type { IPlugin, IPluginManager, IPluginMeta } from '@fki/plugin-system/specs';

export interface IWithKiwiVLE extends IWithLayoutManager {
  kiwiVLE: KiwiVLEPlugin;
}

export const KiwiVLEMeta: IPluginMeta<keyof IWithKiwiVLE, 'kiwiVLE'> = {
  id: 'kiwiVLE',
  dependencies: ['layoutManager'],
};

export class KiwiVLEPlugin implements IPlugin<IWithKiwiVLE> {
  public meta: IPluginMeta<keyof IWithKiwiVLE, 'kiwiVLE'> = KiwiVLEMeta;

  public initialize(pluginMan: IPluginManager<IWithKiwiVLE>): Promise<void> {
    const layoutMan = pluginMan.get('layoutManager');
    layoutMan.panelTypes.add('KiwiVLE', {
      createPanel: () => (
        <div style={{ padding: '5px' }}>
          <VLE />
        </div>
      ),
      title: 'KIWI VLE',
      icon: AccountTree,
    });
    layoutMan.panelTypes.add('Empty', {
      createPanel: () => <div />,
      title: 'Empty',
      icon: AccountTree,
    });
    return Promise.resolve();
  }

  // eslint-disable-next-line class-methods-use-this
  public terminate(): Promise<void> {
    return Promise.resolve();
  }
}
