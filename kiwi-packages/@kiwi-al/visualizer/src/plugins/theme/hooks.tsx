import { Theme, ThemeProvider } from '@mui/material';
import React, { FunctionComponent } from 'react';

import { useStatefulEvent } from '@adlete-utils/events-react';

import { usePlugin } from '../../app/hooks';

export interface ICustomThemeProviderProps {
  children: React.ReactNode;
}

export function useCustomTheme(): Theme {
  const themeMan = usePlugin('themeManager');
  return useStatefulEvent(themeMan.events, 'themeChanged');
}

export const CustomThemeProvider: FunctionComponent = ({ children }: ICustomThemeProviderProps) => {
  const theme = useCustomTheme();
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};
