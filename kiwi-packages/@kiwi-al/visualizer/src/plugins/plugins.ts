import type { IWithLayoutManager } from '@adlete/visualizer-core/plugin-layout/LayoutManagerPlugin';
import type { IWithThemeManager } from '@adlete/visualizer-core/plugin-theme/ThemeManagerPlugin';

import type { IWithAppCore } from '../app/AppCore';

import { IWithKiwiVLE } from './vle/VLEPlugin';

export interface IEditorPlugins extends IWithAppCore, IWithLayoutManager, IWithThemeManager, IWithKiwiVLE {}
