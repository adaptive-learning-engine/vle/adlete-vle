import { ILayout } from '@fki/editor-core/layout/LayoutManager';

export const BUILTIN_LAYOUTS: Record<string, ILayout> = {
  default: {
    name: 'default',
    isBuiltin: true,
    model: {
      global: {
        splitterSize: 4,
      },
      borders: [
        {
          type: 'border',
          location: 'left',
          children: [],
        },
      ],
      layout: {
        type: 'row',
        weight: 100,
        children: [
          {
            type: 'tabset',
            weight: 20,
            selected: 0,
            children: [
              {
                type: 'tab',
                name: 'KIWI VLE',
                component: 'KiwiVLE',
              },
            ],
          },
        ],
      },
    },
  },
};
