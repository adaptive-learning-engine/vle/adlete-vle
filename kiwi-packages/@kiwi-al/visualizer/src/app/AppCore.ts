import PluginBasedApp from '@fki/plugin-based-app';
import { PluginFactoryRegistry } from '@fki/plugin-system/PluginFactoryRegistry';
import type { IPluginMeta } from '@fki/plugin-system/specs';

import { FACTORIES } from '../plugins/factories';
import { IEditorPlugins } from '../plugins/plugins';

export interface IWithAppCore {
  appCore: AppCore;
}

export const AppCoreMeta: IPluginMeta<keyof IEditorPlugins, 'appCore'> = {
  id: 'appCore',
};

export class AppCore extends PluginBasedApp<IEditorPlugins> {
  public constructor() {
    const registry = new PluginFactoryRegistry<IEditorPlugins>();

    FACTORIES.forEach((factory) => {
      this.pluginFactories.add(factory.meta.id, factory);
    });

    super(AppCoreMeta, registry);
  }
}
