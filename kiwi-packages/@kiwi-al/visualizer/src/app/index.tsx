import React from 'react';
import { createRoot } from 'react-dom/client';

import { App } from './App';

const root = createRoot(document.getElementById('root'));

root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

if (import.meta.hot) {
  // eslint-disable-next-line no-console
  import.meta.hot.on('vite:beforeUpdate', () => console.clear());
  import.meta.hot.accept((newModule) => {
    // eslint-disable-next-line no-console
    console.info('updated: count is now ', newModule.count);
  });
}

// adding quick debugger functionality
declare global {
  interface Window {
    debuggerIn: (seconds: number) => void;
  }
}

window.debuggerIn = (seconds: number) => {
  setTimeout(() => {
    // eslint-disable-next-line no-debugger
    debugger;
  }, seconds * 1000);
};
