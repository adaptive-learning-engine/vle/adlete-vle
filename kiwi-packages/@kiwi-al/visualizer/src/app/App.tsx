import * as React from 'react';
import 'typeface-roboto';

import { ReactElement, useEffect, useMemo } from 'react';

import { StyledFlexibleLayout } from '@fki/editor-core/theme/StyledFlexibleLayout';

import { Box, CircularProgress, CssBaseline } from '@mui/material';

import { CustomThemeProvider } from '../plugins/theme/hooks';

import { AppCore } from './AppCore';
import { AppCoreContext } from './AppCoreContext';
import { Menu } from './Menu';

interface IAppUIProps {
  core: AppCore;
}

function AppUI({ core }: IAppUIProps): ReactElement {
  const layoutMan = core.plugins.get('layoutManager');
  return (
    <CustomThemeProvider>
      <CssBaseline />
      <Box display="flex" flexDirection="column" style={{ height: '100%' }}>
        <Box>
          <Menu />
        </Box>
        <Box display="flex" flexGrow="1" style={{ position: 'relative' }}>
          <StyledFlexibleLayout layoutManager={layoutMan} />
        </Box>
      </Box>
    </CustomThemeProvider>
  );
}

export function App(): ReactElement {
  const [isInitialized, setIsInitialized] = React.useState(false);
  const core = useMemo(() => new AppCore(), []);

  useEffect(() => {
    async function inner() {
      await core.start();
      setIsInitialized(true);
    }
    inner();
  }, [core]);

  // only render application when project-manager was properly initialized
  let content = null;
  if (isInitialized) {
    content = <AppUI core={core} />;
  } else {
    content = <CircularProgress />;
  }

  return <AppCoreContext.Provider value={core}>{content}</AppCoreContext.Provider>;
}
