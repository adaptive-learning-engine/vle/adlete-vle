import MenuIcon from '@mui/icons-material/Menu';
import { AppBar, Box, IconButton, Toolbar, Typography } from '@mui/material';
import { CommonProps } from '@mui/material/OverridableComponent';
import { styled, Theme } from '@mui/material/styles';
import React, { FunctionComponent } from 'react';

import { AddPanelMenu } from '@fki/editor-core/layout/AddPanelMenu';
import { LayoutChooser } from '@fki/editor-core/layout/LayoutChooser';
import { ToggleDarkLightButton } from '@fki/editor-core/theme/ToggleDarkLightButton';

import { usePlugin } from './hooks';

function styleFn({ theme }: { theme: Theme }) {
  return `
  & .layout-chooser {
    margin-right: ${theme.spacing(2)};
  }

  & .title {
    flex-grow: 1;
  }`;
}

export type IMenuProps = CommonProps;

export const Menu: FunctionComponent = styled(({ className }: IMenuProps) => {
  const layoutMan = usePlugin('layoutManager');
  const themeMan = usePlugin('themeManager');

  return (
    <AppBar className={className} position="relative" color="default">
      <Toolbar>
        <IconButton edge="start" color="inherit" aria-label="menu">
          <MenuIcon />
        </IconButton>
        <Typography className="title">Arithmetics Visualizer</Typography>
        <Box className="layout-chooser">
          <LayoutChooser layoutManager={layoutMan} />
        </Box>
        <AddPanelMenu layoutManager={layoutMan} />
        <ToggleDarkLightButton themeManager={themeMan} />
      </Toolbar>
    </AppBar>
  );
})(styleFn);
