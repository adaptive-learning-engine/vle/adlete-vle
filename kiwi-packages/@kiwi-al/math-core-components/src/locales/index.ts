import i18next, { type i18n } from 'i18next';

import { addResourceBundleWithNS } from '@adlete-vle/lr-core/locale';

import deMessages from './de';
import enMessages from './en';

const ADDED_RESOURCES = new WeakMap<i18n, boolean>();
export function addMathComponentLocales(i18nextInstance?: i18n): void {
  i18nextInstance = i18nextInstance || i18next;
  if (!ADDED_RESOURCES.has(i18nextInstance)) {
    addResourceBundleWithNS('de', deMessages, i18nextInstance);
    addResourceBundleWithNS('en', enMessages, i18nextInstance);
    ADDED_RESOURCES.set(i18nextInstance, true);
  }
}
