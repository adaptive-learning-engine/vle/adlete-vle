const messages = {
  'math-components': {
    'joint-freq-gen-input-info':
      'While handling the sliders, other sliders might change their values to still generate valid bivariate results. The total population is limited to {maxPopulation}.',
    'joint-prob-gen-input-info':
      'While handling the sliders, other sliders might change their values to still generate valid bivariate results',
  },
};

// eslint-disable-next-line import/no-default-export
export default messages;
