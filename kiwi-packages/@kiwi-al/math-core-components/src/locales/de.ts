const messages = {
  'math-components': {
    'joint-freq-gen-input-info':
      'Bei der Bedienung der Schieberegler verändern sich gegebenenfalls die Werte der anderen Schieberegler um eine valide bivariate Verteilung zu erzeugen. Die maximale Gesamtpopulation ist auf {maxPopulation} beschränkt.',
    'joint-prob-gen-input-info':
      'Bei der Bedienung der Schieberegler verändern sich gegebenenfalls die Werte der anderen Schieberegler um eine valide bivariate Verteilung zu erzeugen',
  },
};

// eslint-disable-next-line import/no-default-export
export default messages;
