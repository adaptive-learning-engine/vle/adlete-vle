import { Box } from '@mui/material';
import React, { useMemo } from 'react';

import { useMemoWith } from '@adlete-vle/utils/hooks';

import { IEvent } from '@kiwi-al/math-core/events';
import { compareOutcome, Outcome, variablesToSampleSpace2 } from '@kiwi-al/math-core/random-experiment';
import { ICategoricalVarDef } from '@kiwi-al/math-core/variables';

import { SingleColorMap } from '../shared/colors';

export interface IOutcomeProps {
  outcome: Outcome;
  // hidden: boolean;
}

export interface IColoredEvent extends IEvent {
  color?: string;
}

export function coloredEventsToColorMap(events: IColoredEvent[]): SingleColorMap {
  const colorMap: SingleColorMap = {};
  events.forEach((event) => {
    if (event.color) colorMap[event.name] = event.color;
  });
  return colorMap;
}

export function createGridToIndexMap(variables: ICategoricalVarDef[]): number[][] {
  const var0 = variables[0];
  const var1 = variables[1];

  const numColumns = variables[1].categories.length;

  // expecting ids to follow the schema from variablesToSampleSpace2
  // return ids.reduce((map, id, i) => {
  //   const x = i % numColumns;
  //   const y = Math.floor(i/numColumns);
  //   map[`${y}-${x}`] = id;
  //   return map;
  // }, {} as Record<string, string>)

  return var0.categories.map((cat0, y) => var1.categories.map((cat1, x) => y * numColumns + x));
}

export function mapEventsToOutcomeIndices(events: IColoredEvent[], outcomes: Outcome[]): IColoredEvent[][] {
  const eventsByOutcome: IColoredEvent[][] = outcomes.map(() => []);

  events.forEach((event) =>
    event.outcomes.forEach((outcome) => {
      const index = outcomes.findIndex((value) => compareOutcome(value, outcome));
      if (index === -1) throw new Error('Could not find outcome!');
      eventsByOutcome[index].push(event);
    })
  );

  return eventsByOutcome;
}

function withEmptyEvents(events: IColoredEvent[], minWraps: number): IColoredEvent[] {
  if (events.length === minWraps) return events;

  const result = [...events];
  for (let i = 0; i < minWraps - events.length; ++i) result.push(null);
  return result;
}

export function wrapWithEventBoxes(events: IColoredEvent[], minWraps: number, children: React.ReactElement): React.ReactElement {
  const allEvents = withEmptyEvents(events, minWraps);
  return allEvents.reduce(
    (wrapped, event) => (
      <Box sx={{ border: `2px solid ${event ? event.color : 'transparent'}`, padding: '2px', display: 'inline-block' }}>{wrapped}</Box>
    ),
    children
  );
}

export function belongsToEvent(events: IColoredEvent[], targetEvent: IColoredEvent): boolean {
  return events.includes(targetEvent);
}

export interface ISampleSpaceTableProps {
  events?: IColoredEvent[];
  variables: ICategoricalVarDef[];
  OutcomeComponent: React.ComponentType<IOutcomeProps>;
  focusEvent?: IColoredEvent;
  minWraps?: number;
  // TODO: support sorting
}

export function SampleSpaceTable(props: ISampleSpaceTableProps): React.ReactElement {
  const { variables, OutcomeComponent, focusEvent } = props;

  const events = props.events || [];

  const outcomes = useMemoWith(variablesToSampleSpace2, [variables]);
  const eventsByOutcome = useMemoWith(mapEventsToOutcomeIndices, [events, outcomes]);
  const minWraps = props.minWraps || eventsByOutcome.reduce((prev, curr) => Math.max(prev, curr.length), 0);

  const gridToIndexMap = useMemoWith(createGridToIndexMap, [variables]);

  // rendering to grid
  const elements = useMemo(() => outcomes.map((outcome) => <OutcomeComponent outcome={outcome} />), [outcomes, OutcomeComponent]);
  const rows = gridToIndexMap.map((row, rowIndex) => (
    // eslint-disable-next-line react/no-array-index-key
    <tr key={rowIndex}>
      {row.map((index) => (
        <td key={index} style={{ opacity: focusEvent == null || belongsToEvent(eventsByOutcome[index], focusEvent) ? 1 : 0.2 }}>
          {wrapWithEventBoxes(eventsByOutcome[index], minWraps, elements[index])}
        </td>
      ))}
    </tr>
  ));

  return (
    <table>
      <tbody>{rows}</tbody>
    </table>
  );
}
