import { AxisBottom, AxisLeft } from '@visx/axis';
import { Group } from '@visx/group';
import { scaleBand } from '@visx/scale';
import { Bar } from '@visx/shape';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import type { RecursivePartial } from '@adlete-utils/typescript';
import { useMemoWith } from '@adlete-vle/utils/hooks';

import { formatProbability } from '@kiwi-al/math-core/probability';
import { ICategoricalVarDef } from '@kiwi-al/math-core/variables';

import { createColorScale, createProbabilityScaleInv } from '../shared/scales';
import { IViewport, mergeDefaultViewport } from '../shared/viewport';

export type IProbabilityBarChartProps = {
  // TODO: use UnivariateSeries instead?
  probabilities: number[];
  variable: ICategoricalVarDef;
  viewport?: RecursivePartial<IViewport>;
  // events?: boolean;
};

const DEFAULT_VIEWPORT: IViewport = {
  width: 400,
  height: 250,
  margin: {
    left: 40,
    right: 0,
    top: 20,
    bottom: 25,
  },
};

export function ProbabilityBarChart(props: IProbabilityBarChartProps): React.ReactElement {
  const { probabilities, variable, viewport } = props;
  const { width, height, margin } = mergeDefaultViewport(viewport, DEFAULT_VIEWPORT);

  const { t } = useTranslation();

  const categories = useMemo(() => variable.categories.map((cat) => t(cat.toString())), [variable, t]);

  // === bounds
  const xMax = width - margin.left - margin.right;
  const yMax = height - margin.top - margin.bottom;

  // === Scales
  const colorScale = useMemoWith(createColorScale, [categories]);
  const probabilityScale = useMemoWith(createProbabilityScaleInv, [yMax]);

  // creating and memoizing the x-scale
  const catScale = useMemo(
    () =>
      scaleBand<string>({
        domain: categories,
        padding: 0.2,
      }).rangeRound([0, xMax]),
    [xMax, categories]
  );

  return (
    <svg width={width} height={height}>
      <Group left={margin.left} top={margin.top}>
        {categories.map((cat, i) => {
          const barWidth = catScale.bandwidth();
          const barHeight = yMax - probabilityScale(probabilities[i]);
          const barX = catScale(cat);
          const barY = yMax - barHeight;
          return (
            <Group key={`bar-${cat}`} left={barX} top={barY}>
              <Bar width={barWidth} height={barHeight} fill={colorScale(cat)} />
              <text x={barWidth / 2} y={barHeight / 2} textAnchor="middle" alignmentBaseline="central">
                {formatProbability(probabilities[i], { format: 'Percent' })}
              </text>
            </Group>
          );
        })}
      </Group>
      <AxisBottom left={margin.left} top={margin.top + yMax} scale={catScale} />
      <AxisLeft left={margin.left} top={margin.top} scale={probabilityScale} />
    </svg>
  );
}
