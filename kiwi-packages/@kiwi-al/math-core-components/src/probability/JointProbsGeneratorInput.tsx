import InfoIcon from '@mui/icons-material/Info';
import { IconButton, SliderTypeMap, Stack, Table, TableBody, TableCell, TableRow } from '@mui/material';
import React, { useCallback, useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import { RichTooltip } from '@adlete-vle/lr-core-components/tooltips/RichTooltip';
import { useMemoWith } from '@adlete-vle/utils/hooks';

import {
  BivariateJointProbabilityDataFrame,
  formatProbabilityExpression,
  jointProbDFToProbDataMarginalJoint,
  ProbabilityGenInputProperty,
  probDataMarginalJointToJointProbDF,
  validateProbGenInput,
} from '@kiwi-al/math-core/probability';
import { formatVariableSetOperation } from '@kiwi-al/math-core/sets';
import {
  BivarCategoricalVarDefs,
  getVarWithState,
  jointVarsToString,
  variableToVarCatStrings,
  varWithCatToString,
} from '@kiwi-al/math-core/variables';

import { ColoredSlider } from '../shared/ColoredSlider';
import { SingleColorMap } from '../shared/colors';
import { isIncluded } from '../shared/isIncluded';

import { ProbabilitiesSlider } from './ProbabilitiesSlider';

export interface IJointProbsGeneratorInputProps {
  jointProbs: BivariateJointProbabilityDataFrame;
  onChange: (jointProbs: BivariateJointProbabilityDataFrame) => void;
  hiddenSliders?: ProbabilityGenInputProperty[];
  // disabledSliders?: ProbabilityGenInputProperty[];
  displayVars?: BivarCategoricalVarDefs;
  colors?: SingleColorMap;
}

// TODO:

export function JointProbsGeneratorInput(props: IJointProbsGeneratorInputProps): React.ReactElement {
  const { jointProbs, onChange, hiddenSliders, colors } = props;

  const variables = jointProbs.variables;

  const displayVars = props.displayVars || jointProbs.variables;

  const { t } = useTranslation();

  const input = useMemoWith(jointProbDFToProbDataMarginalJoint, [jointProbs]);

  const onMarginalFreqAChanged = useCallback(
    (probs: number[]) =>
      onChange(
        probDataMarginalJointToJointProbDF(
          validateProbGenInput(
            {
              ...input,
              marginalProbs: [probs[0], input.marginalProbs[1]],
            },
            ProbabilityGenInputProperty.MarginalProb1
          ),
          jointProbs.variables
        )
      ),
    [onChange, input, jointProbs.variables]
  );

  const onMarginalFreqBChanged = useCallback(
    (probs: number[]) => {
      onChange(
        probDataMarginalJointToJointProbDF(
          validateProbGenInput(
            {
              ...input,
              marginalProbs: [input.marginalProbs[0], probs[0]],
            },
            ProbabilityGenInputProperty.MarginalProb2
          ),
          jointProbs.variables
        )
      );
    },
    [onChange, input, jointProbs.variables]
  );

  const onMarginalProbsChanged = useMemo(
    () => [onMarginalFreqAChanged, onMarginalFreqBChanged],
    [onMarginalFreqAChanged, onMarginalFreqBChanged]
  );

  const marginalSliders = useMemo(
    () =>
      jointProbs.variables.map((variable, index) => {
        const property = index === 0 ? ProbabilityGenInputProperty.MarginalProb1 : ProbabilityGenInputProperty.MarginalProb2;
        const colorIds = variableToVarCatStrings(variable);
        const probColors = colors && colorIds.map((colorId) => colors[colorId]);
        const displayVar = displayVars[index];
        const marginals = jointProbs.getMarginalFrequencies(jointProbs.variables[index]);
        const labels = displayVar.categories.map((cat) =>
          formatProbabilityExpression([displayVar, cat], { varFormat: 'StateOnly', probFormat: 'Notation' })
        );
        const onChangeCB = onMarginalProbsChanged[index];
        return (
          !isIncluded(hiddenSliders, property) && (
            <TableRow key={variable.name}>
              {/* TODO: use grid instead of weird CSS hack? */}
              <TableCell style={{ whiteSpace: 'nowrap' }}>{t(displayVar.name)}</TableCell>
              <TableCell style={{ width: '99%' }}>
                <ProbabilitiesSlider
                  labels={labels}
                  probabilities={marginals}
                  onChange={onChangeCB}
                  colors={probColors}
                  // disabled={isIncluded(disabledSliders, ProbabilityGenInputProperty.MarginalProb1)}
                />
              </TableCell>
            </TableRow>
          )
        );
      }),
    [jointProbs, t, displayVars, hiddenSliders, onMarginalProbsChanged, colors]
  );

  const onJointProbChanged = useCallback(
    (event: Event, value: number) =>
      onChange(
        probDataMarginalJointToJointProbDF(
          validateProbGenInput(
            {
              ...input,
              jointProb: value,
            },
            ProbabilityGenInputProperty.JointProb
          ),
          jointProbs.variables
        )
      ),
    [onChange, input, jointProbs.variables]
  );

  const jointProbSlider = useMemo(() => {
    const value = jointProbs.values[0][0];
    const varsWithCats = [getVarWithState(variables, 0, 0), getVarWithState(variables, 1, 0)];
    const displayVarsWithCats = [getVarWithState(displayVars, 0, 0), getVarWithState(displayVars, 1, 0)];
    const colorId = jointVarsToString(varWithCatToString(varsWithCats[0]), varWithCatToString(varsWithCats[1]));
    return (
      <TableRow key="joint-prob">
        <TableCell>
          {formatVariableSetOperation(displayVarsWithCats[0], displayVarsWithCats[1], 'Intersection', {
            varFormat: 'StateOnly',
            setFormat: 'Notation',
          })}
        </TableCell>
        <TableCell>
          <ColoredSlider
            // track={false}
            min={0}
            max={1}
            step={0.01}
            value={value}
            onChange={onJointProbChanged}
            // This is a little hack for the typings to work when using custom colors
            color={colors?.[colorId] as SliderTypeMap['props']['color']}
          />
        </TableCell>
      </TableRow>
    );
  }, [jointProbs, onJointProbChanged, variables, displayVars, colors]);

  const tooltipTitle = t('math-components:joint-prob-gen-input-info');

  return (
    <Stack spacing={2} direction="row" alignItems="start">
      <Table size="small">
        <TableBody>
          {jointProbSlider}
          {marginalSliders}
        </TableBody>
      </Table>
      <RichTooltip title={tooltipTitle}>
        <IconButton color="error">
          <InfoIcon />
        </IconButton>
      </RichTooltip>
    </Stack>
    // <Stack spacing={2} direction="row" alignItems="start">
    //   <Stack spacing={2} sx={{ width: '100%' }}>
    //     {marginalSliders}
    //     {/* {!isIncluded(hiddenSliders, ProbabilityGenInputProperty.JointProb) && (
    //       <JointFrequencySlider
    //         variables={variables}
    //         categories={[variables[0].categories[0], variables[1].categories[0]]}
    //         value={inputConverted.data.jointFrequency}
    //         max={1}
    //         onChange={onJointFreqChanged}
    //         disabled={isIncluded(disabledSliders, ProbabilityGenInputProperty.JointProb)}
    //       />
    //     )} */}
    //   </Stack>
    //   {/* <RichTooltip title={tooltipTitle}>
    //     <IconButton>
    //       <InfoIcon />
    //     </IconButton>
    //   </RichTooltip> */}
    // </Stack>
  );
}
