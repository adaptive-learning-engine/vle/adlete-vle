import React from 'react';

import { RandomVarState } from '@kiwi-al/math-core/variables';

import { IVariableCatSliderBaseProps, VariableCatSliderBase } from '../shared/VariableCatSliderBase';

export type IProbabilitySliderProps<TCategory extends RandomVarState = RandomVarState> = Omit<
  IVariableCatSliderBaseProps<TCategory>,
  'min' | 'max'
>;

export function ProbabilitySlider<TCategory extends RandomVarState = RandomVarState>(
  props: IProbabilitySliderProps<TCategory>
): React.ReactElement {
  const step = props.step || 0.01;

  // eslint-disable-next-line react/jsx-props-no-spreading
  return <VariableCatSliderBase {...props} min={0} max={1} step={step} />;
}
