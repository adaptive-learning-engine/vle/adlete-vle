import { Table, TableBody, TableCell, TableRow } from '@mui/material';
import React, { useCallback, useMemo, useRef } from 'react';
import { useTranslation } from 'react-i18next';

import { BivariateCPDataFrame, formatConditionalProbability, formatProbabilityExpression } from '@kiwi-al/math-core/probability';
import { BivarCategoricalVarDefs } from '@kiwi-al/math-core/variables';

import { isIncluded } from '../shared/isIncluded';

import { ProbabilitiesSlider } from './ProbabilitiesSlider';
import { getCPDisplayVariableSet } from './shared';

export interface IJointProbsFromCPsGeneratorInput {
  givenMarginalProbs: number[];
  condProbs: BivariateCPDataFrame;
}

export type SliderType = 'marginal' | 'conditional';

export interface IJointProbsFromCPsGeneratorInputProps {
  input: IJointProbsFromCPsGeneratorInput;
  displayVariables?: BivarCategoricalVarDefs;
  onChange: (input: IJointProbsFromCPsGeneratorInput) => void;
  hiddenSliders?: SliderType[];
  // disabledSliders?: SliderType[];
}

export function JointProbsFromCPsGeneratorInput(props: IJointProbsFromCPsGeneratorInputProps): React.ReactElement {
  const { onChange, input, hiddenSliders, displayVariables } = props;
  const { condProbs, givenMarginalProbs } = props.input;
  const refInput = useRef(input);
  refInput.current = input;

  const { t } = useTranslation();

  const variables = getCPDisplayVariableSet(condProbs, displayVariables);

  const onMarginalsChange = useCallback(
    (marginals: number[]) => {
      onChange({ ...refInput.current, givenMarginalProbs: marginals });
    },
    [onChange]
  );

  const onCondProbsChanged = useMemo(
    () =>
      variables.original.given.categories.map((givenCat) => (newCondProbs: number[]) => {
        const condProbsCopy = refInput.current.condProbs.copy();
        condProbsCopy.setConditionalProbabilitiesForGiven(givenCat, newCondProbs);
        onChange({ ...refInput.current, condProbs: condProbsCopy });
      }),
    [onChange, variables.original.given]
  );

  // const conditionalLabels = variables.display.conditional.categories.map((cat) => t(cat.toString()));

  const condProbSliders =
    !isIncluded(hiddenSliders, 'conditionals') &&
    variables.original.given.categories.map((cat, i) => {
      const conditionalLabels = variables.display.conditional.categories.map((cat) =>
        formatConditionalProbability(
          [variables.display.conditional, cat],
          [variables.display.given, variables.display.given.categories[i]],
          {
            varFormatCond: 'StateOnly',
            varFormatGiven: 'StateOnly',
            condProbFormat: 'Notation',
          }
        )
      );
      return (
        <TableRow key={cat.toString()}>
          {/* <TableCell>
            {formatConditionalProbability(
              [variables.display.conditional, '?'],
              [variables.display.given, variables.display.given.categories[i]],
              {
                varFormatCond: 'Notation',
                varFormatGiven: 'StateOnly',
                condProbFormat: 'Notation',
              }
            )}
          </TableCell> */}
          <TableCell>
            <ProbabilitiesSlider
              probabilities={condProbs.getConditionalProbabilitiesForGiven(cat)}
              onChange={onCondProbsChanged[i]}
              labels={conditionalLabels}
            />
          </TableCell>
        </TableRow>
      );
    });

  const marginalLabels = variables.display.given.categories.map((cat) => t(cat.toString()));

  return (
    <Table size="small" sx={{ width: 'auto' }}>
      <TableBody>
        {!isIncluded(hiddenSliders, 'marginal') && (
          <TableRow>
            <TableCell>
              {formatProbabilityExpression([variables.display.given, ''], { varFormat: 'VarOnly', probFormat: 'Notation' })}
            </TableCell>
            <TableCell>
              <ProbabilitiesSlider labels={marginalLabels} probabilities={givenMarginalProbs} onChange={onMarginalsChange} />
            </TableCell>
          </TableRow>
        )}
        {condProbSliders}
      </TableBody>
    </Table>
  );
}
