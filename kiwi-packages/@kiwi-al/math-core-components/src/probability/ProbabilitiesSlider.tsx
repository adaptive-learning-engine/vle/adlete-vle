import { Box, Slider, Stack, Typography } from '@mui/material';
import React, { useMemo } from 'react';

import { useMemoWith } from '@adlete-vle/utils/hooks';

import { formatProbability2 } from '@kiwi-al/math-core/probability';

function probsToRangeValues(probabilities: number[]): number[] {
  const result = [];
  let prevSum = 0;
  for (let i = 0; i < probabilities.length - 1; ++i) {
    result.push(probabilities[i] + prevSum);
    prevSum += probabilities[i];
  }
  return result;
}

function rangeValuesToProbs(rangeValues: number[]): number[] {
  const result = [];
  let prevVal = 0;
  for (let i = 0; i < rangeValues.length; ++i) {
    result.push(rangeValues[i] - prevVal);
    prevVal = rangeValues[i];
  }
  result.push(1 - prevVal);
  return result;
}

export interface IProbabilitiesSliderProps {
  probabilities: number[];
  labels: string[];
  colors?: string[];
  formatValue?: (value: number) => string;
  onChange?: (probabilities: number[]) => void;
}

export function ProbabilitiesSlider(props: IProbabilitiesSliderProps): React.ReactElement {
  const { labels, probabilities, onChange, colors } = props;
  const formatValue = props.formatValue || formatProbability2;

  const onChangeInternal = (event: Event, newValues: number[]) => {
    if (onChange) onChange(rangeValuesToProbs(newValues));
  };

  const rangeValues = useMemoWith(probsToRangeValues, [probabilities]);

  const labelElems = useMemo(
    () => (
      <Stack direction="row">
        {labels.map((label, index) => (
          <Box key={label} width={`${probabilities[index] * 100}%`} sx={{ color: colors?.[index] }} textAlign="center">
            {label}
          </Box>
        ))}
      </Stack>
    ),
    [labels, probabilities, colors]
  );

  const valueElems = useMemo(
    () => (
      <Stack direction="row">
        {probabilities.map((prob, index) => (
          // eslint-disable-next-line react/no-array-index-key
          <Box key={index} width={`${probabilities[index] * 100}%`} textAlign="center">
            <Typography variant="caption" component="span">
              {formatValue(probabilities[index])}
            </Typography>
          </Box>
        ))}
      </Stack>
    ),
    [probabilities, formatValue]
  );

  return (
    <Stack sx={{ width: '100%' }}>
      {labelElems}
      <Slider
        track={false}
        min={0}
        max={1}
        step={0.01}
        // valueLabelDisplay="on"
        value={rangeValues}
        onChange={onChangeInternal}
        // getAriaValueText={valuetext}
        // defaultValue={[20, 37, 50, 80]}
        // marks={marks}
      />
      {valueElems}
    </Stack>
  );
}
