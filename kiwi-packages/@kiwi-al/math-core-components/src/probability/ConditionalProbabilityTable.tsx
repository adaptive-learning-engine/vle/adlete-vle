/* eslint-disable react/jsx-props-no-spreading */
import { Box, Button } from '@mui/material';
import { CommonProps } from '@mui/material/OverridableComponent';
import { styled } from '@mui/material/styles';
import { DataGrid, GridCellEditCommitParams, GridColumns, GridValueFormatterParams, GridValueGetterParams } from '@mui/x-data-grid';
import { i18n } from 'i18next';
import React, { useCallback, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { injectI18n } from '@adlete-vle/lr-core/locale';
import { useBoundCallback1, useIncrementState, useMemoWith } from '@adlete-vle/utils/hooks';

import { isFloat } from '@kiwi-al/math-core/datatypes';
import { BivariateCPDataFrame, ICPFormatting } from '@kiwi-al/math-core/probability';
import { Array2D } from '@kiwi-al/math-core/tabular';
import { toLocaleFixed } from '@kiwi-al/math-core/utils';
import { BivarCategoricalVarDefs, createProbabilityVarDef } from '@kiwi-al/math-core/variables';

import { renderEditCellWithTooltip } from '../data-grid/ErrorTooltip';
import {
  createFieldToColumnIndexMap,
  createMarginalRow,
  dataToRows,
  isCellEditable,
  preProcessEditCellPropsFloat,
} from '../data-grid/helpers';
import {
  createValidationClassMatrix,
  getDataCellValidationClassname,
  invalidValidCellsStyleFn,
  IValidation2D,
  ValidateValues2DCB,
} from '../data-grid/validation';

const PROBABILITY_VARDEF = createProbabilityVarDef('Probability');

export type OnUpdateCB = (probs: BivariateCPDataFrame<number | string>) => void;

export function validateProbabilities(
  validationData: BivariateCPDataFrame<number | string>,
  currData: BivariateCPDataFrame<number | string>,
  decimals = 2
): Array2D<boolean> {
  const arr = Array2D.fill(validationData.data.shape, false);
  for (let y = 0; y < arr.shape[0]; ++y) {
    for (let x = 0; x < arr.shape[1]; ++x) {
      const valid = validationData.data.iat(y, x);
      const validStr = typeof valid === 'number' ? toLocaleFixed(valid, decimals) : valid.toString();
      const curr = currData.data.iat(y, x);
      const currStr = typeof curr === 'number' ? toLocaleFixed(curr, decimals) : curr.toString();
      arr.set(x, y, validStr === currStr);
    }
  }
  return arr;
}

export function createProbabilityValidator(decimals = 2): ValidateValues2DCB<BivariateCPDataFrame<number | string>> {
  return (validationData, currData) => validateProbabilities(validationData, currData, decimals);
}

export type ValueFormatterCB = (value: number | string) => string;

export interface IContingencyBarTableFormatting {
  valueFormatter?: ValueFormatterCB;
  labelFormat?: ICPFormatting;
}

const DEFAULT_LABEL_FORMAT: ICPFormatting = {
  conditional: { varFormat: 'StateOnly', condProbFormat: 'Notation' },
  given: { format: 'StateOnly' },
};

export interface ICPTProps extends CommonProps {
  initialData: BivariateCPDataFrame<number | string>;
  displayVariables?: BivarCategoricalVarDefs;
  validation?: IValidation2D<BivariateCPDataFrame<number | string>>;
  onUpdate?: OnUpdateCB;

  editableCells?: Array2D<boolean>;

  formatting?: IContingencyBarTableFormatting;
}

function onCellEdited(
  fieldToColumnIndexMap: Record<string, number>,
  probs: BivariateCPDataFrame<number | string>,
  onUpdate: OnUpdateCB,
  params: GridCellEditCommitParams
) {
  const x = fieldToColumnIndexMap[params.field];
  const y = params.id as number;
  probs.data.iset(y, x, parseFloat(params.value as string));

  if (onUpdate) {
    onUpdate(probs);
  }
}

function sumColumnValueGetter(columnNames: string[], params: GridValueGetterParams): number | string {
  let result = 0;
  for (let i = 0; i < columnNames.length; ++i) {
    const value = params.row[columnNames[i]];
    if (!isFloat(value)) return '';

    result += Number(value);
  }

  return result;
}

function valueFormatter(params: GridValueFormatterParams): string {
  // TODO: customize number of decimals
  const value = params.value;
  return typeof value === 'number' ? toLocaleFixed(value, 2) : value.toString();
}

function createColumns(columnNames: string[], isConditionedOnColumnVar: boolean, i18n: i18n): GridColumns {
  const headingColumn = { field: 'heading', headerName: '', flex: 1, sortable: false };

  const dataColumns: GridColumns = columnNames.map((name, index) => ({
    field: name,
    headerName: name,
    // width: 90,
    flex: 1,
    editable: true,
    index,
    preProcessEditCellProps: preProcessEditCellPropsFloat.bind(null, PROBABILITY_VARDEF),
    renderEditCell: renderEditCellWithTooltip,
    valueFormatter,
    sortable: false,
  }));

  const columns = [headingColumn, ...dataColumns];

  // sum column
  if (!isConditionedOnColumnVar)
    columns.push({
      field: 'sum',
      headerName: i18n.t('math:sum'),
      flex: 1,
      valueGetter: sumColumnValueGetter.bind(null, columnNames),
      valueFormatter,
    });

  return columns;
}

// TODO: support for isConditionedOnColumn == True
export function ConditionalProbabilityTableRaw(props: ICPTProps): React.ReactElement {
  // === Incoming component data and defaults
  const { initialData, validation, className, onUpdate, formatting } = props;
  const displayVariables = props.displayVariables || initialData.variables;
  const { i18n } = useTranslation();

  // TODO: extract to own function
  const labelFormat = useMemo(() => {
    const withI18n = { ...(formatting?.labelFormat || DEFAULT_LABEL_FORMAT) };
    withI18n.conditional = injectI18n(withI18n.conditional, i18n);
    withI18n.given = injectI18n(withI18n.given, i18n);
    return withI18n;
  }, [formatting?.labelFormat, i18n]);

  // === data
  const currData = useMemo(() => initialData.copy(), [initialData]);

  // === columns
  const columnLabels = useMemo(
    () => initialData.getFormattedColumnNames(labelFormat, displayVariables),
    [initialData, labelFormat, displayVariables]
  );
  const fieldToColumnIndexMap = useMemoWith(createFieldToColumnIndexMap, [columnLabels]);
  const columns = useMemoWith(createColumns, [columnLabels, initialData.isConditionedOnColumnVariable, i18n]);

  // === cell rendering
  const [validationClassData, setValidationClassData] = useState<Array2D<string>>(null);
  const getCellClassName = useBoundCallback1(getDataCellValidationClassname, [validationClassData]);

  // === cell editing
  const isCellEditableBound = useBoundCallback1(isCellEditable, [props.editableCells]);

  // counter for force-rendering on cell editing
  const [editCellCount, incEditCellCount] = useIncrementState();

  // TODO: use State for currFreqs to allow instantValidation
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const onCellEditCommit = useCallback(
    (params: GridCellEditCommitParams) => {
      onCellEdited(fieldToColumnIndexMap, currData, onUpdate, params);

      // updating cell count to force update (e.g. for marginals)
      incEditCellCount();
    },
    [currData, onUpdate, incEditCellCount, fieldToColumnIndexMap]
  );

  // === rows
  const rowLabels = useMemo(
    () => initialData.getFormattedRowNames(labelFormat, displayVariables),
    [initialData, labelFormat, displayVariables]
  );

  const rows = useMemo(() => {
    const dataRows = dataToRows(currData.data, columnLabels, rowLabels);
    if (currData.isConditionedOnColumnVariable) {
      const marginalRow = createMarginalRow(currData.data, columnLabels, i18n);
      dataRows.push(marginalRow);
    }
    return dataRows;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currData, columnLabels, rowLabels, i18n, editCellCount]);

  // === rendering
  return (
    <Box className={className}>
      <DataGrid
        columns={columns}
        rows={rows}
        autoHeight
        isCellEditable={isCellEditableBound}
        onCellEditCommit={onCellEditCommit}
        getCellClassName={getCellClassName}
        disableColumnMenu
        hideFooter
      />
      {validation && (
        <Button
          onClick={() => {
            const newValidClassData = createValidationClassMatrix(validation.validate(validation.validationData, currData));
            setValidationClassData(newValidClassData);
          }}
        >
          Validate
        </Button>
      )}
    </Box>
  );
}

export const ConditionalProbabilityTable = styled(ConditionalProbabilityTableRaw)(invalidValidCellsStyleFn);
