import { styled } from '@mui/material/styles';
import React, { useMemo } from 'react';

import { toLocaleFixed } from '@kiwi-al/math-core/utils';

import { invalidValidCellsStyleFn } from '../data-grid/validation';
import { ContingencyTableRaw, IContingencyTableProps } from '../frequency/ContingencyTable';

export type IProbabilityTableProps = IContingencyTableProps;

function probabilityFormatter(value: number | string): string {
  return typeof value === 'string' ? value : toLocaleFixed(value, 2);
}

export function ProbabilityTableRaw(props: IProbabilityTableProps): React.ReactElement {
  const { formatting } = props;

  const formattingWithValueFormatter = useMemo(() => ({ valueFormatter: probabilityFormatter, ...formatting }), [formatting]);

  // eslint-disable-next-line react/jsx-props-no-spreading
  return <ContingencyTableRaw formatting={formattingWithValueFormatter} {...props} />;
}

export const ProbabilityTable = styled(ProbabilityTableRaw)(invalidValidCellsStyleFn);
