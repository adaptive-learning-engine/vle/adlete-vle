import { BivariateCPDataFrame } from '@kiwi-al/math-core/probability';
import { BivarCategoricalVarDefs, ICategoricalVarDef } from '@kiwi-al/math-core/variables';

export interface ICPDisplayVariableSet {
  original: {
    given: ICategoricalVarDef;
    conditional: ICategoricalVarDef;
  };
  display: {
    given: ICategoricalVarDef;
    conditional: ICategoricalVarDef;
  };
}

export function getCPDisplayVariableSet(
  condProbs: BivariateCPDataFrame,
  displayVariables?: BivarCategoricalVarDefs
): ICPDisplayVariableSet {
  const givenVarIndex = condProbs.getGivenVariableIndex();
  const condVarIndex = condProbs.getConditionalVariableIndex();

  displayVariables = displayVariables || condProbs.variables;

  return {
    original: { given: condProbs.variables[givenVarIndex], conditional: condProbs.variables[condVarIndex] },
    display: { given: displayVariables[givenVarIndex], conditional: displayVariables[condVarIndex] },
  };
}
