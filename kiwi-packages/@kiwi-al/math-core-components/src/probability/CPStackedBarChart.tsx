import { Box, Stack } from '@mui/material';
import { AxisBottom } from '@visx/axis';
import { localPoint } from '@visx/event';
import { Group } from '@visx/group';
import { LegendOrdinal } from '@visx/legend';
import { scaleBand } from '@visx/scale';
import { BarStack } from '@visx/shape';
import { BarStack as BarStackData, SeriesPoint } from '@visx/shape/lib/types';
import { defaultStyles, useTooltip, useTooltipInPortal } from '@visx/tooltip';
import { UseTooltipParams } from '@visx/tooltip/lib/hooks/useTooltip';
import { UseTooltipInPortal } from '@visx/tooltip/lib/hooks/useTooltipInPortal';
import { ScaleOrdinal } from 'd3-scale';
import i18next, { i18n } from 'i18next';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import type { RecursivePartial } from '@adlete-utils/typescript';
import { useMemoWith } from '@adlete-vle/utils/hooks';

import { BivariateCPDataFrame, formatProbability } from '@kiwi-al/math-core/probability';
import { BivarCategoricalVarDefs } from '@kiwi-al/math-core/variables';

import { createColorScale, createProbabilityScaleInv } from '../shared/scales';
import { IViewport, mergeDefaultViewport } from '../shared/viewport';

import { getCPDisplayVariableSet } from './shared';

type Bar<Datum, Key> = BarStackData<Datum, Key>['bars'][0];

interface IBarDatum {
  probabilities: Record<string, number>;
  conditionedOn: string;
}

interface ITooltipData {
  bar: SeriesPoint<IBarDatum>;
  key: string;
  index: number;
  height: number;
  width: number;
  x: number;
  y: number;
  color: string;
}

const DEFAULT_VIEWPORT: IViewport = {
  width: 400,
  height: 250,
  margin: {
    left: 40,
    right: 140,
    top: 0,
    bottom: 25,
  },
};

const tooltipStyles = {
  ...defaultStyles,
  minWidth: 60,
  backgroundColor: 'rgba(0,0,0,0.9)',
  color: 'white',
};

// accessors
const getConditionedOn = (d: IBarDatum) => d.conditionedOn;
function getValue(d: IBarDatum, key: string) {
  return d.probabilities[key];
}

let tooltipTimeout: number;

export interface ICPStackBarChartTooltipProps {
  tooltip: UseTooltipParams<ITooltipData>;
  tooltipInPortal: UseTooltipInPortal;
  colorScale: ScaleOrdinal<string, string>;
}

export function CPStackTooltip({ tooltip, tooltipInPortal, colorScale }: ICPStackBarChartTooltipProps): React.ReactElement {
  const { tooltipOpen, tooltipData, tooltipTop, tooltipLeft } = tooltip;
  const { TooltipInPortal } = tooltipInPortal;
  return tooltipOpen && tooltipData ? (
    <TooltipInPortal top={tooltipTop} left={tooltipLeft} style={tooltipStyles}>
      <div style={{ color: colorScale(tooltipData.key) }}>
        <strong>{tooltipData.key}</strong>
      </div>
      <div>{formatProbability(tooltipData.bar[1] - tooltipData.bar[0], { format: 'Percent' })}</div>
      <div>
        <small />
      </div>
    </TooltipInPortal>
  ) : null;
}

export interface ICPStackedBarProps {
  bar: Bar<IBarDatum, string>;
  barStack: BarStackData<IBarDatum, string>;
  tooltip: UseTooltipParams<ITooltipData>;
}

export function CPStackedBar({ barStack, bar, tooltip }: ICPStackedBarProps): React.ReactElement {
  return (
    bar.height >= 1 && (
      <Group key={`bar-stack-${barStack.index}-${bar.index}`} left={bar.x} top={bar.y}>
        <rect
          height={bar.height}
          width={bar.width}
          fill={bar.color}
          onMouseLeave={() => {
            tooltipTimeout = window.setTimeout(() => {
              tooltip.hideTooltip();
            }, 300);
          }}
          onMouseMove={(event) => {
            if (tooltipTimeout) clearTimeout(tooltipTimeout);
            // TooltipInPortal expects coordinates to be relative to containerRef
            // localPoint returns coordinates relative to the nearest SVG, which
            // is what containerRef is set to in this example.
            const eventSvgCoords = localPoint(event);
            const left = bar.x + bar.width / 2;
            tooltip.showTooltip({
              tooltipData: bar,
              tooltipTop: eventSvgCoords?.y,
              tooltipLeft: left,
            });
          }}
        />
        <text x={bar.width / 2} y={bar.height / 2} textAnchor="middle" alignmentBaseline="central">
          {formatProbability(bar.bar[1] - bar.bar[0], { format: 'Percent' })}
        </text>
      </Group>
    )
  );
}

function convertToBarChartData(
  data: BivariateCPDataFrame<number>,
  displayVars?: BivarCategoricalVarDefs,
  i18n: i18n = i18next
): IBarDatum[] {
  const newData: IBarDatum[] = [];
  const givenVarIndex = data.getGivenVariableIndex();
  const condVarIndex = data.getConditionalVariableIndex();
  const condVar = data.variables[condVarIndex];
  const givenVar = data.variables[givenVarIndex];

  const givenVarDisplay = displayVars ? displayVars[givenVarIndex] : data.variables[givenVarIndex];
  const condVarDisplay = displayVars ? displayVars[condVarIndex] : data.variables[condVarIndex];

  const givenCatsTrans = givenVarDisplay.categories.map((cat) => i18n.t(cat.toString()));
  const condCatsTrans = condVarDisplay.categories.map((cat) => i18n.t(cat.toString()));

  givenVar.categories.forEach((givenCat, i) => {
    const barData: IBarDatum = {
      probabilities: condVar.categories.reduce(
        (probs, condCat, j) => {
          probs[condCatsTrans[j]] = data.getConditionalProbability(condCat, givenCat);
          return probs;
        },
        {} as Record<string, number>
      ),
      conditionedOn: givenCatsTrans[i],
    };
    newData.push(barData);
  });

  return newData;
}

export type ICPStackedBarChartProps = {
  data: BivariateCPDataFrame<number>;
  displayVariables?: BivarCategoricalVarDefs;
  viewport?: RecursivePartial<IViewport>;
  // events?: boolean;
};

export function CPStackedBarChart({ data, viewport, displayVariables }: ICPStackedBarChartProps): React.ReactElement {
  const { width, height, margin } = mergeDefaultViewport(viewport, DEFAULT_VIEWPORT);
  const { i18n } = useTranslation();

  const variables = getCPDisplayVariableSet(data, displayVariables);

  const barData = useMemoWith(convertToBarChartData, [data, displayVariables, i18n]);
  const keys = useMemo(() => Object.keys(barData[0].probabilities), [barData]);

  const tooltip = useTooltip<ITooltipData>();

  const tooltipInPortal = useTooltipInPortal({
    scroll: true,
  });

  // bounds
  const xMax = width;
  const yMax = height - margin.top - margin.bottom;

  // === Scales
  const colorScale = useMemoWith(createColorScale, [keys]);
  const probabilityScale = useMemoWith(createProbabilityScaleInv, [yMax]);

  // creating and memoizing the x-scale
  const nameScale = useMemo(
    () =>
      scaleBand<string>({
        domain: barData.map(getConditionedOn),
        padding: 0.2,
      }).rangeRound([0, xMax]),
    [xMax, barData]
  );

  return width < 10 ? null : (
    <Stack direction="row" spacing={2}>
      <Stack spacing={2}>
        <svg ref={tooltipInPortal.containerRef} width={width} height={height}>
          <Group top={margin.top}>
            <BarStack<IBarDatum, string>
              data={barData}
              keys={keys}
              value={getValue}
              x={getConditionedOn}
              xScale={nameScale}
              yScale={probabilityScale}
              color={colorScale}
            >
              {(barStacks) =>
                barStacks.map((barStack) =>
                  barStack.bars.map((bar) => (
                    <CPStackedBar key={`${barStack.index}-${bar.index}`} barStack={barStack} bar={bar} tooltip={tooltip} />
                  ))
                )
              }
            </BarStack>
          </Group>
          <AxisBottom top={yMax} scale={nameScale} />
        </svg>
        <Box textAlign="center">
          {i18n.t('math:given')}: {i18n.t(variables.display.given.name)}
        </Box>
      </Stack>
      <Stack spacing={2}>
        <div>{i18n.t(variables.display.conditional.name)}</div>
        <LegendOrdinal scale={colorScale} direction="column" labelMargin="0 15px 0 0" />
      </Stack>
      <CPStackTooltip tooltip={tooltip} tooltipInPortal={tooltipInPortal} colorScale={colorScale} />
    </Stack>
  );
}
