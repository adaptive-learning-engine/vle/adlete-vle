import { Box, Slider, SliderProps, Stack, styled, Typography } from '@mui/material';
import React, { useCallback } from 'react';

export interface IMinMaxSliderWithDescriptionProps {
  /**
   * The maximum allowed value of the slider.
   * Should not be equal to min.
   * @default 100
   */
  max: number;
  /**
   * The minimum allowed value of the slider.
   * Should not be equal to max.
   * @default 0
   */
  min: number;

  value: number;

  labelDescription: string;
  // labelLeft?: string;
  // labelRight?: string;
  onChange: (value: number) => void;

  additionalSliderProps?: Partial<SliderProps>;

  disabled?: boolean;

  step?: number;

  className?: string;

  color?: string;
}

export function MinMaxSliderWithDescriptionRaw(props: IMinMaxSliderWithDescriptionProps): React.ReactElement {
  const { labelDescription, /* labelLeft, labelRight, */ max, min, step, onChange, value, disabled, additionalSliderProps, className } =
    props;

  const onChangeCB = useCallback((event: Event, val: number | number[]) => onChange(val as number), [onChange]);

  return (
    <Box className={className}>
      <Stack direction="row" alignItems="center" spacing={2}>
        <Typography variant="subtitle2">{labelDescription}:</Typography>
        <Typography>{value}</Typography>
      </Stack>
      <Stack direction="column">
        <Slider
          // eslint-disable-next-line react/jsx-props-no-spreading
          {...additionalSliderProps}
          valueLabelDisplay="auto"
          min={min}
          max={max}
          step={step}
          value={value}
          onChange={onChangeCB}
          disabled={disabled}
        />

        {/* <Stack spacing={2} direction="row" sx={{ mb: 1 }} alignItems="center">
          {labelLeft ? <span>{labelLeft}</span> : null}
          <Slider valueLabelDisplay="auto" min={min} max={max} value={value} onChange={onChangeCB} />
          {labelRight ? <span>{labelRight}</span> : null}
        </Stack> */}
        <Stack direction="row" alignItems="center" justifyContent="space-between">
          <Typography color="text.secondary">{min}</Typography>
          <Typography color="text.secondary">{max}</Typography>
        </Stack>
      </Stack>
    </Box>
  );
}

export const MinMaxSliderWithDescription = styled(MinMaxSliderWithDescriptionRaw)((props) => ({
  '& .MuiSlider-thumb': {
    backgroundColor: props.color,
  },
  '& .MuiSlider-track': {
    color: props.color,
  },
  '& .MuiSlider-rail': {
    color: props.color,
  },
}));
