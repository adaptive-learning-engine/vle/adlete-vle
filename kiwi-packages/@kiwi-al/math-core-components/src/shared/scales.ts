import { scaleLinear, scaleOrdinal } from '@visx/scale';
import { ScaleLinear, ScaleOrdinal } from 'd3-scale';

const purple1 = '#6c5efb';
const purple2 = '#c998ff';
const purple3 = '#a44afe';

export const DEFAULT_GRAPH_COLORS = [purple1, purple2, purple3];

export function createColorScale(keys: string[], colors?: string[]): ScaleOrdinal<string, string> {
  return scaleOrdinal<string, string>({
    domain: keys,
    range: colors || DEFAULT_GRAPH_COLORS,
  });
}

export function createProbabilityScaleInv(max: number): ScaleLinear<number, number> {
  return scaleLinear<number>({
    domain: [0, 1],
    nice: true,
  }).rangeRound([max, 0]);
}
