import { mapObjIndexed } from 'ramda';

import { ICategoricalVarDef } from '@kiwi-al/math-core/variables';

export type MultiColor = string | string[];
export type SingleColorMap = Record<string, string>;
export type MultiColorMap = Record<string, MultiColor>;

export type MultiToSingleColorCB = (MultiColor: string[]) => string;

export function getSingleColor(key: string, colors?: MultiColorMap, toSingleColor?: MultiToSingleColorCB): string {
  if (!colors || !(key in colors)) return null;

  const color = colors[key];
  if (Array.isArray(color)) return toSingleColor ? toSingleColor(color) : null;
  return color;
}

export function getSingleVariableColor(variable: ICategoricalVarDef, colors?: MultiColorMap, toSingleColor?: MultiToSingleColorCB): string {
  return getSingleColor(variable.name, colors, toSingleColor);
}

export function toSingleColorMap(colors: MultiColorMap, toSingleColor?: MultiToSingleColorCB): SingleColorMap {
  if (!colors) return {};

  return mapObjIndexed((color) => {
    if (Array.isArray(color)) {
      if (toSingleColor) return toSingleColor(color);
      return null;
    }
    return color;
  }, colors);
}
