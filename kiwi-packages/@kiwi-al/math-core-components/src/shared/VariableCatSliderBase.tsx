import { i18n } from 'i18next';
import merge from 'lodash.merge';
import React, { useCallback, useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import { formatVariableWithState, IFormatVariableWithStateOptions, RandomVarState, VariableWithState } from '@kiwi-al/math-core/variables';

import { IMinMaxSliderWithDescriptionProps, MinMaxSliderWithDescription } from './MinMaxSliderWithDescription';

type PropsToOmit = 'labelDescription' | 'labelLeft' | 'labelRight' | 'onChange';

const DEFAULT_FORMAT: IFormatVariableWithStateOptions = {
  format: 'Notation',
};

export interface IVariableCatSliderBaseProps<TCategory extends RandomVarState = RandomVarState>
  extends Omit<IMinMaxSliderWithDescriptionProps, PropsToOmit> {
  varWithCat: VariableWithState<TCategory>;
  formatting?: IFormatVariableWithStateOptions;
  getDescription?: (varWithCat: VariableWithState<TCategory>, i18next?: i18n) => string;
  onChange?: (value: number, varWithCat: VariableWithState<TCategory>) => void;
}

function getDefaultDescription<TCategory extends RandomVarState = RandomVarState>(
  formatting: IFormatVariableWithStateOptions,
  varWithCat: VariableWithState<TCategory>
): string {
  return formatVariableWithState(varWithCat, formatting);
}

export function VariableCatSliderBase<TCategory extends RandomVarState = RandomVarState>(
  props: IVariableCatSliderBaseProps<TCategory>
): React.ReactElement {
  const { varWithCat, formatting, onChange, getDescription } = props;

  const { i18n } = useTranslation();

  const onChangeInternal = useCallback((val: number) => onChange(val, varWithCat), [varWithCat, onChange]);

  const description = useMemo(() => {
    if (getDescription) return getDescription(varWithCat, i18n);

    const mergedFormat = merge({ i18n }, DEFAULT_FORMAT, formatting);
    return getDefaultDescription(mergedFormat, varWithCat);
  }, [getDescription, formatting, i18n, varWithCat]);

  return (
    <MinMaxSliderWithDescription
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...props}
      labelDescription={description}
      onChange={onChangeInternal}
    />
  );
}
