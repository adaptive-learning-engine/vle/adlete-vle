import { Stack, Table, TableBody, TableCell, TableRow, Typography } from '@mui/material';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { formatVariableWithState, ICategoricalVarDef, VariableWithState, varWithCatToString } from '@kiwi-al/math-core/variables';

import { SingleColorMap } from './colors';

export interface VarICayNameMapperProps {
  variable: ICategoricalVarDef;
  renamedVariable: ICategoricalVarDef;
  colors?: SingleColorMap;
  showLabel?: boolean;
}

export function VarCatNameMapper({ variable, renamedVariable, showLabel, colors }: VarICayNameMapperProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <Stack spacing={2}>
      {showLabel && (
        <div>
          <Typography variant="subtitle2" component="span">
            {t(variable.name)}
          </Typography>
          {/* <br />({t('kurz')} {t(renamedVariable.name)}) */}
        </div>
      )}

      <Table size="small" sx={{ width: 'auto' }}>
        <TableBody>
          {variable.categories.map((cat, index) => {
            const varWithCat: VariableWithState = [variable, cat];
            const color = colors?.[varWithCatToString(varWithCat)];
            return (
              <TableRow key={cat.toString()}>
                <TableCell sx={{ color }}>{t(renamedVariable.categories[index].toString())}</TableCell>
                <TableCell sx={{ color }}>{formatVariableWithState(varWithCat, { format: 'Notation' })}</TableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Stack>
  );
}
