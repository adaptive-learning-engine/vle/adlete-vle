import { i18n } from 'i18next';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { RandomVarState, VariableWithState } from '@kiwi-al/math-core/variables';

import { IMinMaxSliderWithDescriptionProps, MinMaxSliderWithDescription } from './MinMaxSliderWithDescription';

type PropsToOmit = 'labelDescription' | 'labelLeft' | 'labelRight';

export interface IBivariateSliderBaseProps<TCat1 extends RandomVarState = RandomVarState, TCat2 extends RandomVarState = RandomVarState>
  extends Omit<IMinMaxSliderWithDescriptionProps, PropsToOmit> {
  // TODO: use VariableWithState instead
  varsWithCats: [VariableWithState<TCat1>, VariableWithState<TCat2>];
  getDescriptionLabel: (varsWithCats: [VariableWithState<TCat1>, VariableWithState<TCat2>], i18next?: i18n) => string;
}

export function BivariateSliderBase<TCat1 extends RandomVarState = RandomVarState, TCat2 extends RandomVarState = RandomVarState>(
  props: IBivariateSliderBaseProps<TCat1, TCat2>
): React.ReactElement {
  const { getDescriptionLabel, varsWithCats } = props;
  const { i18n: i18next } = useTranslation();

  // eslint-disable-next-line react/jsx-props-no-spreading
  return <MinMaxSliderWithDescription {...props} labelDescription={getDescriptionLabel(varsWithCats, i18next)} />;
}
