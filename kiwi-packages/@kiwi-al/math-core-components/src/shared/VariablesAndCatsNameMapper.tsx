import { Stack } from '@mui/material';
import React from 'react';

import { BivarCategoricalVarDefs } from '@kiwi-al/math-core/variables';

import { VarCatNameMapper } from './VarCatNameMapper';
import { SingleColorMap } from './colors';

export interface IVariablesAndCatsNameMapperProps {
  variables: BivarCategoricalVarDefs;
  renamedVariables: BivarCategoricalVarDefs;
  colors?: SingleColorMap;
}

export function VariablesAndCatsNameMapper(props: IVariablesAndCatsNameMapperProps): React.ReactElement {
  const { variables, renamedVariables, colors } = props;
  return (
    <Stack spacing={2} direction="row" justifyContent="space-around">
      <VarCatNameMapper variable={variables[0]} renamedVariable={renamedVariables[0]} showLabel colors={colors} />
      <VarCatNameMapper variable={variables[1]} renamedVariable={renamedVariables[1]} showLabel colors={colors} />
    </Stack>
  );
}
