import { Stack, Table, TableBody, TableCell, TableRow } from '@mui/material';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { formatVariableWithState, getVarWithState, ICategoricalVarDef } from '@kiwi-al/math-core/variables';

import { SingleColorMap } from './colors';

export interface IEventNameMapperProps {
  variables: ICategoricalVarDef[];
  eventVariables: ICategoricalVarDef[];
  showLabel?: boolean;
  colors?: SingleColorMap;
}

export function EventNameMapper({ variables, eventVariables, showLabel, colors }: IEventNameMapperProps): React.ReactElement {
  const { t } = useTranslation();

  return (
    <Stack spacing={2}>
      {showLabel && t('Ereignisse')}
      <Table size="small" sx={{ width: 'auto' }}>
        <TableBody>
          {variables.map((variable, index) => (
            <TableRow key={variable.name}>
              <TableCell sx={{ color: colors?.[variables[index].name] }}>{t(eventVariables[index].categories[0].toString())}</TableCell>
              <TableCell sx={{ color: colors?.[variables[index].name] }}>
                {formatVariableWithState(getVarWithState(variables, index, 0), { format: 'Notation' })}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Stack>
  );
}
