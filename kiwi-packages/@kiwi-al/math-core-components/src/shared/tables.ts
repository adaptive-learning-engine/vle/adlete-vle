import { GridRenderCellParams } from '@mui/x-data-grid';

import { getSingleColor, SingleColorMap } from './colors';

export type RenderCellCB = (params: GridRenderCellParams) => React.ReactNode;

export function getCellColor(params: GridRenderCellParams, colors?: SingleColorMap): string {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const colDef = params.colDef as any;
  const colIsMarginal = colDef.isMarginal;
  const rowIsMarginal = params.row.isMarginal;

  let colorKey;
  if (colIsMarginal && rowIsMarginal) {
    colorKey = null;
  } else if (colIsMarginal && !rowIsMarginal) {
    colorKey = params.row.rowId;
  } else if (!colIsMarginal && rowIsMarginal) {
    colorKey = colDef.colId;
  } else {
    colorKey = `${colDef.colId}|${params.row.rowId}`;
  }
  return getSingleColor(colorKey, colors);
}
