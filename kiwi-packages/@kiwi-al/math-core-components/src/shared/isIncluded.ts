export function isIncluded<T>(arr: T[] | null, obj: T): boolean {
  return arr && arr.includes(obj);
}
