import { Slider, styled } from '@mui/material';

// TODO: override props color type
export const ColoredSlider = styled(Slider, { shouldForwardProp: (prop) => prop !== 'color' })((props) => ({
  '& .MuiSlider-thumb': {
    backgroundColor: props.color,
  },
  '& .MuiSlider-track': {
    color: props.color,
  },
  '& .MuiSlider-rail': {
    color: props.color,
  },
}));
