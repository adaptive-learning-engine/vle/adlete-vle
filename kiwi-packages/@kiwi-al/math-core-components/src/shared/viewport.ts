import type { RecursivePartial } from '@adlete-utils/typescript';

export interface IMargin {
  left: number;
  top: number;
  right: number;
  bottom: number;
}

export interface IViewport {
  width: number;
  height: number;
  margin: IMargin;
}

export interface IWithWidthAndHeight {
  width: number;
  height: number;
}

export interface IWithMargin {
  margin: IMargin;
}

export interface IWithViewport {
  viewport: IViewport;
}

export function mergeDefaultMargin(partialMargin: Partial<IMargin>, defaultMargin: IMargin): IMargin {
  if (!partialMargin) return defaultMargin;

  // TODO: find library for merge
  return {
    left: partialMargin.left != null ? partialMargin.left : defaultMargin.left,
    right: partialMargin.right != null ? partialMargin.right : defaultMargin.right,
    top: partialMargin.top != null ? partialMargin.top : defaultMargin.top,
    bottom: partialMargin.bottom != null ? partialMargin.bottom : defaultMargin.bottom,
  };
}

export function mergeDefaultViewport(partialVP: RecursivePartial<IViewport>, defaultVP: IViewport): IViewport {
  if (!partialVP) return defaultVP;

  // TODO: find library for merge
  return {
    width: partialVP.width != null ? partialVP.width : defaultVP.width,
    height: partialVP.height != null ? partialVP.height : defaultVP.height,
    margin: mergeDefaultMargin(partialVP.margin, defaultVP.margin),
  };
}
