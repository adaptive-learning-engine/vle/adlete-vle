import React from 'react';
import ReactMarkdown from 'react-markdown';
import rehypeMathjax from 'rehype-mathjax';
import remarkMath from 'remark-math';

export interface IExpressionProps {
  expression: string;
  inline?: boolean;
}

function ExpressionRaw({ expression, inline }: IExpressionProps): React.ReactElement {
  const text = inline ? `$${expression}$` : `$$${expression}$$`;
  return (
    <ReactMarkdown remarkPlugins={[remarkMath]} rehypePlugins={[[rehypeMathjax, { scale: 4 }]]}>
      {text}
    </ReactMarkdown>
  );
}

export const Expression = React.memo(ExpressionRaw);

// function ExpressionRaw({ expression, inline }: IExpressionProps): React.ReactElement {
//   const text = inline ? `$${expression}$` : `$$${expression}$$`;
//   return (
//     <ReactMarkdown
//       remarkPlugins={[remarkMath]}
//       rehypePlugins={[
//         [
//           rehypeMathjaxChtml,
//           {
//             scale: 4,
//             chtml: {
//               fontURL: 'https://cdn.jsdelivr.net/npm/mathjax@3.2.2/es5/output/chtml/fonts/woff-v2',
//             },
//           },
//         ],
//       ]}
//     >
//       {text}
//     </ReactMarkdown>
//   );
// }
