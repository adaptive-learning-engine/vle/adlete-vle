/* eslint-disable react/jsx-props-no-spreading */
import { Box, Button } from '@mui/material';
import { CommonProps } from '@mui/material/OverridableComponent';
import { styled } from '@mui/material/styles';
import {
  DataGrid,
  GridCellEditCommitParams,
  GridColumns,
  GridRenderCellParams,
  GridRenderEditCellParams,
  GridValueGetterParams,
} from '@mui/x-data-grid';
import type { i18n } from 'i18next';
import React, { useCallback, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';

import { injectI18n } from '@adlete-vle/lr-core/locale';
import { useMemoWith } from '@adlete-vle/utils/hooks';

import { isFloat } from '@kiwi-al/math-core/datatypes';
import { BivariateJointFrequencyDataFrame } from '@kiwi-al/math-core/frequencies';
import { Array2D } from '@kiwi-al/math-core/tabular';
import {
  // BivarCategoricalVarDefs,
  createUnsignedIntegerVarDef,
  formatVariableWithState,
  ICategoricalVarDef,
  IFormatVariableWithStateOptions,
  variableToVarCatStrings,
} from '@kiwi-al/math-core/variables';

import {
  createFieldToColumnIndexMap,
  createMarginalRow,
  createNumberValidationClassMatrix,
  dataToRows,
  EditCellWithTooltip,
  getDataCellValidationClassname,
  invalidValidCellsStyleFn,
  isCellEditable,
  preProcessEditCellPropsInteger,
} from '../data-grid';
import { SingleColorMap } from '../shared/colors';
import { getCellColor, RenderCellCB } from '../shared/tables';

const FREQUENCY_VARDEF = createUnsignedIntegerVarDef('Frequency');

const DEFAULT_LABEL_FORMAT: IFormatVariableWithStateOptions = {
  format: 'Notation',
};

type ValueFormatterCB = (value: number | string) => string;
type OnUpdateCB = (data: BivariateJointFrequencyDataFrame<number | string>) => void;

export interface IContingencyTableFormatting {
  valueFormatter: ValueFormatterCB;
  labelFormat: IFormatVariableWithStateOptions;
}

export interface IContingencyTableProps extends CommonProps {
  initialData: BivariateJointFrequencyDataFrame<number | string>;
  // displayVariables?: BivarCategoricalVarDefs;
  validationData?: BivariateJointFrequencyDataFrame<number | string>;
  onUpdate?: OnUpdateCB;
  editableCells?: Array2D<boolean>;
  showMarginals?: boolean;
  colors?: SingleColorMap;
  formatting?: IContingencyTableFormatting;
}

function onCellEdited(
  fieldToColumnIndexMap: Record<string, number>,
  frequencies: BivariateJointFrequencyDataFrame<number | string>,
  onUpdate: OnUpdateCB,
  params: GridCellEditCommitParams
) {
  const x = fieldToColumnIndexMap[params.field];
  const y = params.id as number;
  frequencies.data.iset(y, x, parseInt(params.value as string, 10));

  if (onUpdate) {
    onUpdate(frequencies);
  }
}

function marginalColumnValueGetter(columnNames: string[], params: GridValueGetterParams): number | string {
  let result = 0;
  for (let i = 0; i < columnNames.length; ++i) {
    const value = params.row[columnNames[i]];
    if (!isFloat(value)) return '';

    result += Number(value);
  }

  // TODO: customize number of decimals
  return Math.round(result * 100) / 100; // rounding to two decimals
}

function renderCell(params: GridRenderCellParams, colors?: SingleColorMap): React.ReactNode {
  const color = getCellColor(params, colors);
  return <div style={{ color }}>{params.formattedValue}</div>;
}

function renderEditCell(params: GridRenderEditCellParams, colors?: SingleColorMap): React.ReactNode {
  const color = getCellColor(params, colors);
  return <EditCellWithTooltip {...params} sx={{ color }} />;
}

function createColumns(
  variable: ICategoricalVarDef,
  columnIds: string[],
  columnLabels: string[],
  showMarginals: boolean,
  renderCellCB: RenderCellCB,
  renderEditCellCB: RenderCellCB,
  i18n: i18n,
  valueFormatter?: ValueFormatterCB
): GridColumns {
  const varColumn = { field: 'heading', headerName: '', flex: 1, sortable: false };

  const marginalColumn = {
    field: variable.name,
    headerName: i18n.t('math:sum'),
    flex: 1,
    valueGetter: marginalColumnValueGetter.bind(null, columnIds),
    renderCell: renderCellCB,
    isMarginal: true,
    sortable: false,
  };

  const freqColumns: GridColumns = columnIds.map((id, index) => ({
    field: id,
    colId: id,
    headerName: columnLabels[index],
    flex: 1,
    // width: 90,
    editable: true,
    index,
    preProcessEditCellProps: preProcessEditCellPropsInteger.bind(null, FREQUENCY_VARDEF),
    renderCell: renderCellCB,
    renderEditCell: renderEditCellCB,
    valueFormatter: valueFormatter ? (params) => valueFormatter(params.value as string | number) : null,
    sortable: false,
  }));
  return showMarginals ? [varColumn, ...freqColumns, marginalColumn] : [varColumn, ...freqColumns];
}

export function ContingencyTableRaw(props: IContingencyTableProps): React.ReactElement {
  // === Incoming component data and defaults
  const { initialData, validationData, className, onUpdate, showMarginals, colors, formatting } = props;
  const { i18n } = useTranslation();
  const valueFormatter = formatting?.valueFormatter;
  const labelFormat = useMemo(() => injectI18n(formatting?.labelFormat || DEFAULT_LABEL_FORMAT, i18n), [formatting?.labelFormat, i18n]);

  // === Data

  const { columnVariable, rowVariable } = initialData;

  // === Columns
  const columnIds = useMemoWith(variableToVarCatStrings, [columnVariable]);
  const columnLabels = useMemo(
    () => columnVariable.categories.map((cat) => formatVariableWithState([columnVariable, cat], labelFormat)),
    [columnVariable, labelFormat]
  );

  const renderCellCB = useMemo(() => (params: GridRenderCellParams) => renderCell(params, colors), [colors]);
  const renderEditCellCB = useMemo(() => (params: GridRenderCellParams) => renderEditCell(params, colors), [colors]);

  const columns = useMemo(
    () => createColumns(columnVariable, columnIds, columnLabels, showMarginals, renderCellCB, renderEditCellCB, i18n, valueFormatter),
    [columnVariable, columnIds, columnLabels, showMarginals, valueFormatter, i18n, renderCellCB, renderEditCellCB]
  );

  // === Rows
  const currData = useMemo(() => initialData.copy(), [initialData]);
  const rows = useMemo(() => {
    const rowIds = variableToVarCatStrings(rowVariable);

    const dataRows = dataToRows(
      currData.data,
      columnIds,
      rowVariable.categories.map((cat) => formatVariableWithState([rowVariable, cat], labelFormat)),
      rowIds
    );

    if (showMarginals) {
      const marginalRow = createMarginalRow(currData.data, columnIds, i18n, rowVariable.name);
      dataRows.push(marginalRow);
    }

    return dataRows;
  }, [currData, columnIds, rowVariable, showMarginals, i18n, labelFormat]);

  // === Validation
  const [validationClassData, setValidationClassData] = useState<Array2D<string>>(null);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const getCellClassName = useCallback(getDataCellValidationClassname.bind(null, validationClassData), [validationClassData]);

  // === Editing
  // TODO: use State for currFreqs to allow instantValidation
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const onCellEditCommit = useCallback(
    (params: GridCellEditCommitParams) => {
      const fieldToColumnIndexMap = createFieldToColumnIndexMap(columnLabels);
      onCellEdited(fieldToColumnIndexMap, currData, onUpdate, params);
    },
    [currData, columnLabels, onUpdate]
  );

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const isCellEditableBound = useCallback(isCellEditable.bind(null, props.editableCells), [props.editableCells]);

  return (
    <Box className={className}>
      <DataGrid
        columns={columns}
        rows={rows}
        autoHeight
        isCellEditable={isCellEditableBound}
        onCellEditCommit={onCellEditCommit}
        getCellClassName={getCellClassName}
        disableColumnMenu
        hideFooter
      />
      {validationData && (
        <Button
          onClick={() => {
            const newValidClassData = createNumberValidationClassMatrix(
              new Array2D(currData.values, false),
              new Array2D(validationData.values, false)
            );
            setValidationClassData(newValidClassData);
          }}
        >
          Validate
        </Button>
      )}
    </Box>
  );
}

export const ContingencyTable = styled(ContingencyTableRaw)(invalidValidCellsStyleFn);
