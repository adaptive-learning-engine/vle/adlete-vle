import InfoIcon from '@mui/icons-material/Info';
import { IconButton, Stack } from '@mui/material';
import React, { useCallback, useMemo, useRef } from 'react';
import { useTranslation } from 'react-i18next';

import { RichTooltip } from '@adlete-vle/lr-core-components/tooltips/RichTooltip';
import { useMemoWith } from '@adlete-vle/utils/hooks';

import {
  convertBivarFreqREGenInput,
  FrequencyGenInputProperty,
  IBivarFreqDataPopMarginalJoint,
  IBivarFreqPopMarginalJointGenInput,
  IBivarFreqREGenInput,
  validateFrequencyGenInput,
} from '@kiwi-al/math-core/frequencies';
import { RandomVarState } from '@kiwi-al/math-core/variables';
import {
  BivarCategoricalVarDefs,
  getVarWithState,
  ICategoricalVarDef,
  VariableWithState,
} from '@kiwi-al/math-core/variables/VariableDefinition';

import { MinMaxSliderWithDescription } from '../shared/MinMaxSliderWithDescription';
import { getSingleColor, MultiColorMap, SingleColorMap } from '../shared/colors';
import { isIncluded } from '../shared/isIncluded';

import { BinaryFrequencySlider } from './BinaryFrequencySlider';
import { JointFrequencySlider } from './JointFrequencySlider';

export interface IJointFrequencyGeneratorInputProps<
  TCat1 extends RandomVarState = RandomVarState,
  TCat2 extends RandomVarState = RandomVarState,
> {
  input: IBivarFreqREGenInput;
  variables: [ICategoricalVarDef<TCat1>, ICategoricalVarDef<TCat2>];
  onChange: (input: IBivarFreqREGenInput) => void;
  hiddenSliders?: FrequencyGenInputProperty[];
  disabledSliders?: FrequencyGenInputProperty[];
  maxPopulation?: number;
  colors?: MultiColorMap;
  className?: string;
  // categories: [TCat1, TCat2];
}

function toGenInput(data: IBivarFreqDataPopMarginalJoint): IBivarFreqPopMarginalJointGenInput {
  return {
    format: 'population-marginal-joint',
    data,
  };
}

export function getMarginalJointColors(variables: BivarCategoricalVarDefs, colors?: MultiColorMap): SingleColorMap {
  // TODO: use varWithCatToColorKey
  return {
    marginal0: getSingleColor(variables[0].name, colors),
    marginal1: getSingleColor(variables[1].name, colors),
    joint: getSingleColor('joint', colors),
  };
}

export function JointFrequencyGeneratorInput<TCat1 extends RandomVarState = RandomVarState, TCat2 extends RandomVarState = RandomVarState>(
  props: IJointFrequencyGeneratorInputProps<TCat1, TCat2>
): React.ReactElement {
  const { variables, input, onChange, hiddenSliders, disabledSliders, colors, className } = props;

  const { t } = useTranslation();

  const maxPopulation = props.maxPopulation || 10000;

  const extractedColors = useMemoWith(getMarginalJointColors, [variables, colors]);

  const inputConverted = convertBivarFreqREGenInput<IBivarFreqPopMarginalJointGenInput>(input, 'population-marginal-joint');

  // using a ref to make useCallback keep working, even if value changes
  const storedInput = useRef<IBivarFreqPopMarginalJointGenInput>(null);
  storedInput.current = inputConverted;

  const onPopulationChanged = useCallback(
    (value: number) =>
      onChange(
        toGenInput(
          validateFrequencyGenInput(
            {
              ...storedInput.current.data,
              population: value,
            },
            FrequencyGenInputProperty.Population
          )
        )
      ),
    [onChange, storedInput]
  );

  const onMarginalFreqAChanged = useCallback(
    (value: number) =>
      onChange(
        toGenInput(
          validateFrequencyGenInput(
            {
              ...storedInput.current.data,
              marginalFrequencies: [value, storedInput.current.data.marginalFrequencies[1]],
            },
            FrequencyGenInputProperty.MarginalFrequency1
          )
        )
      ),
    [onChange, storedInput]
  );

  const onMarginalFreqBChanged = useCallback(
    (value: number) => {
      onChange(
        toGenInput(
          validateFrequencyGenInput(
            {
              ...storedInput.current.data,
              marginalFrequencies: [storedInput.current.data.marginalFrequencies[0], value],
            },
            FrequencyGenInputProperty.MarginalFrequency2
          )
        )
      );
    },
    [onChange, storedInput]
  );

  const onJointFreqChanged = useCallback(
    (value: number) =>
      onChange(
        toGenInput(
          validateFrequencyGenInput(
            {
              ...storedInput.current.data,
              jointFrequency: value,
            },
            FrequencyGenInputProperty.JointFrequency
          )
        )
      ),
    [onChange, storedInput]
  );

  const tooltipTitle = t('math-components:joint-freq-gen-input-info', { maxPopulation: inputConverted.data.population });

  // TODO: support other categories besides 0
  const varsWithCats = useMemo(
    () => [
      getVarWithState(variables, 0, 0),
      getVarWithState(variables, 1, 0),
      [getVarWithState(variables, 0, 0), getVarWithState(variables, 1, 0)],
    ],
    [variables]
  );

  return (
    <Stack className={className} spacing={2} direction="row" alignItems="start">
      <Stack spacing={2} sx={{ width: '100%' }}>
        {!isIncluded(hiddenSliders, FrequencyGenInputProperty.Population) && (
          <MinMaxSliderWithDescription
            min={1}
            max={maxPopulation}
            value={inputConverted.data.population}
            onChange={onPopulationChanged}
            labelDescription="Population"
            disabled={isIncluded(disabledSliders, FrequencyGenInputProperty.Population)}
          />
        )}
        {!isIncluded(hiddenSliders, FrequencyGenInputProperty.JointFrequency) && (
          <JointFrequencySlider
            varsWithCats={varsWithCats[2] as [VariableWithState, VariableWithState]}
            value={inputConverted.data.jointFrequency}
            max={inputConverted.data.population}
            onChange={onJointFreqChanged}
            disabled={isIncluded(disabledSliders, FrequencyGenInputProperty.JointFrequency)}
            color={extractedColors.joint}
          />
        )}
        {!isIncluded(hiddenSliders, FrequencyGenInputProperty.MarginalFrequency1) && (
          <BinaryFrequencySlider
            varWithCat={varsWithCats[0] as VariableWithState}
            min={1}
            max={inputConverted.data.population}
            value={inputConverted.data.marginalFrequencies[0]}
            onChange={onMarginalFreqAChanged}
            disabled={isIncluded(disabledSliders, FrequencyGenInputProperty.MarginalFrequency1)}
            color={extractedColors.marginal0}
          />
        )}
        {!isIncluded(hiddenSliders, FrequencyGenInputProperty.MarginalFrequency2) && (
          <BinaryFrequencySlider
            varWithCat={varsWithCats[1] as VariableWithState}
            min={1}
            max={inputConverted.data.population}
            value={inputConverted.data.marginalFrequencies[1]}
            onChange={onMarginalFreqBChanged}
            disabled={isIncluded(disabledSliders, FrequencyGenInputProperty.MarginalFrequency2)}
            color={extractedColors.marginal1}
          />
        )}
      </Stack>
      <RichTooltip title={tooltipTitle}>
        <IconButton color="error">
          <InfoIcon />
        </IconButton>
      </RichTooltip>
    </Stack>
  );
}
