import React from 'react';

import { RandomVarState } from '@kiwi-al/math-core/variables';

import { IVariableCatSliderBaseProps, VariableCatSliderBase } from '../shared/VariableCatSliderBase';

export function BinaryFrequencySlider<TCategory extends RandomVarState = RandomVarState>(
  props: IVariableCatSliderBaseProps<TCategory>
): React.ReactElement {
  // eslint-disable-next-line react/jsx-props-no-spreading
  return <VariableCatSliderBase {...props} />;
}
