/* eslint-disable react/jsx-props-no-spreading */
import { Box, Stack } from '@mui/material';
import { CommonProps } from '@mui/material/OverridableComponent';
import { DataGrid, GridColumns, GridRenderCellParams, GridValueGetterParams } from '@mui/x-data-grid';
import { scaleLinear } from '@visx/scale';
import { ScaleLinear } from 'd3-scale';
import type { i18n } from 'i18next';
import React, { useMemo, useRef } from 'react';
import { useTranslation } from 'react-i18next';

import { injectI18n } from '@adlete-vle/lr-core/locale';
import { useMemoWith } from '@adlete-vle/utils/hooks';

import { isFloat } from '@kiwi-al/math-core/datatypes';
import { BivariateJointFrequencyDataFrame } from '@kiwi-al/math-core/frequencies';
import {
  BivarCategoricalVarDefs,
  formatVariableWithState,
  ICategoricalVarDef,
  IFormatVariableWithStateOptions,
  variableToVarCatStrings,
} from '@kiwi-al/math-core/variables';

import { createMarginalRow, dataToRows } from '../data-grid';
import { SingleColorMap } from '../shared/colors';
import { getCellColor, RenderCellCB } from '../shared/tables';

type ValueFormatterCB = (value: number | string) => string;
// type OnUpdateCB = (data: BivariateJointFrequencyDataFrame<number | string>) => void;

export interface IContingencyBarTableFormatting {
  valueFormatter?: ValueFormatterCB;
  labelFormat?: IFormatVariableWithStateOptions;
}

const DEFAULT_LABEL_FORMAT: IFormatVariableWithStateOptions = {
  format: 'Notation',
};

export interface IContingencyBarTableProps extends CommonProps {
  initialData: BivariateJointFrequencyDataFrame<number | string>;
  maxBarHeight?: number;
  barWidth?: number;
  showMarginals?: boolean;
  displayVariables?: BivarCategoricalVarDefs;
  colors?: SingleColorMap;
  formatting?: IContingencyBarTableFormatting;
}

interface IRefData {
  yScale?: ScaleLinear<number, number>;
  barWidth: number;
}

function marginalColumnValueGetter(columnNames: string[], params: GridValueGetterParams): number | string {
  let result = 0;
  for (let i = 0; i < columnNames.length; ++i) {
    const value = params.row[columnNames[i]];
    if (!isFloat(value)) return '';

    result += Number(value);
  }

  // TODO: customize number of decimals
  return Math.round(result * 100) / 100; // rounding to two decimals
}

function createYScale(maxHeight: number, maxValue: number): ScaleLinear<number, number> {
  return scaleLinear<number>({
    range: [0, maxHeight],
    round: true,
    domain: [0, maxValue],
  });
}

function renderCell(
  yScale: ScaleLinear<number, number>,
  barWidth: number,
  params: GridRenderCellParams,
  colors?: SingleColorMap
): React.ReactNode {
  const value = params.value;
  const maxHeight = yScale.range()[1];
  const scaledValue = yScale(value);

  const cellColor = getCellColor(params, colors);

  // displaying minimal bar if scaled value smaller than 1px, but value bigger 0
  // eslint-disable-next-line no-nested-ternary
  const barHeight = value === 0 ? 0 : scaledValue < 1 ? 1 : scaledValue;

  return (
    <Stack direction="row" spacing={2} alignContent="center" justifyContent="center">
      <div>
        <Box sx={{ height: `${maxHeight - barHeight}px`, width: barWidth }} />
        <Box sx={{ height: `${barHeight}px`, width: barWidth, backgroundColor: cellColor || 'gray' }} />
      </div>
      <Box display="flex" alignSelf="self-end" sx={{ color: cellColor }}>
        {value}
      </Box>
    </Stack>
  );
}

function createColumns(
  variable: ICategoricalVarDef,
  // TODO: get states and display states from variables
  columnIDs: string[],
  columnLabels: string[],
  showMarginals: boolean,
  renderCellCB: RenderCellCB,
  i18n: i18n,
  valueFormatter?: ValueFormatterCB
): GridColumns {
  const varColumn = { field: 'heading', headerName: '', flex: 1, sortable: false, headerClassName: 'heading-column' };

  const freqColumns: GridColumns = columnIDs.map((id, index) => ({
    field: id,
    colId: id,
    headerName: columnLabels[index],
    flex: 1,
    // width: 90,
    index,
    renderCell: (params: GridRenderCellParams) => renderCellCB(params),
    valueFormatter: valueFormatter ? (params) => valueFormatter(params.value as string | number) : null,
    sortable: false,
  }));

  const marginalColumn = {
    field: variable.name,
    headerName: i18n.t('math:sum'),
    flex: 1,
    renderCell: (params: GridRenderCellParams) => renderCellCB(params),
    valueGetter: marginalColumnValueGetter.bind(null, columnIDs),
    isMarginal: true,
    sortable: false,
  };

  return showMarginals ? [varColumn, ...freqColumns, marginalColumn] : [varColumn, ...freqColumns];
}

export function ContingencyBarTable(props: IContingencyBarTableProps): React.ReactElement {
  // === Incoming component data and defaults
  const { initialData, className, showMarginals, colors, formatting } = props;
  const { i18n } = useTranslation();
  const displayVars = props.displayVariables || initialData.variables;
  const labelFormat = useMemo(() => injectI18n(formatting?.labelFormat || DEFAULT_LABEL_FORMAT, i18n), [formatting?.labelFormat, i18n]);

  // === Data
  const maxBarHeight = props.maxBarHeight || 50;
  const yScale = useMemo(() => createYScale(maxBarHeight, initialData.getPopulation()), [maxBarHeight, initialData]);
  const refData = useRef<IRefData>({ yScale, barWidth: props.barWidth || 15 });
  refData.current.yScale = yScale;
  refData.current.barWidth = props.barWidth || 15;

  // === Columns
  const { columnVariable, rowVariable } = initialData;
  const columnVarDisplay = displayVars[initialData.getVariableIndex(columnVariable)];

  const columnIds = useMemoWith(variableToVarCatStrings, [columnVariable]);
  const columnLabels = useMemo(
    () => columnVarDisplay.categories.map((cat) => formatVariableWithState([columnVarDisplay, cat], labelFormat)),
    [columnVarDisplay, labelFormat]
  );

  const renderCellCB = useMemo(
    () => (params: GridRenderCellParams) => renderCell(refData.current.yScale, refData.current.barWidth, params, colors),
    [colors]
  );

  const columns = useMemo(
    () => createColumns(columnVariable, columnIds, columnLabels, showMarginals, renderCellCB, i18n),
    [columnVariable, columnIds, columnLabels, showMarginals, i18n, renderCellCB]
  );

  // === Rows
  const currData = useMemo(() => initialData.copy(), [initialData]);
  const rowVarDisplay = displayVars[initialData.getVariableIndex(rowVariable)];
  const rows = useMemo(() => {
    const rowIds = variableToVarCatStrings(rowVariable);
    const rowLabels = rowVarDisplay.categories.map((cat) => formatVariableWithState([rowVarDisplay, cat], labelFormat));

    const dataRows = dataToRows(currData.data, columnIds, rowLabels, rowIds);

    if (showMarginals) {
      const marginalRow = createMarginalRow(currData.data, columnIds, i18n, rowVariable.name);
      dataRows.push(marginalRow);
    }

    return dataRows;
  }, [currData, columnIds, rowVariable, rowVarDisplay, showMarginals, i18n, labelFormat]);

  const fullClassName = `contingency-bar-table ${className}`;

  return (
    <Box className={fullClassName}>
      <DataGrid columns={columns} rows={rows} autoHeight disableColumnMenu hideFooter />
    </Box>
  );
}
