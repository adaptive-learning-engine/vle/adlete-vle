import { AxisLeft } from '@visx/axis';
import { Group } from '@visx/group';
import { scaleBand, scaleLinear } from '@visx/scale';
import { Bar } from '@visx/shape';
import { ScaleBand, ScaleLinear } from 'd3-scale';
import React from 'react';
import { useTranslation } from 'react-i18next';

import type { RecursivePartial } from '@adlete-utils/typescript';
import { useMemoWith } from '@adlete-vle/utils/hooks';

import { BivariateJointFrequencyDataFrame } from '@kiwi-al/math-core/frequencies';
import { formatJointProbability } from '@kiwi-al/math-core/probability';
import { VariableWithState } from '@kiwi-al/math-core/variables';

import { IViewport, mergeDefaultViewport } from '../shared/viewport';

const DEFAULT_VIEWPORT: IViewport = {
  width: 600,
  height: 400,
  margin: {
    left: 40,
    right: 140,
    top: 70,
    bottom: 70,
  },
};

interface IBarDatum {
  context: VariableWithState[];
  value: number;
  index: number;
  x: number;
  y: number;
}

function convertToBarData(data: BivariateJointFrequencyDataFrame<number | string>): IBarDatum[] {
  const barData: IBarDatum[] = [];

  const shape = data.data.shape;

  const variables = data.variables;

  let index = 0;

  // adding marginal
  variables.forEach((variable) => {
    const marginals = data.getMarginalFrequencies(variable);
    barData.push({
      context: [[variable, variable.categories[0]]],
      index: index++,
      x: 0,
      y: 0,
      value: marginals[0],
    });
  });

  // adding joints
  for (let y = 0; y < shape[0]; ++y) {
    for (let x = 0; x < shape[1]; ++x) {
      // const varsWithState = ;
      const rawValue = data.values[y][x];
      const value = typeof rawValue === 'string' ? Number.parseFloat(rawValue) : rawValue;
      barData.push({
        context: [
          [variables[0], variables[0].categories[x]],
          [variables[1], variables[1].categories[y]],
        ],
        value,
        x,
        y,
        index: index++,
      });
    }
  }
  return barData;
}

function createXScale(data: IBarDatum[], xMax: number): ScaleBand<number> {
  return scaleBand<number>({
    range: [0, xMax],
    round: true,
    domain: data.map((d) => d.index),
    padding: 0.4,
  });
}

function createYScale(data: IBarDatum[], yMax: number, maxValue?: number): ScaleLinear<number, number> {
  return scaleLinear<number>({
    range: [yMax, 0],
    round: true,
    domain: [0, maxValue || Math.max(...data.map((d) => d.value))],
  });
}

export type IJointFreqBarChartProps = {
  data: BivariateJointFrequencyDataFrame<number | string>;
  viewport?: RecursivePartial<IViewport>;
};

export function JointFreqBarChart(props: IJointFreqBarChartProps): React.ReactElement {
  // locale
  const { i18n } = useTranslation();

  // viewport
  const { width, height, margin } = mergeDefaultViewport(props.viewport, DEFAULT_VIEWPORT);

  // bounds
  const xMax = width - margin.left - margin.right;
  const yMax = height - margin.bottom - margin.top;

  // data
  const variables = props.data.variables;
  const barData = useMemoWith(convertToBarData, [props.data]);

  // scales, memoize for performance
  const maxValue = props.data.getPopulation();
  const xScale = useMemoWith(createXScale, [barData, xMax]);
  const yScale = useMemoWith(createYScale, [barData, yMax, maxValue]);

  return width < 10 ? null : (
    <svg width={width} height={height}>
      <Group left={margin.left} top={margin.top}>
        {/* <AxisBottom top={yMax} scale={xScale} numTicks={width > 520 ? 10 : 5} label="foo" /> */}
        <AxisLeft scale={yScale} />
        {barData.map((d) => {
          const key =
            d.context.length > 1
              ? formatJointProbability(d.context[0], d.context[1], { jointProbFormat: 'Notation', varFormat: 'Notation', i18n })
              : 'foo';
          const barWidth = xScale.bandwidth();
          const barHeight = yMax - (yScale(d.value) ?? 0);
          const barX = xScale(d.index);
          const barY = yMax - barHeight;
          return (
            <>
              <Group left={barX} top={yMax}>
                <text key={`text-${key}`}>
                  <tspan x="0" dy="1.2em">
                    {d.context.length > 1 && i18n.t(d.context[0][1].toString())}
                  </tspan>
                  <tspan x="0" dy="1.2em">
                    {d.context.length > 1 && i18n.t(d.context[1][1].toString())}
                  </tspan>
                </text>
              </Group>
              <Bar key={`bar-${key}`} x={barX} y={barY} width={barWidth} height={barHeight} fill="rgba(23, 233, 217, .5)" />
            </>
          );
        })}
        <Group left={width - margin.right} top={yMax}>
          <text>
            <tspan x="0" dy="1.2em">
              {i18n.t(variables[0].name)}
            </tspan>
            <tspan x="0" dy="1.2em">
              {i18n.t(variables[1].name)}
            </tspan>
          </text>
        </Group>
      </Group>
    </svg>
  );
}
