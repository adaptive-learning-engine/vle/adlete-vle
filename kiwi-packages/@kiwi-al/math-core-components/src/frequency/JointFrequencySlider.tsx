import merge from 'lodash.merge';
import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import type { PartialBy } from '@adlete-utils/typescript';

import { formatJointFrequency, IFormatJointFrequencyOptions } from '@kiwi-al/math-core/frequencies';
import { RandomVarState, VariableWithState } from '@kiwi-al/math-core/variables';

import { BivariateSliderBase, IBivariateSliderBaseProps } from '../shared/BivariateSliderBase';

type PartialProps = 'getDescriptionLabel' | 'min';
// type OmittedProps = 'min' | 'max';

export interface IJointFrequencySliderProps<TCat1 extends RandomVarState = RandomVarState, TCat2 extends RandomVarState = RandomVarState>
  extends PartialBy<IBivariateSliderBaseProps<TCat1, TCat2>, PartialProps> {
  formatting?: IFormatJointFrequencyOptions;
}

export function getDefaultDescriptionLabel<TCat1 extends RandomVarState = RandomVarState, TCat2 extends RandomVarState = RandomVarState>(
  format: IFormatJointFrequencyOptions,
  varsWithCats: [VariableWithState<TCat1>, VariableWithState<TCat2>]
): string {
  // TODO: support other cat indices

  return formatJointFrequency(varsWithCats[0], varsWithCats[1], format);
}

const DEFAULT_FORMAT: IFormatJointFrequencyOptions = {
  varFormat: 'Notation',
  jointFormat: 'Notation',
  jointSeparator: ' ∩ ',
};

export function JointFrequencySlider<TCat1 extends RandomVarState = RandomVarState, TCat2 extends RandomVarState = RandomVarState>(
  props: IJointFrequencySliderProps<TCat1, TCat2>
): React.ReactElement {
  const { min = 0, formatting } = props;
  const { i18n } = useTranslation();
  const getDescriptionLabel = useMemo(() => {
    if (props.getDescriptionLabel) return props.getDescriptionLabel;

    const mergedFormat = merge({ i18n }, DEFAULT_FORMAT, formatting);
    return getDefaultDescriptionLabel.bind(null, mergedFormat);
  }, [props.getDescriptionLabel, formatting, i18n]);

  return (
    <BivariateSliderBase
      // eslint-disable-next-line react/jsx-props-no-spreading
      {...props}
      min={min}
      getDescriptionLabel={getDescriptionLabel}
    />
  );
}
