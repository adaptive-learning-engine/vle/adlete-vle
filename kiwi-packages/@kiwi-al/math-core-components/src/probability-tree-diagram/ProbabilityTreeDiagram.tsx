// import { LinearGradient } from '@vx/gradient';
import { Group } from '@visx/group';
import { Tree } from '@visx/hierarchy';
import { LinkHorizontal } from '@visx/shape';
import { hierarchy, HierarchyPointLink, HierarchyPointNode } from 'd3-hierarchy';
import React from 'react';

import type { RecursivePartial } from '@adlete-utils/typescript';

// TODO: move type somewhere else?
import { IViewport, mergeDefaultViewport } from '../shared/viewport';

const DEFAULT_VIEWPORT: IViewport = {
  width: 400,
  height: 300,
  margin: {
    left: 20,
    right: 20,
    top: 20,
    bottom: 20,
  },
};

export enum ProbTreeNodeLabelType {
  Frequency = 'Frequency', // e.g. 100
  ProbabilityPercent = 'ProbabilityPercent', // e.g. 10%
  Probability = 'Probability', // e.g. 0.1
  Event = 'Event', // e.g. red
  Notation = 'Notation', // e.g. P(A and B)
  Description = 'Description', // e.g. P(A and B)
}

export enum ProbTreeLinkLabelType {
  FrequencyRatio = 'OccuranceRatio', // e.g. 10 out of 100
  ProbabilityPercent = 'ProbabilityPercent', // e.g. 10%
  Probability = 'Probability', // e.g. 0.1
  Notation = 'Notation', // e.g. P(B|A)
}

export interface IWithNodeLabelProps<TDatum extends IProbabilityTreeDatum> {
  getNodeLabel(node: TDatum, labelType: string): string;
  nodeLabelTypes: string[];
}

export interface IWithLinkLabelProps<TDatum extends IProbabilityTreeDatum> {
  getLinkLabel(source: TDatum, target: TDatum, labelType: string): string;
  linkLabelTypes: string[];
}

export interface IProbTreeDiagramProps<TDatum extends IProbabilityTreeDatum>
  extends IWithNodeLabelProps<TDatum>,
    IWithLinkLabelProps<TDatum> {
  viewport?: RecursivePartial<IViewport>;
  root: TDatum;
}

export interface IProbabilityTreeDatum {
  id: string;
  // incomingEdgeLabel?: StringOrLanguageMap | StringOrLanguageMap[];
  // description?: StringOrLanguageMap | StringOrLanguageMap[];
  children?: this[];
}

const lightpurple = '#374469';
const white = '#ffffff';
const bg = '#272b4d';

interface IProbabilityTreeNodeProps<TDatum extends IProbabilityTreeDatum> extends IWithNodeLabelProps<TDatum> {
  node: HierarchyPointNode<IProbabilityTreeDatum>;
}

function ProbabilityTreeNode<TDatum extends IProbabilityTreeDatum>(props: IProbabilityTreeNodeProps<TDatum>) {
  const { node, getNodeLabel, nodeLabelTypes } = props;
  // const centerX = -width / 2;
  // const centerY = -height / 2;

  // const isRoot = node.depth === 0;
  // const isParent = !!node.children;

  // if (isRoot) return <RootNode node={node} />;
  // if (isParent) return <ParentNode node={node} />;

  return (
    <Group top={node.x} left={node.y}>
      {/* <rect
        height={height}
        width={width}
        y={centerY}
        x={centerX}
        fill={bg}
        stroke={blue}
        strokeWidth={1}
        onClick={() => {
          alert(`clicked: ${JSON.stringify(node.data.name)}`);
        }}
      /> */}
      <text dy=".33em" fontSize={9} fontFamily="Arial" textAnchor="middle" style={{ pointerEvents: 'none' }} fill={white}>
        {getNodeLabel(node.data as TDatum, nodeLabelTypes[0])}
      </text>
    </Group>
  );
}

interface IProbabilityTreeLinkProps<TDatum extends IProbabilityTreeDatum> extends IWithLinkLabelProps<TDatum> {
  link: HierarchyPointLink<IProbabilityTreeDatum>;
}

function ProbabilityTreeLink<TDatum extends IProbabilityTreeDatum>(props: IProbabilityTreeLinkProps<TDatum>) {
  const { link, getLinkLabel, linkLabelTypes } = props;
  const { source, target } = props.link;
  const midX = (source.x + target.x) / 2;
  const midY = (source.y + target.y) / 2;

  const label = (
    <text fill="white" textAnchor="middle" x={midY} y={midX} dy="0.33em" fontSize={10}>
      {getLinkLabel(source.data as TDatum, target.data as TDatum, linkLabelTypes[0])}
    </text>
  );

  return (
    <>
      <LinkHorizontal data={link} stroke={lightpurple} strokeWidth="1" fill="none" />
      {label}
    </>
  );
}

export function ProbabilityTreeDiagram<TDatum extends IProbabilityTreeDatum>(props: IProbTreeDiagramProps<TDatum>): React.ReactElement {
  const { width, height, margin } = mergeDefaultViewport(props.viewport, DEFAULT_VIEWPORT);

  const { root, getNodeLabel, getLinkLabel, nodeLabelTypes, linkLabelTypes } = props;

  const rootNode = hierarchy(root);

  const yMax = height - margin.top - margin.bottom;
  const xMax = width - margin.left - margin.right;

  return (
    <svg width={width} height={height}>
      {/* <LinearGradient id="lg" from={peach} to={pink} /> */}
      <rect width={width} height={height} rx={14} fill={bg} />
      <Tree root={rootNode} size={[yMax, xMax]}>
        {(tree) => (
          <Group top={margin.top} left={margin.left}>
            {tree.links().map((link, i) => (
              // TODO: use better link id instead of index
              // eslint-disable-next-line react/no-array-index-key
              <ProbabilityTreeLink key={`link-${i}`} link={link} getLinkLabel={getLinkLabel} linkLabelTypes={linkLabelTypes} />
            ))}
            {tree.descendants().map((node, i) => (
              // eslint-disable-next-line react/no-array-index-key
              <ProbabilityTreeNode key={`node-${i}`} node={node} getNodeLabel={getNodeLabel} nodeLabelTypes={nodeLabelTypes} />
            ))}
          </Group>
        )}
      </Tree>
    </svg>
  );
}
