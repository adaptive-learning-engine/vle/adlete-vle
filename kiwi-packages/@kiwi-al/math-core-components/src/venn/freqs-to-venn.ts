import i18next, { i18n as i18nInstance } from 'i18next';

import { BivariateJointFrequencyDataFrame } from '@kiwi-al/math-core/frequencies';
import { assertIsBinaryVariable, getOtherCategory, RandomVarState } from '@kiwi-al/math-core/variables';

import { IVennDiagramDatum } from './VennDiagramWithCircles';

export function convertFrequenciesToVennData(
  data: BivariateJointFrequencyDataFrame,
  categories: [RandomVarState, RandomVarState],
  i18n: i18nInstance = i18next
): IVennDiagramDatum[] {
  const { rowVariable, columnVariable } = data;
  assertIsBinaryVariable(rowVariable);
  assertIsBinaryVariable(columnVariable);

  const setData: IVennDiagramDatum[] = [];

  const [rowCat, columnCat] = categories;
  const rowOtherCat = getOtherCategory(rowVariable, rowCat) as string;
  const columnOtherCat = getOtherCategory(columnVariable, columnCat) as string;

  // TODO: verify it is yes and no
  // work with arbitrary strings
  const aAndB = data.data.at(rowCat as string, columnCat as string) as number;
  const a = data.data.at(rowCat as string, columnOtherCat) as number;
  const b = data.data.at(rowOtherCat, columnCat as string) as number;

  // const aLabel = `${rowVariable.name}: ${rowCat} (${a + aAndB})`;
  // const bLabel = `${columnVariable.name}: ${columnCat} (${b + aAndB})`;

  const aLabel = `${i18n.t(rowVariable.name)}: ${i18n.t(rowCat.toString())}`;
  const bLabel = `${i18n.t(columnVariable.name)}: ${i18n.t(columnCat.toString())}`;

  // setData.push({ sets: [aLabel], cardinality: a, label: `${rowVariable.name}: ${a + aAndB} (${a + aAndB})` });
  // setData.push({ sets: [bLabel], cardinality: b, label: `${columnVariable.name}: ${b + aAndB} (${b + aAndB})` });
  // setData.push({ sets: [aLabel, bLabel], cardinality: aAndB, label: `${aAndB}` });

  setData.push({ sets: [aLabel], cardinality: a, label: `x` });
  setData.push({ sets: [bLabel], cardinality: b, label: `y` });
  setData.push({ sets: [aLabel, bLabel], cardinality: aAndB, label: `z` });
  return setData;
}
