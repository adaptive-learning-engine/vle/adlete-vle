import { Stack, Table, TableBody, TableCell, TableRow } from '@mui/material';
import { createVennJSAdapter, ISet, ISetCombinations, ISets, VennDiagram, VennDiagramProps } from '@upsetjs/react';
import { layout } from '@upsetjs/venn.js';
import React from 'react';

import { toLocaleFixed } from '@kiwi-al/math-core/utils';

export interface IVennDiagramDatum {
  sets: string[];
  cardinality: number;
  label?: string;
}

export interface IVennDiagramDatum2 {
  sets: string[];
  size: number;
  label?: string;
}

export interface IVennLabelConfig {
  showName?: boolean;
  showCardinality?: boolean;
  numDecimals?: number;
  rename?: string[];
}

export interface IVennDiagramWithCirclesProps<T> extends VennDiagramProps<T>, React.RefAttributes<SVGSVGElement> {
  setLabelConfig?: IVennLabelConfig;
}

const VENN_JS_ADAPTER = createVennJSAdapter(layout);

// const TEST_GEN: IVennDiagramLayoutGenerator = {
//   maxSets: 100,
//   compute(sets, combinations, width, height) {
//     const result = VENN_JS_ADAPTER.compute(sets, combinations, width, height);
//     console.log(result);
//     return result;
//   },
// };

function calcSetNameByConfig<T>(set: ISet<T>, index: number, config: IVennLabelConfig): string {
  const setName = config.rename ? config.rename[index] : set.name;

  if (config.showName && config.showCardinality) {
    return `${setName} (${toLocaleFixed(set.cardinality, config.numDecimals ? config.numDecimals : 0)})`;
  }
  if (config.showName) return setName;
  if (config.showCardinality) return toLocaleFixed(set.cardinality, config.numDecimals ? config.numDecimals : 0);

  throw new Error(`Config must contain either 'showName' or 'showCardinality'!`);
}

// export function VennDiagramWithCircles({ data, width, height }: IVennDiagramWithCirclesProps): React.ReactElement {
//   width = width || 400;
//   height = height || 200;

//   const d3Container = useRef(null);

//   useEffect(() => {
//     if (d3Container.current) {
//       const chart = VennDiagram();
//       select(d3Container.current).datum(dataWithSize).call(chart);
//     }
//   }, [data, d3Container.current]);

//   return <svg className="d3-component" width={width} height={height} ref={d3Container} />;
// }

function applySetLabelConfig<T>(
  setLabelConfig: IVennLabelConfig,
  sets: ISets<T>,
  combinations: ISetCombinations<T>
): [ISets<T>, ISetCombinations<T>] {
  // VennDiagram unfortunately always uses the set name as a label, so we need to hack it right!

  const newCombinations = (combinations as ISetCombinations<T>).map((combi) => {
    const copy = { ...combi };
    copy.sets = new Set(copy.sets);
    return copy as typeof combi;
  });
  const newSets = sets.map((origSet, index) => {
    const set = { ...origSet, name: calcSetNameByConfig(origSet, index, setLabelConfig) };
    newCombinations.forEach((combi) => {
      if (combi.sets.has(origSet)) {
        const combiSets = combi.sets as Set<ISet<T>>;
        combiSets.delete(origSet);
        combiSets.add(set);
      }
    });
    return set;
  });

  return [newSets, newCombinations];
}

function VennLegend<T>(props: IVennDiagramWithCirclesProps<T>): React.ReactElement {
  return (
    <Table size="small" sx={{ width: 'auto' }}>
      <TableBody>
        {props.sets.map((set, index) => (
          <TableRow key={set.name}>
            <TableCell>{props.setLabelConfig.rename ? props.setLabelConfig.rename[index] : set.name}</TableCell>
            <TableCell>{set.name}</TableCell>
          </TableRow>
        ))}
      </TableBody>
    </Table>
  );
}

export function VennDiagramWithCircles<T>(props: IVennDiagramWithCirclesProps<T>): React.ReactElement {
  const origProps = props;

  if (props.setLabelConfig) {
    const [sets, combinations] = applySetLabelConfig(props.setLabelConfig, props.sets, props.combinations as ISetCombinations<T>);

    props = {
      ...props,
      sets,
      combinations,
    };
  }

  const valueFormat = (cardinality: number) => toLocaleFixed(cardinality, (props.setLabelConfig && props.setLabelConfig.numDecimals) || 0);

  return (
    <Stack direction="row" alignItems="flex-start" justifyContent="flex-start">
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      <VennDiagram layout={VENN_JS_ADAPTER} valueFormat={valueFormat} {...props} />
      {/* eslint-disable-next-line react/jsx-props-no-spreading */}
      {props.setLabelConfig && props.setLabelConfig.rename && <VennLegend {...origProps} />}
    </Stack>
  );
}
