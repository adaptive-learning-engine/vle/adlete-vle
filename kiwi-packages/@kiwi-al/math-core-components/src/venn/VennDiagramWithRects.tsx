import type { ISetCombinations, ISets } from '@upsetjs/model';
import React from 'react';

import type { RecursivePartial } from '@adlete-utils/typescript';

import { IViewport, mergeDefaultViewport } from '../shared/viewport';

const DEFAULT_VIEWPORT: IViewport = {
  width: 500,
  height: 500,
  margin: {
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },
};

const DEFAULT_SVG_STYLE = {
  border: '1px solid black',
};

const DEFAULT_BACKGROUND_STYLE: React.CSSProperties = {
  fill: 'transparent',
};
const DEFAULT_FOREGROUND_STYLE: React.CSSProperties = {
  stroke: 'blue',
  strokeWidth: '1',
  fill: 'transparent',
};

export interface IVennSetStyle {
  background?: React.CSSProperties;
  foreground?: React.CSSProperties;
  text?: React.CSSProperties;
}

export interface IVennDiagramWithRectsProps<T> {
  sets: ISets<T>;
  viewport?: RecursivePartial<IViewport>;

  combinations?: ISetCombinations<T>;
  setStyles?: Record<string, IVennSetStyle>;
  maxCardinality: number;
}

// TODO: use similar to upset.js
export interface ISetLayoutData {
  setId: string;
  rectCenterX: number;
  rectCenterY: number;
  textCenterX: number;
  textCenterY: number;
  width: number;
  height: number;
}

export function VennDiagramWithRects<T>(props: IVennDiagramWithRectsProps<T>): React.ReactElement {
  const { sets, combinations, viewport, maxCardinality, setStyles } = props;
  const { width, height } = mergeDefaultViewport(viewport, DEFAULT_VIEWPORT);
  const jointSet = combinations.find((combi) => combi.sets.size === 2);

  const maxArea = width * height;

  const allSets = [sets[0], jointSet, sets[1]];

  const cardinalities = [sets[0].cardinality, jointSet.cardinality, sets[1].cardinality];
  const probabilities = cardinalities.map((cardinality) => cardinality / maxCardinality);
  // const probabilities = cardinalities.map((cardinality) => 0.1);
  const areas = probabilities.map((probability) => probability * maxArea);
  const widths = areas.map((area) => Math.sqrt(area));
  const heights = areas.map((area) => Math.sqrt(area));
  heights[1] = Math.min(heights[0], heights[2]);
  widths[1] = areas[1] / heights[1];

  const allWidth = widths[0] + widths[2] - widths[1];
  const startX = width / 2 - allWidth / 2;
  const endX = width / 2 + allWidth / 2;

  const xPositions = [startX + widths[0] / 2, startX + widths[0] - widths[1] / 2, endX - widths[2] / 2];
  const textXPositions = [startX + 15, xPositions[1], endX - 15];

  // center
  // const textXPositions = [startX + (widths[0] - widths[1]) / 2, xPositions[1], endX - (widths[2] - widths[1]) / 2];

  // TODO: better calculation
  const textYPositions = [height / 2 - heights[0] / 2 + 20, height / 2, height / 2 - heights[2] / 2 + 20];

  const layoutData: ISetLayoutData[] = xPositions.map((x, i) => ({
    setId: allSets[i].name,
    rectCenterX: x,
    rectCenterY: height / 2,
    textCenterX: textXPositions[i],
    textCenterY: textYPositions[i],
    width: widths[i],
    height: heights[i],
  }));

  // const aXPos = startX + widths[0] / 2;
  // const jointXPos = startX + widths[0] - widths[1] / 2;
  // const bXPos = endX - widths[2] / 2;

  const backgrounds = layoutData.map((layout) => {
    const style = { ...DEFAULT_BACKGROUND_STYLE, ...setStyles?.[layout.setId]?.background };
    return (
      <rect
        key={`${layout.setId}-bg`}
        width={layout.width}
        height={layout.height}
        x={layout.rectCenterX - layout.width / 2}
        y={layout.rectCenterY - layout.height / 2}
        style={style}
      />
    );
  });
  const foregrounds = layoutData.map((layout) => {
    const style = { ...DEFAULT_FOREGROUND_STYLE, ...setStyles?.[layout.setId]?.foreground };
    return (
      <rect
        key={`${layout.setId}-fg`}
        width={layout.width}
        height={layout.height}
        x={layout.rectCenterX - layout.width / 2}
        y={layout.rectCenterY - layout.height / 2}
        style={style}
      />
    );
  });
  const texts = layoutData.map((layout) => {
    const style = setStyles?.[layout.setId]?.text;
    return (
      <text
        key={`${layout.setId}-text`}
        x={layout.textCenterX}
        y={layout.textCenterY}
        textAnchor="middle"
        alignmentBaseline="central"
        style={style}
      >
        {layout.setId}
      </text>
    );
  });

  const svgStyle = { ...DEFAULT_SVG_STYLE, ...setStyles?.['sample-space']?.background };
  const sampleSpaceLabelStyle = { ...DEFAULT_SVG_STYLE, ...setStyles?.['sample-space']?.text };

  return (
    <svg width={width} height={height} style={svgStyle}>
      <text key="sample-space-text" x={10} y={20} style={sampleSpaceLabelStyle}>
        Ω
      </text>
      {backgrounds}
      {foregrounds}
      {texts}
    </svg>
  );
}
