import { GridCellParams, GridEditCellProps, GridPreProcessEditCellProps, GridStateColDef } from '@mui/x-data-grid';
import { i18n } from 'i18next';

import type { TupleToUnion } from '@adlete-utils/typescript';

import { isFloat, isInteger } from '@kiwi-al/math-core/datatypes';
import { Array2D, IGenericDataFrame } from '@kiwi-al/math-core/tabular';
import { ICategoricalVarDef, INumericVarDef, IVarDef, VariableDataType, VariableType } from '@kiwi-al/math-core/variables';

export type GridStateColDefWithIndex = GridStateColDef & {
  index: number;
};

export interface IDatagridRow<TRow extends unknown[]> {
  [key: string | number]: TupleToUnion<TRow> | string | number | boolean;
  id: string | number;
  index: number;
  heading: string | number;
  rowId: string;
}

export function dataToRows<TRow extends unknown[]>(
  data: IGenericDataFrame<TRow>,
  columnHeadingNames: string[],
  rowHeadingNames?: string[],
  rowIds?: string[]
): IDatagridRow<TRow>[] {
  const rows = [];
  for (let y = 0; y < data.shape[0]; ++y) {
    const row: IDatagridRow<TRow> = {
      id: y,
      index: y,
      rowId: rowIds ? rowIds[y] : undefined,
      heading: rowHeadingNames ? rowHeadingNames[y] : undefined,
    };
    for (let x = 0; x < data.shape[1]; ++x) {
      row[columnHeadingNames[x]] = data.iat(y, x);
    }
    rows.push(row);
  }
  return rows;
}

export function createMarginalRow<TRow extends unknown[]>(
  data: IGenericDataFrame<TRow>,
  columnHeadingNames: string[],
  i18n: i18n,
  rowId?: string
): IDatagridRow<TRow> {
  const marginals = data.sum({ axis: 0 });
  const y = data.shape[0] + 1;

  const marginalRow: IDatagridRow<TRow> = {
    id: y,
    index: y,
    heading: i18n.t('math:sum').toString(),
    rowId,
    isMarginal: true,
  };

  for (let x = 0; x < data.shape[1]; ++x) {
    marginalRow[columnHeadingNames[x]] = marginals.iat(x);
  }
  return marginalRow;
}

export function isCellEditable(editableCells: boolean | Array2D<boolean>, params: GridCellParams): boolean {
  if (typeof editableCells === 'boolean') return editableCells;

  if (editableCells != null) {
    const [maxY, maxX] = editableCells.shape;

    // TODO: handle x and y actually existing
    const x = (params.colDef as GridStateColDefWithIndex).index;
    const y = params.row.index;

    return y < maxY && x < maxX ? editableCells.get(x, y) : false;
  }

  return false;
}

export function createFieldToColumnIndexMap(columnNames: string[]): Record<string, number> {
  const map: Record<string, number> = {};
  columnNames.forEach((name, index) => {
    map[name] = index;
  });
  return map;
}

export function preProcessEditCellPropsFloat(variable: INumericVarDef, params: GridPreProcessEditCellProps): GridEditCellProps {
  const origValue = params.props.value as string;
  const value = Number(origValue);

  let error = '';

  if (!isFloat(origValue)) error = `Error: Value '${origValue}' is not a floating point number!`;
  else if (variable.range && (value < variable.range[0] || value > variable.range[1]))
    error = `Error: Value '${origValue}' is not between ${variable.range[0]} and ${variable.range[1]}!`;
  else if (variable.isUnsigned && value < 0) error = `Error: Value '${origValue}' must be greater than 0!`;

  return error === '' ? { ...params.props, value } : { ...params.props, error };
}

export function preProcessEditCellPropsInteger(variable: INumericVarDef, params: GridPreProcessEditCellProps): GridEditCellProps {
  const origValue = params.props.value as string;
  const value = Number(origValue);

  let error = '';

  if (!isInteger(origValue)) error = `Error: Value '${origValue}' is not an integer number!`;
  else if (variable.range && (value < variable.range[0] || value > variable.range[1]))
    error = `Error: Value '${origValue}' is not between ${variable.range[0]} and ${variable.range[1]}!`;
  else if (variable.isUnsigned && value < 0) error = `Error: Value '${origValue}' must be greater than 0!`;

  return error === '' ? { ...params.props, value } : { ...params.props, error };
}

export function preProcessEditCellPropsCategorical(variable: ICategoricalVarDef, params: GridPreProcessEditCellProps): GridEditCellProps {
  const value = params.props.value as string;
  let error = '';

  // TODO: handle numeric categorical variables
  if (variable.categories.indexOf(value) === -1)
    error = `Error: Value '${value}' is not a valid value! Values can be ${variable.categories.join(', ')}.`;

  return error === '' ? { ...params.props, value } : { ...params.props, error };
}

export function preProcessEditCellProps(variable: IVarDef, params: GridPreProcessEditCellProps): GridEditCellProps {
  if (variable.type === VariableType.Categorical) return preProcessEditCellPropsCategorical(variable as ICategoricalVarDef, params);

  switch (variable.dtype) {
    case VariableDataType.Float:
      return preProcessEditCellPropsFloat(variable, params);
    case VariableDataType.Integer:
      return preProcessEditCellPropsFloat(variable, params);
    default:
      throw new Error(`Variable datatype ${variable.dtype} currently not supported!`);
  }
}
