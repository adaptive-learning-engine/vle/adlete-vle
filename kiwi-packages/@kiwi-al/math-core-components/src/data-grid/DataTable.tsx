// import { DataGrid } from '@mui/x-data-grid';
import { Box } from '@mui/material';
import { CommonProps } from '@mui/material/OverridableComponent';
import { styled } from '@mui/material/styles';
import { DataGrid, GridCellEditCommitParams, GridColumns } from '@mui/x-data-grid';
import React, { useCallback, useMemo, useState } from 'react';

import { Array2D, TabularDataset } from '@kiwi-al/math-core/tabular';
import { IVarDef } from '@kiwi-al/math-core/variables';

import { renderEditCellWithTooltip } from './ErrorTooltip';
import { createFieldToColumnIndexMap, dataToRows, isCellEditable, preProcessEditCellProps } from './helpers';
import { getDataCellValidationClassname, invalidValidCellsStyleFn } from './validation';

export type OnUpdateCB<TRow> = (data: TabularDataset<TRow>) => void;

export interface IDataTableProps<TRow> extends CommonProps {
  initialData: TabularDataset<TRow>;
  // validationData?: TabularDataset<TRow>;
  onUpdate?: OnUpdateCB<TRow>;

  editableCells?: boolean | Array2D<boolean>;
}

function onCellEdited<TRow>(
  fieldToColumnIndexMap: Record<string, number>,
  dataset: TabularDataset<TRow>,
  onUpdate: OnUpdateCB<TRow>,
  params: GridCellEditCommitParams
): void {
  const x = fieldToColumnIndexMap[params.field];
  const y = params.id as number;
  dataset.data.iset(y, x, params.value);

  if (onUpdate) {
    onUpdate(dataset);
  }
}

function variablesToColumns(variables: IVarDef[]): GridColumns {
  const varColumn = { field: 'id', headerName: 'ID', width: 50 };

  const varColumns: GridColumns = variables.map((variable, index) => ({
    field: variable.name,
    headerName: variable.name,
    width: 90,
    editable: true,
    index,
    preProcessEditCellProps: preProcessEditCellProps.bind(null, variable),
    renderEditCell: renderEditCellWithTooltip,
  }));
  return [varColumn, ...varColumns];
}

export function DataTableRaw<TRow extends unknown[]>(props: IDataTableProps<TRow>): React.ReactElement {
  const { className, onUpdate } = props;
  const { initialData } = props;
  const [validationClassData] = useState<Array2D<string>>(null);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const isCellEditableBound = useCallback(isCellEditable.bind(null, props.editableCells), [props.editableCells]);

  const columnNames = useMemo(() => initialData.variables.map((variable) => variable.name), [initialData.variables]);

  const currData = useMemo(() => initialData.copy(), [initialData]);
  const columns = useMemo(() => variablesToColumns(initialData.variables), [initialData]);
  const rows = useMemo(() => dataToRows(currData.data, columnNames), [currData, columnNames]);

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const getCellClassName = useCallback(getDataCellValidationClassname.bind(null, validationClassData), [validationClassData]);

  // TODO: use State for currFreqs to allow instantValidation
  // eslint-disable-next-line react-hooks/exhaustive-deps
  const onCellEditCommit = useCallback(
    (params: GridCellEditCommitParams) => {
      const fieldToColumnIndexMap = createFieldToColumnIndexMap(columnNames);
      onCellEdited(fieldToColumnIndexMap, currData, onUpdate, params);
    },
    [currData, columnNames, onUpdate]
  );

  return (
    <Box className={className}>
      <DataGrid
        columns={columns}
        rows={rows}
        autoHeight
        isCellEditable={isCellEditableBound}
        onCellEditCommit={onCellEditCommit}
        getCellClassName={getCellClassName}
      />
      {/* <Button
        onClick={() => {
          const newValidClassData = createNumberValidationClassMatrix(
            new Array2D(currData.values, false),
            new Array2D(validationData.values, false)
          );
          setValidationClassData(newValidClassData);
        }}
      >
        Validate
      </Button> */}
    </Box>
  );
}

export const DataTable = styled(DataTableRaw)(invalidValidCellsStyleFn);
