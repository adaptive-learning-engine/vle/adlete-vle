import { Theme } from '@mui/material';
import { GridCellParams } from '@mui/x-data-grid';

import { Array2D } from '@kiwi-al/math-core/tabular';

import { GridStateColDefWithIndex } from './helpers';

export function invalidValidCellsStyleFn({ theme }: { theme: Theme }): string {
  return `
    .invalid {
      background-color: ${theme.palette.error.main};
      color: ${theme.palette.error.contrastText};
    }

    .valid {
      background-color: ${theme.palette.success.main};
      color: ${theme.palette.success.contrastText};
    }
  `;
}

export function getDataCellValidationClassname(validationClassData: Array2D<string>, params: GridCellParams): string {
  if (!validationClassData) return '';

  const x = (params.colDef as GridStateColDefWithIndex).index;
  const y = params.row.index;

  if (x === undefined) return '';
  if (!validationClassData.has(x, y)) return '';

  return validationClassData.get(x, y);
}

export function createNumberValidationClassMatrix(data: Array2D<number | string>, validData: Array2D<number | string>): Array2D<string> {
  if (!validData) return null;

  return data.map((val, x, y) => {
    const isValid = Number(val) === validData.get(x, y);
    return isValid ? 'valid' : 'invalid';
  });
}

export function createValidationClassMatrix(validity: Array2D<boolean>): Array2D<string> {
  return validity.map((isValid) => (isValid ? 'valid' : 'invalid'));
}

export type ValidateValues2DCB<TData> = (validationData: TData, currData: TData) => Array2D<boolean>;

export interface IValidation2D<TData> {
  validationData: TData;
  validate: ValidateValues2DCB<TData>;
}
