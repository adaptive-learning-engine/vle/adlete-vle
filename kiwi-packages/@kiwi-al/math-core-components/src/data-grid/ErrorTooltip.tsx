/* eslint-disable react/jsx-props-no-spreading */
import Tooltip, { tooltipClasses, TooltipProps } from '@mui/material/Tooltip';
import { styled, Theme } from '@mui/material/styles';
import { GridEditInputCell, GridRenderEditCellParams } from '@mui/x-data-grid';
import React from 'react';

// function styleFn({ theme }: { theme: Theme }): string {
//   return `
//     .${tooltipClasses.tooltip}: {
//       background-color: ${theme.palette.error.main};
//       color: ${theme.palette.error.contrastText};
//     }
//     `;
// }

function styleFn2({ theme }: { theme: Theme }) {
  return {
    [`& .${tooltipClasses.tooltip}`]: {
      backgroundColor: theme.palette.error.main,
      color: theme.palette.error.contrastText,
    },
  };
  // const result = {};
  // result[`& .${tooltipClasses.tooltip}`] = {
  //   backgroundColor: theme.palette.error.main,
  //   color: theme.palette.error.contrastText,
  // };
  // return result;
}

function TooltipWithClassname({ className, ...props }: TooltipProps) {
  return <Tooltip {...props} classes={{ popper: className }} componentsProps={{ popper: { className } }} />;
}
// export const ErrorTooltip = styled(ErrorTooltipRaw)(({ theme }) => ({
//   [`& .${tooltipClasses.tooltip}`]: {
//     backgroundColor: theme.palette.error.main,
//     color: theme.palette.error.contrastText,
//   },
// }));

export const ErrorTooltip = styled(TooltipWithClassname)(styleFn2);

export function EditCellWithTooltip(props: GridRenderEditCellParams): React.ReactElement {
  const { error } = props;

  return (
    <ErrorTooltip open={!!error} title={error || ''}>
      <span>
        <GridEditInputCell {...props} error={false} />
      </span>
    </ErrorTooltip>
  );
}

export function renderEditCellWithTooltip(params: GridRenderEditCellParams): React.ReactElement {
  return <EditCellWithTooltip {...params} />;
}
